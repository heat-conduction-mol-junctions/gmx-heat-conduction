#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{

//-----------------------------------------------------------------------------

	//Conducting preliminaries
	fputs("conducting preliminaries\n",stdout);
	if (4 != argc)
	{
		fputs("wrong number of arguments\n",stderr);
		exit(3);
	}
	FILE *ffile=fopen(argv[1],"r");
	if ( NULL == ffile )
        {
                fputs("*ffile couldn't be opened for reading\n",stderr);
                exit(2);
        }
	else fputs("*ffile successfully opened for reading\n",stdout);
	unsigned i,j,n=atoi(argv[2]);
	printf("n=%d\n",n);
	if (4e4 <= n)
	{
		fputs("number of time steps too large -- memory allocation will fail.\n",stderr);
		exit(4);
	}
	if (3.1e4 <= n)
        {
        	fputs("number of time steps seems large -- memory allocation might fail. press any key to continue.\n",stdout);
                getc(stdin);
        }

	double *f=NULL, *conv=NULL;

//-----------------------------------------------------------------------------

	//Dynamically allocating memory
	fputs("dynamically allocating memory\n",stdout);
	f = (double *) calloc(n,n*sizeof(double));
	if ( NULL == f )
	{
		fputs("*f could not be allocated\n",stderr);
		exit(1);
	}
	else fputs("*f successfully allocated\n",stdout);
	conv = (double *) calloc(n,n*sizeof(double));
        if ( NULL == conv )
        {
                fputs("*conv could not be allocated\n",stderr);
                exit(1);
        }
	else fputs("*conv successfully allocated\n",stdout);

//-----------------------------------------------------------------------------

	//Reading *f and initializing *conv
	fputs("reading *f and initializing *conv for time-steps:\n",stdout);
        for (i=0;i<n;i++)
	{
		fscanf(ffile,"%lf\n",&f[i]);
		conv[i]=0;
		if (150 >= n) printf("%d ",i);
	} // end for(i)
        fclose(ffile);

//-----------------------------------------------------------------------------

	//Computing convolution
	fputs("computing convolution for time-steps:\n",stdout);
	for (i=0;i<n;i++)
	{
		//for (j=i;j<n;j++) conv[j-i]+=f[i]*f[j]/(n-(j-i)); // (23-12-12) False: This is a convolution, not a histogram. To see why this is wrong, assume the input signal is just constant (in time). The convolution ends up begin off by a factor of n (the size of the colvolution window).
		for (j=i;j<n;j++) conv[j-i]+=f[i]*f[j];
		if (150 >= n) printf("%d ",i);
	}// end for(i)

//-----------------------------------------------------------------------------

	//Outputting
	fputs("outputting\n",stdout);
	FILE *convfile=fopen(argv[3],"wt");
	if ( NULL == convfile )
	{
		fputs("convfile couldn't be opened for writing\n",stderr);
		exit(2);
	}
	//for (i=0; i<( (9999 >= n) ? (n-1000) : (0.9*n) ) ;i++) fprintf(convfile,"%e\n",conv[i]);
        for (i=0; i<n ;i++) fprintf(convfile,"%e\n",conv[i]);

//-----------------------------------------------------------------------------

free(f);
free(conv);
fclose(convfile);
return 0;
}//end main
