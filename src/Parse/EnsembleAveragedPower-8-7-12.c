#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[], char *envp[]){

//=============================================================
//arguments passed to the program from the command line:
//------------------------------------------------------
//
//argc is the number of command line arguments
//argv is an array of the command line arguments as strings
//
//	argv[1]	=	output-N.txt				(input file)		-- Current trajectory output file
//	argv[2]	=	EnsembleAverageHeatCurrent-(N-1).dat	(input file)		-- Previous average file
//	argv[3] =	N					(input variable)	-- Counter of current trajectory
//	argv[4]	=	EnsembleAverageHeatCurrent-N.dat	(output file)		-- Current average file
//	argv[5]	=	natoms					(input variable)	-- Number of output atoms
//
//      .       .
//      .       .
//      .       .
//
//----------------------------------------------------------------------------------------------
//
//	i	=	natoms
//#	i 	= 	3*natoms							-- Number of fields in output (0=time,1=I(1),2=dE/dt(1),3=eta(1),4=I(2),5=dE/dt(2),6=eta(2),...)
//
//envp is a pointer to the array of environment variables
//=============================================================

	if (argc!=6){ printf("\n\nwrong number of command line arguments\n\n"); return 100; }
	
	int	N=atoi(argv[3]),j,i=atoi(argv[5]),mismatch=1;				// line number of mismatching time indeces

	FILE 	*Current	= fopen(argv[1],"rt");
		if (Current==NULL) {printf("\n\nError opening HeatCurrent file for trajectory number %d: output-%d.txt\n",N,N); return (1);}

	FILE 	*Previous 	= fopen(argv[2],"rt");
		if (Current==NULL) {printf ("\n\nError opening file for ensemble average of previous trajectory, number %d: EnsembleAverageHeatCurrent-%d.dat\n",N-1,N-1); return (2);}


	FILE	*Average	= fopen(argv[4],"wt");
		if (Average==NULL) {printf ("\n\nError opening for writing file for ensemble average, number %d: EnsembleAverageHeatCurrent-%d.dat\n",N,N); return (3);}


        double  C0,P0;		//time
	double	C[i],P[i];	//all other fields


	while ( feof(Current) == 0 ){	//while the HeatCurrent-N file hasn't returned end_of_file:


		fscanf(Current,"%le",&C0);
                fscanf(Previous,"%le",&P0);

		// if the times do not agree,  notify and exit
                if (C0==P0) fprintf(Average,"%e ",C0);
		else{
			printf("\nin line %d times do not match\n",mismatch);
			printf("\n\n	time 	%e	%e\n\n",C0,P0);
                        return 4;
			}

		for (j=0;j<i;j++){
			fscanf(Current,"%le",&C[j]);
			fscanf(Previous,"%le",&P[j]);
			fprintf(Average,"%e ",(C[j]+(N-1)*P[j])/N);
                        //printf("\n%4d %d %d %+e %+e",mismatch,j,i,C[j],P[j]);if (10 < mismatch){printf("\nread 10 lines. stopping.\n");return 6;}//if (1000 < mismatch){printf("\nread 1000 lines or more. stopping.\n");return 6;}//getchar();//DEBUG: 28-5-12
			}

		//scanf(Current,"\n");
		//scanf(Previous,"\n");
		fprintf(Average,"\n");
		
		mismatch++;

		}

	fclose(Current);
	fclose(Previous);
	fclose(Average);

	return 0;
}
