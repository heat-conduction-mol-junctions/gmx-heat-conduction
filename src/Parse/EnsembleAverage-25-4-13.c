#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]){

//=============================================================
//arguments passed to the program from the command line:
//------------------------------------------------------
//
//argc is the number of command line arguments
//argv is an array of the command line arguments as strings
//
//	argv[1]	=	e.g. SingleRealiz-N.txt				(input file)		-- Current trajectory output file
//	argv[2]	=	e.g. EnsembleAverage-(N-1).dat	(input file)		-- Previous average file
//	argv[3] =	N					(input variable)	-- Counter of current trajectory
//	argv[4]	=	e.g. EnsembleAverage-N.dat	(output file)		-- Current average file
//	argv[5]	=	nFields					(input variable)	-- Number of fields in output (e.g. -1=step,0=time,1=atom#,2...17)
//	argv[6]	=	CompFields	(inupt variable)	-- Number of first fields to compare rather than average over (e.g. 1 = time, 2 = time & atom index)
//	argv[7] =	outatoms				(input variable)	-- Number of output atoms
//	.	.
//	.	.
//	.	.
//
//envp is a pointer to the array of environment variables
//=============================================================

	if (argc!=8){ printf("\n\nwrong number of command line arguments\n\n"); exit(100); }
	
	int	N=atoi(argv[3]),field,nFields=atoi(argv[5]),CompFields=atoi(argv[6]),outatoms=atoi(argv[7]),mismatch=1;				// line number of mismatching time indeces

	if (2 == outatoms) outatoms=1;//In diatomics, parse-*.c only outputs for one of the atoms.

	FILE 	*Current	= fopen(argv[1],"rt");
		if (Current==NULL) {printf("\n\nError opening input file for trajectory number %d: %s\n",N,argv[1]); exit(4);}

	FILE 	*Previous 	= fopen(argv[2],"rt");
		if (Current==NULL) {printf ("\n\nError opening input file for previous trajectory, number %d: %s\n",N-1,argv[2]); exit(5);}


	FILE	*Average	= fopen(argv[4],"wt");
		if (Average==NULL) {printf ("\n\nError opening for writing file for ensemble average, number %d: %s\n",N,argv[4]); exit(6);}

	double	C[nFields],P[nFields];
	int	Comp[CompFields],Pomp[CompFields];

	while ( feof(Current) == 0 ){	//while the SingleRealiz-N file hasn't returned end_of_file:

		switch (CompFields)
			{
			case 1:
					fscanf(Current ,"%le",&C[0]);
					fscanf(Previous,"%le",&P[0]);
					if (C[0]!=P[0]){
						printf("In line %d of realization %d, step does not match: %e %e\n",mismatch,N,C[0],P[0]);
						exit(1);
						}
					fprintf(Average,"%e ",C[0]);
					break;
			case 3:
			        fscanf(Current ,"%d %le %d",&Comp[0],&C[1],&Comp[2]);
			        fscanf(Previous,"%d %le %d",&Pomp[0],&P[1],&Pomp[2]);
			        if (Comp[0]!=Pomp[0]){
			        	printf("In line %d of realization %d, step does not match: %d %d\n",mismatch,N,Comp[0],Pomp[0]);
			        	exit(1);
			        	}
				fprintf(Average,"%d ",Comp[0]);
			        if (C[1]!=P[1]){
			        	printf("In line %d of realization %d, time does not match: %e %e\n",mismatch,N,C[1],P[1]);
			                exit(2);
			                }
				fprintf(Average,"%le ",C[1]);
			        if (Comp[2]!=Pomp[2]){
			                printf("In line %d of realization %d, atom index does not match: %d %d\n",mismatch,N,Comp[2],Pomp[2]);
			                exit(3);
			                }
				fprintf(Average,"%d ",Comp[2]);			        break;
		}


		for (field=CompFields;field<nFields;field++){
			fscanf(Current,"%le",&C[field]);
			fscanf(Previous,"%le",&P[field]);
			fprintf(Average,"%e ",(C[field]+(N-1)*P[field])/N);

			//*DEBUG 25-4-13*/printf("%d %le %le %le\n",field, C[field],P[field],(C[field]+(N-1)*P[field])/N);

			}

		fprintf(Average,"\n");//end of line

		mismatch++;//increase line counter

		if (( (0 == CompFields) || (3 == CompFields) )&&( 0 == ( ( mismatch - 1 ) % outatoms ) )) fprintf(Average,"\n");// if the line counter is an integer multiple of the number of atoms, add an iso-scan line (for 3-D GNUPlotting)

		}

	fclose(Current);
	fclose(Previous);
	fclose(Average);

	return 0;
}
