#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[], char *envp[]){

//=============================================================
//arguments passed to the program from the command line:
//------------------------------------------------------
//
//argc is the number of command line arguments
//argv is an array of the command line arguments as strings
//
//	argv[1]	=	output-N.txt				(input file)		-- Current trajectory output file
//	argv[2]	=	EnsembleAverageHeatCurrent-(N-1).dat	(input file)		-- Previous average file
//	argv[3] =	N					(input variable)	-- Counter of current trajectory
//	argv[4]	=	EnsembleAverageHeatCurrent-N.dat	(output file)		-- Current average file
//	argv[5]	=	i					(input variable)	-- Number of fields in output (-1=step,0=time,1=atom#,2...17)
//	argv[6] =	outatoms				(input variable)	-- Number of output atoms
//	.	.
//	.	.
//	.	.
//
//envp is a pointer to the array of environment variables
//=============================================================

	if (argc!=7){ printf("\n\nwrong number of command line arguments\n\n"); return 100; }
	
	int	N=atoi(argv[3]),j,i=atoi(argv[5]),outatoms=atoi(argv[6]),mismatch=1;				// line number of mismatching time indeces

	//DEV (12-7-12): The following line was commented out because it is no longer true (due to changes in parse-*.c)
	//if (2 == outatoms) outatoms=1;//In diatomics, parse-*.c only outputs for one of the atoms.

	FILE 	*Current	= fopen(argv[1],"rt");
		if (Current==NULL) {printf("\n\nError opening HeatCurrent file for trajectory number %d: output-%d.txt\n",N,N); return (1);}

	FILE 	*Previous 	= fopen(argv[2],"rt");
		if (Current==NULL) {printf ("\n\nError opening file for ensemble average of previous trajectory, number %d: EnsembleAverageHeatCurrent-%d.dat\n",N-1,N-1); return (2);}


	FILE	*Average	= fopen(argv[4],"wt");
		if (Average==NULL) {printf ("\n\nError opening for writing file for ensemble average, number %d: EnsembleAverageHeatCurrent-%d.dat\n",N,N); return (3);}


	int	C_1,P_1;	//step
        double  C0,P0;		//time
	int	C1,P1;		//atom index number
	double	C[i],P[i];	//all other fields


	while ( feof(Current) == 0 ){	//while the HeatCurrent-N file hasn't returned end_of_file:


		fscanf(Current,"%d %le %d",&C_1,&C0,&C1);
                fscanf(Previous,"%d %le %d",&P_1,&P0,&P1);

		// if the step, time, and atom indeces do not agree,  notify and exit
                if ((C0==P0)&&(C1==P1)&&(C_1==P_1)){
			fprintf(Average,"%d ",C_1);
			fprintf(Average,"%e ",C0);
			fprintf(Average,"%d ",C1);
			}
		else{
			printf("\nin line %d step, time, or atom indeces do not match\n",mismatch);
			printf("\n\n	step	%d	%d\n	time 	%e	%e\n	atom	%d	%d\n\n",C_1,P_1,C0,P0,C1,P1);
                        return 4;
			}

		for (j=2;j<i;j++){
			fscanf(Current,"%le",&C[j]);
			fscanf(Previous,"%le",&P[j]);
			fprintf(Average,"%e ",(C[j]+(N-1)*P[j])/N);
			}

		//scanf(Current,"\n");
		//scanf(Previous,"\n");
		fprintf(Average,"\n");//end of line (for atom j)
		
		mismatch++;//increase line counter

		if ( 0 == ( ( mismatch - 1 ) % outatoms ) ) fprintf(Average,"\n");// if the line counter is an integer multiple of the number of atoms, add an iso-scan line (for 3-D GNUPlotting)


		}

	fclose(Current);
	fclose(Previous);
	fclose(Average);

	return 0;
}
