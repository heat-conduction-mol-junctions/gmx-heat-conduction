#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define START_COLOR 27
#define COLOR_ATTRIBUTES_OFF 0
#define BOLD_FONT 1
#define BLINK_FONT 5
#define YELLOW_FG_COLOR 33
#define GREEN_FG_COLOR 32
#define RED_FG_COLOR 31

/*
		DEBUG compilation:
		gcc ~/Documents/GROMACS/Parse/parse-*.c -lm -O0 -g3 -pg -Wall -Wextra           -o ~/Documents/GROMACS/Parse/parse-*.out

		ISO C90 compliant compilation:
		gcc ~/Documents/GROMACS/Parse/parse-*.c -lm -O0 -g3 -pg -Wall -Wextra -pedantic -o ~/Documents/GROMACS/Parse/parse-*.out

		RELEASE compilation:
		gcc ~/Documents/GROMACS/Parse/parse-*.c -lm -O3                                 -o ~/Documents/GROMACS/Parse/parse-*.out
*/

/*
		Set color
       printf("%c[%d;%d;%dm",27,ATTRIB,FG,BG);// To set or reset. Any of the ATTRIB/FG/BG can be removed

		Text attributes
       0    All attributes off
       1    Bold on
       4    Underscore (on monochrome display adapter only)
       5    Blink on
       7    Reverse video on
       8    Concealed on

		Foreground colors
       30    Black
       31    Red
       32    Green
       33    Yellow
       34    Blue
       35    Magenta
       36    Cyan
       37    White

		Background colors
       40    Black
       41    Red
       42    Green
       43    Yellow
       44    Blue
       45    Magenta
       46    Cyan
       47    White
*/

// Static dimensions:
#define DIM 3
#define N_SCHEMES 3
// Other definitions:
#define	P_GMX_2_P_SI 1.66054e-9	 // W / (KJ/mole per ps)
//#define J_GMX_2_J_SI 1.66054e-18 // W*m / (nm*KJ/mole per ps)
#define J_GMX_2_J_SI 1.0 // 20-8-12
#define Rgas 8.3144 			//Gas constant in J per mole Kelvin
#define FORCE_NAME_LENGTH 8
#define MAX_PARTICIPANTS 4		//ignore the case of 5-member (or more) interactions
//DEV NOTE: Check whether any of the A[B].C[D][E] should be A[B]->C[D][E] (so far, the compiler did not object to X.Y but did to X->Y, for all existing instances)
//DEV NOTE: Try to make some variables static or dynamic
#define MAX_FILENAME_LENGTH 30 // in characters. This is much shorter than the limit defined in the C standard limits library, but still big enough.
//==========================================================================================================

typedef double dvec[DIM]; //define new variable type, "dvec": DIM-dimensional vector of double-percission numbers
typedef double schemecoef[N_SCHEMES]; //define new variable type, "schemecoef": N_SCHEMES-dimensional vector of double-percission numbers
typedef double schemeatoms[MAX_PARTICIPANTS][N_SCHEMES];
typedef double schemevec[DIM][N_SCHEMES];

typedef struct
	{ //define structure template "iterm_t" for holding a single interaction term and all its relevant information
	unsigned short nr; //number of participating atoms in this term
	unsigned short *iatoms; //atom indexes for participants in this term (pointer to array/vector of length "nr")
	dvec *f; //force vectors for each participant in this term (array of length "nr" of vectors of length "DIM")
	double vterm; //potential energy due to this term
	schemecoef *c; //potential localization coefficients for this term (array of length "nr" of vectors of length "N_SCHEMES")
	//"iatoms", "f" and "c" could be merged into one (composite) data structure of dimensionality "nr"
	double *termPower0;
	double termPower0tot;
	schemecoef *termLocalPower;
	schemeatoms *termPairwisePower;
	schemevec *termCurrent;
	} iterm_t; //suffix _t stands for template (due to typedef)

typedef struct
	{ //define structure template "state_t" for holding phase space coordinates for all atoms, in a given time-step
	  //unsigned short natoms;//originally in state.h
	double t; //originally local in md.c
	dvec *x; //positions  (array of length "natoms" of vectors of length "DIM")
	dvec *v; //velocities (array of length "natoms" of vectors of length "DIM")
	dvec *vfrog; //anti- leap-frogged velocities (array of length "natoms" of vectors of length "DIM")
	} state_t;

//-----------------------------------------------------------------------------------------------------------

//global variables:

//DEV NOTE: When pointers to scalars (int, double...) are passed as arguments to functions, only their value is copied and passed, not the original's address. Therefore, any changes made to these variables is local to the function, rather than global. To change the global value of a scalar pointed to by a pointer, pass the pointer address to the function, and then change what appears at that adress from within the function.
long double TempPhaseTime;

//-----------------------------------------------------------------------------------------------------------

//function prototypes:
int ColumnHeadings(const unsigned short natoms,
//FILE *outfile,
      FILE *hrout,
      const unsigned short a_hr);
//int ReadTop(FILE *trajectory, unsigned short natoms, double m[]);
int ReadNAtoms(FILE *trajectory, unsigned short natoms);
int ReadAtomMasses(
      FILE *trajectory,
      const unsigned short natoms,
      double m[],
      unsigned short *bOutput,
      FILE *outatomsfile,
      FILE *atomsmassesfile);
int MaxTerms(const unsigned short natoms);
int ReadPhase(FILE *trajectory, const unsigned short natoms, state_t state);
int ChkAtmSuprpos(const unsigned short natoms, state_t state, double TempTime);
int CalcKin(
      const unsigned short natoms,
      const double m[],
      double k[],
      state_t state);
int AntiLeapFrog(
      const unsigned short natoms,
      state_t state,
      state_t stateprev,
      const double k[],
      double kprev[],
      double kfrog[]);
int ReadForce(
      FILE *forces,
      const unsigned short tau,
      iterm_t *itermlist,
      unsigned short LeftmostBathAtom,
      FILE *RanForFile);
int PotLoc(const unsigned short tau, iterm_t *itermlist, schemecoef *u);

int CalcTermPowAndCurrent(
      unsigned short const tau,
      state_t const state,
			//const unsigned step, //DEBUG 14-5-13
			//const unsigned seg_length,//DEBUG 14-5-13
			const double *m,
      iterm_t *itermlist,
      schemecoef **BondPower, /* P_i<-j = Sum_tau P_tau,i<-j = -P_j<-i	, j-in-tau */
			//unsigned short const natoms,
      //*BondCurrent, /* I_i<-j = Sum_tau I_tau,i<-j = I_j<-i	, j-in-tau */
      schemecoef *power, /* P_i = Sum_tau Sum_j-in-tau P_tau,i<-j = d/dt E_i(t) */
      schemevec *currentFromLeft /* I_i = Sum_tau Sum_j-in-tau I_tau,i<-j = 0 at steady-state due to Kirchoff's current law */,
      unsigned short LeftmostBathAtom);
//int CalcTermPow(
//      const unsigned short tau,
//      const state_t state,
//      iterm_t *itermlist,
//      schemecoef *power);
////int CalcTermCurrent(unsigned short tau, state_t state, iterm_t *itermlist, schemevec *current, schemevec currentacc,FILE *TermPairwiseCurrentFile);
//int CalcTermCurrent(const unsigned short tau, state_t state,
///*state_t stateprev,*/
//iterm_t *itermlist, schemevec *current);
////    schemevec currentacc); //13-8-12
////int CalcTermCurrent(unsigned short tau, state_t state, iterm_t *itermlist, schemecoef *power, schemevec *current, schemecoef *currentacc);
int CoarseGrainTimeSeg(
      const state_t state,
      const state_t stateprev,
      const unsigned step,
      FILE *hrout,
      FILE *outfile,
      FILE *currentfile,
      FILE *currenthrout,
      FILE *BondPowerFile,
      schemecoef **BondPower,
      schemecoef *poweracc,
      schemevec *currentFromLeft,
      /*schemevec currentacc,*/
      schemecoef *DeltaE,
      const unsigned seg_length,
      const unsigned short natoms,
      double kfrog[],
			double T[],
			double T2[],
      schemecoef *u,
      schemecoef *E2,
      const unsigned short *bOutput,
      const unsigned short a_hr);
int SetupNextTimeStep(
      const unsigned short natoms,
      state_t state,
      state_t stateprev,
      double k[],
      double kprev[],
      double kfrog[],
      double kfrogprev[],
      schemecoef *u,
      schemecoef *uprev,
      schemecoef *power
      //     , unsigned short LastTermIndex,
      //      iterm_t *itermlist
      );
/*
 int SetupNextTimeStep(unsigned short natoms, state_t state, state_t stateprev, double k[],
 double kprev[], double kfrog[], double kfrogprev[], schemecoef *u,
 schemecoef *uprev, schemecoef *power);
 */
int InitTermCurrent(unsigned short LastTermIndex, iterm_t *itermlist);
int FinishUp(
      const unsigned step,
      const unsigned seg_length,
      FILE *trajectory,
      FILE *forces,
      FILE *outfile,
      FILE *hrout,
      FILE *outatomsfile,
      FILE *atomsmassesfile,
      FILE *currentfile,
      FILE *currenthrout,
      FILE *TermPairwiseCurrentFile,
      FILE *RanForFile,
      FILE *BondPowerFile);
//int FinishUp(unsigned step, unsigned seg_length, FILE *trajectory, FILE *forces, FILE *outfile, FILE *hrout, FILE *outatomsfile, FILE *atomsmassesfile, FILE *currentfile, FILE *currenthrout);//13-8-12

int debug_31_5_11(
      const state_t state,
      const state_t stateprev,
      const unsigned short a_hr,
      const unsigned short natoms,
      const double *m,
      const double *kfrog,
      const double *kfrogprev,
      const iterm_t *itermlist,
      const unsigned short LastTermIndex,
      const schemecoef *u,
      const schemecoef *uprev,
      const unsigned short maxterms,
      const schemecoef *power
      /* , const schemecoef *poweracc*/
      );

int debug_25_7_12(
      const unsigned short tau,
      const unsigned short a_hr,
      const state_t state,
      const iterm_t *itermlist,
      /*schemecoef *power,*/
      /*schemevec *current,*/
      /*schemevec currentacc,*/
      FILE *TermPairwiseCurrentFile);

int debug_15_3_13(
      const unsigned short tau,
      const unsigned short a_hr,
      const unsigned step,
      const state_t state,
      const iterm_t *itermlist,
      schemecoef **BondPower,
      schemecoef *power,
      schemecoef *poweracc,
      schemevec *currentFromLeft,
      FILE *TermPairwiseCurrentFile);

//==========================================================================================================

//MAIN:
int main(int argc, char *argv[])
	{
		//this C program takes two input files (one of velocities and another of forces) and calculates the energy current through the atoms
		/* program arguments:
		 argv[1]	trajectory file name
		 argv[2]	forces file name
		 argv[3]	output file name
		 argv[4]	output time-averaging segment length (in number of steps)
		 in all: 5 argument variables (argc=6)
		 */

		//-----------------------------------------------------------------------------------------------------------
		if (argc != 5)
			{
				fprintf(stderr,
				      "\n\nWrong number of arguments: %d instead of 4\n\n",
				      argc - 1);
				exit(1);
			}

		unsigned short bForcesStepOver = 0, *bOutput = NULL, maxterms = -1,
		      LastTermIndex = 0, tau, ExitCode = 0, LeftmostBathAtom = 9999;
		const unsigned short a_hr = 0;
		unsigned short j = 0, a = 0, /*q,*/scanfnum = 0;
		unsigned step = 0;
		const unsigned seg_length = atoi(argv[4]);

		unsigned short natoms = 0;
		/*
		 int	natoms=atoi(argv[4]);//this should also be const but then we wouldn't be able to define variable length arrays (with length natoms).
		 */

		FILE *trajectory = fopen(argv[1], "rb"); //"rt"
		if (trajectory == NULL)
			{
				fputs("\n\nError opening trajectory file\n", stderr);
				exit(1);
			}
		FILE *forces = fopen(argv[2], "rb"); //"rt"
		if (forces == NULL)
			{
				fputs("\n\nError opening forces file\n", stderr);
				exit(1);
			}
		FILE *outfile = fopen(argv[3], "wt");
		if (outfile == NULL)
			{
				fputs("\n\nError opening out file\n", stderr);
				exit(1);
			}

		//DEBUG 23-7-12
		FILE *TermPairwiseCurrentFile = fopen("TermPairwiseCurrent.txt", "wt");
		if (NULL == TermPairwiseCurrentFile)
			{
				fputs("\n\nError opening Term Pair-wise Current file\n", stderr);
				exit(98);
			}

		FILE *outatomsfile = fopen("outputatoms.txt", "wt");
		if (outatomsfile == NULL)
			{
				fputs("\n\nError opening output atoms file\n", stderr);
				exit(1);
			}
		FILE *atomsmassesfile = fopen("atomsmasses.txt", "wt");
		if (atomsmassesfile == NULL)
			{
				fputs("\n\nError opening atoms massesfile\n", stderr);
				exit(1);
			}
		FILE *currentfile = fopen("current.txt", "wt"); //16-4-12 do same as for hroutfilename, using argv[3]
		if (currentfile == NULL)
			{
				fputs("\n\nError opening current file\n", stderr);
				exit(1);
			}
		FILE *currenthrout = fopen("currenthr.txt", "wt"); //16-6-12 do same as for hroutfilename, using argv[3]
		if (currenthrout == NULL)
			{
				fputs("\n\nError opening human-readable current file\n", stderr);
				exit(1);
			}

		//char *hroutfilename = NULL;
		char *hroutfilename = (char *) calloc(MAX_FILENAME_LENGTH,
		      MAX_FILENAME_LENGTH * sizeof(char));
		if (hroutfilename == NULL)
			{
				fputs(
				      "\nCould not allocate memory for human-readable output file name\n",
				      stderr);
				exit(1);
			}

		char *s = NULL;
		s = (char *) calloc(MAX_FILENAME_LENGTH,
		      MAX_FILENAME_LENGTH * sizeof(char));
		if (s == NULL)
			{
				fputs(
				      "\nCould not allocate memory for human-readable output file name without extension\n",
				      stderr);
				exit(1);
			}

		s = strcat(strcat(s, "hr"), argv[3]); // s = NULL --> "hr"argv[3]
		hroutfilename = strcat(strtok(s, "."), ".txt"); //hroutfilename = s - previous file extension + ".txt" file extension

		/*
		 char hroutfilename[MAX_FILENAME_LENGTH];
		 sprintf(hroutfilename, "hr%s", argv[3]);
		 */
		FILE *hrout = fopen(hroutfilename, "wt");
		if (hrout == NULL)
			{
				fputs("\n\nError opening human-readable out file\n", stderr);
				exit(1);
			}
		free(s);
		//free(hroutfilename);//Valgrind says that this line "Invalid free() / delete / delete[]" 19-4-12

		FILE *RanForFile = fopen("RanFor.txt", "wt");
		if (hrout == NULL)
			{
				fputs("\n\nError opening Random Force file\n", stderr);
				exit(1);
			}

		FILE *BondPowerFile = fopen("BondPower.txt", "wt");
		if (hrout == NULL)
			{
				fputs("\n\nError opening Bond Power file\n", stderr);
				exit(111);
			}

		//	char tempterm[FORCE_NAME_LENGTH];
		int tempterm = 0;
		double tforces = 0, *m = NULL, *k = NULL, *kprev = NULL, *kfrog = NULL,
		      *kfrogprev = NULL, *T = NULL, *T2 = NULL;

		iterm_t *itermlist = NULL, *templist = NULL; //interaction term list (array of length "maxterms" of structures of type interactionTerm)

		state_t state =
			{ 0, NULL, NULL, NULL }, stateprev =
			{ 0, NULL, NULL, NULL }; //structure "state" (unnecessary, but in keeping with the GROMACS md.c form: state->x[j][q] , etc.)

		schemecoef *power = NULL, *poweracc = NULL, *DeltaE = NULL, *u = NULL,
		      *uprev = NULL, *E2= NULL;

		schemevec *currentFromLeft = NULL; //, currentacc;
		//	schemecoef *currentacc = NULL;

		schemecoef **BondPower = NULL;

		//==========================================================================================================

		//read natoms from top of trajectory file
		natoms = ReadNAtoms(trajectory, natoms);
		if (0 == natoms)
			{
				fputs("\nFailed to read natoms from top of trajectory file\n",
				      stderr);
				exit(1);
			}
		/*	if (0!=ReadNAtoms(trajectory,natoms)){
		 fputs("\nFailed to read natoms from top of trajectory file\n",stderr);exit(1);}
		 */
		/*	if (0!=ReadTop(trajectory,natoms,m)){//read molecular topology from top of trajectory file
		 fputs("\nFailed to read molecular topology from top of trajectory file\n",stderr);exit(1);}
		 */
		if (natoms == 0)
			{
				fputs("\nnatoms=0, exiting...\n\n", stderr);
				exit(32);
			} //DEBUG

		//-----------------------------------------------------------------------------------------------------------

		//DEV NOTE: put this into a function:
		//dynamically allocate data structures with dimensionality "natoms" here
		m = (double *) calloc(natoms, natoms * sizeof(double));
		if (m == NULL)
			{
				fputs("\nCould not allocate memory for atom masses\n", stderr);
				exit(1);
			}

		state.x = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (state.x == NULL)
			{
				fputs("\nCould not allocate memory for positions\n", stderr);
				exit(1);
			}
		state.v = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (state.v == NULL)
			{
				fputs("\nCould not allocate memory for velocities\n", stderr);
				exit(1);
			}
		state.vfrog = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (state.vfrog == NULL)
			{
				fputs("\nCould not allocate memory for leap-frog velocities\n",
				      stderr);
				exit(1);
			}
		stateprev.x = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (stateprev.x == NULL)
			{
				fputs("\nCould not allocate memory for previous step positions\n",
				      stderr);
				exit(1);
			}
		stateprev.v = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (stateprev.v == NULL)
			{
				fputs("\nCould not allocate memory for previous step velocities\n",
				      stderr);
				exit(1);
			}
		stateprev.vfrog = (dvec *) calloc(natoms, natoms * sizeof(dvec));
		if (stateprev.vfrog == NULL)
			{
				fputs(
				      "\nCould not allocate memory for previous step leap-frog velocities\n",
				      stderr);
				exit(1);
			}

		k = (double *) calloc(natoms, natoms * sizeof(double));
		if (k == NULL)
			{
				fputs("\nCould not allocate memory for kinetic energies\n", stderr);
				exit(1);
			}
		kprev = (double *) calloc(natoms, natoms * sizeof(double));
		if (kprev == NULL)
			{
				fputs(
				      "\nCould not allocate memory for previous half-step kinetic energies\n",
				      stderr);
				exit(1);
			}
		kfrog = (double *) calloc(natoms, natoms * sizeof(double));
		if (kfrog == NULL)
			{
				fputs(
				      "\nCould not allocate memory for leap-frog kinetic energies\n",
				      stderr);
				exit(1);
			}
		kfrogprev = (double *) calloc(natoms, natoms * sizeof(double));
		if (kfrogprev == NULL)
			{
				fputs(
				      "\nCould not allocate memory for previous step leap-frog kinetic energies\n",
				      stderr);
				exit(1);
			}
    T = (double *) calloc(natoms, natoms * sizeof(double));
    if (T == NULL)
      {
        fputs(
              "\nCould not allocate memory for time corase-grained local temperatures\n",
              stderr);
        exit(1);
      }
    T2 = (double *) calloc(natoms, natoms * sizeof(double));
    if (T2 == NULL)
      {
        fputs(
              "\nCould not allocate memory for time corase-grained local temperatures, squared\n",
              stderr);
        exit(1);
      }
    E2 = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
    if (E2 == NULL)
      {
        fputs(
              "\nCould not allocate memory for time corase-grained local energies, squared\n",
              stderr);
        exit(1);
      }


BondPower = (schemecoef **) calloc(natoms, natoms * sizeof(schemecoef *));
	if(BondPower == NULL)
		{
		fprintf(stderr, "out of memory creating first rank of BondPower matrix\n");
		exit(22);
		}
	for(j = 0; j < natoms; j++)
		{
		BondPower[j] = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if(BondPower[j] == NULL)
			{
			fprintf(stderr, "out of memory creating second rank of BondPower matrix\n");
			exit(23);
			}
		}

	//*DEBUG 7-5-13AM*/for(j=0;j<natoms;j++) for(scanfnum=0;scanfnum<natoms;scanfnum++) for(a=0;a<N_SCHEMES;a++) printf("BondPower[ai=%d][aj=%d][a=%d] = %+8.1e\n",j,scanfnum,a,BondPower[j][scanfnum][a]);

/*
		BondPower = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if (NULL == BondPower)
			{
				fputs("\nCould not allocate memory for Bond Power\n", stderr);
				exit(22);
			}
*/

		power = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if (power == NULL)
			{
				fputs("\nCould not allocate memory for local power\n", stderr);
				exit(1);
			}
		poweracc = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef)); //is this variable really necessary?
		if (poweracc == NULL)
			{
				fputs("\nCould not allocate memory for local power accumulator\n",
				      stderr);
				exit(1);
			} //poweracc is an accumulator for power but shouldn't be absolutely necessary. However, using power as an accumulator in itself did not work for some reason (power just got the value of the power in the current time-step, as opposed to the sum of the powers over the multiple time-steps in the time-averaging segment (check for instance the case of seg_length=2).

		currentFromLeft = (schemevec *) calloc(natoms,
		      natoms * sizeof(schemevec));
		if (currentFromLeft == NULL)
			{
				fputs("\nCould not allocate memory for local current\n", stderr);
				exit(1);
			} //crurrentFromLeft is defined as the current through the atom with index i originating in all atoms with index j<i. The assumption is that any atom with index j<i is found "to the left" of the atom with index i. To take into account the baths, only the bath coupled to the atom with lowest index contributes.
		/*
		 for (q = 0; q < DIM; q++)
		 for (a = 0; a < N_SCHEMES; a++)
		 currentacc[q][a] = 0; //init
		 */
		/*
		 currentacc = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		 if (currentacc == NULL)
		 {
		 fputs("\nCould not allocate memory for current accumulator\n", stderr);
		 exit(1);
		 }
		 */

		DeltaE = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if (DeltaE == NULL)
			{
				fputs("\nCould not allocate memory for local energy diference\n",
				      stderr);
				exit(1);
			}
		u = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if (u == NULL)
			{
				fputs("\nCould not allocate memory for local potential energy\n",
				      stderr);
				exit(1);
			}
		uprev = (schemecoef *) calloc(natoms, natoms * sizeof(schemecoef));
		if (uprev == NULL)
			{
				fputs(
				      "\nCould not allocate memory for previous step local potential energy\n",
				      stderr);
				exit(1);
			}
		bOutput = (unsigned short *) calloc(natoms, natoms * sizeof(int));
		if (bOutput == NULL)
			{
				fputs(
				      "\nCould not allocate memory for identities of Output atoms\n",
				      stderr);
				exit(1);
			}

		maxterms = MaxTerms(natoms);
		if (0 == maxterms)
			{
				fputs("\nError calculating maximum number of interaction terms\n",
				      stderr);
				exit(1);
			} //maximum number of interaction terms (from combinatorical considerations)

		//-----------------------------------------------------------------------------------------------------------

		//read atom masses from top of trajectory file
		if (0
		      != ReadAtomMasses(trajectory, natoms, m, bOutput, outatomsfile,
		            atomsmassesfile))
			{
				fputs("\nFailed to read atom masses from top of trajectory file\n",
				      stderr);
				exit(1);
			}

		//Variable initialization is already included in calloc

		//-----------------------------------------------------------------------------------------------------------
		LeftmostBathAtom = natoms + 1;

		stateprev.t = 0;
		if (0 != ReadPhase(trajectory, natoms, stateprev))
			{ //read zeroeth step phase space coordinates
				fputs(
				      "\nFailed to read first step phase data from trajectory file\n",
				      stderr);
				exit(1);
			}
		stateprev.t = TempPhaseTime;

		//-----------------------------------------------------------------------------------------------------------

		if (0 != CalcKin(natoms, m, kprev, stateprev))
			{
				; //calculate zeroeth step (minus half-step) kinetic energy
				fputs("\nFailed to calculate first step kinetic energy\n", stderr);
				exit(1);
			}

		//==========================================================================================================
		/*
		 if (0 != ColumnHeadings(natoms, outfile, hrout, a_hr))
		 {//print output column headings to file
		 fputs("\nFailed to print column headings in output files\n", stderr);
		 exit(1);
		 }
		 */
		//==========================================================================================================
		while (feof(trajectory) == 0)
			{ //while the trajectory file hasn't returned end_of_file:

				//read entire trajectory file line for the next time-step: Since GROMACS uses a leap-frog algorithm, in this step we will only use the kinetic energy from the next (half) step, in order to get the velocities at this step)

				if (0 != ReadPhase(trajectory, natoms, state))
					{ //read current step phase space coordinates
						fputs("\nFailed to read phase data from trajectory file\n",
						      stderr);
						exit(1);
					}
				state.t = TempPhaseTime;

				//-----------------------------------------------------------------------------------------------------------

				//read forces file lines for this step

				//dynamic allocation for unknown number of interaction terms
				//DEV NOTE: Put this in a function!
				if (step == 0)
					{ //unknown # of terms ab-initio

						LastTermIndex = maxterms;

						itermlist = (iterm_t *) calloc(LastTermIndex,
						      LastTermIndex * sizeof(iterm_t));
						if (itermlist == NULL)
							{
								perror("memory allocation and initialization error");
								fprintf(stderr,
								      "\nCould not allocate memory (%ld bytes) for interaction term list (%d terms) on first time step\n", LastTermIndex * sizeof(iterm_t), LastTermIndex);
								exit(1);
							}

						for (tau = 0; tau < LastTermIndex; tau++)
							{
								itermlist[tau].nr = MAX_PARTICIPANTS;
								itermlist[tau].f = (dvec *) calloc(itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(dvec));
								if (itermlist[tau].f == NULL)
									{
										fputs(
										      "\nCould not allocate memory for force terms on first time step\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].iatoms = (unsigned short *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(int));
								if (itermlist[tau].iatoms == NULL)
									{
										fputs(
										      "\nCould not allocate memory for interacting atoms indexes on first time step\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].c = (schemecoef *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemecoef));
								if (itermlist[tau].c == NULL)
									{
										fputs(
										      "\nCould not allocate memory for localization coefficients on first time step\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termPower0 = (double *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(double));
								if (itermlist[tau].termPower0 == NULL)
									{
										fputs(
										      "\nCould not allocate memory for local power base unit\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termLocalPower = (schemecoef *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemecoef));
								if (itermlist[tau].termLocalPower == NULL)
									{
										fputs(
										      "\nCould not allocate memory for local power\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termPairwisePower =
								      (schemeatoms *) calloc(itermlist[tau].nr,
								            itermlist[tau].nr * sizeof(schemeatoms));
								if (itermlist[tau].termPairwisePower == NULL)
									{
										fputs(
										      "\nCould not allocate memory for pairwise power\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termCurrent = (schemevec *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemevec));
								if (itermlist[tau].termCurrent == NULL)
									{
										fputs(
										      "\nCould not allocate memory for local current\n",
										      stderr);
										exit(1);
									}

								///*DEBUG 17-4-12*/ for (a=0;a<N_SCHEMES;a++) for (q=0;q<DIM;q++) for (j=0;j<itermlist[tau].nr;j++) {printf("%.2g\n",itermlist[tau].termCurrent[j][q][a]);itermlist[tau].termCurrent[j][q][a]=0;}

							}

					}

				//-----------------------------------------------------------------------------------------------------------

				//dynamic allocation for now known number of interaction terms
				//DEV NOTE: Put this in a function!
				if (step == 1)
					{ //reallocate f[][][] according to # of terms known from last step. For step>1 no new reallocation is necessary (but remember to free up memory when closing!)

						templist = (iterm_t *) calloc(LastTermIndex,
						      LastTermIndex * sizeof(iterm_t)); //15-6-11
						if (templist == NULL)
							{
								fputs(
								      "\nCould not reallocate memory for interaction term list on second time step\n",
								      stderr);
								exit(1);
							}

						for (tau = 0; tau < LastTermIndex; tau++)
							templist[tau].nr = itermlist[tau].nr;
						for (tau = 0; tau < maxterms; tau++)
							{
								free(itermlist[tau].f);
								free(itermlist[tau].iatoms);
								free(itermlist[tau].c);
								free(itermlist[tau].termPower0);
								free(itermlist[tau].termLocalPower);
								free(itermlist[tau].termPairwisePower);
								free(itermlist[tau].termCurrent);
							}

						free(itermlist);
						itermlist = templist;

						/*
						 //15-6-11
						 for(tau=0;tau<LastTermIndex;tau++){

						 templist[tau].f=(dvec *)malloc(templist[tau].nr*sizeof(dvec));
						 if (tempforce==NULL){fputs("\nCould not allocate memory for force terms on second time step\n",stderr);exit(1);}
						 free(itermlist[tau].f);itermlist[tau].f=tempforce;free(tempforce);

						 tempiatoms=(int *)malloc(templist[tau].nr*sizeof(int));
						 if (tempiatoms==NULL){fputs("\nCould not allocate memory for interacting atoms indexes on second time step\n",stderr);exit(1);}
						 free(itermlist[tau].iatoms);itermlist[tau].iatoms=tempiatoms;free(tempiatoms);

						 tempcoef=(schemecoef *)malloc(templist[tau].nr*sizeof(schemecoef));
						 if (tempcoef==NULL){fputs("\nCould not allocate memory for localization coefficients on second time step\n",stderr);exit(1);}
						 free(itermlist[tau].tempcoef);itermlist[tau].c=tempcoef;free(tempcoef);

						 temppower0=(double *)malloc(templist[tau].nr*sizeof(double));
						 if (temppower0==NULL){fputs("\nCould not allocate memory for power due to interaction on second time step\n",stderr);exit(1);}
						 free(itermlist[tau].termPower0);itermlist[tau].termPower0=temppower0;free(temppower0);

						 temptermpower=(schemecoef *)malloc(templist[tau].nr*sizeof(schemecoef));
						 if (temptermpower==NULL){fputs("\nCould not reallocate memory for local power on second time step\n",stderr);exit(1);}
						 free(itermlist[tau].termLocalPower);itermlist[tau].termLocalPower=temptermpower;free(temptermpower);

						 }
						 */
						//1-7-11
						for (tau = 0; tau < LastTermIndex; tau++)
							{

								itermlist[tau].nr = templist[tau].nr;

								itermlist[tau].f = (dvec *) calloc(itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(dvec));
								if (itermlist[tau].f == NULL)
									{
										fputs(
										      "\nCould not allocate memory for force terms on second time step\n",
										      stderr);
										exit(1);
									}

								itermlist[tau].iatoms = (unsigned short *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(int));
								if (itermlist[tau].iatoms == NULL)
									{
										fputs(
										      "\nCould not allocate memory for interacting atoms indexes on second time step\n",
										      stderr);
										exit(1);
									}

								itermlist[tau].c = (schemecoef *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemecoef));
								if (itermlist[tau].c == NULL)
									{
										fputs(
										      "\nCould not allocate memory for localization coefficients on second time step\n",
										      stderr);
										exit(1);
									}

								itermlist[tau].termPower0 = (double *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(double));
								if (itermlist[tau].termPower0 == NULL)
									{
										fputs(
										      "\nCould not allocate memory for power due to interaction on second time step\n",
										      stderr);
										exit(1);
									}

								itermlist[tau].termLocalPower = (schemecoef *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemecoef));
								if (itermlist[tau].termLocalPower == NULL)
									{
										fputs(
										      "\nCould not reallocate memory for local power on second time step\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termPairwisePower =
								      (schemeatoms *) calloc(itermlist[tau].nr,
								            itermlist[tau].nr * sizeof(schemeatoms));
								if (itermlist[tau].termPairwisePower == NULL)
									{
										fputs(
										      "\nCould not reallocate memory for local pairwise power on second time step\n",
										      stderr);
										exit(1);
									}
								itermlist[tau].termCurrent = (schemevec *) calloc(
								      itermlist[tau].nr,
								      itermlist[tau].nr * sizeof(schemevec));
								if (itermlist[tau].termCurrent == NULL)
									{
										fputs(
										      "\nCould not reallocate memory for local current on second time step\n",
										      stderr);
										exit(1);
									}

							}
						//free(templist);
					}

				//DEV NOTE: Put this in a function!
				tau = 0;
				bForcesStepOver = 0;

				while (1 != bForcesStepOver)
					{
						//read force type
						/*
						 if (fgets(tempterm, FORCE_NAME_LENGTH, forces) == NULL)
						 {
						 fputs(
						 "\n\nError reading forces file (force headers)\n",
						 stderr);
						 exit(1);
						 }
						 */
						if ( (feof(forces) == 0) && (1 != fscanf(forces, "%d\n", &tempterm)) )
							{
								fprintf(stderr,"\n\nForce file ends prematurely on step %d (state.t = %e , stateprev.t = %e , tforces = %e , tau = %3d, tempterm = %d).\n\n", step, state.t, stateprev.t, tforces, tau, tempterm);
								exit(113);
							}
						//					if ((int) atoi(tempterm) == 0)

	          ///*DBG 28-7-13*/if (49997 < step) fprintf(stderr,"step %d: state.t = %e , stateprev.t = %e , tforces = %e , tau = %3d, tempterm = %d\n", step, state.t, stateprev.t, tforces, tau, tempterm);

						if (tempterm == 0)
							{ //if it's a a new time-step:
								scanfnum = tforces = -1;
								scanfnum = fscanf(forces, "%lf\n", &tforces);

								if ((1 != scanfnum) || (fabs(stateprev.t - tforces) > (state.t -stateprev.t) / 2.)) //commented out 28-7-13
								///*DBG 28-7-13*/if ((1 != scanfnum) || ( (stateprev.t != tforces) && (fabs(state.t - stateprev.t) < fabs(tforces - stateprev.t - 10.) ) ) )
									{
										if (tforces != stateprev.t) fprintf(stderr,
										      "\n\nDEBUG: step %d : state.t=%g\n\ntforces=%g - stateprev.t=%g = %g (%g)\n\ntau=%d       out of  LastTermIndex=%d\n\nscanfnum=%d\n",
										      step, state.t,
													tforces, stateprev.t, tforces - stateprev.t,fabs(tforces - stateprev.t - 10.),
										      tau, LastTermIndex,
													scanfnum);
										fputs("\n\nError in forces file time-stamp\n",
										      stderr);
										exit(18);
									}

								bForcesStepOver = 1;
								///*DEBUG 10-6-12*/fprintf(stderr,"step=%d tau=%d LastTermIndex=%d\n",step,tau,LastTermIndex);
								if (step == 0) LastTermIndex = tau;

							}

						else
							{ //it's a force term
							  //							itermlist[tau].nr = (int) atoi(tempterm);
								itermlist[tau].nr = tempterm;

								//printf("DEBUG:	step=%d	tau=%d	LastTermIndex=%3d	term.nr=%d:\n",step,tau,LastTermIndex,itermlist[tau].nr);//DEBUG//2-6-11
								//for (i=0;i<itermlist[tau].nr;i++) printf("%60d\n",itermlist[tau].iatoms[i]);//DEBUG//2-6-11
								//for (i=0;i<itermlist[tau].nr;i++) printf("								%d\n",itermlist[tau].iatoms[i]);//DEBUG

								ReadForce(forces, tau, itermlist, LeftmostBathAtom,
								      RanForFile);

								tau++;

								//DEV NOTE: Move PotLoc and TermPowCalc HERE, eliminating the need for *itermlist (instead, only one iterm_t is handled at any given time -- their contributions to the power are addative, and they are local in time).

							}
					} //end reading forces for this step

				//==========================================================================================================

				//initialize energies (and related variables) for current step
				for (j = 0; j < natoms; j++)
					k[j] = 0;
				for (a = 0; a < N_SCHEMES; a++)
					for (j = 0; j < natoms; j++)
						u[j][a] = 0;

				//-----------------------------------------------------------------------------------------------------------

				//calculate leap-frog kinetic energy and velocities based on the previous and next half-steps
				if (0 != CalcKin(natoms, m, k, state))
					{
						fputs("\nError in recalculating kinetic energies\n", stderr);
						exit(1);
					} //next half-step kinetic energy

				if (0 != AntiLeapFrog(natoms, state, stateprev, k, kprev, kfrog))
					{
						fputs("\nError in reversing leap-frog algorithm\n", stderr);
						exit(1);
					};

				for (j = 0; j < natoms; j++){
					T[j] += kfrog[j] * 1000 / (1.5 * Rgas); //from kinetic energy (GMX units, KJ/mole) to temperature (in Kelvin)
	        T2[j] += pow( kfrog[j] * 1000 / (1.5 * Rgas) ,2);
					//*DBG 4-6-13*/printf("%e %e %e\n",kfrog[j],T[j],T2[j]);
					for (a=0;a<N_SCHEMES;a++) E2[j][a] += pow( kfrog[j] + u[j][a] ,2);
					}


				//==========================================================================================================

				for (tau = 0; tau < LastTermIndex; tau++)
					PotLoc(tau, itermlist, u); //localize potential energy

				//==========================================================================================================

				//(accumulator of the) local energy change during the current time-step
				for (j = 0; j < natoms; j++)
					for (a = 0; a < N_SCHEMES; a++)
						DeltaE[j][a] += kfrog[j] + u[j][a] - kfrogprev[j]
						      - uprev[j][a];
				//*DBG 4-6-13*/printf("%e %e %e %e %e\n",kfrog[j],u[j][a],kfrogprev[j],uprev[j][a],DeltaE[j][a]);

				//==========================================================================================================

				//calculate power and current:

				for (tau = 0; tau < LastTermIndex; tau++)
					{ //loop over interaction terms (the pot. energy is addative in these terms, therefore so is the power)

						//*DEBUG 6-5-13*/if (2e-4==state.t){ printf("t<%6.4f %3d %1d\t",state.t,tau,itermlist[tau].nr); for(j=0;j<itermlist[tau].nr-1;j++) for(a=j+1;a<itermlist[tau].nr;a++) printf("%2d %2d %+8.3f",itermlist[tau].iatoms[j],itermlist[tau].iatoms[a],BondPower[itermlist[tau].iatoms[j]][itermlist[tau].iatoms[a]][0]);printf("\n");}

          //*DEBUG 7-5-13*/if ((2e-4 == state.t)&&(2==tau)) printf("A \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",itermlist[2].termPairwisePower[0][1][0],BondPower[0][2][0]);


						if (0
						      != CalcTermPowAndCurrent(tau, state, /*step, seg_length,*/ m, itermlist,
						            BondPower, /*natoms,*/ power, currentFromLeft,
						            LeftmostBathAtom))
							{
								fputs(
								      "\nError in Calculation of Term Pairwise Power and Current\n",
								      stderr);
								exit(21);
							}

          //*DEBUG 7-5-13*/if ((2e-4 == state.t)) printf("G \t tau=%2d P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",tau,itermlist[2].termPairwisePower[0][1][0],BondPower[0][2][0]);


						/*DEBUG 6-5-13/if ((2e-4==state.t)){ printf("t=%6.4f tau=%3d n_tau=%1d",state.t,tau,itermlist[tau].nr); for(j=0;j<itermlist[14].nr-1;j++) for(a=j+1;a<itermlist[tau].nr;a++) 
							if (0==BondPower[itermlist[14].iatoms[j]][itermlist[14].iatoms[a]][0]) printf(" %c[%d;%dm aj=%2d ai=%2d P_04=%+7.1e %c[%dm",START_COLOR,BOLD_FONT,GREEN_FG_COLOR,itermlist[tau].iatoms[j],itermlist[tau].iatoms[a],BondPower[itermlist[14].iatoms[j]][itermlist[14].iatoms[a]][0],START_COLOR,COLOR_ATTRIBUTES_OFF);
							else printf(" %c[%d;%dm aj=%2d ai=%2d P_04=%+7.1e %c[%dm",START_COLOR,BLINK_FONT,RED_FG_COLOR,itermlist[tau].iatoms[j],itermlist[tau].iatoms[a],BondPower[itermlist[14].iatoms[j]][itermlist[14].iatoms[a]][0],START_COLOR,COLOR_ATTRIBUTES_OFF);
					

printf("\n");}
*/

						//						if (0 != CalcTermPow(tau, state, itermlist, power))
						//							{
						//								fputs("\nError in Calculation of Termwise Power\n",
						//								      stderr);
						//								exit(2);
						//							}

						/*DEBUG 18-4-12*/
						//printf("%.2g %d",stateprev.t,tau);
						//printf(" %.7g",stateprev.x[0][0]);
						//if (0 != CalcTermCurrent(tau, state, itermlist, current, currentacc,TermPairwiseCurrentFile)) {
						//						if (0
						//						      != CalcTermCurrent(tau, state, /*stateprev,*/itermlist,
						//						            current))						//, currentacc))
						//							{ // 13-8-12
						//								fputs("\nError in Calculation of Termwise Current\n",
						//								      stderr);
						//								exit(2);
						//							}
						/*DEBUG 18-4-12*/
						//printf("\t%.2g %d",stateprev.t,tau);
						//printf(" %.7g",state.x[0][0]);
						//printf("\t%-8.2g",itermlist[tau].termPairwisePower[0][0][0]);
						//printf(" %-8.2g",itermlist[tau].termCurrent[0][0][0]);
						//printf(" %-8.2g",current[0][0][0]);
						//printf(" %-8.2g",currentacc[0][0]);
						//printf("\t%.7g\n",stateprev.x[0][0]);
						/* Minimal code to reproduce the error: 18-4-12

						 clear ; cd /home/inonshar/Documents/GROMACS/ButaneDiThiol/18-4-12/ ; gcc ../../Parse/parse-18-4-12.c -g -lm -O0 -Wall -o ../../Parse/parse-18-4-12.out ; ExitCode=$? ; echo ExitCode=$ExitCode ; if [ 0 -eq $ExitCode ] ; then cp ../../Parse/parse-18-4-12.out . ; cd 29A144F2/debug/ ; cp ../../parse-18-4-12.out . ; gdb --args ./parse-18-4-12.out trajectory-1.txt forces-1.txt output1.txt 1 ; fi

						 */

						/*
						 //DEBUG 25-7-12
						 if (0 != debug_25_7_12(tau, a_hr, state, itermlist,
						 //power,
						 TermPairwiseCurrentFile))
						 {
						 fputs("\nError in debug_25_7_12\n", stderr);
						 exit(2);
						 }
						 */

						//						//DEBUG 15-3-13
						//						if (0 != 
						//											debug_15_3_13(
						//									      /*const unsigned short*/ tau,
						//									      /*const unsigned short*/ a_hr,
						//												/*const unsigned*/ step,
						//									      /*const state_t*/ state,
						//									      /*const iterm_t */itermlist,
						//												/*schemeatoms */BondPower,
						//									      /*schemecoef */power,
						//                        /*schemecoef */poweracc,
						//									      /*schemevec */currentFromLeft,
						//									      /*FILE */TermPairwiseCurrentFile)
						//											)
						//              {
						//                fputs("\nError in debug_15_3_13\n", stderr);
						//                exit(2);
						//              }						
					} //end loop over interaction terms "tau"


          //*DEBUG 7-5-13*/if ((2e-4 == state.t)) printf("H \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",itermlist[2].termPairwisePower[0][1][0],BondPower[0][2][0]);


				for (tau = 0; tau < LastTermIndex; tau++)
					{

/*						
						if (0 != debug_15_3_13(tau,a_hr,step,state,itermlist,BondPower,power,poweracc,currentFromLeft,TermPairwiseCurrentFile))
							{
								fputs("\nError in debug_15_3_13\n", stderr);
								exit(2);
							}
*/
						
					}

				//==========================================================================================================

				++step;

				for (a = 0; a < N_SCHEMES; a++)
					for (j = 0; j < natoms; j++)
						poweracc[j][a] += power[j][a];

				//-----------------------------------------------------------------------------------------------------------

				//print output to file
				if (step % seg_length == 0)
					{
						ExitCode = 0;
						ExitCode = CoarseGrainTimeSeg(state, stateprev, step, hrout,
						      outfile, currentfile, currenthrout, BondPowerFile,
						      BondPower, poweracc, currentFromLeft, /*currentacc, */
						      DeltaE, seg_length, natoms, kfrog, T, T2, u, E2, bOutput, a_hr);
					}
				if (0 != ExitCode)
					{
						fputs("\nError in Coarse Graining over Time Segment\n",
						      stderr);
						exit(2);
					} //coarse-grain over time segments


				//-----------------------------------------------------------------------------------------------------------

				/*
				 if (0!=debug_31_5_11(state,stateprev,a_hr,natoms,m,kfrog,kfrogprev,itermlist,LastTermIndex,u,uprev,maxterms,power,poweracc)){
				 fputs("\nError in debug_31_5_11 function\n", stderr);
				 exit(888);
				 }
				 */

				/*
				 //DBG 29-7-12
				 if (1==step) printf("j kfrog[j]\ta power eta\n");
				 for (j=0;j<natoms;j++){
				 printf("\n%d %4g",j,kfrog[j]);
				 for (a=0;a<N_SCHEMES;a++) printf("\t%d %+4g %+4g",a,power[j][a],power[j][a]/kfrog[j]);
				 }
				 */

				//-----------------------------------------------------------------------------------------------------------
				//set up next time-step: rename the current time-step as the previous one
				if (0
				      != SetupNextTimeStep(natoms, state, stateprev, k, kprev,
				            kfrog, kfrogprev, u, uprev, power
				            //,LastTermIndex,
				            //itermlist
				            ))
					{
						fprintf(stderr,
						      "\nError setting up next time step on step %d\n", step);
						exit(3);
					}

				if (0 != InitTermCurrent(LastTermIndex, itermlist))
					{
						fprintf(stderr,
						      "\nError initiallizing termCurrent for next time step on step %d\n",
						      step);
						exit(3);
					}

				stateprev.t = state.t; //these are pointers to scalars, and therefore any changes made to them within the function SetupNextTimeStep will be actually only made to the copies of them which were sent to the function. Here, the change is done outside the function, with global scope.

				//==========================================================================================================

			} //trajectory file eof

		//==========================================================================================================

		//finish up
		ExitCode = 0;
		if (0
		      != FinishUp(step, seg_length, trajectory, forces, outfile, hrout,
		            outatomsfile, atomsmassesfile, currentfile, currenthrout,
		            TermPairwiseCurrentFile, RanForFile, BondPowerFile))
		//if (0 != FinishUp(step, seg_length, trajectory, forces, outfile, hrout, outatomsfile, atomsmassesfile, currentfile, currenthrout)) // 13-8-12
			{
				fputs("\nError finishing up\n", stderr);
				ExitCode = 8;
			}

		free(m);
		free(state.x);
		free(state.v);
		free(state.vfrog);
		free(stateprev.x);
		free(stateprev.v);
		free(stateprev.vfrog);
		free(k);
		free(kprev);
		free(kfrog);
		free(kfrogprev);
		free(power);
		free(poweracc);
		for(j=0;j<natoms;j++) free(BondPower[j]);
		free(BondPower);
		free(currentFromLeft);
		free(DeltaE);
		free(u);
		free(uprev);
		free(bOutput);
		//free(hroutfilename);

		for (tau = 0; tau < LastTermIndex; tau++)
			{
				free(itermlist[tau].f);
				free(itermlist[tau].iatoms);
				free(itermlist[tau].c);
				free(itermlist[tau].termPower0);
				free(itermlist[tau].termLocalPower);
				free(itermlist[tau].termPairwisePower);
				free(itermlist[tau].termCurrent);
			}

		free(itermlist);

		//printf("\n\n");
		if (0 != ExitCode) exit(ExitCode);
		else printf("\n#Normal termination of parse-*.c\n");
		return 0;
	} //end main

//==========================================================================================================

//functions:

int ColumnHeadings(unsigned short natoms,
// FILE *outfile
      FILE *hrout,
      unsigned short a_hr)
	{

		/*	fprintf(
		 outfile,
		 "  #			,,,,a=0				,,,,a=1				,,,,a=2\n  #\n  # , t , j,kfrog      ,u[0]   ,DE[0] ,P[0],DE-P[0]	,u[1]  ,DE[1] ,P[1] DE-P[1]	,u[2]  ,DE[2] ,P[2],DE-P[2]");
		 */

		if (natoms == 2) fprintf(hrout,
		      "#    a=%d   |         j=0             |         j=1\n#          |                         |\n#      t |   DE[0]     P[0] DE-P[0]|   DE[1]     P[1] DE-P[1]\n#_________|_________________________|_________________________",
		      a_hr);

		if (natoms == 3)
			{
				//	fprintf(outfile,"\n#  tprev  j kfrog[j]	u[0][j]	DE^{0}_{j}(t) P^{0}_{j}*dt (DE-P)/min(DE,P)	u[1][j]	DE^{1}_{j}(t) P^{1}_{j}*dt (DE-P)/min(DE,P)	u[2][j]	DE^{2}_{j}(t) P^{2}_{j}*dt (DE-P)/min(DE,P)\n");
				fprintf(hrout,
				      "   a_hr=%d |		j=0	    |		j=1	      |		j=2\n   # tprev|	DE0	P0    DE0-P0|	DE1	P1	DE1-P1|	DE2	    P2	 DE2-P2",
				      a_hr);
				//		fprintf(hrout, "#	tprev	DE0		P0	DE0-P0	DE1		P1	DE1-P1	DE2		P2	DE2-P2");
			}

		if (natoms == 4)
			{
				//columns:	 1     2   3         4          5         6        7          8         9        10       11
				//		fprintf(outfile,"#			a=0				a=1				a=2\n#\n#   dt  j kfrog   u[0]  DE[0]     P[0]   DE-P[0]  u[1]  DE[1]     P[1]   DE-P[1] u[2]  DE[2]     P[2]   DE-P[2]");
				//#				a=0				a=1				a=2\n#\n#   t   j kfrog		u[0]   DE[0]   P[0]   DE-P	u[1]   DE[1]   P[1]   DE-P	u[2]   DE[2]   P[2]   DE-P");
				/*"#     t   j  kfrog[j]    u[0][j]  DE[0][j]  P[0][j] DE-P[0][j]   u[1][j]  DE[1][j]  P[1][j] DE-P[1][j]   u[2][j]  DE[2][j]  P[2][j] DE-P[2][j]"*/
				fprintf(hrout,
				      "#   t     |   DE0       P0   DE0-P0 |   DE1       P1   DE1-P1 |   DE2       P2   DE2-P2 |   DE3       P3   DE3-P3\n__________|_________________________|_________________________|_________________________|_________________________");
			}
		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int ReadNAtoms(FILE *trajectory, unsigned short natoms)
	{
		int scanfnum, temp;
		scanfnum = fscanf(trajectory, "%d\n", &temp);
		natoms = (unsigned short) temp;

		if (scanfnum != 1)
			{
				fputs("\n\nError reading trajectory file (natoms)\n", stderr);
				exit(3);
			}
		else return natoms;
		//DEV NOTE: why does "natoms" not change on the global scale when changed in this function? To solve this problem temporarily, "natoms" is used as the return value of the function.
	}

//-----------------------------------------------------------------------------------------------------------

int ReadAtomMasses(
      FILE *trajectory,
      unsigned short natoms,
      double m[],
      unsigned short *bOutput,
      FILE *outatomsfile,
      FILE *atomsmassesfile)
	{
		int scanfnum;
		unsigned short j;
		for (j = 0; j < natoms; j++)
			{
				scanfnum = fscanf(trajectory, "%lf ", &m[j]);
				if (scanfnum != 1)
					{
						fprintf(stderr, "\n\nError reading trajectory file (m[%d])\n",
						      j);
						exit(4);
					}
				fprintf(atomsmassesfile, "%lf ", m[j]);
				//if ((m[j]>=4.0)&&(m[j]<=300.0)){bOutput[j]=1;fprintf(outatomsfile,"%d ",j+1);}
				//bOutput[j]=1;fprintf(outatomsfile,"%d ",j+1); //15-8-12
				if ((m[j] >= 0.) && (m[j] <= 300.0))
					{
						bOutput[j] = 1;
						fprintf(outatomsfile, "%d ", j + 1);
					} //16-8-12
			}
		fprintf(outatomsfile, "\n");
		fprintf(atomsmassesfile, "\n");
		scanfnum = fscanf(trajectory, "\n");
		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int MaxTerms(unsigned short natoms)
	{
		unsigned short maxterms = 0, h, i, j, ppp, qqq, maxtypes[6] =
			{ 0, 0, 4, 6, 2, 1 };
		if (natoms == 1) return 1; // = maxtypes[natoms]+natoms (the second term is due to possible system-bath interactions)
		if (natoms == 2) return 6;
		else for (i = 2;
		      i < ((natoms < MAX_PARTICIPANTS) ? natoms : MAX_PARTICIPANTS); i++)
			{ //interaction orders
				for (h = 0; h < maxtypes[i]; h++)
					{ //number of distinct interaction types for a given order
						ppp = 1;
						qqq = 1;
						for (j = 0; j < i; j++)
							{
								ppp = ppp * (natoms - j);
								qqq = qqq * (j + 1);
							}
						maxterms += ppp / qqq; //add number of permutations to counter
					}
			}
		/*DEBUG 10-6-12*/if (0 == maxterms) fprintf(stderr,
		      "maxterms=0 natoms=%d MAX_PARTICIPANTS=%d", natoms,
		      MAX_PARTICIPANTS);
		return maxterms + 2; // +2 for two bath/terminal setup
	}

//-----------------------------------------------------------------------------------------------------------

int ReadPhase(FILE *trajectory, unsigned short natoms, state_t state)
	{
		int scanfnum = 0;
		unsigned short j, q;
		///*DBG 31-1-13:*/long double dbg_lng_dbl;double dbg_dbl;

		scanfnum = fscanf(trajectory, "%Lf\n", &TempPhaseTime);

		/*
		 double TempTime=0;
		 scanfnum = fscanf(trajectory, "%lf\n", &TempTime);
		 *TempPhaseTime = &TempTime;
		 */
		/*
		 double Temp,*TempTime=NULL;

		 scanfnum=fscanf(trajectory,"%lf\n",&Temp);
		 *TempTime=Temp;
		 **TempPhaseTime=*TempTime;
		 */

		if (scanfnum != 1)
			{
				fputs("\n\nError reading trajectory file (first line time)\n",
				      stderr);
				exit(1);
			}
		for (j = 0; j < natoms; j++)
			{

				for (q = 0; q < DIM; q++)
				///*DBG 30-1-13:*/fscanf(trajectory, "%Lf ", &dbg_lng_dbl);fscanf(trajectory, "%lf ", &dbg_dbl);printf("%Lf %lf\n",dbg_lng_dbl,dbg_dbl);for (q = 2; q < DIM; q++)
					{
						scanfnum = fscanf(trajectory, "%lf ", &state.x[j][q]);
						if (scanfnum != 1)
							{
								fputs(
								      "\n\nError reading trajectory file (first line position)\n",
								      stderr);
								exit(1);
							}
					}

				///*DBG 30-1-13:*/printf("%g\n",state.x[0][0]);

				for (q = 0; q < DIM; q++)
					{
						scanfnum = fscanf(trajectory, "%lf ", &state.v[j][q]);
						if (scanfnum != 1)
							{
								fputs(
								      "\n\nError reading trajectory file (first line velocity)\n",
								      stderr);
								exit(1);
							}
					}
			}

		//Check to make sure no two atoms are superpositioned -- possibly due to energy minimization glitch
		if (ChkAtmSuprpos(natoms, state, TempPhaseTime) != 0)
			{
				fprintf(stderr,
				      "\n\nError in reading position coordinates: At time %Lf ns, atom superposition test failed. Exiting...\n\n",
				      TempPhaseTime);
				exit(3);
			}

		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int ChkAtmSuprpos(unsigned short natoms, state_t state, double TempTime)
	{
		//Check to make sure no two atoms are superpositioned -- possibly due to energy minimization glitch
		unsigned short i, j, q, bSuperposition;
		for (j = 0; j < natoms; j++)
			for (i = 0; i < j; i++)
				{
					bSuperposition = 0;
					for (q = 0; q < DIM; q++)
						bSuperposition += (state.x[i][q] == state.x[j][q]) ? 1 : 0; //check for superposition on each axis
					if (bSuperposition == 3)
						{
							fprintf(stderr,
							      "\n\nError in reading position coordinates: At time %g ns, atoms %d and %d (in [0 , natoms-1]) are superpositioned! Exiting...\nDumping problematic position coordinates:\n%d %+lf %+lf %+lf\n%d %+lf %+lf %+lf\n\n",
							      TempTime, i, j, i, state.x[i][0], state.x[i][1],
							      state.x[i][2], j, state.x[j][0], state.x[j][1],
							      state.x[j][2]);
							exit(3);
						}
				}
		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int CalcKin(
      const unsigned short natoms,
      const double m[],
      double k[],
      state_t state)
	{
		unsigned short j, q;
		for (j = 0; j < natoms; j++)
			{
				for (q = 0; q < DIM; q++)
					{
						k[j] += state.v[j][q] * state.v[j][q] * m[j] / 2.0;
					}
			}
		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int AntiLeapFrog(
      const unsigned short natoms,
      state_t state,
      state_t stateprev,
      const double k[],
      double kprev[],
      double kfrog[])
	{ //doesn't need the masses
		unsigned short j, q;
		for (j = 0; j < natoms; j++)
			{

				// DEVNOTE 19-9-12
				//
				// For both md-vv-avek and sd,
				// velocities are fine without anti-leap-frog, however
				// kinetic energies need anti-leap-frog in order for
				// E_{tot}^{GMX} (from ener.edr) = Sum_j (E_j^{parse})
				// E_j^{parse} \equiv kfrog_j + u_j^a

				/* DEVNOTE 6-5-11: Needed if leap-frog algorithm is used.*/
				for (q = 0; q < DIM; q++)
					state.vfrog[j][q] =
					      ((stateprev.v[j][q] + state.v[j][q]) == 0) ?
					            0 :
					            ((stateprev.v[j][q] + state.v[j][q])
					                  / fabs(stateprev.v[j][q] + state.v[j][q]))
					                  * sqrt(
					                        (stateprev.v[j][q] * stateprev.v[j][q]
					                              + state.v[j][q] * state.v[j][q])
					                              / 2.0); //ternary operator to avoid division by zero for atoms at rest

					                  // DEVNOTE 6-5-11: Needed if leap-frog algorithm is used.
				kfrog[j] = (kprev[j] + k[j]) / 2.0; //the leap-frog algorithm pertains to the kinetic energy, not the velocities, but since the mass remain unchanged, vfrog \propto \sqrt{vprev^2+v^2} (the proportionality factor is a unit vector in the direction of vfrog, which is taken to be halfway between the direction of vprev and the direction of v
				//
				//kfrog[j] = k[j];//non-leap-frog algorithm -- e.g. velocity Verlet
			}

		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int ReadForce(
      FILE *forces,
      unsigned short tau,
      iterm_t *itermlist,
      unsigned short LeftmostBathAtom,
      FILE *RanForFile)
	{
		//reads forces from forces.txt and sets the values of the  static localization scheme coefficients according to the number of participating atoms (does nothing for the dynamic values).
		int scanfnum = 0, temp;
		unsigned short i, a, q;

		for (i = 0; i < itermlist[tau].nr; i++)
			{
				scanfnum += fscanf(forces, "%d ", &temp);
				itermlist[tau].iatoms[i] = (unsigned short) temp;
				// printf("%d %d %d %d\n",tau,i,temp,itermlist[tau].iatoms[i]);
				/*if ( (1 <= i) && (itermlist[tau].iatoms[i-1] == itermlist[tau].iatoms[i]) ){
				 fputs("atom index numbers read in badly from forces file.\n",stderr);
				 return 5;
				 }*/
			}
		if (scanfnum != itermlist[tau].nr)
			{
				fputs("\n\nError reading forces file (wrong number of indexes)\n",
				      stderr);
				exit(1);
			}
		scanfnum = 0;

		switch (itermlist[tau].nr)
			{ //use seperate force completion depending on number of participants (save read-write time to hard-disk)
			case 2:
				for (q = 0; q < DIM; q++)
					scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[0][q]);
				if (scanfnum != (itermlist[tau].nr - 1) * DIM)
					{
						fputs(
						      "\n\nError reading forces file (wrong number of force components)\n",
						      stderr);
						exit(1);
					}
				if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm))
					{
						fputs("\n\nError reading forces file (energy)\n", stderr);
						exit(1);
					}
				for (q = 0; q < DIM; q++)
					itermlist[tau].f[1][q] = -itermlist[tau].f[0][q]; //force completion
				for (a = 0; a < N_SCHEMES; a++)
					{
						for (i = 0; i < itermlist[tau].nr; i++)
							{
								itermlist[tau].c[i][a] = 0.5;
								//itermlist[tau].c[i][a] = ( (0 == itermlist[tau].iatoms[i]) ? (1./3.) : (2./3.)); // 16-10-12: C[0]=1/3 , C[1]=2/3 <== c[i][a] = 1 - m[ai]/(m[ai]+m[aj])
							} // end loop over i
					} //end loop over a
				break;
			case 3:
				for (q = 0; q < DIM; q++)
					{
						scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[0][q]);
						scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[2][q]);
					}
				if (scanfnum != (itermlist[tau].nr - 1) * DIM)
					{
						fputs(
						      "\n\nError reading forces file (wrong number of force components)\n",
						      stderr);
						exit(1);
					}
				if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm))
					{
						fputs("\n\nError reading forces file (energy)\n", stderr);
						exit(1);
					}
				for (q = 0; q < DIM; q++)
					itermlist[tau].f[1][q] = -itermlist[tau].f[0][q]
					      - itermlist[tau].f[2][q]; //force completion
				for (i = 0; i < itermlist[tau].nr; i++)
					{
						itermlist[tau].c[i][0] = (1.0 / itermlist[tau].nr); //equipartition
						itermlist[tau].c[i][1] =
						      ((i == 0) || (i == (itermlist[tau].nr - 1))) ?
						            0 : (1.0 / (itermlist[tau].nr - 2)); //apex/vertex
					}
				break;
			case 4:
				for (q = 0; q < DIM; q++)
					for (i = 0; i < itermlist[tau].nr; i++)
						scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[i][q]); //no force completion
				if (scanfnum != itermlist[tau].nr * DIM)
					{
						fputs(
						      "\n\nError reading forces file (wrong number of force components)\n",
						      stderr);
						exit(1);
					}
				if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm))
					{
						fputs("\n\nError reading forces file (energy)\n", stderr);
						exit(1);
					}
				for (i = 0; i < itermlist[tau].nr; i++)
					{
						itermlist[tau].c[i][0] = (1.0 / itermlist[tau].nr); //equipartition
						itermlist[tau].c[i][1] =
						      ((i == 0) || (i == (itermlist[tau].nr - 1))) ?
						            0 : (1.0 / (itermlist[tau].nr - 2)); //apex/vertex
					}
				break;
			case 1:
				for (q = 0; q < DIM; q++)
					scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[0][q]);
				if (scanfnum != DIM)
					{
						fputs(
						      "\n\nError random force from forces file (wrong number of force components)\n",
						      stderr);
						exit(1);
					}
				if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm))
					{
						fputs(
						      "\n\nError reading random force coupling constant, gamma\n",
						      stderr);
						exit(1);
					}
				for (a = 0; a < N_SCHEMES; a++)
					itermlist[tau].c[0][a] = 1.0;

				/*DEV*/
				fprintf(RanForFile, "%e %e %e\n", itermlist[tau].f[0][0],
				      itermlist[tau].f[0][1], itermlist[tau].f[0][2]);
				LeftmostBathAtom =
				      (LeftmostBathAtom > itermlist[tau].iatoms[0]) ?
				            itermlist[tau].iatoms[0] : LeftmostBathAtom;
				break;
			default:
				fprintf(stderr,
				      "\nTerm has either less than 1 or more than 4 participating atoms: tau=%d , itermlist[%d].nr=%d\n",
				      tau, tau, itermlist[tau].nr);
				exit(1);
			}

		return 0;
	}

//------------------------------------------------------------------------------------------------------------

int PotLoc(unsigned short tau, iterm_t *itermlist, double u[][N_SCHEMES])
	{ //localize potential energy for given term
		unsigned short i, a, ai, q, nr = itermlist[tau].nr;
		double f2tot = 0, f2[nr], fiq;

		//............................................................................................................

		//total squared angle bending forces for given term
		for (i = 0; i < nr; i++)
			{ //loop over participating atoms. This loop is necessary only for the dynamic localization scheme (0) since u[0][j] depends on f2tot. When using simpler (static) localization schemes, the distribution of the term's potential energy among the participating atoms is given in advance, and f2tot needs not be calculated at all.
				f2[i] = 0.0;
				for (q = 0; q < DIM; q++)
					{
						fiq = itermlist[tau].f[i][q];
						///*DEBUG 17-4-12*/printf("itermlist[%d].f[%d][%d]=%.2g\n",tau,i,q,itermlist[tau].f[i][q]);
						f2[i] += fiq * fiq;
					}
				f2tot += f2[i];

			}

		//............................................................................................................

		//total dynamic localization scheme
		for (i = 0; i < nr; i++)
			itermlist[tau].c[i][2] = (f2tot == 0.0) ? 0.0 : (f2[i] / f2tot);

		//............................................................................................................

		//localize potential energy
		for (a = 0; a < N_SCHEMES; a++)
			for (i = 0; i < nr; i++)
				{
					ai = itermlist[tau].iatoms[i];

					/*//DBG 25-12-12
					 if ((0==tau)&&(0==a)&&(i==0)) printf("DBG (25-12-12):\nai a          u    tau nr i          c            vterm\n");
					 printf("%d  ",ai);
					 printf("%d  ",a);
					 printf("%e  ",u[ai][a]);
					 printf("%d  ",tau);
					 printf("%d  ",itermlist[tau].nr);
					 printf("%d  ",i);
					 printf("%e  ",itermlist[tau].c[i][a]);
					 printf("%e  ",itermlist[tau].vterm);
					 printf("%e  ",itermlist[tau].c[i][a] * itermlist[tau].vterm);
					 printf("\n");*/

					u[ai][a] +=
					      (1 == itermlist[tau].nr) ?
					            0 : itermlist[tau].c[i][a] * itermlist[tau].vterm;

				}

		return 0;
	}
//-----------------------------------------------------------------------------------------------------------

int CalcTermPowAndCurrent(
      unsigned short const tau,
      state_t const state,
			//const unsigned step, //DEBUG 14-5-13
			//const unsigned seg_length,//DEBUG 14-5-13
			const double *m,
      iterm_t *itermlist,
      schemecoef **BondPower, /* P_i<-j = Sum_tau P_tau,i<-j = -P_j<-i	, j-in-tau */
			//unsigned short const natoms,
      //*BondCurrent, /* I_i<-j = Sum_tau I_tau,i<-j = I_j<-i	, j-in-tau */
      schemecoef *power, /* P_i = Sum_tau Sum_j-in-tau P_tau,i<-j = d/dt E_i(t) */
      schemevec *currentFromLeft /* I_i = Sum_tau Sum_j-in-tau I_tau,i<-j = 0 at steady-state due to Kirchoff's current law */,
      unsigned short LeftmostBathAtom)
	{
		unsigned short a, ai, q;
		double R2 = 0;


		/*DEBUG 7-5-13AM/if ((2e-4==state.t)){ printf("t=%6.4f A tau=%3d n_tau=%1d",state.t,tau,itermlist[tau].nr); for(q=0;q<itermlist[14].nr-1;q++) for(a=q+1;a<itermlist[tau].nr;a++)
    	if (0==BondPower[itermlist[14].iatoms[q]][itermlist[14].iatoms[a]][0]) printf(" %c[%d;%dm aj=%2d ai=%2d P_04=%+7.1e %c[%dm",START_COLOR,BOLD_FONT,GREEN_FG_COLOR,itermlist[tau].iatoms[q],itermlist[tau].iatoms[a],BondPower[itermlist[14].iatoms[q]][itermlist[14].iatoms[a]][0],START_COLOR,COLOR_ATTRIBUTES_OFF);
      else printf(" %c[%d;%dm aj=%2d ai=%2d P_04=%+7.1e %c[%dm",START_COLOR,BLINK_FONT,RED_FG_COLOR,itermlist[tau].iatoms[q],itermlist[tau].iatoms[a],BondPower[itermlist[14].iatoms[q]][itermlist[14].iatoms[a]][0],START_COLOR,COLOR_ATTRIBUTES_OFF);
			printf("\n");}
		*/

		if (1 == itermlist[tau].nr) /* system-bath interaction (DEV: Put this into a separate function) */
			{
				double P_Bath_i = 0, P_i_Bath = 0, v2 = 0;
				ai = itermlist[tau].iatoms[0];
				for (q = 0; q < DIM; q++) /* scalar products */
					{
						v2 += state.vfrog[ai][q] * state.vfrog[ai][q];
						P_i_Bath += itermlist[tau].f[0][q] * state.vfrog[ai][q]; // power from random force
						R2 += itermlist[tau].f[0][q] * itermlist[tau].f[0][q];
					}
            P_Bath_i = (-2.0 * m[ai] * itermlist[tau].vterm) * v2; // power lost through dissipation

				for (a = 0; a < N_SCHEMES; a++)
					{
						itermlist[tau].termLocalPower[0][a] = P_Bath_i + P_i_Bath;
						itermlist[tau].termPairwisePower[0][0][a]=itermlist[tau].termLocalPower[0][a];

						//*DEBUG 13-5-13*/if (0 == a) printf("t=%.4f tau=%1d vterm=%g ai=%2d a=%1d P_Bath_i=%+11e P_i_Bath=%+11e termLocalPower=%+11e BondPower(b4 t)=%+11e ",state.t,tau,itermlist[tau].vterm,ai,a,P_Bath_i,P_i_Bath,itermlist[tau].termLocalPower[0][a],BondPower[ai][ai][a]);
            //*DEBUG 14-5-13*/if ((0 == (step+1) % seg_length)&&(0 == a)) printf("t=%.4f tau=%1d vterm=%g ai=%2d a=%1d P_Bath_i=%+11e P_i_Bath=%+11e termLocalPower=%+11e BondPower(b4 t)=%+11e ",state.t,tau,itermlist[tau].vterm,ai,a,P_Bath_i,P_i_Bath,itermlist[tau].termLocalPower[0][a],BondPower[ai][ai][a]);


						BondPower[ai][ai][a] += itermlist[tau].termLocalPower[0][a];
            //BondPower[ai][ai][a] = itermlist[tau].termLocalPower[0][a];


          //*DEBUG 13-5-13*/if (0 == a) printf("BondPower(t)=%+11e\n",BondPower[ai][ai][a]);
					///*DEBUG 14-5-13*/if ((0 == (step+1) % seg_length)&&(0 == a)) printf("BondPower(t)=%+11e\n",BondPower[ai][ai][a]);


						power[ai][a] += itermlist[tau].termLocalPower[0][a];
						for (q = 0; q < DIM; q++)
							{
								itermlist[tau].termCurrent[0][q][a] = P_Bath_i
								      * state.vfrog[ai][q] / sqrt(v2)
								      + P_i_Bath * itermlist[tau].f[0][q] / sqrt(R2);
								currentFromLeft[ai][q][a] +=
								      (LeftmostBathAtom == itermlist[tau].iatoms[0]) ?
								            itermlist[tau].termCurrent[0][q][a] : 0;
							} //end q loop

					} //end a loop


          //*DEBUG 7-5-13AM*/ if (2e-4 == state.t) printf("t=%6.4f B tau=%3d n_tau=%1d P_04=%+8.1e\n",state.t,tau,itermlist[tau].nr,BondPower[0][4][0]);


			}
		else /* i.e. 1 != itermlist[tau].nr <==> inter-atomic interaction (DEV: Put this into a separate function) */
			{
				unsigned short i, j, aj;
				double termPairwiseCurrentq;
				for (a = 0; a < N_SCHEMES; a++)
					{

						for (i = 0; i < itermlist[tau].nr; i++)
							{
								itermlist[tau].termLocalPower[i][a] = 0;
								///*DBG 19-3-13*/printf("b4 init  : %e %d %d %d %e\n",state.t,tau,a,i,itermlist[tau].termCurrent[i][0][a]);
								for (q = 0; q < DIM; q++)
									itermlist[tau].termCurrent[i][q][a] = 0;
								///*DBG 19-3-13*/printf("after init: %e %d %d %d %e\n",state.t,tau,a,i,itermlist[tau].termCurrent[i][0][a]);
							} // end i loop


	         //*DEBUG 7-5-13AM*/ if (2e-4 == state.t) printf("t=%6.4f C tau=%3d n_tau=%1d P_04=%+8.1e\n",state.t,tau,itermlist[tau].nr,BondPower[0][4][a]);
          //*DEBUG 7-5-13*/if ((2e-4 == state.t)&&(2==tau)) printf("t=%6.4f C tau=%3d n_tau=%1d a=%d \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",state.t,tau,itermlist[tau].nr,a,itermlist[tau].termPairwisePower[0][1][a],BondPower[0][2][a]);
					//*DEBUG 7-5-13AM*/if ((2e-4 == state.t)&&(2==tau)) printf("C \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",itermlist[2].termPairwisePower[0][1][0],BondPower[0][2][0]);



						for (i = 0; i < itermlist[tau].nr - 1; i++)
							{
								ai = itermlist[tau].iatoms[i];
								for (j = i + 1; j < itermlist[tau].nr; j++)
									{
										aj = itermlist[tau].iatoms[j];
										itermlist[tau].termPairwisePower[i][j][a] =
										      itermlist[tau].termPairwisePower[j][i][a] =
										            R2 = 0;
										for (q = 0; q < DIM; q++)
											{
												itermlist[tau].termPairwisePower[i][j][a] +=
												      itermlist[tau].c[j][a]
												            * itermlist[tau].f[i][q]
												            * state.vfrog[ai][q]
												            - itermlist[tau].c[i][a]
												                  * itermlist[tau].f[j][q]
												                  * state.vfrog[aj][q];
												R2 += (state.x[ai][q] - state.x[aj][q])
												      * (state.x[ai][q] - state.x[aj][q]);
												///*DBG 19-3-13*/printf("%d %d %d %d %d %e %e %e %e %e\n",tau,a,i,j,q,state.vfrog[ai][q],state.vfrog[aj][q],itermlist[tau].f[i][q],itermlist[tau].f[j][q],itermlist[tau].termPairwisePower[i][j][a]);
											}
										itermlist[tau].termPairwisePower[j][i][a] =
										      -itermlist[tau].termPairwisePower[i][j][a]; // P_tau,j<-i = - P_tau,i<-j
										itermlist[tau].termLocalPower[i][a] +=
										      itermlist[tau].termPairwisePower[i][j][a]; // P_tau,i = Sum_j P_tau,i<-j
										itermlist[tau].termLocalPower[j][a] +=
										      itermlist[tau].termPairwisePower[j][i][a]; // P_tau,j = Sum_i P_tau,j<-i
                    //*DEBUG 6-5-13*/if ((0 == a)&&(2e-4==state.t)) printf("before %6.4f %3d %1d %2d %2d %+13.4f (The last variable should be zero at its first appearance in each time-step\n",state.t,tau,itermlist[tau].nr,ai,aj,BondPower[ai][aj][a]);
				            //*DEBUG 6-5-13*/if ((0 == a)&&(0 == ai)&&(8 == aj)) printf("before %6.4f %3d %2d %2d %+7.3f %+7.3f\n",state.t,tau,ai,aj,itermlist[tau].termPairwisePower[i][j][a],BondPower[ai][aj][a]);

          /*DEBUG 7-5-13AM/ if ((2e-4 == state.t)&&(3==tau)&&(0==a)){
						for(q = 0;q< itermlist[tau].nr;q++) printf("%d %d\t",q,itermlist[tau].iatoms[q]); printf("\n");
						printf("t=%6.4f D tau=%3d n_tau=%1d ai=%d aj=%d P_01=%+8.1e P_10=%+8.1e P_40=%+8.1e P_04=%+8.1e\n",state.t,tau,itermlist[tau].nr,ai,aj,BondPower[0][1][a],BondPower[1][0][a],BondPower[4][0][a],BondPower[0][4][a]);
						printf("%+8.1e %+8.1e\n",itermlist[tau].termPairwisePower[i][j][a],itermlist[tau].termPairwisePower[j][i][a]);
					}
					*/

										BondPower[ai][aj][a] +=
										      itermlist[tau].termPairwisePower[i][j][a]; //P_i<-j = Sum_tau P_tau,i<-j


										//*DEBUG 6-5-13*/if ((0 == a)&&(0 == ai)&&(8 == aj)) printf("after  %6.4f %3d %2d %2d %+7.3f %+7.3f\n",state.t,tau,ai,aj,itermlist[tau].termPairwisePower[i][j][a],BondPower[ai][aj][a]);
										///*DBG 19-3-13*/printf("t=%.2e tau=%d a=%d ai=%d aj=%d Ptauij=%9.2e Pij=%9.2e Pji=%9.2e ",state.t,tau,a,ai,aj,itermlist[tau].termPairwisePower[i][j][a],BondPower[ai][aj][a],BondPower[aj][ai][a]);
										///*DBG 17-2-13*/printf("%d %d %d %d %g %g %g %g\n",tau,a,i,j,itermlist[tau].termPairwisePower[i][j][a],itermlist[tau].termPairwisePower[j][i][a],itermlist[tau].termLocalPower[i][a],itermlist[tau].termLocalPower[j][a]);


          //*DEBUG 7-5-13AM*/ if ((2e-4 == state.t)&&(3==tau)&&(0==a)) printf("t=%6.4f E tau=%3d n_tau=%1d ai=%d aj=%d P_01=%+8.1e P_10=%+8.1e P_40=%+8.1e P_04=%+8.1e\n",state.t,tau,itermlist[tau].nr,ai,aj,BondPower[0][1][a],BondPower[1][0][a],BondPower[4][0][a],BondPower[0][4][a]);
					//*DEBUG 7-5-13*/ if ((2e-4 == state.t)&&(25==tau)&&(0==ai)&&(8==aj)&&(0==a)) printf("t=%6.4f E tau=%3d n_tau=%1d i=%1d j=%1d ai=%2d aj=%2d \t P_08=%+8.1e \t P_25,0<-8=%+8.1e \t P_25,8<-0=%+8.1e \t P_80=%+8.1e\n",state.t,tau,itermlist[tau].nr,i,j,ai,aj,itermlist[tau].termPairwisePower[i][j][a],BondPower[ai][aj][a],itermlist[tau].termPairwisePower[j][i][a],BondPower[aj][ai][a]);

									 	BondPower[aj][ai][a] +=
                          itermlist[tau].termPairwisePower[j][i][a];

					//*DEBUG 7-5-13AM*/ if ((2e-4 == state.t)&&(3==tau)&&(0==a)) printf("t=%6.4f F tau=%3d n_tau=%1d ai=%d aj=%d P_01=%+8.1e P_10=%+8.1e P_40=%+8.1e P_04=%+8.1e\n",state.t,tau,itermlist[tau].nr,ai,aj,BondPower[0][1][a],BondPower[1][0][a],BondPower[4][0][a],BondPower[0][4][a]);
					//*DEBUG 7-5-13*/ if ((2e-4 == state.t)&&(25==tau)&&(0==ai)&&(8==aj)&&(0==a)) printf("t=%6.4f F tau=%3d n_tau=%1d i=%1d j=%1d ai=%2d aj=%2d \t P_08=%+8.1e \t P_25,0<-8=%+8.1e \t P_25,8<-0=%+8.1e \t P_80=%+8.1e\n",state.t,tau,itermlist[tau].nr,i,j,ai,aj,itermlist[tau].termPairwisePower[i][j][a],BondPower[ai][aj][a],itermlist[tau].termPairwisePower[j][i][a],BondPower[aj][ai][a]);


										for (q = 0; q < DIM; q++)
											{
												termPairwiseCurrentq =
												      itermlist[tau].termPairwisePower[i][j][a]
												            * (state.x[ai][q]
												                  - state.x[aj][q])
												            / sqrt(R2); // I_tau,i<-j = C_tau,j * (F dot v)_tau,i - C_tau,i * (F dot v)_tau,j
												itermlist[tau].termCurrent[i][q][a] +=
												      termPairwiseCurrentq; // I_tau,i = Sum_j I_tau,i<-j
												itermlist[tau].termCurrent[j][q][a] +=
												      termPairwiseCurrentq;
												///*DBG 19-3-13*/printf("%e %d %d %d %d %d %e %e %e %e %e %e %e\n",state.t,tau,a,ai,aj,q,state.x[ai][q],state.x[aj][q],sqrt(R2),itermlist[tau].termPairwisePower[i][j][a],termPairwiseCurrentq,itermlist[tau].termCurrent[i][q][a],itermlist[tau].termCurrent[j][q][a]);
												///*DBG 20-3-13*/if (0==a) printf("t=%9.2e\t tau=%d\t a=%d\t ai=%d\t aj=%d\t q=%d\t Itauijq=%9.2e\t Itauiq=%9.2e\t Itaujq=%9.2e\n",state.t,tau,a,ai,aj,q,termPairwiseCurrentq,itermlist[tau].termCurrent[i][q][a],itermlist[tau].termCurrent[j][q][a]);
												currentFromLeft[ai][q][a] +=
												      (aj < ai) ? termPairwiseCurrentq : 0;//consider only energy transfer from atoms to the "left" of ai (i.e. with atom index less than ai)
												//BondCurrent[ai][aj][q][a] += itermlist[tau].termPairwiseCurrentq[i][j][q][a]; // I_i<-j = Sum_tau I_tau,i<-j
												//BondCurrent[aj][ai][q][a] += itermlist[tau].termPairwiseCurrentq[j][i][q][a];
												currentFromLeft[aj][q][a] +=
												      (ai < aj) ? termPairwiseCurrentq : 0;//consider only energy transfer from atoms to the left of aj (i.e. with atom index less than aj)
											} // end Currents' q loop
										///*DBG 19-3-13*/printf("->Ijx=%9.2e\n",currentFromLeft[aj][0][a]);
									} // end j loop
							} // end i loop
						for (i = 0; i < itermlist[tau].nr; i++)
							power[itermlist[tau].iatoms[i]][a] +=
							      itermlist[tau].termLocalPower[i][a]; // P_i = Sum_tau P_tau,i

          //*DEBUG 7-5-13*/if ((2e-4 == state.t)&&(2==tau)) printf("t=%6.4f F tau=%3d n_tau=%1d a=%d \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",state.t,tau,itermlist[tau].nr,a,itermlist[tau].termPairwisePower[0][1][a],BondPower[0][2][a]);
          //*DEBUG 7-5-13*/if ((2e-4 == state.t)&&(2==tau)) printf("C \t P_eq(2,0<-2)=%+8.1e = P_eq(0<-2)=%+8.1e\n",itermlist[2].termPairwisePower[0][1][0],BondPower[0][2][0]);
					//*DEBUG 7-5-13*/ if ((2e-4 == state.t)&&(25==tau)&&(0==a)) printf("t=%6.4f G tau=%3d n_tau=%1d i=%1d j=%1d ai=%2d aj=%2d \t P_08=%+8.1e \t P_25,0<-8=%+8.1e \t P_25,8<-0=%+8.1e \t P_80=%+8.1e\n",state.t,tau,itermlist[tau].nr,0,3,0,8,itermlist[tau].termPairwisePower[0][3][a],BondPower[0][8][a],itermlist[tau].termPairwisePower[3][0][a],BondPower[8][0][a]);

					} // end a loop

				///*DBG 19-3-13*/a=0; for (i=0;i<itermlist[tau].nr;i++) for (q=0;q<DIM;q++) printf("%e %d %d %d %e\n",state.t,tau,a,q,itermlist[tau].termCurrent[i][q][a]);


			} // end else (1 != itermlist[tau].nr)

		return 0;
	} // end CalcTermPowAndCurrent()

//-----------------------------------------------------------------------------------------------------------
//
////int CalcTermCurrent(unsigned short tau, state_t state, iterm_t *itermlist, schemecoef *power, double *current) { //15-3-12
//
///*Calculate energy currents:
//
// Pair-wise power into atom i from atom j due to term tau
// P[tau][i][j] = c[tau][j]*F[tau][i]*v[i]-c[tau][i]*F[tau][j]*v[j]
//
// Pair-wise into atom i from atom j due to term tau
// J[tau][i][j] = -(r[i]-r[j])*P[tau][i][j]
//
// Total into atom i due to term tau
// J[tau][i] = Sum[j in tau] J[tau][i][j]
//
// Net local energy current into atom i
// J[i] = Sum[tau] J[tau][i]
//
// System total
// J = Sum[i] J[i] = current
//
// In pseudo-code (logical order, not best performance -- loop over tau is external to this functionl; a is the index number of the localization scheme, q is the index number of the Cartesian component of a vector quantity):
//
// Pair-wise power into atom i from atom j due to term tau
// P[tau][i][j!=i][a]	= itermlist[tau].termPairwisePower[i][j][a]
// = Sum[q] itermlist[tau].c[j][a]*itermlist[tau].f[i][q]*state.vfrog[ai][q]-itermlist[tau].c[i][a]*itermlist[tau].f[j][q]*state.vfrog[aj][q]
// = Sum[q] itermlist[tau].f[j][q]*
// { state.vfrog[ai][q] + itermlist[tau].c[i][a]*( state.vfrog[aj][q]-state.vfrog[ai][q] ) }
//
// Pair-wise current into atom i from atom j due to term tau (small density fluctuation approximation)
// J[tau][i][j!=i][q][a]	= itermlist[tau].termPairwiseCurrent[i][j!=i][q][a]
// = -(state.x[ai][q]-state.x[aj][q]) * itermlist[tau].termPairwisePower[i][j][a]
//
// Total into atom i due to term tau
// J[tau][i][q][a]	= itermlist[tau].termCurrent[i][q][a] = Sum[j!=i in tau] J[tau][i][j][q][a]
//
// Net local energy current into atom i
// J[ai][q][a] = current[ai][q][a] = Sum[tau] J[tau][i][q][a]
//
// System total
// J[q][a]	= current[q][a] = (1/2) Sum[ai] J[ai][q][a]
// # 1/2 to avoid double counting...
//
//
// Logical loop heirarchy: a             , i , tau , j
// Actual  loop heirarchy: tau (external), a , i   , j
//
// */
////int CalcTermCurrent(unsigned short tau, state_t state, iterm_t *itermlist,	schemevec *current, schemevec currentacc, FILE *TermPairwiseCurrentFile) {//16-4-12
//int CalcTermCurrent(unsigned short tau, state_t state,
///*state_t stateprev,*/
//iterm_t *itermlist, schemevec *current)
////    , schemevec currentacc)
//	{ //13-8-12
//		unsigned short a, i, ai, q, j, aj; //17-4-12 Join this function with CalcTermPower
//		double r2, RanFor2, temp, gvvPower, RvPower;
//		for (a = 0; a < N_SCHEMES; a++)
//			{ //loop over potential energy localization schemes
//
//				for (i = 0; i < itermlist[tau].nr; i++)
//					for (q = 0; q < DIM; q++)
//						itermlist[tau].termCurrent[i][q][a] = 0; //J[tau][i] init.
//
//				for (i = 0; i < itermlist[tau].nr; i++)
//					{ //loop over atoms participating in interaction term tau
//
//						ai = itermlist[tau].iatoms[i]; //get atom global index
//
//						/*
//						 //DEBUG 17-4-12
//						 for (q = 0; q < DIM; q++){
//						 printf("state.t=%.2g tau=%d i=%d q=%d a=%d\n",state.t,tau,i,q,a);
//						 printf("itermlist[%d].termCurrent[%d][%d][%d]=%.2g\n",tau,i,q,a,itermlist[tau].termCurrent[i][q][a]);
//						 }
//						 */
//
//						if (1 == itermlist[tau].nr)
//							{
//								//Heat current to and from thermal reservoirs
//								//
//								// P_{i<--Bath} = (Sum q) R[i][q]*v[i][q]				= gamma[i]*KB*T_Bath[i]
//								// P_{Bath<--i} = (Sum q) v[i][q]*v[i][q]*gamma[i]	= gamma[i]*EK[i]
//								//
//								// at steady-state, the averages of these two are equal: d/dt EK[i] = -gamma[i]*EK[i] + gamma[i]*KB*T_Bath[i] + ( terms from interatomic interactions )
//								//
//								// R[ai][q] = itermlist[tau].f[0][q] such that ( 1 == itermlist[tau].nr ) and ( itermlist[tau].iatoms[0] == ai )
//
//								temp = RanFor2 = gvvPower = RvPower = 0;
//								for (q = 0; q < DIM; q++)
//									{
//										temp += pow(state.vfrog[ai][q], 2);	// |[vfrog(t)]^2|
//										RanFor2 += pow(itermlist[tau].f[0][q], 2);// |[R(t)]^2|
//										gvvPower += -itermlist[tau].vterm
//										      * state.vfrog[ai][q] * state.vfrog[ai][q]; // -gamma*v^2
//										RvPower += itermlist[tau].f[0][q]
//										      * state.vfrog[ai][q]; // R*v
//									}
//								for (q = 0; q < DIM; q++)
//									{
//										/*itermlist[tau].termCurrent[0][q][a] += (-itermlist[tau].vterm
//										 * state.vfrog[ai][q] + itermlist[tau].f[0][q])
//										 * state.vfrog[ai][q] * (state.x[ai][q] - stateprev.x[ai][q]); // itermlist[tau].vterm is used to contain the coupling strength, gamma */
//										/*
//										 P_{B<--i} = -gamma*v^2
//										 P_{i<--B} = R_i*v
//										 Delta_r_i(t) = r_i(t) - r_i(t-dt)
//										 I_{B<--i} = P_{B<--i}*Delta_r_i/|Delta_r_i|
//										 I_{i<--B} = P_{i<--B}*R_i/|R_i|
//
//										 itermlist[tau].termCurrent[0][q][a] -= itermlist[tau].vterm * state.vfrog[ai][q] * state.vfrog[ai][q] * (state.x[ai][q] - stateprev.x[ai][q]) / sqrt(r2);
//										 itermlist[tau].termCurrent[0][q][a] += itermlist[tau].f[0][q] * state.vfrog[ai][q] * itermlist[tau].f[0][q] / sqrt(RanFor2);
//										 */
//
//										itermlist[tau].termCurrent[0][q][a] +=
//										      (0 != temp) ?
//										            gvvPower * state.vfrog[ai][q]
//										                  / sqrt(temp) :
//										            0;
//										itermlist[tau].termCurrent[0][q][a] +=
//										      (0 != RanFor2) ?
//										            RvPower * itermlist[tau].f[0][q]
//										                  / sqrt(RanFor2) :
//										            0;
//									} // end "q" loop
//							} // end if (1 == itermlist[tau].nr)
//
//						else /* i.e. (1 != itermlist[tau].nr) */
//							{
//								for (j = i + 1; j < itermlist[tau].nr; j++)
//									{ //loop over all OTHER atoms participating in tau
//
//										aj = itermlist[tau].iatoms[j];
//
//										itermlist[tau].termPairwisePower[i][j][a] = 0; //P[tau][i][j] init.
//
//										for (q = 0; q < DIM; q++) /*scalar product*/
//											{
//												temp = itermlist[tau].c[j][a]
//												      * itermlist[tau].f[i][q]
//												      * state.vfrog[ai][q]
//												      - itermlist[tau].c[i][a]
//												            * itermlist[tau].f[j][q]
//												            * state.vfrog[aj][q];
//												itermlist[tau].termPairwisePower[i][j][a] +=
//												      temp; //P[tau][i][j] (pairwise power from j to i due to tau)
//												itermlist[tau].termPairwisePower[j][i][a] -=
//												      temp; //P[tau][j][i]=-P[tau][i][j] (pairwise power from i to j due to tau)
//												/*DEBUG 23-7-12*/
//												//fprintf(TermPairwiseCurrentFile,"\ntau=%d a=%d i=%d ai=%d j=%d aj=%d q=%d\titermlist[%d].termPairwisePower[%d][%d][%d]=%8.2g\n\titermlist[%d].f[%d][%d]=%8.2g\titermlist[%d].c[%d][%d]=%.2g\n\tstate.vfrog[%d][%d]=%8.2g\tstate.vfrog[%d][%d]=%8.2g\n",tau,a,i,ai,j,aj,q,tau,i,j,a,itermlist[tau].termPairwisePower[i][j][a],tau,j,q,itermlist[tau].f[j][q],tau,i,a,itermlist[tau].c[i][a],aj,q,state.vfrog[aj][q],ai,q,state.vfrog[ai][q]);
//												//printf("\ntau=%d a=%d i=%d ai=%d j=%d aj=%d q=%d\titermlist[%d].termPairwisePower[%d][%d][%d]=%8.2g\n\titermlist[%d].f[%d][%d]=%8.2g\titermlist[%d].c[%d][%d]=%.2g\n\tstate.vfrog[%d][%d]=%8.2g\tstate.vfrog[%d][%d]=%8.2g\n",tau,a,i,ai,j,aj,q,tau,i,j,a,itermlist[tau].termPairwisePower[i][j][a],tau,j,q,itermlist[tau].f[j][q],tau,i,a,itermlist[tau].c[i][a],aj,q,state.vfrog[aj][q],ai,q,state.vfrog[ai][q]);
//											} // end loop over q
//
//										//DEBUG 16-10-12
//										//printf("itermlist[%d].termPairwisePower[%d][%d][%d](%e) = %e\n",tau,i,j,a,state.t,itermlist[tau].termPairwisePower[i][j][a]);
//
//										/*
//										 for (q = 0; q < DIM; q++)
//										 {//Cartesian components of vector quantity
//										 itermlist[tau].termPairwiseCurrent[i][j][q][a]=-(state.x[ai][q]-state.x[aj][q])*itermlist[tau].termPairwisePower[i][j][q][a];//J[tau][i][j]
//										 itermlist[tau].termCurrent[i][q][a]+=itermlist[tau].termPairwiseCurrent[i][j][q][a];//J[tau][i]
//										 }//end q loop
//										 */
//
//										r2 = 0;
//										for (q = 0; q < DIM; q++)
//											r2 += pow((state.x[ai][q] - state.x[aj][q]),
//											      2);
//										if (0 == r2)
//											{
//												printf(
//												      "%d %d\n%d %d\n\t%e %e %e\n\t%e %e %e\n",
//												      i, j, ai, aj, state.x[ai][0],
//												      state.x[ai][1], state.x[ai][2],
//												      state.x[aj][0], state.x[aj][2],
//												      state.x[aj][2]);
//												fputs("Overlapping atoms!\n", stderr);
//												exit(3);
//											}
//
//										for (q = 0; q < DIM; q++)
//											{
//												temp =
//												      itermlist[tau].termPairwisePower[i][j][a]
//												            * (state.x[ai][q]
//												                  - state.x[aj][q])
//												            / sqrt(r2);
//												itermlist[tau].termCurrent[i][q][a] += temp;	//J[tau][i] (power into i due to interaction term tau)
//												itermlist[tau].termCurrent[j][q][a] += temp;	//J[tau][j]=J[tau][i]
//												///*DBG 28-1-13*/ if (0 == a){ if ((0 == stateprev.t)&&(0 == q)) printf("  tau a  i  j      P     q     temp        I[i]       I[j]\n") ; printf("%5d %1d %2d %2d %10g %1d %10g %10g %10g\n",tau,a,i,j,itermlist[tau].termPairwisePower[i][j][a],q,temp,itermlist[tau].termCurrent[i][q][a],itermlist[tau].termCurrent[j][q][a]);}
//												///*DBG 30-1-13*/ if ((0 == a)&&(0 == tau)&&(0 == ai)&&(0 == q)) printf("%6g\n",state.x[ai][q]);
//												///*DBG 30-1-13*/ if (0 == a) printf("%06g %2d %1d %2d %2d %1d %12g %12g %12g %12g %12g %12g\n",state.t,tau,a,i,j,q,itermlist[tau].termPairwisePower[i][j][a],state.x[ai][q],state.x[aj][q],(state.x[ai][q] - state.x[aj][q]),sqrt(r2),itermlist[tau].termPairwisePower[i][j][a] * (state.x[ai][q] - state.x[aj][q]) / sqrt(r2));
//											}
//										//sanity check 16-10-12
//										for (q = 0; q < DIM; q++)
//											if (isnan(itermlist[tau].termCurrent[i][q][a]))
//												{
//													printf("%d %d %d %d %d %e %e %e %e %e\n",
//													      tau, i, q, a, j, state.x[ai][q],
//													      state.x[aj][q],
//													      itermlist[tau].termPairwisePower[i][j][a],
//													      itermlist[tau].termCurrent[i][q][a],
//													      sqrt(r2));
//													fputs(
//													      "A term current is not a number!\n",
//													      stderr);
//													exit(1);
//												}
//										//current[aj][q][a] += itermlist[tau].termCurrent[j][q][a];// J[i] will come outside the loop over j
//									} //end j loop
//							} //end else ( 1 != itermlist[tau].nr ) clause
//
//						/*
//						 for (q = 0; q < DIM; q++)
//						 {
//						 //current[ai][q][a] += itermlist[tau].termCurrent[i][q][a]; //J[i] (net local energy current)
//						 //DEBUG 17-4-12 printf("current[%d][%d][%d]=%.0e itermlist[%d].termCurrent[%d][%d][%d]=%.0e\n",ai,q,a,current[ai][q][a],tau,i,q,a,itermlist[tau].termCurrent[i][q][a]);
//						 //currentacc[q][a] += 0.5 * itermlist[tau].termCurrent[i][q][a]; //J (tot. sys. energy current)
//
//						 } //end q loop
//						 */
//
//					} //end i loop
//
//				for (i = 0; i < itermlist[tau].nr; i++)
//					{
//						ai = itermlist[tau].iatoms[i];
//						for (q = 0; q < DIM; q++)
//							current[ai][q][a] += itermlist[tau].termCurrent[i][q][a];
//					}
//
//			} //end "a" loop
//
//		return 0;
//
//	}
//
////-----------------------------------------------------------------------------------------------------------
//
//int CalcTermPow(
//      unsigned short tau,
//      state_t state,
//      iterm_t *itermlist,
//      schemecoef *power)
//	{
//		unsigned short i, ai, a, q;
//
//		//calculate itermlist[tau].locpower0[i][a] & itermlist[tau].termPower0tot[i]
//		itermlist[tau].termPower0tot = 0;
//		for (i = 0; i < itermlist[tau].nr; i++)
//			{ //loop over atoms "i" participating in interaction term "tau"
//				ai = itermlist[tau].iatoms[i];
//				itermlist[tau].termPower0[i] = 0;
//				for (q = 0; q < DIM; q++)
//					{
//						//itermlist[tau].termPower0[i] += itermlist[tau].f[i][q] * state.vfrog[ai][q];//power going into atom "i" due to its interaction in the "tau" term //COMMENTED OUT DEBUG 23-9-12
//
//						//DEBUG 23-9-12
//						itermlist[tau].termPower0[i] += itermlist[tau].f[i][q]
//						      * state.vfrog[ai][q];
//
//						///*DBG 29-1-13*/ printf("\n%3d %3d %1d %12g %12g %12g",tau,ai,q,itermlist[tau].f[i][q],state.vfrog[ai][q],itermlist[tau].f[i][q]*state.vfrog[ai][q]);
//
//						if (1 == itermlist[tau].nr) itermlist[tau].termPower0[i] +=
//						      -itermlist[tau].vterm * state.vfrog[ai][q]
//						            * state.vfrog[ai][q];
//
//						///*DBG 29-1-13*/ if (1 == itermlist[tau].nr) printf(" %12g %12g",itermlist[tau].vterm , -itermlist[tau].vterm * state.vfrog[ai][q] * state.vfrog[ai][q]);
//					}
//				itermlist[tau].termPower0tot += itermlist[tau].termPower0[i]; //total power going into the potential energy of the interaction term "tau"
//			} //end loop over participating atoms "i" in term "tau"
//
//		//............................................................................................................
//
//		//calc itermlist[tau].termLocalPower[i] and add to power[j][a]
//		for (a = 0; a < N_SCHEMES; a++)
//			{ //loop over potential localization schemes
//			  //compute termLocalPower[][] using c[][a]
//				for (i = 0; i < itermlist[tau].nr; i++)
//					{ //loop over atoms "j" participating in interaction term "tau"
//						itermlist[tau].termLocalPower[i][a] =
//						      (1 == itermlist[tau].c[i][a]) ?
//						            itermlist[tau].termPower0[i] :
//						            itermlist[tau].termPower0[i]
//						                  - itermlist[tau].c[i][a]
//						                        * itermlist[tau].termPower0tot; //local power into atom "j" due to interaction term "tau"
//						ai = itermlist[tau].iatoms[i];
//						power[ai][a] += itermlist[tau].termLocalPower[i][a]; //aggregate this term's contribution to net local power into atom j
//						///*DBG 29-1-13*/ if ((0 == tau)&&(0 == a)&&(0 == i)) printf("tau a  ai  termPower      power\n"); if (0 == a) printf("%3d %1d %3d %10g %10g\n",tau,a,ai,itermlist[tau].termLocalPower[i][a],power[ai][a]);
//					}						//end "i" loop
//			} //end "a" loop (potential energy localization scheme)
//
//		return 0;
//	} //end CalcTermPow()
//
//-----------------------------------------------------------------------------------------------------------

int CoarseGrainTimeSeg(
      const state_t state,
      const state_t stateprev,
      const unsigned step,
      FILE *hrout,
      FILE *outfile,
      FILE *currentfile,
      FILE *currenthrout,
      FILE *BondPowerFile,
      schemecoef **BondPower,
      schemecoef *poweracc,
      schemevec *currentFromLeft,
      /*    schemevec currentacc*/
      schemecoef *DeltaE,
      const unsigned seg_length,
      const unsigned short natoms,
      double *kfrog,
			double *T,
			double *T2,
      schemecoef *u,
      schemecoef *E2,
      const unsigned short *bOutput,
      const unsigned short a_hr)
	{
		unsigned short i, j, a, q;
		double Deltat = state.t - stateprev.t; //, smallerof[N_SCHEMES];

		//for (a = 0; a < N_SCHEMES; a++) smallerof[a] = 0;

		//	schemecoef powertot;

		fprintf(hrout, "\n%e ", state.t);
		fprintf(currenthrout, "\n%e ", state.t);


    //*4-6-13*/printf("%.4f ",state.t);


		for (j = 0; j < natoms; j++)
			{

				//DEV NOTE: put all which follows under one "a" loop over N_SCHEMES, instead of having one loop for normalizing accumulators and then other loops for printing output...

				//normalize accumulators
				T[j] /= seg_length;
				T2[j] /= seg_length;
				for (a = 0; a < N_SCHEMES; a++)
					{
						for (i = 0; i < natoms; i++)
							BondPower[i][j][a] /= seg_length;
						poweracc[j][a] /= seg_length;
						for (q = 0; q < DIM; q++)
							{
								currentFromLeft[j][q][a] /= seg_length;
								//if (j == 0) currentacc[q][a] /= seg_length;
								///*DEBUG 19-4-12*/if (j==0) if (q==2) printf("%g\n",currentacc[q][a]);
							}
						DeltaE[j][a] /= seg_length;
						E2[j][a] /= seg_length;
						//smallerof[a] = (fabs(poweracc[j][a]) < fabs(DeltaE[j][a]/ Deltat)) ? fabs(poweracc[j][a]) : fabs( DeltaE[j][a] / Deltat);
					}

				//print output to file (block format)
				/*if (natoms == 2)// for the specific case of a diatomic system
				 {
				 if (j == 0)
				 {//since DE[1] \approx -DE[0] and P[1]=-P[0] , only j=0 is necessary
				 fprintf(
				 outfile,
				 //"\n%3d,%5.0e,%d,%e",
				 "\n%3d %e %d %e",
				 step,
				 state.t,
				 j,
				 kfrog[j]);
				 fprintf(
				 currentfile,
				 "\n%3d %e %d %e %e %e",
				 step,
				 state.t,
				 j,
				 currentacc[0][a] * J_GMX_2_J_SI,
				 currentacc[1][a] * J_GMX_2_J_SI,
				 currentacc[2][a] * J_GMX_2_J_SI);


				 for (a = 0; a < N_SCHEMES; a++){
				 fprintf(
				 outfile,
				 //",%e,%e,%e,%e",
				 " %e %e %e %e",
				 u[j][a],
				 DeltaE[j][a] * P_GMX_2_P_SI / Deltat,
				 poweracc[j][a] * P_GMX_2_P_SI,
				 (poweracc[j][a] * Deltat - DeltaE[j][a])
				 / smallerof[a]);
				 fprintf(
				 currentfile,
				 " %e %e %e",
				 current[j][0][a] * J_GMX_2_J_SI,
				 current[j][1][a] * J_GMX_2_J_SI,
				 current[j][2][a] * J_GMX_2_J_SI);
				 }
				 }
				 }
				 */
				/*else*/if (bOutput[j] == 1) //if data for atom j is to be printed out
					{
						fprintf(outfile,
						//"\n%d,%e,%d,%e",
						      "\n%d %e %d %e", step, state.t, j, kfrog[j]);
						fprintf(currentfile, "\n%3d %e %d", step, state.t, j);

						for (a = 0; a < N_SCHEMES; a++)
							{
								fprintf(outfile,
								      //",%e,%+e,%+e,%e,%e",
								      " %e %+e %+e %e %e", u[j][a], DeltaE[j][a],
								      poweracc[j][a] * Deltat,
								      (poweracc[j][a] * Deltat - DeltaE[j][a])
								            / DeltaE[j][a], T[j]);
								/*
								 fprintf(currentfile, " %e %e %e",
								 currentacc[0][a] * J_GMX_2_J_SI,
								 currentacc[1][a] * J_GMX_2_J_SI,
								 currentacc[2][a] * J_GMX_2_J_SI);
								 */
								fprintf(currentfile, " %e %e %e",
								      currentFromLeft[j][0][a] * J_GMX_2_J_SI,
								      currentFromLeft[j][1][a] * J_GMX_2_J_SI,
								      currentFromLeft[j][2][a] * J_GMX_2_J_SI);
							} //end loop over "a"

						//*4-6-13*/printf("%.0f ",sqrt(T2[j]-pow(T[j],2)));
						//*6-6-13*/printf("%.0f\t",sqrt(E2[j][a]-pow(kfrog[j]+u[j][a],2)));

					} //end if atom "j" is an output atom

				//print output to human readable output file (one line per time-step format)
				a = a_hr;
				//if (natoms == 2) fprintf(hrout, " %+e %+e %+6.1g", DeltaE[j][a] / Deltat, poweracc[j][a], (smallerof[a] == 0) ? 0 : 1 * (poweracc[j][a] - DeltaE[j][a] / Deltat) / smallerof[a]);
				/*else*/if (bOutput[j] == 1) fprintf(hrout,
				/*
				 "|%+.1e %+.1e %+6.1g%%",
				 DeltaE[j][a] / Deltat,
				 poweracc[j][a],
				 (smallerof[a] == 0) ? 0 : 100 * (poweracc[j][a]
				 - DeltaE[j][a] / Deltat) / smallerof[a]);
				 */
				"%e ", poweracc[j][a]);
				for (q = 0; q < DIM; q++)
					fprintf(currenthrout, "%e ", currentFromLeft[j][q][a]);

				//4-4-13
				for (i = 0; i < natoms; i++)
					fprintf(BondPowerFile, "%+15.7e ", BondPower[i][j][a]);
				fprintf(BondPowerFile, "\n");
				//printf("%g %d %d %d %g\n", state.t, a, j, i, BondPower[j][i][a]);//15-3-13

			} //end j loop


		//*4-6-13*/printf("\n");


		fprintf(outfile, "\n"); //iso-scan line for GNUPlot pm3d algorithm
		fprintf(currentfile, "\n");
		//fprintf(hroutfile,"\n");
		fprintf(BondPowerFile, "\n"); //BondPower 4-4-13

		//nullify segment accumulators
		for (j = 0; j < natoms; j++)
			{
				T[j]=T2[j]=0;
				for (a = 0; a < N_SCHEMES; a++)
					{
						poweracc[j][a] = 0;
						for (i = 0; i < natoms; i++)
							BondPower[i][j][a] = 0;
						for (q = 0; q < DIM; q++)
							{

								/*DEBUG*/
								/*if (j==0) printf("step=%d currentacc[%d][%d]=%0.1e \n",step,q,a,currentacc[q][a]);
								 printf("current[%d][%d][%d]=%0.1e\n",j,q,a,current[j][q][a]);*/
								//printf("step=%d current[%d][%d][%d]=%e currentacc[%d][%d]=%e \n",step,j,q,a,current[j][q][a],q,a,currentacc[q][a]);
								//if ( 0 != current[j][q][a]) printf("step=%d current[%d][%d][%d]=%e currentacc[%d][%d]=%e \n",step,j,q,a,current[j][q][a],q,a,currentacc[q][a]);
								//else{puts("\nCurrent is zero\n", stderr); exit(2);}
								currentFromLeft[j][q][a] = 0;
								//if (j == 0) currentacc[q][a] = 0;
							}
						DeltaE[j][a] = E2[j][a] = 0;
					}
			}
		return 0;
	} //end CoarseGrainTimeSeg (*if step % seg_length*)

//-----------------------------------------------------------------------------------------------------------

int SetupNextTimeStep(
      unsigned short natoms,
      state_t state,
      state_t stateprev,
      double k[],
      double kprev[],
      double kfrog[],
      double kfrogprev[],
      schemecoef *u,
      schemecoef *uprev,
      schemecoef *power
      //      ,unsigned short LastTermIndex,
      //      iterm_t *itermlist
      )
	{
		/*
		 int SetupNextTimeStep(unsigned short natoms, state_t state, state_t stateprev, double k[],
		 double kprev[], double kfrog[], double kfrogprev[], schemecoef *u,
		 schemecoef *uprev, schemecoef *power) {
		 */
		unsigned short j, a, q;

		for (j = 0; j < natoms; j++)
			{
				kprev[j] = k[j]; //kinetic energy of the next half-step becomes that of the previous half-step
				kfrogprev[j] = kfrog[j]; //(leap-frog) kinetic energy of this step becomes that of the previous step
				for (a = 0; a < N_SCHEMES; a++)
					{
						uprev[j][a] = u[j][a];
						power[j][a] = 0;
					}
				for (q = 0; q < DIM; q++)
					{

						/*DEBUG 17-4-12*/
						//if ((0 == j)&&(0 == q)) printf("\n%.2g %.2g\n",stateprev.t,state.t);
						//printf("\t%d %d\n",j,q);
						//printf("\tstateprev.x[j][q]=%.2g\n",stateprev.x[j][q]);//GDB says this line gives a SIGSEGV at the start of the third time-step (even when the simulation is invoked for more than 3 steps) 18-4-12
						/* Minimal BASH command to reproduce the error
						 clear ; cd /home/inonshar/Documents/GROMACS/ButaneDiThiol/17-4-12/ ; gcc ../../Parse/parse-17-4-12.c -g -lm -O0 -Wall -o ../../Parse/parse-17-4-12.out ; if [ 0 -eq $? ] ; then cp ../../Parse/parse-17-4-12.out . ; fi ; cd 387B772/debug/ ; cp ../../parse-17-4-12.out . ; gdb --args ./parse-17-4-12.out trajectory-1.txt forces-1.txt output1.txt 1

						 r
						 (to run until segmentation fault)
						 */

						//printf("\tstate.x[%d][%d]=%.2g\n",j,q,state.x[j][q]);
						stateprev.x[j][q] = state.x[j][q];
						stateprev.v[j][q] = state.v[j][q];
						stateprev.vfrog[j][q] = state.vfrog[j][q];
					}
			}

		return 0;
	} //end function SetupNextTimeStep

//-----------------------------------------------------------------------------------------------------------

int InitTermCurrent(unsigned short LastTermIndex, iterm_t *itermlist)
	{
		unsigned short tau, a, q, i;
		for (tau = 0; tau < LastTermIndex; tau++)
			for (a = 0; a < N_SCHEMES; a++)
				for (q = 0; q < DIM; q++)
					for (i = 0; i < itermlist[tau].nr; i++)
						itermlist[tau].termCurrent[i][q][a] = 0;
		return 0;
	}

//-----------------------------------------------------------------------------------------------------------

int FinishUp(
      unsigned step,
      unsigned seg_length,
      FILE *trajectory,
      FILE *forces,
      FILE *outfile,
      FILE *hrout,
      FILE *outatomsfile,
      FILE *atomsmassesfile,
      FILE *currentfile,
      FILE *currenthrout,
      FILE *TermPairwiseCurrentFile,
      FILE *RanForFile,
      FILE *BondPowerFile)
	{
		//int FinishUp(unsigned step, unsigned seg_length, FILE *trajectory, FILE *forces, FILE *outfile, FILE *hrout, FILE *outatomsfile, FILE *atomsmassesfile, FILE *currentfile, FILE *currenthrout) {//13-8-12

		unsigned short ExitCode = 0;

		if ((step % seg_length) != 0)
			{
				fprintf(stderr,
				      "\n\nIrregular segment length in segment:	step=%d	segment=%d seg_length=%d	steps left over=%d\n\n",
				      step, (int) (step / seg_length), seg_length,
				      seg_length - (step % seg_length));
				ExitCode = 8;
			}
		fclose(trajectory);
		fclose(forces);
		fprintf(outfile, "\n");
		fclose(outfile);

		fprintf(TermPairwiseCurrentFile, "\n");
		fclose(TermPairwiseCurrentFile); //13-8-12

		fprintf(hrout, "\n");
		fclose(hrout);
		fclose(outatomsfile);
		fclose(atomsmassesfile);
		fprintf(currentfile, "\n");
		fclose(currentfile); //Valgrind says that in this line "Syscall param write(buf) points to uninitialised byte(s)" 19-4-12
		fclose(currenthrout);
		fclose(RanForFile);
		fclose(BondPowerFile);
		//DEV NOTE: (+ leave this comment even after implementing it)
		//remember to free up all nested pointers (e.g. *f in *termlist) explicitly before freeing up the nesting data structure (*termlist)
		return ExitCode;
	} //end function FinishUp

//===============================================================================================================================================

int debug_31_5_11(
      const state_t state,
      const state_t stateprev,
      const unsigned short a_hr,
      const unsigned short natoms,
      const double *m,
      const double *kfrog,
      const double *kfrogprev,
      const iterm_t *itermlist,
      const unsigned short LastTermIndex,
      const schemecoef *u,
      const schemecoef *uprev,
      const unsigned short maxterms,
      const schemecoef *power
      //      , const schemecoef *poweracc
      )
	{

		unsigned short j, q, a, tau, i;
		double kfrog_calc[natoms], kfrogprev_calc[natoms],
		      u_calc[natoms][N_SCHEMES];
		double termPower0_calc[LastTermIndex][maxterms],
		      termPower0tot_calc[LastTermIndex];
		double powerK_calc[natoms];
		double powerU_calc[natoms][N_SCHEMES];

		/*
		 int j,q,a,tau,i;
		 double *kfrog_calc,*kfrogprev_calc;
		 schemecoef *u_calc;
		 double *termPower0_calc,*termPower0tot_calc;
		 double *powerK_calc;
		 schemecoef *powerU_calc;

		 //Dynamic Memory Allocation
		 kfrog_calc = (double *) malloc(natoms * sizeof(double));
		 if (kfrog_calc == NULL) {
		 fputs("\nCould not allocate memory for kfrog_calc\n", stderr);
		 exit(1);
		 }
		 kfrogprev_calc = (double *) malloc(natoms * sizeof(double));
		 if (kfrogprev_calc == NULL) {
		 fputs("\nCould not allocate memory for kfrogprev_calc\n", stderr);
		 exit(1);
		 }
		 u_calc = (schemecoef *) malloc(natoms * sizeof(schemecoef));
		 if (u_calc == NULL) {
		 fputs("\nCould not allocate memory for u_calc\n", stderr);
		 exit(1);
		 }
		 termPower0_calc = (double *) malloc(LastTermIndex * maxterms * sizeof(double));
		 if (termPower0_calc == NULL) {
		 fputs("\nCould not allocate memory for termPower0_calc\n", stderr);
		 exit(1);
		 }
		 termPower0tot_calc = (double *) malloc(LastTermIndex * sizeof(double));
		 if (termPower0tot_calc == NULL) {
		 fputs("\nCould not allocate memory for termPower0tot_calc\n", stderr);
		 exit(1);
		 }
		 powerK_calc = (double *) malloc(natoms * sizeof(double));
		 if (powerK_calc == NULL) {
		 fputs("\nCould not allocate memory for powerK_calc\n", stderr);
		 exit(1);
		 }
		 powerU_calc = (schemecoef *) malloc(natoms * sizeof(schemecoef));
		 if (powerU_calc == NULL) {
		 fputs("\nCould not allocate memory for powerU_calc\n", stderr);
		 exit(1);
		 }
		 */
		/*
		 schemecoef *powerU_calc;
		 powerU_calc = (schemecoef *) malloc(natoms * sizeof(schemecoef));
		 if (powerU_calc == NULL) {
		 fputs("\nCould not allocate memory for powerU_calc\n", stderr);
		 exit(1);
		 }
		 */

		//Time
		//printf("%.0e %.0e %.0e %.0e ",state.t,stateprev.t,state.t-stateprev.t,state.t/(state.t-stateprev.t));
		//printf("%d",(int)(state.t/(state.t-stateprev.t)));2-6-11
		a = a_hr;

		//Kinetic Energies
		for (j = 0; j < natoms; j++)
			{
				kfrog_calc[j] = kfrogprev_calc[j] = 0;
				for (q = 0; q < DIM; q++)
					{
						kfrog_calc[j] += 0.5 * m[j] * state.vfrog[j][q]
						      * state.vfrog[j][q];
						kfrogprev_calc[j] += 0.5 * m[j] * stateprev.vfrog[j][q]
						      * stateprev.vfrog[j][q];
					}
				//	printf("	%+.0e %+.0e",(kfrog_calc[j]-kfrog[j])/kfrog[j],(kfrog_calc[j]-kfrogprev_calc[j]-(kfrog[j]-kfrogprev[j]))/(kfrog[j]-kfrogprev[j]));
			}

		//Potential Energy Localization
		for (j = 0; j < natoms; j++)
			u_calc[j][a] = 0;
		for (tau = 0; tau < LastTermIndex; tau++)
			for (i = 0; i < itermlist[tau].nr; i++)
				u_calc[itermlist[tau].iatoms[i]][a] +=
				      (1 == itermlist[tau].nr) ?
				            0 : itermlist[tau].vterm * itermlist[tau].c[i][a];

		/*
		 for(tau=0;tau<LastTermIndex;tau++){
		 for(i=0;i<itermlist[tau].nr;i++){
		 printf(" tau=%d i=%d j=%d",tau,i,itermlist[tau].iatoms[i]);
		 printf("	%+.1e",itermlist[tau].c[i][a]);
		 }
		 printf("	%+.1e",itermlist[tau].vterm);
		 }
		 */

		//for(j=0;j<natoms;j++) printf("	%+.0e %+.0e",u_calc[j][a],u[j][a]);
		//for(j=0;j<natoms;j++) printf("	%+.0e",(u_calc[j][a]-u[j][a])/u[j][a]);
		//Calculate Term Power
		for (tau = 0; tau < LastTermIndex; tau++)
			{
				termPower0tot_calc[tau] = 0;
				for (i = 0; i < itermlist[tau].nr; i++)
					{
						termPower0_calc[tau][i] = 0;
						for (q = 0; q < DIM; q++)
							termPower0_calc[tau][i] += itermlist[tau].f[i][q]
							      * state.vfrog[itermlist[tau].iatoms[i]][q];
						//		for(q=0;q<DIM;q++) printf("	%d %d %d %d f=%+.0e v=%+.0e  p0_calc=%+.0e\n",tau,i,itermlist[tau].iatoms[i],q,itermlist[tau].f[i][q],state.vfrog[itermlist[tau].iatoms[i]][q],termPower0_calc[tau][i]);printf("					p0=%+.0e	i.e. %+.3g%%\n\n",itermlist[tau].termPower0[i],100*(termPower0_calc[tau][i]-itermlist[tau].termPower0[i])/itermlist[tau].termPower0[i]);
						//		printf("	%d %d %+.0e %+.0e",tau,i,termPower0_calc[tau][i],itermlist[tau].termPower0[i]);//DEBUG
						//		printf("	%+.0e",(termPower0_calc[tau][i]-itermlist[tau].termPower0[i])/itermlist[tau].termPower0[i]);//DEBUG
						termPower0tot_calc[tau] += termPower0_calc[tau][i];
					}
				//	printf("	%+.0e",(termPower0tot_calc[tau]-itermlist[tau].termPower0tot)/itermlist[tau].termPower0tot);//DEBUG
			}

		//Kinetic Energy Continuity: DeltaK = F_j * v_j
		for (j = 0; j < natoms; j++)
			powerK_calc[j] = 0;
		for (tau = 0; tau < LastTermIndex; tau++)
			for (i = 0; i < itermlist[tau].nr; i++)
				powerK_calc[itermlist[tau].iatoms[i]] += termPower0_calc[tau][i];
		//for(j=0;j<natoms;j++) printf("	%+.0e %+.0e %+5.3g%%",powerK_calc[j],(kfrog[j]-kfrogprev[j])/(state.t-stateprev.t),100*(powerK_calc[j]-(kfrog[j]-kfrogprev[j])/(state.t-stateprev.t))/((kfrog[j]-kfrogprev[j])/(state.t-stateprev.t)));

		//Potential Energy Continuity: DeltaV_tau,j = C_tau,j * Sum{ (F_tau,i * v_i) , i}
		for (j = 0; j < natoms; j++)
			powerU_calc[j][a] = 0;
		for (tau = 0; tau < LastTermIndex; tau++)
			for (i = 0; i < itermlist[tau].nr; i++)
				powerU_calc[itermlist[tau].iatoms[i]][a] -= itermlist[tau].c[i][a]
				      * termPower0tot_calc[tau];
		//for(j=0;j<natoms;j++) printf("	%+.0e %+.0e %+5.3g%%",powerU_calc[j][a],(u[j][a]-uprev[j][a])/(state.t-stateprev.t),100*(powerU_calc[j][a]-(u[j][a]-uprev[j][a])/(state.t-stateprev.t))/((u[j][a]-uprev[j][a])/(state.t-stateprev.t)));
		//for(j=0;j<natoms;j++) printf("	%+.0e %+.0e %+5.3g%%",powerU_calc[j][a],(u[j][a]-uprev[j][a])/(state.t-stateprev.t),100*(powerU_calc[j][a]-(u[j][a]-uprev[j][a])/(state.t-stateprev.t))/((u[j][a]-uprev[j][a])/(state.t-stateprev.t)));

		//Total Local Power

		//for(j=0;j<natoms;j++) printf("  %+.0e",powerU_calc[j][a]);

		//for(j=0;j<natoms;j++) printf("	%+.0e %+.0e %+.0e %+.0e %+.0e",powerK_calc[j],powerU_calc[j][a],powerK_calc[j]+powerU_calc[j][a],power[j][a],power[j][a]-(powerK_calc[j]+powerU_calc[j][a]));
		//for(j=0;j<natoms;j++) printf("	%+.3g%%",100*(powerK_calc[j]+powerU_calc[j][a]-power[j][a])/((0==power[j][a]) ? 1 : power[j][a]));

		/*
		 if(0!=stateprev.t) for(a=0;a<N_SCHEMES;a++) for (j=0;j<natoms;j++) if(fabs(powerK_calc[j]+powerU_calc[j][a])<pow(2,-51)*fabs((fabs(powerK_calc[j])>fabs(powerU_calc[j][a]) ? powerK_calc[j] : powerU_calc[j][a]))){
		 fprintf(stderr,"\nAttention! Components of local power calculation errors may be due to machine double-precision:\nlocalization scheme %d , atom %d , power from K = %.0e , power from U = %.0e , sum = %.0e , machine epsilon = %.3e\n\n",a,j,powerK_calc[j],powerU_calc[j][a],powerK_calc[j]+powerU_calc[j][a],pow(2,-52));
		 exit(1000);
		 }
		 */
		/*
		 if(0!=stateprev.t) for(a=0;a<N_SCHEMES;a++) for (j=0;j<natoms;j++) if(fabs(power[j][a]-(powerK_calc[j]+powerU_calc[j][a]))<pow(2,-51)*fabs((fabs(power[j][a])>fabs(powerK_calc[j]+powerU_calc[j][a]) ? power[j][a] : (powerK_calc[j]+powerU_calc[j][a])))){
		 fprintf(stderr,"\nAttention! Local power calculation errors may be due to machine double-precision:\nlocalization scheme %d , atom %d , power = %.0e , calculated power = %.0e , difference = %.0e , machine epsilon = %.3e\n\n",a,j,power[j][a],powerK_calc[j]+powerU_calc[j][a],power[j][a]-(powerK_calc[j]+powerU_calc[j][a]),pow(2,-52));
		 exit(1000);
		 }
		 */
		/*
		 if(0!=stateprev.t) for(a=0;a<N_SCHEMES;a++) for (j=0;j<natoms;j++) if(fabs(power[j][a]-(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a]))<pow(2,-51)*fabs((fabs(power[j][a])>fabs(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a]) ? power[j][a] : (kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])))){
		 fprintf(stderr,"\nAttention! Energy continuity errors may be due to machine double-precision:\nlocalization scheme %d , atom %d , power = %.0e , local energy change in time-step = %.0e , difference = %.0e , machine epsilon = %.3e\n\n",a,j,power[j][a],kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a],power[j][a]-(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a]),pow(2,-52));
		 exit(1000);
		 }
		 */
		//for(j=0;j<natoms;j++) printf("  %+.0e",powerU_calc[j][a]);
		//for(j=0;j<natoms;j++) printf("   %+.0e %+.0e %+.0e %+.0e %+.0e",powerK_calc[j],powerU_calc[j][a],powerK_calc[j]+powerU_calc[j][a],(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t),(powerK_calc[j]+powerU_calc[j][a]-(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t))/((fabs((kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t))<pow(2,-51)) ? 1 : (kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t)));
		//for(j=0;j<natoms;j++) printf("	%d %d	%+.0e %+.0e %+.3g%%	%+.0e %+.0e %+.3g%%	%+.0e %+.0e %+.3g%%\n",a,j,powerK_calc[j],(kfrog[j]-kfrogprev[j])/(state.t-stateprev.t),100*(powerK_calc[j]-(kfrog[j]-kfrogprev[j])/(state.t-stateprev.t))/((kfrog[j]-kfrogprev[j])/(state.t-stateprev.t)),powerU_calc[j][a],(u[j][a]-uprev[j][a])/(state.t-stateprev.t),100*(powerU_calc[j][a]-(u[j][a]-uprev[j][a])/(state.t-stateprev.t))/((u[j][a]-uprev[j][a])/(state.t-stateprev.t)),powerK_calc[j]+powerU_calc[j][a],(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t),100*(powerK_calc[j]+powerU_calc[j][a]-(kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t))/((kfrog[j]+u[j][a]-kfrogprev[j]-uprev[j][a])/(state.t-stateprev.t)));//2-6-11
		//for(j=0;j<natoms;j++) printf("  %+.0e %+.0e %+.0e %+.0e %+.0e",powerK_calc[j],powerU_calc[j][a],powerK_calc[j]+powerU_calc[j][a],poweracc[j][a],(powerK_calc[j]+powerU_calc[j][a]-poweracc[j][a])/((fabs(poweracc[j][a])<pow(2,-51)) ? 1 : poweracc[j][a]));
		//for(j=0;j<natoms;j++) printf("	%+.0e",(powerK_calc[j]+powerU_calc[j][a]-poweracc[j][a])/poweracc[j][a]);
		if (0 != stateprev.t) for (a = 0; a < N_SCHEMES; a++)
			for (j = 0; j < natoms; j++)
				if (fabs(
				      power[j][a]
				            - (kfrog[j] + u[j][a] - kfrogprev[j] - uprev[j][a]))
				      < pow(2, -51)
				            * fabs(
				                  (fabs(power[j][a])
				                        > fabs(
				                              kfrog[j] + u[j][a] - kfrogprev[j]
				                                    - uprev[j][a]) ?
				                        power[j][a] :
				                        (kfrog[j] + u[j][a] - kfrogprev[j]
				                              - uprev[j][a]))))
					{
						fprintf(stderr,
						      "\nAttention! Energy continuity errors may be due to machine double-precision:\nlocalization scheme %d , atom %d , power = %.0e , local energy change in time-step = %.0e , difference = %.0e , machine epsilon = %.3e\n\n",
						      a, j, power[j][a],
						      kfrog[j] + u[j][a] - kfrogprev[j] - uprev[j][a],
						      power[j][a]
						            - (kfrog[j] + u[j][a] - kfrogprev[j] - uprev[j][a]),
						      pow(2, -52));
						exit(1000);
					}

		//free(powerU_calc);

		printf("\n"); //2-6-11
		return 0;
	} //end function debug_31_5_11

int debug_25_7_12(
      const unsigned short tau,
      const unsigned short a_hr,
      const state_t state,
      const iterm_t *itermlist,
      /*		schemecoef *power,*/
      /*    schemevec *current,*/
      /*    schemevec currentacc,*/
      FILE *TermPairwiseCurrentFile)
	{

		unsigned short a, i, ai = -1, q; //j, aj;

		//fprintf(TermPairwiseCurrentFile,"\ntau=%d a=%d i=%d ai=%d j=%d aj=%d q=%d\titermlist[%d].termPairwisePower[%d][%d][%d]=%8.2g\n\titermlist[%d].f[%d][%d]=%8.2g\titermlist[%d].c[%d][%d]=%.2g\n\tstate.vfrog[%d][%d]=%8.2g\tstate.vfrog[%d][%d]=%8.2g\n",tau,a,i,ai,j,aj,q,tau,i,j,a,itermlist[tau].termPairwisePower[i][j][a],tau,j,q,itermlist[tau].f[j][q],tau,i,a,itermlist[tau].c[i][a],aj,q,state.vfrog[aj][q],ai,q,state.vfrog[ai][q]);

		for (a = 0; a < N_SCHEMES; a++)
			{
				if (a_hr == a)
					{
						fprintf(TermPairwiseCurrentFile, "\n%013.4e %d ", state.t,
						      tau);
						for (i = 0; i < itermlist[tau].nr; i++)
							{
								ai = itermlist[tau].iatoms[i];
								fprintf(TermPairwiseCurrentFile, "%d ", ai);
								//for (q = 0; q < DIM; q++) fprintf(TermPairwiseCurrentFile,"%.16g ", state.x[ai][q]);
								//for (q = 0; q < DIM; q++) fprintf(TermPairwiseCurrentFile,"%.16g ", state.vfrog[ai][q]);
								fprintf(TermPairwiseCurrentFile, "%g %g ",
								      itermlist[tau].c[i][a],
								      itermlist[tau].termLocalPower[i][a]); //,power[ai][a]);
								for (q = 0; q < DIM; q++)
									{
										//fprintf(TermPairwiseCurrentFile, "%g ", currentacc[q][a]);
										//for (i = 0; i < (itermlist[tau].nr - 1); i++)
										//{
										//ai = itermlist[tau].iatoms[i];
										//fprintf(TermPairwiseCurrentFile,"%d %d %g %g %g %g ",i,ai,current[ai][q][a], itermlist[tau].termCurrent[i][q][a], itermlist[tau].c[i][a],state.vfrog[ai][q]);
										/*fprintf(TermPairwiseCurrentFile, "%d %g %g %g ", ai,
										 current[ai][q][a], itermlist[tau].c[i][a],
										 itermlist[tau].termCurrent[i][q][a]);*/
										fprintf(TermPairwiseCurrentFile, "%g ",
										      itermlist[tau].termCurrent[i][q][a]);
										/*
										 for (j = i + 1; j < itermlist[tau].nr; j++)
										 {
										 aj = itermlist[tau].iatoms[j];
										 //fprintf(TermPairwiseCurrentFile,"%d %d %g %g %g ",j,aj,state.vfrog[aj][q],itermlist[tau].f[j][q],itermlist[tau].termPairwisePower[i][j][a]);
										 //fprintf(TermPairwiseCurrentFile,"%d %d %g %g ",j,aj,state.vfrog[aj][q],itermlist[tau].termPairwisePower[i][j][a]);
										 //fprintf(TermPairwiseCurrentFile,"%d %d %g ",j,aj,itermlist[tau].termPairwisePower[i][j][a]);
										 } //for j
										 */
										//} //for i
									} //for q
							} //for i
					} //if a_hr == a
			} //for a
		//fprintf(TermPairwiseCurrentFile, "\n");
		return 0;
	}

int debug_15_3_13(
      const unsigned short tau,
      const unsigned short a_hr,
      const unsigned step,
      const state_t state,
      const iterm_t *itermlist,
      schemecoef **BondPower,
      schemecoef *power,
      schemecoef *poweracc,
      schemevec *currentFromLeft,
      FILE *TermPairwiseCurrentFile)
	{

		unsigned short a, i, j, ai, aj, q;

		/* for (time), for (tau) are external to this function */
		/* step t tau nr i j x[i] x[j] x[i]-x[j] |x[i]-x[j]| v[i] v[j] v[i]-v[j] |v[i]-v[j]| c[i] c[j] f[tau][i] f[tau][j] \
				 P[tau][i][j] I[tau][i][j] I[tau][i] I[tau][j] P[i][j] I[i][j] P[i] Pacc[i] I[i] ->I[i] */

		//if ((0==step) && (0==tau)) fprintf(TermPairwiseCurrentFile,"#step t tau nr i j x[i] x[j] x[i]-x[j] |x[i]-x[j]| v[i] v[j] v[i]-v[j] |v[i]-v[j]| c[i] c[j] f[tau][i] f[tau][j] P[tau][i][j] I[tau][i][j] I[tau][i] I[tau][j] P[i][j] I[i][j] P[i] Pacc[i] I[i] ->I[i]\n");
		if ((0 == step) && (0 == tau)) fprintf(TermPairwiseCurrentFile,
		      "#step t tau nr ai aj x[i][0] x[i][1] x[i][2] x[j][0] x[j][1] x[j][2] x[i][0]-x[j][0] x[i][1]-x[j][1] x[i][2]-x[j][2] |x[i]-x[j]| v[i][0] v[i][1] v[i][2] v[j][0] v[j][1] v[j][2] v[i][0]-v[j][0] v[i][1]-v[j][1] v[i][2]-v[j][2] |v[i]-v[j]| c[i] c[j] f[tau][i][0] f[tau][i][1] f[tau][i][2] f[tau][j][0] f[tau][j][1] f[tau][j][2] P[tau][i][j] P[tau][j][i] P[tau][i] I[tau][i][0] I[tau][i][1] I[tau][i][2] P[tau][j] I[tau][j][0] I[tau][j][1] I[tau][j][2] P[i][j] P[j][i] P[i] Pacc[i] ->I[i][0] ->I[i][1] ->I[i][2] \n");

		a = a_hr;

		///*DBG 15-3-13*/printf("%d %d %d\n",step,tau,itermlist[tau].nr);

		for (i = 0; i < itermlist[tau].nr; i++)

			{

				ai = itermlist[tau].iatoms[i];

				if (1 != itermlist[tau].nr)

					{

						for (j = 0; j < i; j++)

							{

								if ((j > 0) || (i > 1)) fprintf(TermPairwiseCurrentFile,
								      "\n");

								aj = itermlist[tau].iatoms[j];

								fprintf(TermPairwiseCurrentFile, "%d %e %d %d %d %d ",
								      step, state.t, tau, itermlist[tau].nr, ai, aj);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.x[ai][q]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.x[aj][q]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.x[ai][q] - state.x[aj][q]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      sqrt(
								            pow(state.x[ai][0] - state.x[aj][0], 2)
								                  + pow(state.x[ai][1] - state.x[aj][1],
								                        2)
								                  + pow(state.x[ai][2] - state.x[aj][2],
								                        2)));
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.vfrog[ai][q]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.vfrog[aj][q]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      state.vfrog[ai][q] - state.vfrog[aj][q]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      sqrt(
								            pow(state.vfrog[ai][0] - state.vfrog[aj][0],
								                  2)
								                  + pow(
								                        state.vfrog[ai][1]
								                              - state.vfrog[aj][1], 2)
								                  + pow(
								                        state.vfrog[ai][2]
								                              - state.vfrog[aj][2],
								                        2)));
								fprintf(TermPairwiseCurrentFile, "%e %e ",
								      itermlist[tau].c[i][a], itermlist[tau].c[j][a]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      itermlist[tau].f[i][q]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      itermlist[tau].f[j][q]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      itermlist[tau].termPairwisePower[i][j][a]);
            fprintf(TermPairwiseCurrentFile, "%e ",
                  itermlist[tau].termPairwisePower[j][i][a]);
								//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", itermlist[tau].termPairwiseCurrent[i][j][q][a]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      itermlist[tau].termLocalPower[i][a]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      itermlist[tau].termCurrent[i][q][a]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      itermlist[tau].termLocalPower[j][a]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      itermlist[tau].termCurrent[j][q][a]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      BondPower[ai][aj][a]);
                fprintf(TermPairwiseCurrentFile, "%e ",
                      BondPower[aj][ai][a]);
								//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", BondCurrent[i][j][q][a]);
								fprintf(TermPairwiseCurrentFile, "%e ", power[ai][a]);
								fprintf(TermPairwiseCurrentFile, "%e ",
								      poweracc[ai][a]);
								//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", current[i][q][a]);
								for (q = 0; q < DIM; q++)
									fprintf(TermPairwiseCurrentFile, "%e ",
									      currentFromLeft[ai][q][a]);

							}		// end loop over j

					}		// end if (1 != itermlist[tau].nr)

				else		//(1 == itermlist[tau].nr)
					{
						fprintf(TermPairwiseCurrentFile, "%d %e %d %d %d %d ", step,
						      state.t, tau, itermlist[tau].nr, ai, ai);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ", state.x[ai][q]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ", state.x[ai][q]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ", 0.);
						//fprintf(TermPairwiseCurrentFile,"%e ", sqrt( pow(state.x[ai][0],2) + pow(state.x[ai][1],2) + pow(state.x[ai][2],2) ) );
						fprintf(TermPairwiseCurrentFile, "%e ", 0.);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      state.vfrog[ai][q]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      state.vfrog[ai][q]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ", 0.);
						//fprintf(TermPairwiseCurrentFile,"%e ", sqrt( pow(state.vfrog[ai][0],2) + pow(state.vfrog[ai][1],2) + pow(state.vfrog[ai][2],2) ) );
						fprintf(TermPairwiseCurrentFile, "%e ", 0.);
						fprintf(TermPairwiseCurrentFile, "%e %e ",
						      itermlist[tau].c[i][a], itermlist[tau].c[i][a]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      itermlist[tau].f[i][q]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      itermlist[tau].f[i][q]);
						fprintf(TermPairwiseCurrentFile, "%e ",
						      itermlist[tau].termPairwisePower[i][i][a]);
            fprintf(TermPairwiseCurrentFile, "%e ",
                  itermlist[tau].termPairwisePower[i][i][a]);
						//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", itermlist[tau].termPairwiseCurrent[i][i][q][a]);
						fprintf(TermPairwiseCurrentFile, "%e ",
						      itermlist[tau].termLocalPower[i][a]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      itermlist[tau].termCurrent[i][q][a]);
						fprintf(TermPairwiseCurrentFile, "%e ",
						      itermlist[tau].termLocalPower[i][a]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      itermlist[tau].termCurrent[i][q][a]);
						fprintf(TermPairwiseCurrentFile, "%e ", BondPower[ai][ai][a]);
            fprintf(TermPairwiseCurrentFile, "%e ", BondPower[ai][ai][a]);
						//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", BondCurrent[i][i][q][a]);
						fprintf(TermPairwiseCurrentFile, "%e ", power[ai][a]);
						fprintf(TermPairwiseCurrentFile, "%e ", poweracc[ai][a]);
						//for (q=0;q<DIM;q++) fprintf(TermPairwiseCurrentFile,"%e ", current[i][q][a]);
						for (q = 0; q < DIM; q++)
							fprintf(TermPairwiseCurrentFile, "%e ",
							      currentFromLeft[ai][q][a]);

					}		// end else (1 == itermlist[tau].nr)

			}		//end loop over i

		fprintf(TermPairwiseCurrentFile, "\n");
		return 0;
	}
