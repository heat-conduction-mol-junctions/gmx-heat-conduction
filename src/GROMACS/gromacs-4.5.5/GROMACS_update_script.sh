#! /bin/bash
# (make script does not work if the source directory path has spaces in it!)
# create "if" line right at the beginning which checks for this and exits with error if finds one
#sudo -n #execute the "sudo" command non-interactively : if the sudo password has already been input, the script should continue, otherwise it will exit, thus prompting the user to manually execute "sudo" and enter the password
#if [ $? -ne 0 ] ; then echo "bad sudo password" 			; exit 2 ; fi
sudo make
if [ $? -ne 0 ] ; then echo "error running make" 			; exit 1 ; fi
sudo make check
if [ $? -ne 0 ] ; then echo "error running make check" 			; exit 1 ; fi
sudo make install
if [ $? -ne 0 ] ; then echo "error running make install" 		; exit 1 ; fi
sudo make links
if [ $? -ne 0 ] ; then echo "error running make links" 			; exit 1 ; fi
sudo make tests
if [ $? -ne 0 ] ; then echo "error running make tests" 			; exit 1 ; fi
echo -e "\nNormal termination of GROMACS updating script.\n"
