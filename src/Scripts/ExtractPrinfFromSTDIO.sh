#! /bin/bash

StartLine=$[ $(grep "Running parser & heat current calculation..." $1 -n | cut -d ":" -f1) + 1 ] # One empty line between "Running parser..." and first printf line
EndLine=$[ $(grep "#Normal termination of parse-\*.c" $1 -n | cut -d ":" -f1) - 5 ] # Five empty lines between last printf line and "#Normal termination..."

head $1 -n $EndLine | tail -n $[ $EndLine - $StartLine]
