#! /bin/bash -e
echo $(pwd) $0
echo
echo "BondPowers*.txt is transposed relative to the matrix in Labbook-*.lyx"
echo "Note: This script uses a naive formula for the calculation of variances. When the standard deviation is significantly smaller than the root mean square value, this formula may lead to a numerical instability in the form of catastrophic cancellation, rendering the result devoid of significance."
echo

#if [ -f PairwisePowers.txt ] ; then echo "PairwisePowers.txt exits. Aborting." ; exit 11 ; else touch PairwisePowers.txt ; fi
if [ -f LowerTriangleRowsFirst.txt ] ; then echo "LowerTriangleRowsFirst.txt exits. Aborting." ; exit 11 ; else touch LowerTriangleRowsFirst.txt ; fi
if [ ! -f BondPower.txt ] ; then echo "BondPower.txt does not exist. Aborting." ; exit 12 ; fi

natoms=$(head -n2 conf-*.gro | tail -n1)
echo "natoms=$natoms"

LeftSulfurAtom=$[ $(grep S conf-*.gro -n | cut -d ":" -f1 | tail -n2 | head -n1) - 2 ]
if [ 0 -ge $LeftSulfurAtom ]
	then
	LeftSulfurAtom=$[ $(grep C conf-*.gro -n | cut -d ":" -f1 | head -n1) - 2 ]
	RightSulfurAtom=$[ $(grep C conf-*.gro -n | cut -d ":" -f1 | tail -n1) - 2 ]
else
	RightSulfurAtom=$[ $(grep S conf-*.gro -n | cut -d ":" -f1 | tail -n1) - 2 ]
fi
echo "LeftSulfurAtom=$LeftSulfurAtom"
echo "RightSulfurAtom=$RightSulfurAtom"

for i in $(seq 1 $natoms)
	do
	outputatoms=( "${outputatoms[@]}" "$i")
done
echo "\${#outputatoms[@]}=${#outputatoms[@]}"
echo "\${outputatoms[@]}=${outputatoms[@]}"

SulfurAtoms=( "$LeftSulfurAtom" "$RightSulfurAtom" )
echo "\${#SulfurAtoms[@]}=${#SulfurAtoms[@]}"
echo "\${SulfurAtoms[@]}=${SulfurAtoms[@]}"

LeftBathPowerIndex=-1 # zero-based indexing in BASH arrays
for j in $(seq 1 $LeftSulfurAtom)
	do
	for k in $(seq 1 $j)
		do
		LeftBathPowerIndex=$[ 1 + $LeftBathPowerIndex]
	done
done
echo "\$LeftBathPowerIndex=$LeftBathPowerIndex"

RightBathPowerIndex=-1
for j in $(seq 1 $RightSulfurAtom)
  do
  for k in $(seq 1 $j)
    do
    RightBathPowerIndex=$[ 1 + $RightBathPowerIndex]
  done
done
echo "\$RightBathPowerIndex=$RightBathPowerIndex"



echo "======================================="


sumP=0
sumSqP=0
sumIinterfacial=0
sumSqIinterfacial=0
sumIspacial=0
sumSqIspacial=0

n=0
for i in BondPower-*.txt
  do

	n=$[1+$n]

  ni=$(echo $i | sed -e s/[^0-9]//g)
  echo $ni
  echo -n "$n $ni " >> LowerTriangleRowsFirst.txt


  PairwisePowerskj=()
  sumPairwisePowerFromRight=0

	P=0
	sdP=0
	Iinterfacial=0
	sdIinterfacial=0
	Ispacial=0
	sdIspacial=0


	for j in $(seq 1 $natoms)
		# j is the atom from which energy is transfered
		do
		for k in $(seq 1 $j)
			# k is the atom to which energy is transfered
			do
			PairwisePowerFromRightkj=$(head -n $j $i | tail -n1 | cut -d " " -f $[ 2 * $k ])
      PairwisePowerskj=( "${PairwisePowerskj[@]}" "$PairwisePowerFromRightkj" )
			sumPairwisePowerFromRight=$(echo $sumPairwisePowerFromRight $PairwisePowerFromRightkj | awk '{print ($1 + $2)}')
			echo -n "$PairwisePowerFromRightkj " >> LowerTriangleRowsFirst.txt
		done # k
		jMin1=$[ $j - 1 ]
		atomT=$(head -n $[ 1 + $j ] output-$ni.txt | tail -1 | cut -d " " -f9)
		sumAtomT[$jMin1]=$(echo ${sumAtomT[$jMin1]} $atomT | awk '{print($1 + $2)}')
		sumSqAtomT[$jMin1]=$(echo ${sumSqAtomT[$jMin1]} $atomT | awk '{print($1 + $2 * $2)}')
		#echo "DBG (12-10-13): $ni $jMin1 $atomT ${sumAtomT[$jMin1]} ${sumSqAtomT[$jMin1]}"
  done # j

	#echo "DBG ${PairwisePowerskj[@]}"
	#echo "DBG ${PairwisePowerskj[$LeftBathPowerIndex]}"

  #P=$[ ${PairwisePowerskj[$LeftSulfurAtom]} + ${PairwisePowerskj[$RightSulfurAtom]} ]
	P=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} ${PairwisePowerskj[$RightBathPowerIndex]} | awk '{print ($1 + $2)}')
	#sumP=$[ $P + $sumP ]
	sumP=$(echo $P $sumP | awk '{print ($1 + $2)}')
	sumSqP=$(echo $P $sumSqP | awk '{print ($1 * $1 + $2)}')
	sdP=$(echo $sumP $sumSqP $n | awk '{print (sqrt($2/$3 - ($1/$3)^2))}')

	#echo "DBG $n $ni $P $sumP $sumSqP $sdP"

	#Iinterfacial=$[ ( ${PairwisePowerskj[$LeftSulfurAtom]} - ${PairwisePowerskj[$RightSulfurAtom]} ) / 2 ]
	Iinterfacial=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} ${PairwisePowerskj[$RightBathPowerIndex]} | awk '{print (($1 - $2 )/2)}')
	#sumIinterfacial=$[ $Iinterfacial + $sumIinterfacial ]
	sumIinterfacial=$(echo $Iinterfacial $sumIinterfacial | awk '{print ($1 + $2)}')
  sumSqIinterfacial=$(echo $Iinterfacial $sumSqIinterfacial | awk '{print ($1 * $1 + $2)}')
  sdIinterfacial=$(echo $sumIinterfacial $sumSqIinterfacial $n | awk '{print (sqrt($2/$3 - ($1/$3)^2))}')

	#Ispacial=$[ 2 * ${PairwisePowerskj[$LeftSulfurAtom]} - $sumPairwisePowerFromRight ]
	Ispacial=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} $sumPairwisePowerFromRight $natoms | awk '{print((2 * $1 - $2)/(1 + $3))}')
	#sumIspacial=$[ $Ispacial + $sumIspacial ]
	sumIspacial=$(echo $Ispacial $sumIspacial | awk '{print ($1 + $2)}')
  sumSqIspacial=$(echo $Ispacial $sumSqIspacial | awk '{print ($1 * $1 + $2)}')
  sdIspacial=$(echo $sumIspacial $sumSqIspacial $n | awk '{print (sqrt($2/$3 - ($1/$3)^2))}')


	#DEV: Add sum, sumSq, and sd array variables for PairwisePowerskj in order to calculate sd of P_j->k for all j,k

	echo -n "$P $sdP $Iinterfacial $sdIinterfacial $Ispacial $sdIspacial" >> LowerTriangleRowsFirst.txt
	echo >> LowerTriangleRowsFirst.txt

done # i

echo "======================================="

echo >> LowerTriangleRowsFirst.txt
echo -n "$n avg " >> LowerTriangleRowsFirst.txt

echo "Getting averages row from BondPower.txt"

i="BondPower.txt"
	for j in $(seq 1 $natoms)
    # j is the atom from which energy is transfered
    do
		if [ 2 -eq $natoms ] && [ 2 -eq $j ] ; then echo "Special case for BondPower.txt when natoms==2 (16-10-13)." ; j=$[ 1 + $j ] ; fi
    for k in $(seq 1 $j)
      # k is the atom to which energy is transfered
      do
      PairwisePowerFromRightkj=$(head -n $j $i | tail -n1 | cut -d " " -f $k)
      PairwisePowerskj=( "${PairwisePowerskj[@]}" "$PairwisePowerFromRightkj" )
      sumPairwisePowerFromRight=$(echo $sumPairwisePowerFromRight $PairwisePowerFromRightkj | awk '{print ($1 + $2)}')
      echo -n "$PairwisePowerFromRightkj " >> LowerTriangleRowsFirst.txt
    done # k
  done # j

  P=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} ${PairwisePowerskj[$RightBathPowerIndex]} | awk '{print ($1 + $2)}')
  Iinterfacial=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} ${PairwisePowerskj[$RightBathPowerIndex]} | awk '{print (($1 - $2 )/2)}')
  Ispacial=$(echo ${PairwisePowerskj[$LeftBathPowerIndex]} $sumPairwisePowerFromRight $natoms | awk '{print((2 * $1 - $2)/(1 + $3))}')

  echo -n "$P $sdP $Iinterfacial $sdIinterfacial $Ispacial $sdIspacial" >> LowerTriangleRowsFirst.txt
	echo >> LowerTriangleRowsFirst.txt


	touch EnsembleAveragedTemperatures.txt
	for j in $(seq 1 $natoms)
		do
    jMin1=$[ $j - 1 ]
    #echo "$j " >> EnsembleAveragedTemperatures.txt
		#echo -n "$j " >> EnsembleAveragedTemperatures.txt
		avgAtomT=$(echo ${sumAtomT[$jMin1]} $n | awk '{print($1 / $2)}')
		sdAtomT=$(echo ${sumSqAtomT[$jMin1]} $n $avgAtomT | awk '{print(sqrt($1 / $2 - $3 * $3))}')
		#sdAtomT=$(echo ${sumSqAtomT[$jMin1]} $n ${sumAtomT[$jMin1]} | awk '{print(sqrt(($3 * $3 - $1) / $2))}')
		echo -n "$avgAtomT $sdAtomT " >> EnsembleAveragedTemperatures.txt
		echo "DBG (12-10-13): $jMin1 ${sumAtomT[$jMin1]} /$n = $avgAtomT ${sumSqAtomT[$jMin1]} / $n - $avgAtomT^2 = $sdAtomT"
	done
	echo >> EnsembleAveragedTemperatures.txt

echo "======================================="

echo "#Normal termination of Ensemble Pairwise Powers script"
exit 0
