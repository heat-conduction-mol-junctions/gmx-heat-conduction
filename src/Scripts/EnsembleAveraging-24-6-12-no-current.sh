#! /bin/bash
# This script goes over all trajectories in a GROMACS run instance, and averages over the HeatCurrent files
# then it formats the average heat current file as an XMGrace document, and fits and plots it using GNOplot
# finally, the graph (as a JPEG file) is shown using the Eye Of GNOME image viewer
echo "Setting up ensemble averaging..."

n=1
	while read GMXSimParam
	do
		#echo $GMXSimParam
		if [ $n -eq 1 ] ; then MAX_TRAJ=$GMXSimParam ; fi
                if [ $n -eq 2 ] ; then dt=$GMXSimParam ; fi
                if [ $n -eq 3 ] ; then SegLength=$GMXSimParam ; fi
                if [ $n -eq 4 ] ; then N_EXPONENTIALS=$GMXSimParam ; fi
                if [ $n -eq 5 ] ; then bDO_EOG=$GMXSimParam ; fi
                if [ $n -eq 6 ] ; then bINKSCAPE=$GMXSimParam ; fi
                if [ $n -eq 7 ] ; then bDO_VID=$GMXSimParam ; fi
                if [ $n -eq 8 ] ; then STEADY_STATEname=$GMXSimParam ; fi
		if [ $n -eq 9 ] ; then DeltaT=$GMXSimParam ; fi
		if [ $n -eq 10 ] ; then bProduction=$GMXSimParam ; fi
                if [ $n -eq 11 ] ; then INSTANCE=$GMXSimParam ; fi
		n=$[ 1 + $n ]
	done < N.dat
	echo -e "Parameters read in:\nMAX_TRAJ=$MAX_TRAJ\ndt=$dt\nSegLength=$SegLength\nN_EXPONENTIALS=$N_EXPONENTIALS\nbDO_EOG=$bDO_EOG\nbINKSCAPE=$bINKSCAPE\nbDO_VID=$bDO_VID\nSTEADY_STATEname=$STEADY_STATEname\nDeltaT=$DeltaT\nbProduction=$bProduction\nINSTANCE=$INSTANCE"
	if ( [ -z $MAX_TRAJ ] || [ -z $dt ] || [ -z $SegLength ] || [ -z $N_EXPONENTIALS ] || [ -z $bDO_EOG ] || [ -z $bINKSCAPE ] || [ -z $bDO_VID ] || [ -z $DeltaT ] || [ -z $bProduction ] || [ -z $INSTANCE ] ) ; then echo -e "\nRequired environment variable not defined (see above). Exiting.\n" ; exit 4 ; fi



#	COUNTER=1                                       ###initialize counter
#	while read line                                 ###outer loop necessary for eof test
#	do
#	#echo -e "\n$COUNTER	$line"                  ###print counter and line to stdout (-e enables \n)
#	ARRAY[$COUNTER]=$line                           ###define array, one element at a time
#	#echo "$COUNTER'	${ARRAY[$COUNTER]}"     ###print array to stdout, one element at a time
#	COUNTER=$[$COUNTER+1]                           ###advance counter
#	done < N.dat					###get input lines to be read from N.dat file

#echo;echo;echo                                  ###spacera

#N=${ARRAY[1]}
#echo "N	=	$N"
#INSTANCE=${ARRAY[2]}
#echo "INSTANCE	=	$INSTANCE"

#Reading output atoms into array (only the array size is important here)
OLDIFS=$IFS
IN="$( cat outputatoms.txt)"
set -- "$IN"
IFS=" "
declare -a outputatoms=($*)
IFS=$OLDIFS
for i in ${outputatoms[@]} ; do if [[ $i == '' ]] ; then unset ${outputatoms[$i]} ; fi ; done
echo -e "\n#{ "${outputatoms[@]} "} = " ${#outputatoms[@]}

echo -e "\nAveraging over ensemble:"

cp ../EnsembleAveraging-*.out .
        NumEnsAvgProgs=0
        for EnsAvgProgName in EnsembleAveraging-*.out ; do NumEnsAvgProgs=$[ 1 + $NumEnsAvgProgs ] ; done
        if [ 0 -eq $NumEnsAvgProgs ] ; then echo "No Ensemble Averaging Programs! Exiting..." ; exit 5 ; fi
	# ./$EnsAvgProgName
        if [ 1 -lt $NumEnsAvgProgs ] ; then echo "Multiple Ensemble Averaging Programs! Choose the one you want. Exiting..." ; exit 5 ; fi


#case "${#outputatoms[@]}" in
#	2) NumOutputFields=15;;#diatomic
#	[?]) NumOutputFields=18;;
#esac

#if [ 2 -eq ${#outputatoms[@]} ] ; then NumOutputFields=15 ; NumHROutputFields=2 ; NumCurrentOutputFields=14
#else \
NumOutputFields=18 ; NumHROutputFields=1 ; NumCurrentOutputFields=20
#fi

LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f output-$i.txt ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then 
		cp output-$i.txt EnsembleAverageHeatCurrent-$i.dat
	else 
		./$EnsAvgProgName output-$i.txt EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageHeatCurrent-$i.dat $NumOutputFields ${#outputatoms[@]}
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over output of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
        	        echo -e "./$EnsAvgProgName output-$i.txt EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageHeatCurrent-$i.dat $NumOutputFields ${#outputatoms[@]}\n"
			echo -e "Exiting...\n"
			exit 1
		fi
		rm EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat
	fi
        LastGoodTrajectory=$i
	#rm output-$i.txt
	rm -f $i-std.io
	rm -f $i-std.err

		#print to screen the number of the current trajectory which was processed (but do not print new-line character)
		echo -n "$i "
else
		echo -n " ($i NOT FOUND)  "
		NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]
fi

	#advance trajectory counter
	i=$[$i+1]
done
echo -e "\n\nEnsemble averaging over human readable files..."

#	#add XMGrace formatting information to final EnsembleAverageHeatCurrent-N.dat file
#	sed '1i\# this line is a comment line\n@    title "Ensemble Average Heat Current and Energy"\n@    xaxis  label "Time [picoseconds]"\n@    yaxis  label "Energy [kJ/mole] or Energy Current [kJ/mole ps\\S-1\\N]"\n\@TYPE xy\n@ view 0.2, 0.1, 1.25, 0.875\n@ legend on\n@ legend loctype view\n@ legend 0.85, 0.85\n@ s0 legend "Atom Index"\n@ s1 legend "Atom Energy"\n@ s2 legend "X Energy Current"\n@ s3 legend "Y Energy Current"\n@ s4 legend "Z Energy Current"' EnsembleAverageHeatCurrent-$N.dat >EnsembleAveragedHeatCurrent.xvg

#	#mv EnsembleAverageHeatCurrent-$MAX_TRAJ.dat EnsembleAveragedHeatCurrent.dat
#rm EnsembleAverageHeatCurrent-$MAX_TRAJ.dat
mv EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat output.txt

#rm EnsembleAveraging-24-2-10.out



cp ../EnsembleAveragedPower-*.out .
        NumEnsAvgedProgs=0
        for EnsAvgedProgName in EnsembleAveragedPower-*.out ; do NumEnsAvgedProgs=$[ 1 + $NumEnsAvgedProgs ] ; done
        if [ 0 -eq $NumEnsAvgedProgs ] ; then echo "No Ensemble Averaged Power Programs! Exiting..." ; exit 5 ; fi
        if [ 1 -lt $NumEnsAvgedProgs ] ; then echo "Multiple Ensemble Averaged Power Programs! Choose the one you want. Exiting..." ; exit 5 ; fi

#rename file for first trajectory as average over the first trajectory, and remove the trajectory standard output
#mv output-1.txt EnsembleAverageHeatCurrent-1.dat
#rm -f 1-std.io
#rm -f 1-std.err
LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f hroutput-$SegLength-$i.xvg ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then
               cp hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$i.dat
        else
		./$EnsAvgedProgName hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedPower-$i.dat $NumHROutputFields
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over human-readable heat current output of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
        	        echo -e "./$EnsAvgedProgName hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedPower-$i.dat $NumHROutputFields\n"
			echo -e "Exiting...\n"
			exit 1
		fi
	rm EnsembleAveragedPower-$LastGoodTrajectory.dat
	fi
        LastGoodTrajectory=$i
        #rm hroutput-$SegLength-$i.xvg

	#print to screen the number of the current trajectory which was processed (but do not print new-line character)
	echo -n "$i "

else
		echo -n " ($i NOT FOUND)  "
		NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]

fi	

#tracejob -v $JobId

#advance trajectory counter
i=$[$i+1]
done
echo -e "\n\nEnsemble averaging over energy current files..."
mv EnsembleAveragedPower-$LastGoodTrajectory.dat hroutput$SegLength.xvg
echo -e "\nFinished Averaging over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories.\n"


echo

#cp ../I-24-2-10.gp .
#/usr/local/bin/gnuplot I-24-2-10.gp &>$INSTANCE.gpl			#fit and plot using GNUplot, output to JPEG file
#mv EnsembleAveragedHeatCurrent.svg $INSTANCE.svg

#read ISSM <conductance.dat
#echo $INSTANCE $ISSM >> ../conductances.dat




#Average over current files
echo -e "\nAveraging over current files...:"
LastGoodTrajectory=0
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f current-$i.txt ] ; then
        if [ 0 -eq $LastGoodTrajectory ] ; then
                cp current-$i.txt EnsembleAverageCurrent-$i.dat
        else
                ./$EnsAvgProgName current-$i.txt EnsembleAverageCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageCurrent-$i.dat $NumCurrentOutputFields ${#outputatoms[@]}
                if [ "$?" != "0" ] ; then
                        #if the last command returned an unsuccesful exit code, exit this script with code 1
                        echo -e "\nError in Ensemble Averaging over current file of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
                        echo -e "./$EnsAvgProgName current-$i.txt EnsembleAverageCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageCurrent-$i.dat $NumCurrentOutputFields ${#outputatoms[@]}\n"
                        echo -e "Exiting...\n"
                        exit 1
                fi
                rm EnsembleAverageCurrent-$LastGoodTrajectory.dat
        fi
        LastGoodTrajectory=$i
        #rm current-$i.txt

                #print to screen the number of the current trajectory which was processed (but do not print new-line character)
                echo -n "$i "
else
                echo -n " ($i NOT FOUND)  "
                NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]
fi
        #advance trajectory counter
        i=$[$i+1]
done
echo
mv EnsembleAverageCurrent-$LastGoodTrajectory.dat current.txt
sed '/^$/d' current.txt > current-no-spaces.txt
cp ../current-*.gp .
        NumPlotScripts=0
        for PlotScriptName in current-*.gp ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
        if [ 0 -eq $NumPlotScripts ] ; then echo "No current plot scripts! Exiting..." ; exit 5 ; fi
#        if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting current from script" ; gnuplot ./$PlotScriptName ; if [ 0 -ne $? ] ; then echo "Error GNUPlotting current file. Exiting." ; exit 10 ; fi ; fi
        if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple current plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi
        rm $PlotScriptName


#echo -n "presenting plot using Eye Of GNOME"
#echo
#	#eog EnsembleAveragedHeatCurrent.jpg &	#show JPG file using Eye Of GNOME
#eog $INSTANCE.svg &


if [ 0 -eq $bProduction ] ; then
	cp ../Plot-*.sh .
	NumPlotScripts=0
	for PlotScriptName in Plot-*.sh ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
	if [ 0 -eq $NumPlotScripts ] ; then echo "No plot scripts! Exiting..." ; exit 5 ; fi
	if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting from script" ; ./$PlotScriptName ; fi
	if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi


	rm $PlotScriptName  $EnsAvgedProgName $EnsAvgProgName
else
	if [ 1 -eq $bProduction ] ; then

	cp ../PlotSt*.sh .
        NumPlotScripts=0
        for PlotScriptName in PlotSt*.sh ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
        if [ 0 -eq $NumPlotScripts ] ; then echo "No steady-state plot scripts! Exiting..." ; exit 5 ; fi
        if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting steady-state from script" ; ./$PlotScriptName ; fi
        if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple steady-state plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi


        rm $PlotScriptName  $EnsAvgedProgName $EnsAvgProgName

	fi
fi

date +%H:%M:%S.%N
echo -e "\n(Averaged over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories out of $MAX_TRAJ submitted)\n"

echo

#if all went correctly, exit with code 0
exit 0
