reset
set term jpeg enhanced size 1200,900 ; set output "13-5-13.jpeg" #set output "319605E6.jpeg" 
set key below 
set fit errorvariables 

set multiplot layout 2,3 

set style data points

set title "P_{2<-2}" 
fit A0 'BondPower.txt' u 0:2 every 8::1 via A0 
#p 'BondPower.txt' u 0:2 every 8::1, A0, A0+FIT_STDFIT lc rgb "black" lw 2, A0-FIT_STDFIT lc rgb "black" lw 2 
p 'BondPower.txt' u 0:2 every 8::1, A0 t sprintf("%g",A0), A0+FIT_STDFIT lc rgb "black" lw 2 t sprintf("%g",A0+FIT_STDFIT), A0-FIT_STDFIT lc rgb "black" lw 2 t sprintf("%g",A0-FIT_STDFIT)

set title "P_{6<-6}" 
fit A0 'BondPower.txt' u 0:6 every 8::5 via A0
p 'BondPower.txt' u 0:6 every 8::5, A0 t sprintf("%g",A0), A0+FIT_STDFIT lc rgb "black" lw 2 t sprintf("%g",A0+FIT_STDFIT), A0-FIT_STDFIT lc rgb "black" lw 2 t sprintf("%g",A0-FIT_STDFIT)

set title "Local Temperatures" 
p for[i=0:6] 'output.txt' u 2:9 every 8::i pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

set title "Local Energies" 
p for[i=0:6] 'output.txt' u 2:($4+$5) every 8::i pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

set title "Local Power" 
p for[i=0:6] 'output.txt' u 2:7 every 8::i pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

set title "Absolute Error in Local Energy Continuity" 
p for[i=0:6] 'output.txt' u 2:($6-$7) every 8::i:1 pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

unset multiplot

