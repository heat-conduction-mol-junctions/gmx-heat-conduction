	#! /bin/bash -e
	# the -e flag at the top (also "set -e") makes the script exit if any command not inside an "if" statement exits with an unsuccessfull exit code. This can be reveresd using "set +e"
	echo

  if [ "hydrogen.tau.ac.il" == $HOSTNAME ]
		then
		PREFIX="/usr/local/bin"
		SCRIPTS="$HOME/Documents/GROMACS/Scripts"
		PARSE="$HOME/Documents/GROMACS/Parse"
	else
	  PREFIX="$HOME/gmx-heat-conduction/bin/gromacs/bin"
	  SCRIPTS="$HOME/gmx-heat-conduction/src/Scripts"
	  PARSE="$HOME/gmx-heat-conduction/bin/Parse"
	fi

	#part 1: define some variables

	btraj0=0

	# When running on Hydrogen:
	if [ ! -f topol.top ] ; then
		if [ ! -f topol.itp ] ; then
			echo "Missing GROMACS topology file (topol.*p). Exiting."
			exit 1
		fi
	fi
	if [ ! -f conf.gro ] ; then echo "Missing GROMACS configuration file (conf.gro). Exiting." ; exit 1 ; fi
	# Make sure the topol.itp directs to included topology files using the full path (/export/bio/gromacs40_d/share/gromacs/top/)
	# Recompile parse-*.out on Hydrogen, since the C libraries are not necessarily of the same version as on Sodium
	# Inkscape is not installed on Hydgrogen, and therefore ImageMagick convert is used instead:
	bINKSCAPE=0 ; inkscape -V ; if [ 0 -eq $? ] ; then bINKSCAPE=1 ; fi
	# use the Linux command "env" to list all environment variables:
	#bGNUPLOT44=0 ; if [ "`gnuplot -V`" = "gnuplot 4.4 patchlevel 0-rc1" ] ; then bGNUPLOT44=1 ; echo -e "GNUPlot version 4.4 is installed.\n" fi

	#Default values for command-line options
	bDO_EM=0  # 1 if you want energy minimization to be preformed (changed using command-line option -e)

	# 25-6-12) Commented-out the following line:
	#if [ ! -f em.trr ] ; then bDO_EM=1 ; fi # if ener.trr does not exist, it will be automatically generated 

	bDO_EOG=0 # 1 if you want the resulting graphic to be put up using Eye of GNOME (changed using command-line option -g)
	SimDuration=30 # Default simulated duration in time-steps (changed using comman-line option -l, followed by simulated durtion in time-steps)
	bDO_NM=0  # 1 if you want normal mode analysis to be preformed (changed using command-line option -m)
	bDO_NDX=0 # 1 if you want the atomic index file index.ndx to be automatically generated
	if [ ! -f index.ndx ] ; then bDO_NDX=1 ; fi # if index.ndx does not exist, it will be automatically generated
	bDO_MD=1 # 0 if you don't want an MD run to be executed (changed using command-line option -d)
	bN=0 # number of realizations has not been set manually
	N=1 # change if you want to simulate a statistical ensemble of realizations. If you enter a number other than 1 you will be shown the queue and asked to enter the new value
	SegLength=1 # no coarse-graining in time (changed using command-line option -s, followed by segment length in time-steps)
	bDO_G_TRAJ=0 # 1 if you want g_traj to be executed (changed using command-line option -t)
	bDO_G_ENERGY=0 # 1 if you want g_energy to be executed (changed using command-line option -y)
	bDO_VID=0 # 1 if you want video output (changed using command-line option -v)

	integrationAlgorithm=t	#d=leap-frog algorithm, v=velocity Verlet, a=velocity Verlet using averaged Kinetic Energy of two half-steps, s=stochastic dynamics (solve Langevin equation), t=resource-efficient stochastic dynamics
	dt=0.0002 # [ps] (default=1e-3) energy conservation up to 1-10% per time-step for dt=0.1fs=1e-4ps , <1% for dt=0.01fs=1e-5ps & for seg_length >= 1 period (e.g. 1000 steps) there are no freak jumps near stationary points (as DeltaE-->0 , P-->0)

	N_EXPONENTIALS=2
	STEADY_STATEname=STEADY_STATE
	bProduction=0
	bVgen=0

	Nbaths=0
	Tcold=0 #Can be non-integer, fraction part will be discarded later.
		#Examples: 
			#T_b(H2)	=	20.28					--> 	Tcold=20
			#T_b(N2)	=	77.36					--> 	Tcold=77
			#T_b(CO2	=	194.7					--> 	Tcold=195
			#T_STP=T_f(H2O) =	273.15	--> 	Tcold=273
			#T_SATP		=	298.15				--> 	Tcold=298
			#T_b(H2O)	=	373.15				--> 	Tcold=373
	DeltaT=1 # if DeltaT <= 1 , then BASH arithmetics won't differenciate between Thot & Tcold: Must use awk or dc!


	while getopts "a:b:c:deghi:kl:mnpr:s:tuvx:" optionName; do
	case "$optionName" in
		a) DeltaT="$OPTARG";;
		b) Nbaths="$OPTARG";;
		c) Tcold="$OPTARG";;
		d) bDO_MD=0;;
		e) bDO_EM=1;;
		g) bDO_EOG=1;;
		h) echo -e "
	Usage: ./SingleTrajectory-DD-MM-YY.sh [OPTIONS]
	Set-up and execute a single instance of GROMACS.
	\nCommand-line options:\n
		-a	Temperature bias (in Kelvin). Default is 1K.
		-b	Number of baths: 0 (NVE), 1 (NVT) or 2 (NVT1T2). Default is 0.
		-c	Temperature of the coldest heat bath (in Kelvin). Default is 0K.
		-d	DO NOT perform Molecular Dynamics run
		-e	Perform Energy Minimization before Molecular Dynamics run
		-g	Visualize local heat current, energy continuity, and temperature profile for the trajectory, using Eye Of GNOME
		-h	Display this Help message
		-i	Integration algorithm:
						d = md		(leap-frog algorithm)
						v = md-vv	(velocity Verlet)
						a = md-vv-avek	(velocity Verlet using averaged Kinetic Energy of two half-steps)
						s = sd		(stochastic dynamics -- solve Langevin equation)
						t = sd1		(resource-efficient stochastic dynamics)
		-k	Generate random initial velocities. Default is NO.
		-l X    Set Simulation duration to X time-steps
		-m	Perform Normal Mode analysis (if em.trr does not exist, energy minimization will be performed immediately beforehand)
		-n	Automatically generate an index file (will be performed anyway if one doesn't exist)
		-p	Identify this as a batch of production runs
		-r X    Number of realizations for ensemble averaging
		-s X    Set Time coarse-graining to X time-steps
		-t	Performs trajectory decoding using g_traj
		-u	Performs energy analysis using g_energy
		-v	Visualize temperature profile for the trajectory, using video
		-x	Time-step length in attoseconds. Minimum and default is 200 as.
	\nMandatory input files:\n
		topol.top or topol.itp	Included Topoy file for GROMACS (forcefield parameters and other options)
		conf.gro                GROMACS initial molecular configuration (single-precission)
		parse-DD-MM-YY.out	Parser program (parses GROMACS output and calculates expression for heat current)
		and this file.
	\nOptional input files:\n
	\nOptional output files:\n
		em.trr                  GROMACS final Energy Minimized configuration in double-precission
	\nInon Sharony, Abraham Nitzan group, Department of Chemical Physics, Tel Aviv University, Israel (2011).
	\n"; exit 0;;
		i) integrationAlgorithm="$OPTARG";;
		k) bVgen=1;;
		l) SimDuration="$OPTARG";;
		m) bDO_NM=1; if [ ! -f em.trr ] ; then bDO_EM=1; fi;;
		n) bDO_NDX=1;;
		p) bProduction=1;;
		r) N="$OPTARG";bN=1;;
		s) SegLength="$OPTARG";;
		t) bDO_G_TRAJ=1;;
		u) bDO_G_ENERGY=1;;
		v) bDO_VID=1;;
		x) dt="$OPTARG";;
		[?]) echo -e "\nBad command line option (not d, e, g, h, l, m, n, r, s, t, u, or v). Exiting.\n"; exit 1;;
	esac # end BASH "case"
	done # end "while" loop

  #Output of the following two BASH expressions is the integer part
  case "$Nbaths" in
    #2) DeltaT=$[ $Thot - $Tcold ] ; avgT=$[ ( $Thot + $Tcold ) / 2 ] ;;
		#2) avgT=$(echo $Tcold $DeltaT | awk '{printf("%d", $1 + $2 / 2)}');;
    #1) DeltaT=0 ; avgT=$Tcold ;;
    #0) DeltaT=0 ; avgT=0 ;;
		2) ;;
		1) DeltaT=0;;
		0) DeltaT=0 ; Tcold=0;;
    [?]) echo "Wrong number of baths! Exiting." ; exit 2 ;;
  esac

	if [ 1 -eq $bProduction ]
		then
		#if [ ! -f traj-*tss.trr ] ; then echo "No traj-*tss.trr file/s! Aborting." ; exit 55 ; fi
		NumTSSfiles=0
		for tssFile in traj-*tss.trr ; do NumTSSfiles=$[ 1 + $NumTSSfiles ] ; done
		if [ 0 -eq $NumTSSfiles ] ; then echo "No traj-*tss.trr file/s! Aborting." ; exit 55 ; fi
		
		N_EXPONENTIALS=1
	fi

	export bDO_MD ; if [ 0 != $? ] ; then echo -e "\nbDO_MD could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_EM ; if [ 0 != $? ] ; then echo -e "\nbDO_EM could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_EOG ; if [ 0 != $? ] ; then echo -e "\nbDO_EOG could not be exported... Exiting.\n" ; exit 11 ; fi
	export SimDuration ; if [ 0 != $? ] ; then echo -e "\nSimDuration could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_NM ; if [ 0 != $? ] ; then echo -e "\nbDO_NM could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_NDX ; if [ 0 != $? ] ; then echo -e "\nbDO_NDX could not be exported... Exiting.\n" ; exit 11 ; fi
	export SegLength ; if [ 0 != $? ] ; then echo -e "\nSegLength could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_G_TRAJ ; if [ 0 != $? ] ; then echo -e "\nbDO_G_TRAJ could not be exported... Exiting.\n" ; exit 11 ; fi
	export bDO_G_ENERGY ; if [ 0 != $? ] ; then echo -e "\nbDO_G_ENERGY could not be exported... Exiting.\n" ; exit 11 ; fi
	export integrationAlgorithm ; if [ 0 != $? ] ; then echo -e "\nintegrationAlgorithm could not be exported... Exiting.\n" ; exit 11 ; fi
	export dt ; if [ 0 != $? ] ; then echo -e "\ndt could not be exported... Exiting.\n" ; exit 11 ; fi
	export DeltaT ; if [ 0 != $? ] ; then echo -e "\nDeltaT could not be exported... Exiting.\n" ; exit 11 ; fi
	export N_EXPONENTIALS ; if [ 0 != $? ] ; then echo -e "\nN_EXPONENTIALS could not be exported... Exiting.\n" ; exit 11 ; fi
	export STEADY_STATEname ; if [ 0 != $? ] ; then echo -e "\nSTEADY_STATEname could not be exported... Exiting.\n" ; exit 11 ; fi
	export bProduction
	export bVgen
	#export avgT
	export Tcold
	export Nbaths

	#echo "
	#_________________________________________________________________________________________________________________________________________________________
	#|SimDuration=$SimDuration			| dt=$dt				| SegLength=$SegLength	|			|		|
	#|integrationAlgorithm=$integrationAlgorithm	|					|			|			|		|
	#|bDO_MD=$bDO_MD 				| bDO_EM=$bDO_EM			| bDO_NM=$bDO_NM	| bDO_NDX=$bDO_NDX	|		|
	#|bProduction=$bProduction			|					|			|			|		|
	#|bTcoupl=$bTcoupl				| bVgen=$bVgen				| Nbaths=$Nbaths	| avgT=$avgT		| DeltaT=$DeltaT|
	#|bDO_G_TRAJ=$bDO_G_TRAJ				| bDO_G_ENERGY=$bDO_G_ENERGY		| bDO_EOG=$bDO_EOG	| bDO_VID=$bDO_VID	|		|
	#|STEADY_STATEname=$STEADY_STATEname		| N_EXPONENTIALS=$N_EXPONENTIALS	|			|			|		|
	#|_______________________________________________|_______________________________________|_______________________|_______________________|_______________|
	#"


	#Define a unique name for this instace of running the script
	#This name should be discriptive, for future refference
	#Define an environment variable $INSTANCE which holds this name
	#Export the variable $INSTANCE so that it may be forwarded to the PBS script running on the compute (remote) node
	#Create a directory with the name stored in $INSTANCE under the directory from where this script was run
	#(this directory will hold the output from all the nodes)
	DATEANDTIME=$(date +%s%N)
	INSTANCE=$(echo ""$DATEANDTIME" 1000 / 1000000000 % 16 o p" | dc)	#	DATEANDTIME (with microsecond accuracy) in hexadecimal base (instead of decimal base) - dc is a command line calculator...!

	export INSTANCE ; if [ 0 != $? ] ; then echo -e "\nINSTANCE could not be exported... Exiting.\n" ; exit 11 ; fi

        echo "INSTANCE=$INSTANCE
        SimDuration=$SimDuration | dt=$dt | SegLength=$SegLength
        integrationAlgorithm=$integrationAlgorithm
        bDO_MD=$bDO_MD | bDO_EM=$bDO_EM | bDO_NM=$bDO_NM | bDO_NDX=$bDO_NDX
        bProduction=$bProduction
        bVgen=$bVgen | Nbaths=$Nbaths | Tcold=$Tcold | DeltaT=$DeltaT
        bDO_G_TRAJ=$bDO_G_TRAJ | bDO_G_ENERGY=$bDO_G_ENERGY | bDO_EOG=$bDO_EOG | bDO_VID=$bDO_VID
        STEADY_STATEname=$STEADY_STATEname | N_EXPONENTIALS=$N_EXPONENTIALS"
        #bVgen=$bVgen | Nbaths=$Nbaths | avgT=$avgT | DeltaT=$DeltaT


#initialize trajectory counter
TRAJ_COUNT=1;


echo -e "\nPlease make sure that the Present Working Directory name does not include characters such as hyphens or commas.\n"


#part 2: loop over N trajectories, submitting a single-trajectory script for each trajectory

echo "Deleting all Janitorial jobs to avoid deleting any job about to be submitted:"
set +e # dont quit if a janitorial job to be deleted can't be found (e.g. it might be EXITING while deletion takes place). As long as we get rid of all of them, we're fine
jMax=5
for i in $(qstat -u inonshar|grep Janitor|grep inonshar|grep -v " E "|cut -d "." -f1)
	do 
	echo $i 
	j=1
	qdel $i
	while ([ 0 -ne $? ] && [ $j -le $jMax ])
		do 
		echo "A Janitorial job ($i) could not be delted in order to avoid deleting the job about to be submitted. Let's hope it self-terminates... (attempt #$j out of $jMax)" 
		sleep 5
		j=$[ 1 + $j ]
		qdel $i
	done
done
set -e
sleep 1
if [ 0 -ne $(qstat -u inonshar | grep Janitor | grep inonshar| grep -v " E " | wc -l) ]
	then 
	echo "Some Janitorial jobs could not be deleted in order to avoid deleting the job about to be submitted. Aborting!"
	#exit 197
fi


#get number of trajectories to average over from the user

#N=$1 # first command-line argument
#if [ -z $1 ] ; then echo Please enter number of trajectories to run: ; read N ; fi # if not defined on command line, do so now
#showq | grep "Processors Active"

showq -u inonshar | grep "Processors Active"
#if [ 0 -eq $(showq -u inonshar | grep "Processors Active" | cut -d " " -f6) ] ; then echo "$[ $(showq -u inonshar | grep "Processors Active" | cut -d " " -f16) -  $(showq -u inonshar | grep "Processors Active" | cut -d " " -f13)] processors are currently free"
#else echo "$[ $(showq -u inonshar | grep "Processors Active" | cut -d " " -f15) -  $(showq -u inonshar | grep "Processors Active" | cut -d " " -f12)] processors are currently free"
#fi

if [ 1 -ne $bN ] ; then echo "Number of trajectories has not been set. Please enter number of trajectories to run:" ; read N ; else echo "Queing $N new jobs..." ; fi

#N="2"


mkdir $INSTANCE

#echo -e "SimDuration=$SimDuration\ndt=$dt\nSegLength=$SegLength\nintegrationAlgorithm=$integrationAlgorithm\nbDO_MD=$bDO_MD\nbDO_EM=$bDO_EM\nbDO_NM=$bDO_NM\nbDO_NDX=$bDO_NDX\nbProduction=$bProduction\nbVgen=$bVgen\nNbaths=$Nbaths\navgT=$avgT\nDeltaT=$DeltaT\nbDO_G_TRAJ=$bDO_G_TRAJ\nbDO_G_ENERGY=$bDO_G_ENERGY\nbDO_EOG=$bDO_EOG\nbDO_VID=$bDO_VID\nSTEADY_STATEname=$STEADY_STATEname\nN_EXPONENTIALS=$N_EXPONENTIALS\nINSTANCE=$INSTANCE" > $INSTANCE/options.dat
echo -e "SimDuration=$SimDuration\ndt=$dt\nSegLength=$SegLength\nintegrationAlgorithm=$integrationAlgorithm\nbDO_MD=$bDO_MD\nbDO_EM=$bDO_EM\nbDO_NM=$bDO_NM\nbDO_NDX=$bDO_NDX\nbProduction=$bProduction\nbVgen=$bVgen\nNbaths=$Nbaths\nTcold=$Tcold\nDeltaT=$DeltaT\nbDO_G_TRAJ=$bDO_G_TRAJ\nbDO_G_ENERGY=$bDO_G_ENERGY\nbDO_EOG=$bDO_EOG\nbDO_VID=$bDO_VID\nSTEADY_STATEname=$STEADY_STATEname\nN_EXPONENTIALS=$N_EXPONENTIALS\nINSTANCE=$INSTANCE" > $INSTANCE/options.dat


#redirect and append STDERR to file (restored later)
exec 9<>$INSTANCE/$INSTANCE.log ; if [ "$?" != "0" ] ; then exit 1 ;fi;
exec 9<&2 ; if [ "$?" != "0" ] ; then exit 1 ;fi;

#export the number of trajectories (N) for the use of the ensemble averaging script
#MAX_TRAJ=$N
#export MAX_TRAJ
#can't do that because I don't have env rights on Hydrogen headnode...
#rather, put the value of N into the file N.dat which is placed in the directory $INSTANCE
#echo $N > $INSTANCE/N.dat
#echo -e "$N\n$INSTANCE" > $INSTANCE/N.dat
echo -e "$N\n$dt\n$SegLength\n$N_EXPONENTIALS\n$bDO_EOG\n$bINKSCAPE\n$bDO_VID\n$STEADY_STATEname\n$DeltaT\n$bProduction\n$INSTANCE" > $INSTANCE/N.dat

#NOTE THAT THE NAME MAX_TRAJ SHOULD ALSO INCLUDE THE $INSTANCE NUMBER, TO DIFFERENCIATE MAX_TRAJ BETWEEN TWO CONCURRENTLY RUNNING INSTANCES OF GROMACS!
#This is not that important if the different instances are saved in separate sub-directories anyway

if [ 1 -eq $bProduction ] ; then
        #tssNSteps=$( echo `gmxdump_d -s topol-tss.tpr -quiet | awk -F"=" 'NR==4{print $2}'` $dt | awk '{print($1*$2)}' )
	#tssNSteps=$( echo `$PREFIX/gmxdump_d -s topol-tss.tpr -quiet | awk -F"=" 'NR==4{print $2}'`)
	tssNSteps=$( eval $PREFIX/gmxdump_d -s topol-tss.tpr -quiet | awk -F"=" 'NR==4{print $2}')
	#if [ 999000 -eq $tssNSteps ] ; then tssSegLength=1000 ; fi
	#tssSegLength= 
	#if [ -1 -eq 10^( Floor( log(tssNSteps) ) ) - tssNSteps ] ; then
	#tssSegLength=1
	#
	#if [ ( Ceil( log(tssNSteps) )   - log(tssNSteps) ) -lt log(2) ] ; then #tssNSteps is nearly a power of 10, and therefore probably equal to tssSimDuration-tssSegLength
	#tssSegLength= 10^( Ceil( log(tssNSteps) ) ) - tssNSteps
	#
	#What to do for 1<tssSegLength<(10^n) ?
  tTSSfMin=$(echo $tssNSteps $dt $SegLength $N | awk '{print (($1-$3-$4)*$2)}')
	tTSSfMax=$(echo $tssNSteps $dt               | awk '{print (($1   )*$2)}')

	#DEBUG: 17-1-12
        echo -e "\ntssNSteps=$tssNSteps dt=$dt SegLength=$SegLength\ntTSSfMin=$tTSSfMin tTSSfMax=$tTSSfMax\nN=$N SegLength=$SegLength"

	#FractionAtStSt=10 # fraction of Towrds-Steady-State trajectory which will be included as the sub-trajectory which is AT-Steady-State
	t0TSS="0.09" # 500
	if [ 1 -eq $(echo $t0TSS $tTSSfMax | awk '{if ( $1 > $2 ) print 1; else print 0}') ] ; then echo -e "\n	ERROR: $tTSSfMax ps towards-steady-state trajectory is shorter than minimal approximated time to reach steady-state, $t0TSS ps.\n" ; exit 117 ; fi
	FractionAtStSt=$(echo $tTSSfMin $t0TSS | awk '{print ($1/($1-$2))}')
	DstepRealiz=1 # number of time-steps between realization sampling
	# if tss trajectory length is more than 10 ps and $FractionAtStSt times the ensemble size, increase the time seperation between realization samples:
	if [ 1 -eq $(echo $tTSSfMax | awk '{if ( 10 < $1 ) print 1; else print 0}') ] ; then

		echo "INFO: Towards steady-state trajectory is longer than 10 ps"

		if [ 1 -eq $(echo $tssNSteps $N $SegLength $FractionAtStSt | awk '{if ( $2 < (($1-$3)/$4) ) print 1; else print 0}') ] ; then
			echo "DEBUG: $(echo $FractionAtStSt $N | awk '{print ($1*$2)}') time-steps (between trajectories) is less than $tssNSteps time-steps"
			DstepRealiz=$(echo $tssNSteps $N $SegLength $FractionAtStSt | awk '{print int(($1-$3)/($4*$2)) }')
			echo "DEBUG: DstepRealiz = $DstepRealiz = (tssNSteps = $tssNSteps - SegLength = $SegLength) / (FractionAtStSt = $FractionAtStSt * N = $N)"
			tTSSfMin=$(echo $tssNSteps $dt $SegLength $N $DstepRealiz | awk '{print (($1-$3-$4*$5)*$2)}')
			echo "INFO: DstepRealiz is equal to $DstepRealiz time-steps, now taking towards steady-state trajectory from $tTSSfMin ps"

		else echo "DEBUG: $(echo $FractionAtStSt $N | awk '{print ($1 * $2)}') time-steps (between trajectories) is more than $tssNSteps time-steps"
		fi # end if $N < ($tssNSteps - $SegLength)/$FractionAtStSt

	else if [ $N -ge $tssNSteps ] ; then echo "ERROR: Towards steady-state trajectory is shorter than number of realizations." ; exit 25 
		else echo "DEBUG: Towards steady-state trajectory is shorter than 10 ps ==> one trajectory each time-step"
		fi
	fi # end if tss trajectory longer than 10 ps


	#echo 0 | g_traj_d -quiet -f traj-tss -s topol-tss -fp -b $tTSSfMin -e $tTSSfMax -oxt traj-atss.trr # g_traj -oxt yields only position coordinates in the output *.trr file. No velocities for initial conditions!
        if [ ! -f traj-atss.trr ] ; then 
					$PREFIX/trjconv_d -f traj-tss.trr -o traj-atss.trr -quiet -b $tTSSfMin -e $tTSSfMax
	        if [ 0 -ne $? ] ; then echo "error extracting last time-steps from pre-production run. exiting." ; exit 3 ; fi
				  if [ ! -f traj-atss.trr ] ; then echo "error creating starting trajectory for production run. exiting." ; exit 79 ; fi
				fi


fi # end if 1=bProduction

echo -e "\nSubmitting jobs:"

NotOK=0

#loop over each trajectory from 1 to N
while [ $TRAJ_COUNT -le $N ]
do

bOK=1

#Export the variable $TRAJ_COUNT so that it may be forwarded to the PBS script running on the compute (remote) node
export TRAJ_COUNT
echo "TRAJ_COUNT = $TRAJ_COUNT / $N"

cp $(ls -tr1 $SCRIPTS/SingleTrajectory-*.sh | tail -n1) .
cp $(ls -tr1 $PARSE/parse-*.out | tail -n1) .

#Submit the single-trajectory script to the queue
NumSingleTrajectoryScripts=0
for SingleTrajectoryScriptName in SingleTrajectory-*.sh ; do NumSingleTrajectoryScripts=$[ 1 + $NumSingleTrajectoryScripts ] ; done
if [ 0 -eq $NumSingleTrajectoryScripts ] ; then echo -e "\nNo single-trajectory BASH scripts. exiting...\n"; exit 1 ; fi
if [ 1 -lt $NumSingleTrajectoryScripts ] ; then echo -e "\nMultiple single-trajectory BASH scripts -- keep only the one you want to use. exiting...\n"; exit 1 ; fi

if [ 1 -eq $bProduction ]
then
	#sleep 2 # wait X seconds (can also be a decimal fraction of a second)
	clear
	echo "SimDuration=$SimDuration dt=$dt SegLength=$SegLength TRAJ_COUNT=$TRAJ_COUNT NotOK=$NotOK"
	rm -f traj.trr

        #t0=$(echo $tssNSteps $dt $SegLength $TRAJ_COUNT $DstepRealiz | awk '{print (($1-$3-$4*$5)*$2)}')
        #t0=$(echo $tssNSteps $TRAJ_COUNT 10 $dt | awk '{print (($1-$2*$3)*$4)}')
				t0=$(echo $tssNSteps $TRAJ_COUNT $DstepRealiz $dt | awk '{print (($1-$2*$3)*$4)}')

	if [ 1 -ne $(echo $t0 | awk '{print (($1 > 0) ? 1 : 0)}') ] ; then echo -n "ERROR: 0 >=! " ; bOK=0 ; else echo -n "DEBUG: " ; fi
	echo "t0 = $t0 = (tssNSteps = $tssNSteps - TRAJ_COUNT = $TRAJ_COUNT * DstepRealiz = $DstepRealiz) * dt = $dt"a

	skip_to=$(echo $t0 $dt | awk '{print ($1 - 100 * $2)}')

	#t1=$(echo $t1 $dt | awk '{print ($1+$2)}')
	echo "Sampling realization from $t0 ps into steady-state trajectory:"
	#echo 0 | g_traj_d -quiet -f traj-atss -s topol-tss -fp -b $t0 -e $t0 -oxt traj.trr # g_traj -oxt yields only position coordinates in the output *.trr file. No velocities for initial conditions!
	#$PREFIX/trjconv_d -quiet -f traj-atss.trr -o traj.trr -dump $t0
  $PREFIX/trjconv_d -quiet -f traj-atss.trr -o traj.trr -b $skip_to -dump $t0
	if [ 0 -ne $? ] ; then echo "Error in trjconv while extracting time-step from pre-production trajectory at steady-state. exiting." ; fi
	#export bVgen=1
  #sleep 2 # wait X seconds (can also be a decimal fraction of a second)
	#if [ -f traj-$TRAJ_COUNT.trr ] ; then mv traj.trr $INSTANCE/traj-$TRAJ_COUNT.trr
	if [ 0 -ne $bOK ] && [ ! -f traj.trr ] ; then echo "failed to find traj.trr!" ; bOK=0 ; fi
	#echo "Error after trjconv in extracting time-step from pre-production trajectory $TRAJ_COUNT at steady-state (trjconv exited with code 0). exiting." ; TRAJ_COUNT=$[$TRAJ_COUNT-1] ; fi
  if [ 0 -ne $bOK ] && [ ! -s traj.trr ] ; then echo "Zero-sized traj.trr generated!" ; bOK=0 ; fi


	btraj0=0
	if [ -f traj0.trr ]
	then
		btraj0=1
		echo "\"traj0.trr\" found. Abort now if you want to refrain from overriding traj-atss.trr using traj0.trr . Press any key to continue."
		read
		mv traj0.trr traj.trr
		ls {traj0.trr,traj.trr} -lX # DBG 17-7-13PM
	fi # traj0.trr

fi # bProduction

if [ 1 -ne $N ] && [ 0 -eq $bOK ] ; then 
	NotOK=$[ 1 + $NotOK ]
	echo "$NotOK failed trajectories"
	N=$[ 1 + $N ] # or "continue"
else
	MAIL_OPTS="a"
	if [ $TRAJ_COUNT -eq $N ] ; then MAIL_OPTS="ae" ; fi
	EXIT_STATUS=-1
	while [ 0 -ne $EXIT_STATUS ]
		do
		if [ 1 -eq $bProduction ] && [ ! -f traj.trr ] ; then echo "failed to find traj.trr!" ; fi
		if [ $HOSTNAME == "power.tau.ac.il" ]
			then
			qsub -N GMX-$INSTANCE-$TRAJ_COUNT -d $PWD -m $MAIL_OPTS -q nano1 $SingleTrajectoryScriptName
			EXIT_STATUS=$?
		else
			qsub -N GMX-$INSTANCE-$TRAJ_COUNT -d $PWD -m $MAIL_OPTS         $SingleTrajectoryScriptName
			EXIT_STATUS=$?
		fi
		sleep 2
	done
fi
sleep 2

echo ; if [ 1 -eq $btraj0 ] ; then mv traj.trr traj0.trr ; fi ; echo

#echo -----------------------------------------------------------

#advance the trajectory counter and close the loop
TRAJ_COUNT=$[ $TRAJ_COUNT + 1 ]
done


#echo =============================
echo "DONE: Submitted $[$N-$NotOK] trajectories out of a total $N requested."

#Cleanup -- if no error codes have been encountered so far:
rm -f traj.trr # traj-atss.trr

#part 3: finish

#Print out the discriptive name of this instance of running the script
#This is also the name of the sub-directory under the current directory where the output files will be located

#cp * $INSTANCE/
if [ -f topol.top ] ; then cp topol.top $INSTANCE/topol-$INSTANCE.top ; fi
if [ -f topol.itp ] ; then cp topol.itp $INSTANCE/topol-$INSTANCE.itp ; fi
if [ -f  conf.gro ] ; then cp conf.gro $INSTANCE/conf-$INSTANCE.gro  ; fi

# index.ndx and grompp.mdp are only copied by SingleTrajectory, since they might undergo changes from the original
#if [ -f index.ndx ] ; then cp index.ndx $INSTANCE/index-$INSTANCE.ndx ; echo -n "	index.ndx exists and has been copied to INSTANCE sub-directory." ; echo ; fi
#if [ -f grompp.mdp ] ; then cp grompp.mdp $INSTANCE/grompp-$INSTANCE.mdp ; echo -n "     grompp.mdp exists and has been copied to INSTANCE sub-directory." ; echo ;  fi

#echo "If you would like to copy the Ensemble Averaging and Fitting program automatically to this instance's subdirectory, press Y,"
#echo " otherwise press any other key"
#read key
#if [ "$key" == "Y" ]
#then 

#cp EnsembleAveraging-25-2-10.sh	$INSTANCE/
#cp $(ls -tr1 $HOME/gmx-heat-conduction/src/Scripts/EnsembleAveraging-*.sh | tail -n1) .
#NumAveragingScripts=0
#for EnsembleAveragingBashScript in EnsembleAveraging-*.sh ; do NumAveragingScripts=$[ 1 + $NumAveragingScripts ] ; done
#if [ 0 -eq $NumAveragingScripts ] ; then echo -e "\nNo ensemble averaging BASH scripts. exiting...\n"; exit 1 ; fi
#if [ 1 -lt $NumAveragingScripts ] ; then echo -e "\nMultiple ensemble averaging BASH scripts -- keep only the one you want to use. exiting...\n"; ls -1 EnsembleAveraging-*.sh ; exit 1 ; fi
#cp $EnsembleAveragingBashScript $INSTANCE/

#NumPlotScripts=0
#for PlotBashScript in Plot-*-hydrogen.sh ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
#if [ 0 -eq $NumPlotScripts ] ; then echo -e "\nNo Plot BASH scripts. exiting...\n"; exit 1 ; fi
#if [ 1 -lt $NumPlotScripts ] ; then echo -e "\nMultiple Plot BASH scripts -- keep only the one you want to use. exiting...\n"; exit 1 ; fi
#cp $PlotBashScript $INSTANCE/

#fi


#Once all the trajectories are running, print to screen a snapshot of the cluster status
#showq		#shows information about the queue
#qstat -u inonshar
echo ; showq -u inonshar | grep "Active Jobs:" ; echo
#qstat -n	#queue status with details on the nodes each of the jobs is being run on
#qstat -q	#queue status with details on the total number of jobs running, queued, etc.


#restore STDERR from file
exec 2<&9

date >&9
echo INSTANCE=$INSTANCE
echo INSTANCE=$INSTANCE > INSTANCE.txt
echo "#Normal termination of Master Control script." >&9
if [ 0 -eq $(wc -c $INSTANCE/$INSTANCE.log | cut -d " " -f1) ] ; then echo "Empty Master Control STDERR log file." ; rm $INSTANCE/$INSTANCE.log ; fi
echo
#Exit the script with a succesful status ID = 0
exit 0
