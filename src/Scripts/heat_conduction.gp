#! /usr/local/bin/gnuplot

set datafile missing 'nan'

#COLUMNHEADERS="`head -1 HeatConduction.txt | cut -d' ' -f3-10`"
COLUMNHEADERS="`head -1 HeatConduction.txt`"
print COLUMNHEADERS
ROWHEADERS="`awk 'BEGIN{getline}{printf "%s ",$1}' HeatConduction.txt`"
print ROWHEADERS
NATOMS=`head -n2 conf.gro | tail -n1 | awk '{print($1)}'`

set for [i=1:words(ROWHEADERS)] xtics ( word(ROWHEADERS,i) i+1 )

set xtic rotate by -90 offset 0,graph -0.075
set ylabel "Power / [KJ/mole per ps]"
set y2tic
set y2label "Temperature / [10K]"

set terminal jpeg transparent nocrop enhanced size 1200,900
set output 'heat_conduction.jpeg'
p for [i=1:3] 'HeatConduction.txt' every ::1 u 0:(column(3+2*i)):(column(4+2*i)):xticlabel(1) w yerrorbars pt 7 t word(COLUMNHEADERS, 3+2*i), 'HeatConduction.txt' every ::1 u 0:($3 / 10):($3 / 10):(($3 + $4)/10):xticlabel(1) w yerrorbars pt 5 ps 1 t word(COLUMNHEADERS, 3+2*0)
# p for [i=1:3] 'HeatConduction.txt' every ::1 u 0:(column(3+2*i)):(column(4+2*i)):xticlabel(1) w yerrorbars t word(COLUMNHEADERS, 3+2*i) axes x1y1, 'HeatConduction.txt' every ::1 u 0:3:4:xticlabel(1) w yerrorbars t word(COLUMNHEADERS, 3+2*0) axes x1y2

