#! /bin/bash -e
# (make script does not work if the source directory path has spaces in it!)
# create "if" line right at the beginning which checks for this and exits with error if finds one
#sudo -n #execute the "sudo" command non-interactively : if the sudo password has already been input, the script should continue, otherwise it will exit, thus prompting the user to manually execute "sudo" and enter the password
#if [ $? -ne 0 ] ; then echo "bad sudo password" 			; exit 2 ; fi

DIRPREFIX=$(readlink -f ../../../bin/gromacs)
CFLAGSARG="-g3 -msse3 -O0"

if [ -f Makefile ] 
then
	echo "A Makefile already exists, so GROMACS configure has already been run. Please make sure what you want is a first-time installation, or use GROMACS_update_script.sh"
	exit 1
else
        ./configure --prefix=$DIRPREFIX --enable-double --enable-debug CFLAGS="$CFLAGSARG"
	if [ $? -ne 0 ] ; then echo "error running configure"                   ; exit 2 ; fi
fi

make clean
if [ $? -ne 0 ] ; then echo "error running make clean" 			; exit 3 ; fi
make distclean
if [ $? -ne 0 ] ; then echo "error running make distclean"		; exit 4 ; fi

./configure --prefix=$DIRPREFIX --enable-double --enable-debug CFLAGS="$CFLAGSARG"
if [ $? -ne 0 ] ; then echo "error running configure" 			; exit 5 ; fi
make
if [ $? -ne 0 ] ; then echo "error running make" 			; exit 6 ; fi
make check
if [ $? -ne 0 ] ; then echo "error running make check" 			; exit 7 ; fi
make install
if [ $? -ne 0 ] ; then echo "error running make install" 		; exit 8 ; fi
make links
if [ $? -ne 0 ] ; then echo "error running make links" 			; exit 9 ; fi
make tests
if [ $? -ne 0 ] ; then echo "error running make tests" 			; exit 10 ; fi

exit 0
