reset

set term svg enhanced size 1200,900
set output "Energy_Current_`echo $(date +%d-%m-%y)`.svg"

CURRENTFILENAME="current.txt"
max(x,y)=(x > y) ? x : y
min(x,y)=(x < y) ? x : y
gauss(x,m,s)=(1/sqrt(2*2*asin(1)*s*s))*exp(-(x-m)*(x-m)/(2*s*s)) # Normalized Gaussian function of x with mean "m" and standard deviation "s"
r_10_0=100 # equilibrium distance between atoms 1 and 0
NSTEPS=`cut -d "=" -f2- options.dat | head -n 1 | tail -n 1` # number of simulation steps as taken in input
SEG_LENGTH=`cut -d "=" -f2- options.dat | head -n 3 | tail -n 1` # time coarse-graining segmenth length (in time-steps)
NSEGS=NSTEPS/SEG_LENGTH # number of time coarse-graining segments

set fit errorvariables
fit A 'CURRENTFILENAME' u 2:(1000*$4) via A # fit J_x to constant
fit B 'CURRENTFILENAME' u 2:(1000*$5) via B # fit J_y to constant
fit C 'CURRENTFILENAME' u 2:(1000*$6) via C # fit J_z to constant 
fit P 'output.txt' u 2:(1000*r_10_0*$7) every 3::0:1 via P # fit P times the equilibrium distnace to constant

set multiplot layout 1,3

#fitting parameter results (label)
#set label sprintf("<J_x> = %e +/- %e\n<J_y> = %e +/- %e\n<J_z> = %e +/- %e\n<P>*r_{12}^0 = %e +/- %e",A,A_err,B,B_err,C,C_err,P,P_err) at graph 0.2,0.3
set label sprintf("<J_x> = %g +/- %g\n<J_y> = %g +/- %g\n<J_z> = %g +/- %g\n<P>*r_{12}^0 = %g +/- %g",A,A_err,B,B_err,C,C_err,P,P_err) at graph 0.2,0.7

#data
pr "Plotting data"
set title "Data\n"
set size 0.8,1
set key below
set xlabel "t/[ps]"
set ylabel "Energy Current / [ KJ/mole * m per s ]"
set yrange [] writeback # write-back the final y-range for use in a later plot
p for [i=4:6] 'CURRENTFILENAME' u 2:(1000*column(i)) every 3::0:1 w dots t sprintf("Data column #%d",i), 'output.txt' u 2:(1000*r_10_0*$7) every 3::0:1 w dots t "P(t)*r_{12}^0", A lc 1 t "<J_x>", B lc 2 t "<J_y>", C lc 3 t "<J_z>", P lc 4 t "<P>*r_{12}^0"

unset label

#fit of data to normalized Gaussian distribution
pr "Plotting fit"
set size 0.2,1
set origin 0.8,0
set title "Fit of data\nnorm. Gaussian distrib."
set key below
set parametric # this is a parametric plot: x(t),y(t)
set samples 1000 # increase number of sampels for plotting functions from the default 100
set style fill solid 0.1
set xlabel "Relative Frequency"
#set xrange [0:*] reverse
set xrange [0:(NSEGS/2)*max(gauss(C,C,C_err),gauss(P,P,P_err))/NSEGS] reverse # set x-range to cover all of both the <J_x> and <P>*r Gaussians
#set xtics format "" # don't show x-axis values (relative frequencies)
unset ylabel
unset ytics
#set y2tics
#set trange [GPVAL_Y_MIN:GPVAL_Y_MAX]
Nsigma=3 # plot ranges 3 std dev.s from mean value # plot 3 standard deviations at least beyond lowest and highest Gaussian peaks
T_MIN=min(A-Nsigma*A_err,min(B-10*Nsigma*B_err,min(C-10*Nsigma*C_err,P-Nsigma*P_err)))
T_MAX=max(A+Nsigma*A_err,max(B+10*Nsigma*B_err,max(C+10*Nsigma*C_err,P+Nsigma*P_err)))
#set trange [GPVAL_Y_MIN:GPVAL_Y_MAX]
set trange [T_MIN:T_MAX]
#set yrange [GPVAL_Y_MIN:GPVAL_Y_MAX]
#set yrange [T_MIN:T_MAX]
set yrange restore
p gauss(t,A,A_err),t w filledcu t "J_x", gauss(t,B,B_err),t w filledcu t "J_y", gauss(t,C,C_err),t w filledcu t "J_z", gauss(t,P,P_err),t w filledcu t "<P>*r_{12}^0", t,0 w l lc 0 lw 2 noti
#p gauss(t,A,A_err),t t "J_x", gauss(t,B,B_err),t t "J_y", gauss(t,C,C_err),t t "J_z", gauss(t,P,P_err),t t "<P>*r_{12}^0"

#inset
pr "Plotting inset"
set size 0.15,0.15
set origin 0.81,0.2
unset title
set key off
set samples 1000
unset xlabel
unset xtics
set xrange [0:(NSEGS/2)*max(gauss(C,C,C_err),gauss(P,P,P_err))/NSEGS] reverse
Nsigma=2 # plot ranges 3 std dev.s from mean value # plot 3 standard deviations at least beyond lowest and highest Gaussian peaks
T_MIN=min(A-Nsigma*A_err,min(B-10*Nsigma*B_err,min(C-10*Nsigma*C_err,P-Nsigma*P_err)))
T_MAX=max(A+Nsigma*A_err,max(B+10*Nsigma*B_err,max(C+10*Nsigma*C_err,P+Nsigma*P_err)))
set trange [T_MIN:T_MAX]
set yrange [T_MIN:T_MAX]
set ytics (0) format ""
set grid ytics lw 4
#p gauss(t,A,A_err),t w filledcu t "J_x", gauss(t,B,B_err),t w filledcu t "J_y", gauss(t,C,C_err),t w filledcu t "J_z", gauss(t,P,P_err),t w filledcu t "<P>*r_{12}^0", t,0 w l lc 0 lw 2 noti
p gauss(t,A,A_err),t t "J_x", gauss(t,B,B_err),t t "J_y", gauss(t,C,C_err),t t "J_z", gauss(t,P,P_err),t t "<P>*r_{12}^0"


unset multiplot
pr "GNUPlot exiting"
