#! /bin/bash -e

if [ -f SulfurBathPowers.txt ] ; then echo "SulfurBathPowers.txt exits. Aborting." ; exit 11 ; else touch SulfurBathPowers.txt ; fi

LeftSulfurAtom=$[ $(grep S conf-*.gro -n | cut -d ":" -f1 | tail -n2 | head -n1) - 2 ]
echo "LeftSulfurAtom=$LeftSulfurAtom"
RightSulfurAtom=$[ $(grep S conf-*.gro -n | cut -d ":" -f1 | tail -n1) - 2 ]
echo "RightSulfurAtom=$RightSulfurAtom"
SulfurAtoms=( "$LeftSulfurAtom" "$RightSulfurAtom" )
echo "\${#SulfurAtoms[@]}=${#SulfurAtoms[@]}"
echo "\${SulfurAtoms[@]}=${SulfurAtoms[@]}"

echo "======================================="

for i in BondPower-*.txt
do
	ni=$(echo $i | sed -e s/[^0-9]//g)
	echo $ni
	echo -n $ni >> SulfurBathPowers.txt

	for j in ${SulfurAtoms[@]}
		do
		#echo "DBG: j=$j"
		echo -n " $(head $i -n $j | tail -n1 | cut -d " " -f $[ 2*$j ])" >> SulfurBathPowers.txt
	done

  echo >> SulfurBathPowers.txt

done

echo "#Normal termination of script"
exit 0
