reset
INSTANCE="`echo $(head -n 11 N.dat | tail -n 1)`"
OUTPUTFILENAME=sprintf("LocalStuff-%s",INSTANCE)
set term jpeg enhanced size 1200,900 ; set output OUTPUTFILENAME.".jpeg"
set term svg enhanced size 1200,900 ; set output OUTPUTFILENAME.".svg"

dt=`head -n2 options.dat | tail -n1 | cut -d "=" -f2`
pr "dt=",dt
SegLength=`head -n3 options.dat | tail -n1 | cut -d "=" -f2`
pr "SegLength=",SegLength
NATOMS=`head conf-*.gro -n2 | tail -n1`
pr "NATOMS=",NATOMS
LeftSulfurAtom=`grep S conf-*.gro -n | cut -d ":" -f1 | tail -n2 | head -n1`-2
pr "LeftSulfurAtom=",LeftSulfurAtom
RightSulfurAtom=`grep S conf-*.gro -n | cut -d ":" -f1 | tail -n1`-2
pr "RightSulfurAtom=",RightSulfurAtom
avgT=`head options.dat -n12 | tail -n1 | cut -d "=" -f2`
pr "avgT=",avgT
if ( 1 == system("if [ -f STDIO-avg.txt ] ; then echo 1 ; else echo 0 ; fi") ) bTstdev=1 ; else bTstdev=0

set key below 
set fit errorvariables 

set multiplot layout 2,3 

set xlabel "t/[ps]"
set style data points
set style fill solid 0.1 noborder

set title "System-Bath Power"
fit P_L 'BondPower.txt' u (dt*SegLength*$0):LeftSulfurAtom every (NATOMS+1)::(LeftSulfurAtom-1) via P_L
if (exists("FIT_STDFIT")) DP_L=FIT_STDFIT ; else DP_L=0
fit P_R 'BondPower.txt' u (dt*SegLength*$0):RightSulfurAtom every (NATOMS+1)::(RightSulfurAtom-1) via P_R
if (exists("FIT_STDFIT")) DP_R=FIT_STDFIT ; else DP_R=0
#p
#      P_L+DP_L lt 0 lc 1 lw 1 w filledcu y1=     P_L-DP_L t sprintf("[%g,%g]",     P_L-DP_L,     P_L+DP_L),
# -1.0*P_R+DP_R lt 0 lc 3 lw 1 w filledcu y1=-1.0*P_R-DP_R t sprintf("[%g,%g]",-1.0*P_R-DP_R,-1.0*P_R+DP_R),
p\
      P_L+DP_L lt 0 lc 1 lw 1 w filledcu y1=     P_L-DP_L noti,\
 -1.0*P_R+DP_R lt 0 lc 3 lw 1 w filledcu y1=-1.0*P_R-DP_R noti,\
 'BondPower.txt' u (dt*SegLength*$0):LeftSulfurAtom every (NATOMS+1)::(LeftSulfurAtom-1) lc 1 t "P_{LeftSulfur<-LeftBath}(t)",\
 'BondPower.txt' u (dt*SegLength*$0):(-1* column(RightSulfurAtom)) every (NATOMS+1)::(RightSulfurAtom-1) lc 3 t "-P_{RightSulfur<-RightBath}(t)",\
      P_L lt 0 lc 1 lw 2 t sprintf("%g",     P_L),\
 -1.0*P_R lt 0 lc 3 lw 2 t sprintf("%g",-1.0*P_R),\
      P_L+DP_L lc 1 lw 0.5 noti,\
      P_L-DP_L lc 1 lw 0.5 noti,\
 -1.0*P_R+DP_R lc 3 lw 0.5 noti,\
 -1.0*P_R-DP_R lc 3 lw 0.5 noti

#      P_L+DP_L lc 1 lw 0.5 t sprintf("%g",     P_L+DP_L),\
#      P_L-DP_L lc 1 lw 0.5 t sprintf("%g",     P_L-DP_L),\
# -1.0*P_R+DP_R lc 3 lw 0.5 t sprintf("%g",-1.0*P_R+DP_R),\
# -1.0*P_R-DP_R lc 3 lw 0.5 t sprintf("%g",-1.0*P_R-DP_R)

set title "Local Temperatures" 
set ylabel "T/[K]"
#p for[i=0:NATOMS] 'output.txt' u 2:9 every (NATOMS+1)::i pt 7 ps 2/1.2**i t sprintf("%d",i+1), avgT w l lt 0
if (1==bTstdev) p for[i=0:NATOMS-1] 'output.txt' u 2:9 every (NATOMS+1)::i pt 7 ps 2/1.2**i t sprintf("%d",i+1), avgT w l lt 0, for[i=0:NATOMS-1] 'STDIO-avg.txt' u 1:(avgT+column(2+i)) w l lc i+1 noti, for[i=0:NATOMS-1] 'STDIO-avg.txt' u 1:(avgT-column(2+i)) w l lc i+1 noti ; else p for[i=0:NATOMS-1] 'output.txt' u 2:9 every (NATOMS+1)::i pt 7 ps 2/1.2**i t sprintf("%d",i+1), avgT w l lt 0

set title "Local Energies" 
set ylabel "E_{i}/[KJ mole^{-1}]"
p for[i=0:NATOMS-1] 'output.txt' u 2:($4+$5) every (NATOMS+1)::i pt 7 ps 2/1.2**i t sprintf("%d",i+1) 
unset ylabel


set title "Local Power" 
p for[i=0:NATOMS-1] 'output.txt' u 2:7 every (NATOMS+1)::i pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

set title "Absolute Error in Local Energy Continuity"
p for[i=0:NATOMS-1] 'output.txt' u 2:($6-$7) every (NATOMS+1)::i:1 pt 7 ps 2/1.2**i t sprintf("%d",i+1) 

unset multiplot

