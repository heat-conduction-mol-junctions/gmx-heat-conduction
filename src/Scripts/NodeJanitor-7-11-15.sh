#! /bin/bash


function MK_JANITOR_SCRPT(){
echo "#PBS -v NODE_COUNT,
#PBS -l nodes=compute-0-$NODE_COUNT:ppn=1,walltime=00:09:00
###PBS -p 20 #priority range=[-1024,1023] default=0
#PBS -o $NODE_COUNT-std.io
#PBS -e $NODE_COUNT-std.err

### clean /scratch/$LOGNAME on compute node $NODE_COUNT

date

ls /scratch/$LOGNAME
if [ 0 -eq $? ]
then
	du -sh /scratch/$LOGNAME
	rm -rvf /scratch/$LOGNAME/*
	du -sh /scratch/$LOGNAME
	date
	exit 0
else
	echo "The directory /scratch/$LOGNAME/ does not exist. Nothing to be done."
	exit 0
fi" > NodeJanitorScript_$NODE_COUNT.sh
}

#main

#if [ ! -z $(qstat -u $LOGNAME) ] ; then exit 2 ; fi # if user has any jobs in the queue, don't perform janitorial duties (as these might interfere with the proper execution of the user's other jobs) and exit with exit code 2.
if [ 0 -ne $(qstat -u $LOGNAME | wc -l) ] ; then echo "User has jobs in the queue. Won't perform janitorial duties (as these might interfere with the proper execution of the user's other jobs) and exit with exit code 2." ; exit 2 ; fi

#one-liner to delete all queued jobs -- use with care!
#for i in $(qstat -u $LOGNAME | grep $LOGNAME | cut -d "." -f1) ; do echo $i ; qdel $i ; done

DATEHEXADEC=$(echo `date +%Y%W%H%M%S%N` 1000 / 16 o p | dc)
mkdir Janitor-$DATEHEXADEC
cd Janitor-$DATEHEXADEC
echo PBS_O_INITDIR=$PBS_O_INITDIR

MAX_NODE="35"
NODE_COUNT="0"
while [ $NODE_COUNT -le $MAX_NODE ] ; do

	#DEV: Add dependency -- if node janitor is queued to run on this node already (and waiting in queue), do not execute again

		#printf("\nNode %2d is not running any jobs for %s.\t:\t",$NODE_COUNT,$LOGNAME)
		#echo -n "Node $NODE_COUNT is not running any jobs for $LOGNAME.    :     "

	if [ -z "$(qstat -nu $LOGNAME | grep compute-0-$NODE_COUNT)" ] ; then
		MK_JANITOR_SCRPT
		qsub -N Janitor-$NODE_COUNT -d $PWD NodeJanitorScript_$NODE_COUNT.sh
		rm NodeJanitorScript_$NODE_COUNT.sh
	else echo "Node $NODE_COUNT is currently running jobs for $LOGNAME. To clean this node, wait for them to finish and run again."
	fi
	NODE_COUNT=$[ 1 + $NODE_COUNT ]
done

exit 0
