reset 

#system("DATAFILE=1-std.io ; nsteps=$(head -n1 options.dat | cut -d \"=\" -f2) ; cat $DATAFILE | head -n $[ $nsteps + 136 ] | tail -n $nsteps > test.txt ; cut -d \"=\" -f11 test.txt | sed -n '1~2p' > LeftSulfurBondPower.txt ; cut -d \"=\" -f11 test.txt | sed -n '0~2p' > RightSulfurBondPower.txt ; head LeftSulfurBondPower.txt ; echo ... ; tail LeftSulfurBondPower.txt ; cut test.txt -d \"=\" -f 7 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurDissipationPower.txt ; cut test.txt -d \"=\" -f 7 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurDissipationPower.txt ; cut test.txt -d \"=\" -f 7 | cut -d " " -f1 | sed -n '0~2p' > RightSulfurDissipationPower.txt ; cut test.txt -d \"=\" -f 8 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurRanForPower.txt ; cut test.txt -d \"=\" -f 8 | cut -d " " -f1 | sed -n '0~2p' > RightSulfurRanForPower.txt")

#set term jpeg enhanced size 1200,900 ; set output "2FC158B5.jpeg"
set term svg enhanced size 1200,900 ; set output "BathPowers.svg"

set key below 
set style data points

set multiplot layout 1,2 

NBATHS=`head -n11 options.dat | tail -n1 | cut -d "=" -f2`
if (2!=NBATHS) pr "NBATHS=",NBATHS,"!=2 ==> quitting...";  quit

fit A0 'LeftSulfurDissipationPower.txt' u 0:1 via A0 
fit A1 'RightSulfurDissipationPower.txt' u 0:1 via A1 
p 'LeftSulfurDissipationPower.txt' u 0:1, 'RightSulfurDissipationPower.txt' u 0:1, A0 lc 1 t sprintf("%g",A0), A1 lc 2 t sprintf("%g",A1) 

fit A0 'LeftSulfurRanForPower.txt' u 0:1 via A0 
fit A1 'RightSulfurRanForPower.txt' u 0:1 via A1 
p 'LeftSulfurRanForPower.txt' u 0:1, 'RightSulfurRanForPower.txt' u 0:1, A0 lc 1 t sprintf("%g",A0), A1 lc 2 t sprintf("%g",A1) 

unset multiplot
