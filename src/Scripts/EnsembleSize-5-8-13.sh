#! /bin/bash -e
date
PlotPoints=7
echo "PlotPoints=$PlotPoints"
nsteps=$(gmxdump_d -s topol*.tpr -quiet | grep nsteps  | cut -d "=" -f2)
echo "nsteps=$nsteps [time-steps]"
#q=$(echo $nsteps $PlotPoints | awk '{print ($1 ^ (1. / (1 + $2)))}') # pow($nsteps , 1. / $[ 1 + $PlotPoints ] )
#echo "q=$q [time-steps]"


dt=$(    gmxdump_d -s topol*.tpr -quiet | grep delta_t | cut -d "=" -f2) 
echo "dt=$dt ps"
t_atss_in_ps=$(echo $nsteps $dt | awk '{print ($1 * $2)}')
echo "t_atss=$t_atss_in_ps [ps]"
qt=$(echo $t_atss_in_ps $PlotPoints | awk '{print ($1 ^ (1. / (1 + $2)))}') # pow($nsteps , 1. / $[ 1 + $PlotPoints ] )
echo "qt=$qt [ps]"
#tSplit=$(echo $nsteps $dt $nSplit | awk '{print ($1 * $2 / $3)}') 

for j in $(seq 1 $PlotPoints )
 do
 tSplit=$(echo $qt $j | awk '{print ($1 ^ $2)}') # pow(qt,j)
 nSplit=$(echo $nsteps $dt $tSplit | awk '{print ($1 * $2 / $3)}')
 nSplit=$(echo $nSplit | awk '{print ($1 - $1 % 1)}')
 #nSplit=$(echo $qt $j $dt | awk '{print (($1 ^ $2) / $3)}') # pow(qt,j) / dt
 #nSplit=$(echo $nSplit | awk '{print ($1 - $1 % 1)}') # round down to integer part
 #tSplit=$(echo $nsteps $dt $nSplit | awk '{print ($1 * $2 / $3)}') 
 #echo -ne "$q^$j~$nSplit(=$tSplit ps)\t"
 echo -ne "$qt^$j = $tSplit ps ~ $nSplit \t"
done
echo

echo "Press Carriage Return to continue execution..."
read

if [ -f EnsembleSize.txt ] ; then echo "EnsembleSize.txt already exists! aborting..." ; exit 8 ; fi 
touch EnsembleSize.txt

#begin main
for j in $(seq 1 $PlotPoints ) 
 do 
 #nSplit=$(echo $q $j | awk '{print ($1 ^ $2 - ($1 ^ $2) % 1)}')
 tSplit=$(echo $qt $j | awk '{print ($1 ^ $2)}') # pow(qt,j)
 nSplit=$(echo $nsteps $dt $tSplit | awk '{print ($1 * $2 / $3)}')
 nSplit=$(echo $nSplit | awk '{print ($1 - $1 % 1)}')
 echo -e "\n$nSplit\n=======\n\n"
 mkdir $nSplit 
 cd $nSplit 
 ../../SplitUp-*.sh $nSplit
 echo "$j $nSplit $(tail fit.log -n7 | head -n1 | cut -d "=" -f2 | cut -d "+" -f1) $(tail fit.log -n13 | head -n1 | cut -d ":" -f2)" >> ../EnsembleSize.txt 
 cd .. 
done

cat EnsembleSize.txt
#for i in $(find -type d) ; do if [ "." != $i ] ; then echo $i ; tail $i/fit.log -n7 | head -n1 | cut -d "=" -f2 | cut -d "+" -f1; tail $i/fit.log -n13 | head -n1 | cut -d ":" -f2 ; fi ; done


date
echo "#Normal termination of ensemble size script."
exit 0
