#PBS -v INSTANCE,TRAJ_COUNT,bDO_MD,bDO_EM,bDO_EOG,SimDuration,bDO_NM,bDO_NDX,SegLength,bDO_G_TRAJ,bDO_G_ENERGY,integrationAlgorithm,dt,N_EXPONENTIALS,DeltaT,bProduction,bVgen,Nbaths,Tcold
### PBS -v avgT
#####PBS -N GROMACS-$LOGNAME-$INSTANCE-$TRAJ_COUNT
#PBS -l nodes=1:ppn=1,mem=2gb,walltime=24:00:00
#PBS -o $INSTANCE/$TRAJ_COUNT-std.io
#PBS -e $INSTANCE/$TRAJ_COUNT-std.err
#PBS -M inonshar@tau.ac.il
#PBS -m a
### Mail options: a - on abort , b - on begin , e - exit

#Explanation of the PBS header:
#use 1 processor on 1 node and a maximal wall-time of three days
#forward the -v environment variables along with the default ones (PBS_O_*) from the head node to the compute node
#put the standard I/O and error files in the sub-directory called $INSTANCE of the $PBS_O_INITDIR where this script was called. use the $TRAJ_COUNT to identify this trajectory's files.
#Make sure there are no space characters in the paths!
echo "SHELL = $SHELL"
######! /bin/bash

#set this script to exit immediately if any command returns an unsuccessfull exit code. This can be reversed using "set +e"
set -e

date
#if [ 1 -eq $bProduction ] ; then ulimit -Sv 900000 ; fi # impose soft usage limit on virtual memory for this shell instance (and all forked processes) of 0.9 GB.
#ulimit -Sv 900000 -Sd 9000000 # limit data segment size, as well
# more specific alternative limits:
# 1. setrlimit -- limit a specific process (e.g. a specific instance of mdrun_d, only).
# 2. cgroups -- limit a user specified control group of processes.
ulimit -c unlimited
ulimit -a
echo
echo =======================================================================================================================
echo ; echo copying to scratch directory from head node:; echo

#part 1: make a sub-directory in the local scratch directory, change to that directory, and copy the contents of the origin dirctory there

pwd
HAS_TREE=0
if [ -f /usr/bin/tree ] ; then HAS_TREE=1 ; fi
if [ 1 -eq $HAS_TREE ] ; then tree -L 1 ; fi
echo
echo ==============================
echo PBS_JOBID = $PBS_JOBID
echo PBS_O_INITDIR = $PBS_O_INITDIR
echo ==============================
echo
#Make a sub-directory under the scratch directory on the compute (remote) node, where GROMACS will get its input files and send its output files, and change to that directory
mkdir -p /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
cd /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
pwd
if [ 1 -eq $HAS_TREE ] ; then tree -L 1 ; fi
echo
#Copy to the current directory the entire contents of the original directory (under the $HOME directory) where the original script calling this one was run

#for i in $(find $PBS_O_INITDIR -maxdepth 1 -type f -size -1000k) ; do if [ -f $i ] ; then  scp -v $i . ; if [ 0 != $? ] ; then echo -e "\nError! scp $i . returned \"$?\"\n" ; fi ; fi ; done

#for i in $(find $PBS_O_INITDIR -maxdepth 1 -type f -size -1000k) ; do rsync -va $i . ; if [ 0 != $? ] ; then echo -e "\nError! rsync -va $i . returned \"$?\"\n" ; fi ; done
#for i in $(find $PBS_O_INITDIR -maxdepth 1 -type f -size -1000k) ; do scp -v $i . ; if [ 0 != $? ] ; then echo -e "\nError! scp $i . returned \"$?\"\n" ; fi ; sleep 1 ; done
prerequisites=($PBS_O_INITDIR/"conf.gro" $PBS_O_INITDIR/"topol.top" $PBS_O_INITDIR/"parse-*.out" $PBS_O_INITDIR/"SingleTrajectory-*.sh")
if [ 1 -eq $bProduction ] ; then prerequisites+=($PBS_O_INITDIR/"topol-tss.tpr" $PBS_O_INITDIR/"traj.trr") ; fi
for i in ${prerequisites[@]} ; do scp -v $i . ; if [ 0 != $? ] ; then echo -e "\nError! scp $i . returned \"$?\"\n" ; fi ; sleep 1 ; done


echo
#ls -lX ; echo
#ls {traj0.trr,traj.trr} -lX ; echo

echo =======================================================================================================================

PREFIX="$HOME/gmx-heat-conduction/bin/gromacs/bin/"
if [ ! -d $PREFIX ]
	then PREFIX=""
fi

GROMPP=$PREFIX"grompp_d"
MDRUN=$PREFIX"mdrun_d"
GMXDUMP=$PREFIX"gmxdump_d"
MKNDX=$PREFIX"make_ndx_d"
EDITCONF=$PREFIX"editconf_d"
GTRAJ=$PREFIX"g_traj_d"
GENERGY=$PREFIX"g_energy_d"
NMEIG=$PREFIX"g_nmeig_d"

echo ----------------------------------------------------------------------------------------------------------------------

#make generic topol.top file
function mktop(){

echo -e "; Include forcefield parameters
#include \"ffG43a1.itp\"
; Included topology
#include \"topol.itp\"


[system]
Generic System Name

[ molecules ]
DRG 1

" > topol.top

}


#make index file
function mkndx(){
#create make_ndx script for molecule of size "natoms"
rm -f make_ndx.txt # remove any previous make_ndx script, as the following function only appends lines to it
i=1
while [ $i -le $natoms ]; do
echo -e "a $i\nname $[2 + $i] $[$i - 1]" >>make_ndx.txt # append to end of make_ndx.txt. Default groups already created are: 0 system, 1 , 2 ResidueName. therefore we start with atom 0 as group 3 (echo -e allows insertion of EOLN characters)
i=$[1 + $i] # increment atom index counter
done
echo -e "q\n" >>make_ndx.txt #input q(uit) signal for interactive make_ndx program

#run make_ndx using the script as input
cat make_ndx.txt | $MKNDX -f -quiet

#clean up after yourself
rm make_ndx.txt


#DEV NOTE:
#Add the appropriate index groups as energy_grps to grompp.mdp!
}

#Energy Minimization Subroutine
function em(){

echo ; date +20%y-%m-%d\ %H:%M:%S%N ; echo

#create Energy Minimization pre-processor options file
echo -e "nsteps              =  10000
constraints	    =  none
integrator	    =  cg
emtol		    =  1e-12
coulombtype         =  Cut-off
optimize_fft  	    =  yes\n" > em.mdp
if [ "$?" != "0" ] || [ ! -f em.mdp ] ; then echo "Error creating em.mdp. Exiting."; ls ; echo ; ls core* ; echo ; scp -v *core* $PBS_O_INITDIR/$INSTANCE/ ; exit 1; fi;
cat em.mdp

if [ -f conf.gro ]; then cp conf.gro conf_b4em.gro ; fi
#sometimes we need energy minimization to ignore one gromacs pre-processor warning (the one about using cut-offs) then we add the flag -maxwarn 1
if [ -f em.trr ]; then #if em.trr exists then use it, otherwise don't
	$GROMPP -f em -n -quiet -t em
else
	$GROMPP -f em -n -quiet
fi
#if grompp did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "grompp error in EM. exiting.";
	ls
	echo
	ls core*
	echo
	scp -v *core* $PBS_O_INITDIR/$INSTANCE/
	exit 1;
	fi;

#remove forces and trajectory output from previous runs
rm -f forces.txt trajectory.txt

#perform molecular dynamics run
$MDRUN -quiet -v -nt 1 -o em
#if mdrun did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
        scp md.log $PBS_O_INITDIR/$INSTANCE/md-$INSTANCE-$TRAJ_COUNT.log
	exit 1
else
	scp em.trr $PBS_O_INITDIR/$INSTANCE/em-$TRAJ_COUNT.trr
fi

#center entire molecule in box, and rename new configuration file
#DEV NOTE: This supposedly aligns the molecule with the box's principle axes.
#DEV NOTE: Alignment along a given vector, e.g. by issuing the flag -align 1 0 0,
#DEV NOTE: does not seem to align the molecule with any box vector either
if [ 1 -lt $natoms ] ; then 
	echo 0 0 0 | $EDITCONF -f confout -n -c -princ -quiet
fi
#if editconf did not return valid exit code
if [ "$?" != "0" ] ; then
        echo "editconf error. exiting.";
	scp confout.gro $PBS_O_INITDIR/$INSTANCE/confout-$TRAJ_COUNT.gro
        exit 1;
        fi;
mv out.gro conf.gro

#rename new energy minimized full-percission trajectory
mv confout.gro conf_em.gro
mv conf_b4em.gro conf.gro
cp conf_em.gro $PBS_O_INITDIR/$INSTANCE/conf_em-$TRAJ_COUNT.gro
#remove unnecessary files
rm -f em.mdp ener.edr md.log mdout.mdp *#
}


#normal mode analysis -- perform high percission energy minimization first! Fmax<<1e-10
function nm(){

#create Normal Mode pre-processing options file
echo -e "integrator = nm\n" > nm.mdp

#gromacs pre processing
$GROMPP -f nm -n -quiet -maxwarn 0 -t em
#if grompp did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "grompp error in NM. exiting.";
	exit 1;
	fi;

scp topol.tpr $PBS_O_INITDIR/$INSTANCE/nm-$INSTANCE-$TRAJ_COUNT.tpr

#remove forces and trajectory output from previous runs
rm -f forces.txt trajectory.txt

#perform molecular dynamics run
$MDRUN -mtx hessian.mtx -quiet -v -nt 1 -o nm
#if mdrun did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
        scp md.log $PBS_O_INITDIR/$INSTANCE/md-$INSTANCE-$TRAJ_COUNT.log
	exit 1;
	fi;

#perform normal mode analysis to find eigenfrequencies and eigenvectors
$NMEIG -quiet -last 999
#if nmeig did not return valid exit code
if [ "$?" != "0" ] ; then
        echo "g_nmeig error. exiting.";
        exit 1;
        fi;

#print Hessian matrix to screen
$GMXDUMP -mtx hessian.mtx

#remove unnecessary files
rm -f md.log mdout.mdp nm.mdp nm.trr *#

#print eigenfrequencies (in wavenumbers) to screen (XMGrace format)
cat eigenfreq.xvg
scp eigenfreq.xvg $PBS_O_INITDIR/$INSTANCE/eigenfreq-$INSTANCE-$TRAJ_COUNT.xvg
scp eigenvec.trr  $PBS_O_INITDIR/$INSTANCE/eigenvec-$INSTANCE-$TRAJ_COUNT.trr
}


#make grompp.mdp
function mkgrompp(){

#set integrator
case "$integrationAlgorithm" in
	d) integrator="md";;              #leap-frog algorithm
        v) integrator="md-vv";;      	#velocity Verlet
	a) integrator="md-vv-avek";;      #velocity Verlet using averaged Kinetic Energy of two half-steps
        s) integrator="sd";;              #stochastic dynamics (solve Langevin equation)
	t) integrator="sd1";;		#resource-efficient stochastic dynamics
esac

echo "integrator = $integrator"

#set temperature coupling

# can't leave uncoupled atoms out of some tc-grp. Must give all atoms some relaxation time...
#sdNoCoupl="-1" # not implemented although stated so in manual
#sdNoCoupl="999999999999.999" # any bigger and GMX won't handle it
sdNoCoupl=1e11 # (22-10-12): energy conservation nonsense errors avoided by using integer value (unsure why...)

if [ $Nbaths -eq 0 ] ; then	#set up microcannonical ensemble (NVE)
	echo "NVE"
	thermostat="no"
	i=0
	while [ $i -lt $natoms ]
	do
		tau_t[$i]=0
		if [ "$integrator" == "sd" -o "$integrator" == "sd1" ] ; then 
			tau_t[$i]=$sdNoCoupl
			#echo "DEBUG (2-7-12): tau_t = ${tau_t[@]}"
		fi
		ref_t[$i]=0
		i=$[ $i + 1]
	done
	#if [ 1 -eq $natoms ] ; then 
	#	if [ "sd" == "$integrator" ]  ; then 
	#		tau_t[0]=$sdNoCoupl
	#		ref_t[0]=0
	#	fi
	#fi
else				#set up cannonical ensemble (NVT)
	thermostat="v-rescale" # Berendsen MD thermostat with additional stochastic term
	#thermostat="nose-hoover" # Deterministic (re-runable) MD thermostat
	tau1=1
	tau2=1
	bGEN_VEL="no"
	if [ 1 -eq $bVgen ]
		then if [ 0 -eq $bProduction ]
			then bGEN_VEL="yes"
			#echo "DEBUG:       bVgen=$bVgen"
		fi
	fi

	#set thermostat/s relaxation time
	case "$Nbaths" in 
			0)   tauBath1=0;tauBath2=0;echo "NVE";;
                        1)   tauBath1=$tau1;tauBath2=0;echo "NVT";;
                        2)   tauBath1=$tau1;tauBath2=$tau2;echo "NVT1T2";;
                        [?]) tauBath1=0;tauBath2=0;echo "unrecognized ensemble";;
                esac # end BASH "case"
	#echo "$Nbaths $tauBath1 $tauBath2 DEBUG 21-6-12"

	if [ "$integrator" == "sd" -o "$integrator" == "sd1" ]; then
		if [ 0 -eq $tauBath1 ] ; then tauBath1=$sdNoCoupl ; fi
		if [ 0 -eq $tauBath2 ] ; then tauBath2=$sdNoCoupl ; fi
	fi

	TcoupleFlag=0

	#Find terminals
	rm -f SulfurAtoms.txt

	#29-5-12
	if [ 2 -eq $(sed -n '2,$p' conf.gro | grep ' S ' | awk '{print (/^\// ? $2 : $3) }' | wc -l) ]
		then sed -n '2,$p' conf.gro | grep ' S ' | awk '{print (/^\// ? $2 : $3) }' > SulfurAtoms.txt ; echo -e "\nTwo Sulfur atom terminal setup found.\n"
	else if [ 2 -eq $(sed -n '2,$p' conf.gro | grep ' A[uU] ' | awk '{print (/^\// ? $2 : $3) }' | wc -l) ]
		then sed -n '2,$p' conf.gro | grep ' A[uU] ' | awk '{print (/^\// ? $2 : $3) }' > SulfurAtoms.txt ; echo -e "\nTwo Gold atom terminal setup found.\n"
		#else echo -e "\nNo singular two-terminal setup found (neither two Sulfur nor two Gold atoms). Exiting. \n" ; exit 19
		else echo -e "1\n$natoms" > SulfurAtoms.txt  ; echo "First and last atomic indexes are coupled to hot and cold bath, respectively."
	
		fi
	fi

	#sed -n '2,$p' conf.gro | grep S | awk '{print (/^\// ? $2 : $3) }' > SulfurAtoms.txt
	declare -a SulfurAtoms ; i=1
	while read SulfurAtoms[$i] ; do
		i=$[$i+1]
	done < SulfurAtoms.txt
	#echo "DEBUG:" ; i=1 ; while [ $i -le 2 ] ; do echo $i ${SulfurAtoms[$i]} ; i=$[$i+1] ; done
	#echo
	#echo ${SulfurAtoms[1]}
	#echo ${SulfurAtoms[2]}
	#echo

	i=0
        while [ $i -lt $natoms ]
        do
		#echo -n $i ; echo
		#if [ [ $i -eq $[ ${SulfurAtoms[1]} - 1 ] ] -o [ $i -eq $[ ${SulfurAtoms[2]} - 1 ] ] ]
                #if [ $i -eq $[ ${SulfurAtoms[1]} - 1 ] ]

		#Set terminals
		SulfurFound=2
		case "$i" in
		        $[${SulfurAtoms[1]} - 1 ]) SulfurFound=1;;
			$[${SulfurAtoms[2]} - 1 ]) SulfurFound=1;;
		        [?]) SulfurFound=0;;
			#*)
		esac # end BASH "case"

		#Set thermostats
		if [ 1 -eq $SulfurFound ] ; then
			case "$TcoupleFlag" in 
                        	#0) tau_t[$i]=$tauBath1 ; ref_t[$i]=$[ $avgT + $DeltaT / 2 ]  ; echo -e "\nFirst (hot) bath found.\n";;
	                        #1) tau_t[$i]=$tauBath2 ; ref_t[$i]=$[ $avgT - $DeltaT / 2 ]   ; echo -e "\nSecond (cold) bath found.\n";;
                          0) tau_t[$i]=$tauBath1 ; ref_t[$i]=$(echo $Tcold $DeltaT | awk '{print($1 + $2)}')  ; echo -e "\nFirst (hot) bath found.\n";;
                          1) tau_t[$i]=$tauBath2 ; ref_t[$i]=$(echo $Tcold $DeltaT | awk '{print($1)}')   ; echo -e "\nSecond (cold) bath found.\n";;
        	                [?]) echo Problem -- DEBUG;;
                	esac # end BASH "case"
			TcoupleFlag=$[1+$TcoupleFlag]
		else
                        ref_t[$i]=0
                        if [ "$integrator" == "sd" -o "$integrator" == "sd1" ] ; then 
				tau_t[$i]=$sdNoCoupl 
			else 
				tau_t[$i]=0 
			fi  
		fi

#			then 
#			if   [ "0" -eq $TcoupleFlag ] ; then tau_t[$i]="1" ; ref_t[$i]=0   ; echo -e "\nFirst (cold) bath found.\n"
#			else	if [ "1" -eq $TcoupleFlag ] ; then tau_t[$i]="1" ; ref_t[$i]="300" ; echo -e "\nSecond (hot) bath found.\n"
#				else echo -e "\nOnly two anchor atoms are allowed (2-terminal junctions only for now). Exiting.\n" ; exit 10
#				fi
#			TcoupleFlag=$[1+$TcoupleFlag]
#			fi
#		else
#			tau_t[$i]=0
#			ref_t[$i]=0 # irrelevant
#		fi
                i=$[ $i + 1]
        done
	if [ 1 -lt $natoms ] ; then if  [ 2 -ne $TcoupleFlag ] ; then echo -e "\nTwo anchor atoms have not been found (only $TcoupleFlag). Exiting...\n" ; exit 23 ; fi
	#else tau_t[0]=$tauBath1 ; ref_t[0]=$avgT
	fi
	#tau_t[0]=1
	#tau_t[ $[ $natoms - 1 ] ]=1
	#ref_t[0]=300
	#ref_t[ $[ $natoms - 1 ] ]=0
fi

#print grompp.mdp file
echo "integrator	=	$integrator
dt		=	$dt
nsteps		=       $SimDuration
;
;------------------------------------------------------------------------------------------------
nstlog		=	1
nstcalcenergy	=	1
;------------------------------------------------------------------------------------------------
;
pbc		=	no		; no periodic boundary conditions (only one molecule)
comm_mode	=	None		;\"Angular\" =  remove center of mass translatory AND rotational motion
;comm_mode	=	Angular		; (13-8-12): For N<=3 (i.e. no anchor atoms)
nstcomm		= 	1		; number of steps between removal of center of mass motion
;
;------------------------------------------------------------------------------------------------
; number of steps between writing out of different phase data and energetics
nstxout		=	1
nstvout		=	1
nstfout		=	1
nstenergy	=	1
; groups to write energetics of" >> grompp.mdp
echo -n "energygrps	=	" >> grompp.mdp
#for i in {1..$natoms} ; do echo -n "$i  " >> grompp.mdp ; done
#i=0 ; while [ $i -le $[$natoms-1] ] ; do echo -n "$i   " >> grompp.mdp ; i=$[ $i + 1 ] ; done

#       IN PROGRESS: loop over elements of the one-dimensional array $outputatoms
i= # empty $i out
#echo ${outputatoms[@]}
for i in "${outputatoms[@]}" ; do echo -n "$i	" >> grompp.mdp ; done


echo "
;
;-------------------------------------------------------------------------------------------------
; neighbor list search
nstype		=	simple		; particle (as opposed to domain) decompositon
coulombtype	=	cut-off
;
;-------------------------------------------------------------------------------------------------
;
; Langevin dynamics temperature coupling
;
ld_seed         =       $TRAJ_COUNT	;  (1993) [integer]
tcoupl		=	$thermostat
nsttcouple	=	1
;" >> grompp.mdp
echo -n "tc-grps		=	" >> grompp.mdp
#for i in {1..$natoms} ; do echo -n "$i	" >> grompp.mdp ; done
#i=0 ; while [ $i -le $[$natoms-1] ] ; do echo -n "$i    " >> grompp.mdp ; i=$[ $i + 1 ] ; done

i=0
while [ $i -lt $natoms ]
do
	echo -n "$i	" >> grompp.mdp
	i=$[ $i + 1 ]
done

echo >> grompp.mdp ; echo -n "tau_t		=	" >> grompp.mdp
#for i in {1..$natoms} ; do echo -n "${tau_t[$i]}  " >> grompp.mdp ; done
#i=0 ; while [ $i -le $[$natoms-1] ] ; do echo -n "${tau_t[$i]}    " >> grompp.mdp ; i=$[ $i + 1 ] ; done

i=0
while [ $i -lt $natoms ]
do
	echo -n "${tau_t[$i]}	" >> grompp.mdp
	i=$[ $i + 1 ]
done

echo -n "		; mass/gamma [a.m.u. nanosecond]" >> grompp.mdp
echo >> grompp.mdp ; echo -n "ref_t		=	" >> grompp.mdp
#for i in {1..$natoms} ; do echo -n "$i  " >> grompp.mdp ; done
#i=0 ; while [ $i -le $[$natoms-1] ] ; do echo -n "${ref_t[$i]}    " >> grompp.mdp ; i=$[ $i + 1 ] ; done

i=0
while [ $i -lt $natoms ]
do
	echo -n "${ref_t[$i]}	" >> grompp.mdp
	i=$[ $i + 1 ]
done

echo "	; reference (bath) temperature" >> grompp.mdp
echo ";-------------------------------------------------------------------------------------------------
;
; Velocity generation
gen_vel	=	$bGEN_VEL		; (no)
gen_temp  = $(echo $Tcold $DeltaT | awk '{print($1 + $2 / 2)}')   ; (300) [K]
gen_seed =  $TRAJ_COUNT   ; (-1) = from PID" >> grompp.mdp
#gen_temp	=	$avgT		; (300) [K]
}


#Molecular Dynamics Subroutine
function md(){

bDO_MK_MDP=0
if ( [ d != $integrationAlgorithm ] || [ 0.001 != $dt ] || [ 4 -ne $SimDuration ] || [ 0 -ne $Nbaths ] ) ; then bDO_MK_MDP=1 ; fi
if [ ! -f grompp.mdp ] ; then echo -e "\ngrompp.mdp does not exist." ; bDO_MK_MDP=1 ; fi 
if [ 1 -eq $bDO_MK_MDP ] ; then echo -e "\nCreating new grompp.mdp now.\n" ; rm -f grompp.mdp ; mkgrompp ; if [ "$?" != "0" ] ; then echo "Error in making grompp.mdp . Exiting."; exit 1; fi; fi

#14-8-12
if [ 1 -eq $TRAJ_COUNT ] ; then scp grompp.mdp $PBS_O_INITDIR/$INSTANCE/grompp-$INSTANCE-$TRAJ_COUNT.mdp ; fi

echo ; date +20%y-%m-%d\ %H:%M:%S%N ; echo
if [ -f conf_md.gro ] ; then
	mv conf.gro conf_b4md.gro;
	cp conf_md.gro conf.gro;
	fi;
#if [ -f traj.trr] ; then	cp traj.trr traj_b4md.trr ; fi ; #for cases where a specific phase space coordinate is given for the starting positions & velocities

if [ 1 -eq $bProduction ] ; then 
	if [ -f traj.trr ] ; then
		if [ ! -s traj.trr ] ; then
			echo "traj.trr file exists, but is of zero size!"
			exit 784
		else
			#if [ $(echo "8 + 2 * $natoms" | bc) -eq $($GMXDUMP -quiet -f traj.trr | wc -l) ]
			#	then echo "Bad traj.trr -- see log file. Aborting." ; exit 117
			#fi
			echo "GROMACS preprocessing using existing trajectory file (traj.trr)"
			set +e
			$GROMPP -n -quiet -maxwarn 1 -t # one warning due to choosing no COMM
			gromppErrorCode=$?
			set -e
			#scp traj.trr $PBS_O_INITDIR/$INSTANCE/traj-tss-$TRAJ_COUNT.trr # Show *.trr file which was read in for this trajectory. Does it contain the initial velocities?

			#$GMXDUMP -f traj.trr # DEBUG 16-1-13
			mv traj.trr traj-in.trr # 17-7-13
		fi
	else
	        echo "traj.trr missing from production run. exiting." 
		exit 1
	fi
else
	if [ 1 -eq $bDO_EM ] ; then 
		$GROMPP -n -t em -quiet -maxwarn 1 
	else 
		$GROMPP -n -quiet -maxwarn 1 
	fi
	gromppErrorCode=$?
fi

if [ 0 -ne $gromppErrorCode ] ; then 
	echo "grompp error code $gromppErrorCode in MD. exiting."
	scp -v grompp.mdp $PBS_O_INITDIR/$INSTANCE/grompp-$INSTANCE-$TRAJ_COUNT.mdp
	if [ 1 -eq $bProduction ]
		then 
	  scp -v traj-in.trr $PBS_O_INITDIR/$INSTANCE/traj-in-$TRAJ_COUNT.trr
		$GMXDUMP -quiet -f traj.trr
		cat conf.gro
	fi
	exit 1
fi

if [ ! -f topol.tpr ] ; then
	echo "Failure to create topol.tpr by grompp. exiting."
	$GROMPP -v -verb 3 -debug 3 -n -quiet -maxwarn 0
	exit 2
fi

#remove forces and trajectory output from previous runs
rm -f forces.txt trajectory.txt

$MDRUN -quiet -v -nt 1
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
	scp -v md.log $PBS_O_INITDIR/$INSTANCE/md-$INSTANCE-$TRAJ_COUNT.log
  scp -v state.cpt $PBS_O_INITDIR/$INSTANCE/state-$INSTANCE-$TRAJ_COUNT.cpt
	grep -i kill /var/log/messages
	if [ 1 = $TRAJ_COUNT ]; then
		valgrind --version
		if [ 0 = $? ]; then
			valgrind -v --log-file=val-md-$TRAJ_COUNT.log --leak-check=full --track-origins=yes $MDRUN -quiet -v -nt 1
		        scp -v val-md-$TRAJ_COUNT.log $PBS_O_INITDIR/$INSTANCE/val-md-$INSTANCE-$TRAJ_COUNT.log
		else
			scp -v topol.tpr $PBS_O_INITDIR/$INSTANCE/topol-$INSTANCE-$TRAJ_COUNT.tpr
		fi
	fi
	exit 1;
fi;

#rm -f state* confout.gro md.log mdout.mdp *#
rm -f state* md.log mdout.mdp *#
#scp confout.gro $PBS_O_INITDIR/$INSTANCE/confout-$TRAJ_COUNT.gro
#scp traj.trr            $PBS_O_INITDIR/$INSTANCE/traj-$TRAJ_COUNT.trr
#scp topol.tpr           $PBS_O_INITDIR/$INSTANCE/topol-$TRAJ_COUNT.tpr

##delete empty lines from forces.txt
#sed '/^$/d' forces.txt > forcesNoEmptyLines.txt
#mv forcesNoEmptyLines.txt forces.txt
}


#Energy analysis (using g_energy) subroutine
function g_nrg(){

#DEV NOTE: in g_energy also include input indexes for many-atom interaction functions and for local temperatures (a function of natoms...)
case "$natoms" in
        2) G_ENERGY_GRPS="38 39" ;; # G_ENERGY_GRPS="37 38" ;;
        3) G_ENERGY_GRPS="43 44 45" ;; # for k_tau = 0 for all tau != bond_stretching
	16) G_ENERGY_GRPS="9 10 11 12 580 582 585 588 591 594";;
        [?]) echo -e "\nUnknown number of atoms, don't know which energy groups to select. Aborting...\n"; break ; return 1 ;;
esac

#G_ENERGY_GRPS="8 9 10 11 12 580 582 585 588 591 594"
if [ 1 -eq $bDO_G_ENERGY ] ; then echo -e "\nRunning GROMACS energy analysis utility...\n" ; echo -e "$G_ENERGY_GRPS 1 4 5 6 7 8 0\n" | $GENERGY -s -dp -quiet ; if [ "$?" != "0" ] ; then echo "Error in GROMACS energy analysis utility. exiting.\n" ; exit 1 ; fi ; rm -f *.xvg.*# ; fi

if [ 1 -eq $bDO_EOG ] ; then echo "Running XMGrace to view energy data... (close XMGrace to continue)" ; (xmgrace -nxy energy.xvg &) ; fi

scp energy.xvg $PBS_O_INITDIR/$INSTANCE/energy-$TRAJ_COUNT.xvg
}


#GNUPlot script generation subroutine
function mkgp(){

if [ 2 -ge $natoms ] ; then echo -e "\n\nWarning! natoms = $natoms . Graphical output is only meaningful for natoms>2.\n\n" ; fi

echo "print \"	Top of GNUPlot script.\"
set term svg size 1000,1400 dynamic enhanced
set output \"mixed-7-8-11.svg\"

#set term jpeg size 1200,900 enhanced
#set output \"mixed-7-8-11.jpeg\"

set view map
set pal defined (-1 \"blue\", 0 \"black\", 1 \"red\")
set style data image

FIRSTLINES=0

print \"\" ; print \"	GNUPlot setting MultiPlot...\"
#set multiplot title \"Energy Continuity\\n(dynamic localization of potential energies)\"
#set multiplot layout 2,3
set multiplot layout 3,2 rowsfirst

set xlabel \"n\"
print \"	GNUPlot setting x tics...\" " >> 7-8-11.gp

j=1
echo -n "set xtics (" >> 7-8-11.gp
for i in ${outputatoms[@]}
do
	echo -n "$i" >> 7-8-11.gp
	if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> 7-8-11.gp ; fi
	j=$[ $j + 1 ]
done
echo  ")" >> 7-8-11.gp # add x-axis ticks only for atoms in outputatoms

#echo "set xtics 1 		# x axis tics every 1 (integers only)

echo "set ylabel \"t/[ps]\"
#set ytics 0,0.02	# y axis begins at zero with major tics every 2 femtoseconds
set mytics 2 		# y axis minor tic every 1/2 of a major tic

print \"	GNUPlot plotting P(n,t)...\"
set title \"Power as a function of Atomic Site and Time\"
set cblabel \"P/[W]\"
#set cbrange [-1e-17:1e-17]
set zlabel rotate
#set zlabel \"P/[W]\"
set autoscale
set zrange [] writeback
set cbrange [] writeback
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:7 noti # t \"Power as a function of Atomic Site and Time\"

print \"	GNUPlot plotting DeltaE(n,t)...\"
set title \"Local energy change during a time-step\nas a function of Atomic Site and Time\"
#set cblabel \"DeltaE/[W]\"
#set zlabel \"DeltaE/[W]\"
set zrange restore
set cbrange restore
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:6 noti # t \"Energy Difference (per time-step) \\n as a function of Atomic Site and Time\"


print \"	GNUPlot performing curve fitting...\"
FIT_LIMIT = 1e-3
# keep fitting residuals for error estimation
set fit errorvariables
DELAY=25
ONSET=DELAY*$dt
" >> 7-8-11.gp

j=0
for i in "${outputatoms[@]}"
do
	echo "entry # " $j " atom # " $i # to stdout
	#echo -e "print \"		GNUPlot fitting atom #$i...\"\nfit [ONSET:*][*:*] STEADY_STATE_$i \"hroutput$SegLength.xvg\" u 1:$[$j + 1 ] via STEADY_STATE_$i\nprint \"$i \",STEADY_STATE_1,\" \",STEADY_STATE_1_err" >> 7-8-11.gp
	echo "print \"\";print \"\";print \"              GNUPlot fitting atom #$i...\"" >> 7-8-11.gp

#	n=1
#	while [ $N_EXPONENTIALS -ge $n ] ; do
#       	echo -n STEADY_STATE_$n_$i >> 7-8-11.gp
#        	echo -n " = 100" >> 7-8-11.gp
#	        echo >> 7-8-11.gp
#        	echo -n RATE_$n_$i >> 7-8-11.gp
#        	echo -n " = 100" >> 7-8-11.gp
#	        echo >> 7-8-11.gp
#	        n=$[ $n + 1 ]
#	done

	
	#f(x,STEADY_STATE_1_$i,RATE_1_$i,STEADY_STATE_2_$i,RATE_2_$i,STEADY_STATE_3_$i,RATE_3_$i)=STEADY_STATE_1_$i*(1-exp(-x/RATE_1_$i))+STEADY_STATE_2_$i*(1-exp(-x/RATE_2_$i))+STEADY_STATE_3_$i*(1-exp(-x/RATE_3_$i))
	#fit [ONSET:*][*:*] [RATE_1_$i = 0.001:1000] [RATE_2_$i = 0.001:1000] [RATE_3_$i = 0.001:1000] f(x,STEADY_STATE_1_$i,RATE_1_$i,STEADY_STATE_2_$i,RATE_2_$i,STEADY_STATE_3_$i,RATE_3_$i) \"hroutput$SegLength.xvg\" u 1:$[$j + 1 ] via STEADY_STATE_1_$i,RATE_1_$i,STEADY_STATE_2_$i,RATE_2_$i,STEADY_STATE_3_$i,RATE_3_$i" >> 7-8-11.gp
	#fit [ONSET:*][*:*] [RATE_1_$i = 0.001:1000] [RATE_2_$i = 0.001:1000] [RATE_3_$i = 0.001:1000] STEADY_STATE_1_$i*(1-exp(-x/RATE_1_$i))+STEADY_STATE_2_$i*(1-exp(-x/RATE_2_$i))+STEADY_STATE_3_$i*(1-exp(-x/RATE_3_$i)) \"hroutput$SegLength.xvg\" u 1:$[$j + 1 ] via STEADY_STATE_1_$i,RATE_1_$i,STEADY_STATE_2_$i,RATE_2_$i,STEADY_STATE_3_$i,RATE_3_$i" >> 7-8-11.gp
	#print \"$i \",STEADY_STATE_1_$i,\" \",STEADY_STATE_$i_err,\" \",RATE_1_$i,\" \",'RATE_1_$i_err',\" \",STEADY_STATE_2_$i,\" \",'STEADY_STATE_2_$i_err',\" \",RATE_2_$i,\" \",'RATE_2_$i_err',\" \",STEADY_STATE_3_$i,\" \",'STEADY_STATE_3_$i_err',\" \",RATE_3_$i,\" \",'RATE_3_$i_err',\" \",STEADY_STATE_1_$i+STEADY_STATE_2_$i+STEADY_STATE_3_$i
	# The curve being fitted is just a constant, therefore y(x)=A0 is being fitted via the variables: A0

	echo "# Restrict a to the range of [LOW:HIGH] using arcus tangent to span the entire dummy-variable \"x\" axis
	LOW1=1
	HIGH1=10
	RATE_1_$i(x) = (HIGH1-LOW1)/pi*(atan(x)+pi/2)+LOW1
	LOW2=1
	HIGH2=50
	RATE_2_$i(x) = (HIGH2-LOW2)/pi*(atan(x)+pi/2)+LOW2
	LOW3=10
	HIGH3=5000
	RATE_3_$i(x) = (HIGH3-LOW3)/pi*(atan(x)+pi/2)+LOW3
	#STEADY_STATE_1_$i=10
	#STEADY_STATE_2_$i=-20
	LOW1S=-150
        HIGH1S=0
        STEADY_STATE_1_$i(x)= (HIGH1S-LOW1S)/pi*(atan(x)+pi/2)+LOW1S
	LOW2S=-100
        HIGH2S=0
        STEADY_STATE_2_$i(x)= (HIGH2S-LOW2S)/pi*(atan(x)+pi/2)+LOW2S
	LOW3S=-100
        HIGH3S=100
	STEADY_STATE_3_$i(x)= (HIGH3S-LOW3S)/pi*(atan(x)+pi/2)+LOW3S

	f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)*(1-exp(-x/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_2_$i(DUMB_2_$i)))+STEADY_STATE_3_$i(DUMMY_3_$i)*(1-exp(-x/RATE_3_$i(DUMB_3_$i)))

	fit f(x) \"hroutput$SegLength.xvg\" u 1:$[$j + 1 ] via DUMMY_1_$i,DUMB_1_$i,DUMMY_2_$i,DUMB_2_$i,DUMMY_3_$i,DUMB_3_$i

	#print \"\"
	#print\"		0.001-10		1-50		10-1000\"
	#print DUMB_1_$i,RATE_1_$i(DUMB_1_$i),DUMB_2_$i,RATE_2_$i(DUMB_2_$i),DUMB_3_$i,RATE_3_$i(DUMB_3_$i)
	#print \"\"" >> 7-8-11.gp

	#RATE_1_$i\_err=(HIGH1-LOW1)/pi* d atan(x) / dx * DUMB_1_$i\_err
	#RATE_2_$i\_err=((HIGH2-LOW2)/pi)*DUMB_2_$i\_err/(1+DUMB_2_$i*DUMB_2_$i)
	echo >> 7-8-11.gp
	echo -n RATE_1_$i\_err >> 7-8-11.gp
	echo -n "=((HIGH1-LOW1)/pi)*" >> 7-8-11.gp
	echo -n DUMB_1_$i\_err >> 7-8-11.gp
	echo -n "/(1+DUMB_1_$i*DUMB_1_$i)" >> 7-8-11.gp
	echo >> 7-8-11.gp
	echo -n RATE_2_$i\_err >> 7-8-11.gp
	echo -n "=((HIGH2-LOW2)/pi)*" >> 7-8-11.gp
	echo -n DUMB_2_$i\_err >> 7-8-11.gp
	echo -n "/(1+DUMB_2_$i*DUMB_2_$i)" >> 7-8-11.gp	
	echo >> 7-8-11.gp
	echo -n RATE_3_$i\_err >> 7-8-11.gp
	echo -n "=((HIGH3-LOW3)/pi)*" >> 7-8-11.gp
	echo -n DUMB_3_$i\_err >> 7-8-11.gp
	echo -n "/(1+DUMB_3_$i*DUMB_3_$i)" >> 7-8-11.gp
	echo >> 7-8-11.gp

	echo -n STEADY_STATE_1_$i\_err >> 7-8-11.gp
        echo -n "=((HIGH1S-LOW1S)/pi)*" >> 7-8-11.gp
        echo -n DUMMY_1_$i\_err >> 7-8-11.gp
        echo -n "/(1+DUMMY_1_$i*DUMMY_1_$i)" >> 7-8-11.gp
        echo >> 7-8-11.gp
        echo -n STEADY_STATE_2_$i\_err >> 7-8-11.gp
        echo -n "=((HIGH2S-LOW2S)/pi)*" >> 7-8-11.gp
        echo -n DUMMY_2_$i\_err >> 7-8-11.gp
        echo -n "/(1+DUMMY_2_$i*DUMMY_2_$i)" >> 7-8-11.gp
        echo >> 7-8-11.gp
        echo -n STEADY_STATE_3_$i\_err >> 7-8-11.gp
        echo -n "=((HIGH3S-LOW3S)/pi)*" >> 7-8-11.gp
        echo -n DUMMY_3_$i\_err >> 7-8-11.gp
        echo -n "/(1+DUMMY_3_$i*DUMMY_3_$i)" >> 7-8-11.gp
        echo >> 7-8-11.gp
j=$[ $j + 1 ]
done

if [ ! -f $STEADY_STATEname.txt ] ; then touch "$STEADY_STATEname.txt" ; fi
echo "set print \"$STEADY_STATEname.txt\"" >> 7-8-11.gp
for i in "${outputatoms[@]}"
do
	echo -n "print \"$i \"" >> 7-8-11.gp
	n=1
	while [ $N_EXPONENTIALS -ge $n ] ; do
        	echo -n "," >> 7-8-11.gp
	        echo -n STEADY_STATE_$n\_$i >> 7-8-11.gp
		echo -n "(" >> 7-8-11.gp
                echo -n DUMMY_$n\_$i >> 7-8-11.gp
                echo -n ")" >> 7-8-11.gp
	        echo -n ",\" \"," >> 7-8-11.gp
        	echo -n STEADY_STATE_$n\_$i\_err >> 7-8-11.gp
	        echo -n ",\" \"," >> 7-8-11.gp
	        echo -n RATE_$n\_$i >> 7-8-11.gp
	        echo -n "(" >> 7-8-11.gp
	        echo -n DUMB_$n\_$i >> 7-8-11.gp
	        echo -n ")" >> 7-8-11.gp
	        echo -n ",\" \"," >> 7-8-11.gp
	        echo -n RATE_$n\_$i\_err >> 7-8-11.gp
	        echo -n ",\" \"" >> 7-8-11.gp
	        n=$[ $n + 1 ]
	done
	echo >> 7-8-11.gp
done
echo "unset print">> 7-8-11.gp

echo "


print \"	GNUPlot plotting P_n(t)...\"
set title \"Power as a function of Atomic Site and Time\"
set style data points
set pointsize 0.1
set ylabel \"P/[KJ/mole per ps]\"
set xlabel \"Time/[ps]\"
set xtics auto
#set xtics 0.02
set autoscale
#set key below
set key outside
#set key box
#set key samplen 100

print \"	GNUPlot looping over output atoms...\"
#loop \"i\" over natoms: \"natoms::(i+FIRSTLINES)\"
plot \\" >> 7-8-11.gp

#loop "i" over natoms: "natoms::(i+FIRSTLINES)"
#i=0
j=1 # the index "j" is used to identify the last output atom, which is important for the formatting of the plot command
#while [ $i -le $[$natoms-1] ]
for i in "${outputatoms[@]}"
do
#	echo -n "\"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] t \"$i\" , STEADY_STATE_$i w lines lw 0.1 t \"fit $i\"" >> 7-8-11.gp

#print using loop over n in N_EXPONENTIALS

echo -n "\"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] t \"$i\" , STEADY_STATE_1_$i(DUMMY_1_$i)*(1-exp(-x/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_2_$i(DUMB_2_$i)))+STEADY_STATE_3_$i(DUMMY_3_$i)*(1-exp(-x/RATE_3_$i(DUMB_3_$i)))  w lines lw 0.1 t \"fit $i\"" >> 7-8-11.gp
#        echo -n "\"output.txt\" every ${#outputatoms[@]}::($j-1+FIRSTLINES) u 2:7:(0) t \"$i\", STEADY_STATE_$i w l t \"fit $i\"" >> 7-8-11.gp
	
#For all but the last atom, add a comma and an escaped (\) escape character (\) after each output atom for GNUPlot to understand that the newlines should be treated as one long line (one long plot command)
#        if [ $i -le $[$natoms-2] ]
        if [ ${#outputatoms[@]} -gt $j ]
                then echo " ,\\" >> 7-8-11.gp # add \\ and newline character
	fi
#        i=$[ $i + 1 ]
        j=$[ $j + 1 ]

done
echo >> 7-8-11.gp


j=1
echo -n "set xtics (" >> 7-8-11.gp
for i in ${outputatoms[@]}
do
        echo -n "$i" >> 7-8-11.gp
        if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> 7-8-11.gp ; fi
        j=$[ $j + 1 ]
done
echo  ")" >> 7-8-11.gp # add x-axis ticks only for atoms in outputatoms


#set xtics 1             # x axis tics every 1 (integers only)


echo "unset title
unset key

set style data image

print \"	GNUPlot plotting [P-DeltaE](n,t)...\"
set title \"Energy Discontinuity\\n as a function of Atomic Site and Time (discontinuities in white)\"
set pal defined (-2.01 \"white\", -2 \"magenta\", -1 \"purple\", 0 \"black\", 1 \"green\", 2 \"yellow\", 2.01 \"white\")
set cblabel \"[W]\"
set autoscale
#set cbrange restore
#set zrange restore
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:(\$7-\$6) noti # t \"Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"


print \"	GNUPlot plotting T(n,t)...\"
set title \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
set view map
set style data image
set xlabel \"n\" " >> 7-8-11.gp

echo "set ylabel \"t/[ps]\"
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic
set pal defined (0  \"blue\", 1 \"red\")
set cblabel \"Temperature/[K]\"
set autoscale
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:9 noti # t \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"

print \"	GNUPlot plotting [[P-DeltaE]/DeltaE](n,t)...\"
set title \"Relative Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"
set pal defined (-2.01 \"white\", -2 \"magenta\", -1 \"purple\", 0 \"black\", 1 \"green\", 2 \"yellow\", 2.01 \"white\")
set cblabel \"%\"
set autoscale
#set cbrange [-100:100]
set zlabel \"%\" norotate
set zrange [-10:10]
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:8 noti # t \"Relative Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"


unset multiplot
print \"	GNUPlot finished.\"" >> 7-8-11.gp

}

#GNUPlotting steady-state fit from previous GP run
function mk_gp_steady_state_script(){
echo "#set term svg dynamic enhanced size 1000,1400
#set output \"$STEADY_STATEname.svg\"

set term epslatex size 8,11 standalone color
set output \"$STEADY_STATEname.tex\"

#set term postscript eps enhanced size 1000,1400
#set output \"$STEADY_STATEname.eps\"

#set term jpeg enhanced size 1200,850
#set output \"$STEADY_STATEname.jpeg\"

#KJ/mole per ps to Watt=J per second:
NA=6.0221416e+23
GMX2SI=1000*1e+12/NA

#Temperature difference:
DT=$DeltaT

#set multiplot title \"Steady State Local Power, Conductance and Local Transient Rates with errorbars\"
#set multiplot layout 2,3 columnsfirst
set multiplot layout 3,2 rowsfirst

set key below" >> $STEADY_STATEgpscriptname

j=1
echo -n "set xtics (" >> $STEADY_STATEgpscriptname
for i in ${outputatoms[@]}
do
        echo -n "$i" >> $STEADY_STATEgpscriptname
        if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> $STEADY_STATEgpscriptname ; fi
        j=$[ $j + 1 ]
done
echo  ")" >> $STEADY_STATEgpscriptname # add x-axis ticks only for atoms in outputatoms

echo "
set title \"Steady State Local Power\"
set xlabel \"Atomic Site\"
set ylabel \"Power [W]\"
set yrange [] writeback
set style data points" >> $STEADY_STATEgpscriptname

echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI):($[ $i + 1 ]*GMX2SI) t \"\$P_$n(t=\\\infty)\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Steady State Local Power Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI):($[ $i + 1 ]*GMX2SI) t \"\$P_$n(t=\\\infty)\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Conductance\"
set xlabel \"Atomic Site\"
set ylabel \"Conductance [W per K]\"
set autoscale
set style data points
set yrange [] writeback" >> $STEADY_STATEgpscriptname

#plot \"$STEADY_STATEname.txt\" u 1:($2*GMX2SI/DT):($3*GMX2SI/DT) t "k1", "$STEADY_STATEname.txt" u 1:($6*GMX2SI/DT):($7*GMX2SI/DT) t "k2", "$STEADY_STATEname.txt" u 1:($10*GMX2SI/DT):($11*GMX2SI/DT) t "k3", "$STEADY_STATEname.txt" u 1:(($2+$3+$4)*GMX2SI/DT):($3*GMX2SI/DT) t "ktot"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI/DT):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\kappa_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Conductance Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

#plot \"$STEADY_STATEname.txt\" u 1:($2*GMX2SI/DT):($3*GMX2SI/DT) t \"k1\", "$STEADY_STATEname.txt" u 1:($6*GMX2SI/DT):($7*GMX2SI/DT) t "k2", "$STEADY_STATEname.txt" u 1:($10*GMX2SI/DT):($11*GMX2SI/DT) t "k3", "$STEADY_STATEname.txt" u 1:(($2+$3+$4)*GMX2SI/DT):($3*GMX2SI/DT) t "ktot"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI/DT):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\kappa_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Transient Rates\"
set logscale y
set xlabel \"Atomic Site\"
set ylabel \"Relaxation Lifetime [ps]\"
set autoscale
set yrange [] writeback
set style data points" >> $STEADY_STATEgpscriptname

#plot "$STEADY_STATEname.txt" u 1:4:5 t "tau1", "$STEADY_STATEname.txt" u 1:8:9 t "tau2", "$STEADY_STATEname.txt" u 1:12:13 t "tau3"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=4+(n-1)*4
i=$[4 * $n ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i):$[ $i + 1 ] t \"\$\\\tau_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Transient Rates Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

#plot "$STEADY_STATEname.txt" u 1:4:5 t "tau1", "$STEADY_STATEname.txt" u 1:8:9 t "tau2", "$STEADY_STATEname.txt" u 1:12:13 t "tau3"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=4+(n-1)*4
i=$[4 * $n ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i):$[ $i + 1 ] t \"\$\\\tau_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
unset multiplot" >> $STEADY_STATEgpscriptname
}

#GNUPlotting steady-state fit from previous GP run
function gp_steady_state(){

#A4 = 148 mm × 210 mm
#A4 after margins = 145 mm × 200 mm 
#DPI = 300 dots per inch = 118.11023622 dots per cm
#--------------------------
#A4 = 17480.31496063 dots × 24803.1496062 dots = 17480 dots × 24803 dots
#A4 after margins = 17125.984251969 dots × 23622.047244094 dots = 17126 dots × 23622 dots = 404.55 Mdots = 5709 px  × 7874 px = 
# prop. to = 1000 px × 1379 px


STEADY_STATEgpscriptname=$STEADY_STATEname.gp
if [ ! -f $STEADY_STATEgpscriptname ] ; then mk_gp_steady_state_script ; if [ $? -ne 0 ] ; then echo -e "\nError creating steady-state fit GNUPlot script. exiting...\n" ; exit 1 ; fi ; fi
touch STEADY_STATE.aux
gnuplot $STEADY_STATEgpscriptname ; latex STEADY_STATE.tex ; dvipdf STEADY_STATE.dvi #; acroread STEADY_STATE.pdf &
#gnuplot $STEADY_STATEgpscriptname
if [ $? -ne 0 ] ; then echo -e "\nError running steady-state fit GNUPlot script. exiting...\n" ; exit 1 ; fi
#rm $STEADY_STATEgpscriptname
}

#Video encoding GNUPlot scripts
function vidscripts(){
echo -n "#set term gif size 200,200 dynamic enhanced
#set term gif animate transparent opt delay 10 size 200,200 x000000
set term gif enhanced animate delay 10 # delay is given in centiseconds
set output \"image-12-4-11.gif\"

#set initial values, increment size, formatting, etc.
reset
#set title \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
set view map
set style data image
set xlabel \"n\"
set xtics 1
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic
set pal defined (0  \"blue\", 1 \"red\")
set cblabel \"Temperature/[K]\"
set autoscale
counter=1
limit_loop_calls= " > animation-7-8-11.gp
echo -n "$SimDuration" >>animation-7-8-11.gp
echo -n " # set this to an integer product of simulated time-segments

#consider putting multiplot here somehow...
#plot \"output.txt\" every :counter u (\$3+0.5):2:7 t \"Heat Current\\nas a function of Atomic Site and Time\"
plot \"output.txt\" every :counter u (\$3+0.5):2:9 t \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
load \"animation-loop-12-4-11.gp\"

" >> animation-7-8-11.gp

echo -n "#This file is the loopand.
#
#First advance the counter
#Repeat the loop if there isn't an exit condition or if there is one, but it hasn't yet been reached
#       Set up this step's plot
#       Replot
#       Increment whatever needs to be (for the next plot)
#       Re-read this file

counter=counter+1
if ((!limit_loop_calls) || (counter<=limit_loop_calls)) \\
  unset label 1; \\
  print \"		Animating frame\" counter \" out of\" limit_loop_calls \" ...\"; \\
  set label 1 \"Time = %.0f\",counter at graph 0.7,0.9 left tc rgbcolor \"white\" front; \\
  replot; \\
  reread
" > animation-loop-12-4-11.gp
}


#Video encoding subroutine
function vid(){
if [ ! -f animation-7-8-11.gp ] || [ ! -f animation-loop-12-4-11.gp ] ; then echo -e "\nMissing video encoding gnuplot scripts. exiting.\n" ; exit 1 ; fi
gnuplot animation-7-8-11.gp
FileName="image-12-4-11"
if [ -f $FileName.avi ] ; then echo -e "\nOutput file "$FileName.avi" already exists! ffmpeg will overwrite it... (-y flag). Exiting.\n" ; exit 1 ; fi
if [ -d Frames ] ; then rm -r Frames ; fi # if Frames from a previous encoding exist, remove them
mkdir Frames # create temporary directory in which to store the frames
convert $FileName.gif Frames\frame%07d.jpg # to prevent problems in encoding, first convert each frame in the *.gif file to a *.jpg image
if [ $? -ne 0 ] ; then echo -e "\nError in conversion of *.gif frames to *.jpg files. Exiting\n" ; exit 2 ; fi
ffmpeg -r 24 -i Frames\frame%07d.jpg -y -an $FileName.avi # framerate = 24 fps (-r 24), automatic overwrite of previous output authorized (-y), audio not recorded (-an)
if [ $? -ne 0 ] ; then echo -e "\nError in encoding of *.jpg frames to *.avi file. Exiting\n" ; exit 3 ; fi
rm Frames\frame*.jpg # remove *.jpg files (frames)
rmdir Frames
rm $FileName.gif
rm animation-7-8-11.gp animation-loop-12-4-11.gp
}

function xmgraceformat(){
#XMGrace formatting for hroutput$SegLength.xvg
echo "Formatting XMGrace output file..."
echo -e "# this line is a comment line\n@    title \"Local Power\"\n@    xaxis  label \"Time [picoseconds]\"\n@    yaxis  label \"Power [kJ/mole ps\\S-1\\N]\"\n@TYPE nxy\n@ view 0.15, 0.1, 1.25, 0.875\n@ legend on\n@ legend loctype view\n@ legend 0.85, 0.85" > hroutput$SegLength.xvg #Adding legend, titles and axes, and rescaling
#Labeling data series
j=0
for i in ${outputatoms[@]}
do
        echo -e "@ s$j legend \"Power through atom $i\"\n@      s$j linestyle 0\n@      s$j symbol 1\n@ s$j symbol size 0.01" >> hroutput$SegLength.xvg
        j=$[ $j + 1 ]
done
cut -d " "  -f2- hroutput$SegLength.txt >>hroutput$SegLength.xvg #Replacing commas in Comma Seperated Values (CSV) format file by spaces (XMGrace legible)
if [ 1 -eq $bDO_EOG ] ; then xmgrace -n -p hroutput$SegLength.xvg ; fi #Displaying using XMGrace, if requested
}

#main

date
pwd
if [ 1 -eq $HAS_TREE ] ; then tree -L 1 ; fi
$GROMPP -version

SimDuration=$(echo "$SimDuration $SegLength - p" | dc)
numparsefiles=0
for parsefilename in parse-*.out ; do numparsefiles=$[ 1 + $numparsefiles ] ; done
if [ 0 -eq $numparsefiles ] ; then echo -e "\nNo parse files. exiting...\n"; exit 1 ; fi
if [ 1 -lt $numparsefiles ] ; then echo -e "\nMultiple parse files -- keep only the one you want to use. exiting...\n"; exit 1 ; fi

sleep 1
if [ 1 -eq $bProduction ] ; then if [ ! -f traj.trr ] ; then echo -e "\nTrajectory file with necessary initial phase-space configuation did not make it to the compute node. exiting..." ; set +e ; ls traj.trr -ltr ; set -e ; exit 101 ; fi ; fi

if [ ! -f conf.gro ] ; then echo -e "\nMissing necessary molecular initial configuration file. exiting...\n"; exit 1 ; fi
if [ ! -f topol.top ] ; then 
	if [ ! -f topol.itp ]  ; then 
		echo -e "\nMissing necessary molecular included topology file. exiting...\n"
		exit 1
	fi
	echo -e "\nGenerating generic topol.top...\n"
	mktop
	if [ "$?" != "0" ] ; then 
		echo "Error in generation of generic topol.top file. exiting."
		exit 1
	fi
fi

if [ -z $SegLength ] ; then echo "Please enter length of coarse-graining time-segment in time-steps as command line argument." ; exit 1 ; fi ;
touch empty# # to avoid getting an error when trying to delete if there aren't any *# from previous GROMACS runs
natoms=`sed -n '2p' conf.gro` #get number of atoms by reading second line of conf.gro


echo "Creating list of atom indexes for output."

declare -a outputatoms # initialize array (since it's going to be auto-incremented, one element at a time)
atomMassesLine= 
if [ 0 -ne ${#outputatoms[@]} ] ;
	then echo -e "\noutputAtoms failed to initialize: {outputatoms[@]}=${outputatoms[@]}, {#outputatoms[@]}=${#outputatoms[@]}\nexiting."
	exit 1
fi

#if [ -f trajectory.txt ] ; then
if [ -f outputatoms.txt ] ; then
#	echo "Previous trajectory.txt exists."

	#atomMassesLine=( $(<`sed -n '2p' trajectory.txt`) )
	#atomMassesLine=$( < $(sed -n '2p' trajectory.txt) )
	OLDIFS=$IFS
	IN="$( cat outputatoms.txt)"
	#IN="$( echo $(sed -n '2p' trajectory.txt) )"
	set -- "$IN"
	IFS=" "
	#read atomMassesLine ( echo $(sed -n '2p' trajectory.txt) )
#	declare -a atomMassesLine=($*)
	declare -a outputatoms=($*)
	IFS=$OLDIFS
#	if [ 0 -ne $? ] ; then echo "exiting." ; exit 1 ; fi
	#echo "atomMassesLine=${atomMassesLine[@]}"
	#echo "#atomMassesLine=${#atomMassesLine[@]}"
	#if [ 16 -ne ${#atomMassesLine[@]} ] ; then echo -e "{#atomMassesLine}=${#atomMassesLine[@]} (and not 16) exiting." ; exit 1 ; fi

#	i=1
#	for m in ${atomMassesLine[@]}
#	do
	        #if ([ "4.0" <  "$m" ] && [ "300.0" > "$m" ])
		#compare_result1=$(echo "4.0 < $m" | bc)
		#compare_result2=$(echo "300.0 > $m" | bc)
#		compare_result1=$(echo "$m" | awk '{if (4.0 < $1) print 1; else print 0}')
#        	compare_result2=$(echo "$m" | awk '{if (300.0 > $1) print 1; else print 0}')
        	#echo $m $compare_result1 $compare_result2
#		if  [ 1 -eq "$compare_result1" ] && [ 1 -eq "$compare_result2" ]
#			then outputatoms=( "${outputatoms[@]}" "$i" )
#			if [ 0 -ne $? ] ; then echo "exiting." ; exit 1 ; fi
#		fi
		#echo "i=$i m=$m outputatoms=${outputatoms[@]}"
#        	i=$[ $i + 1 ]
#	done
#	if [ 0 -ne $? ] ; then echo "exiting." ; exit 1 ; fi

	#echo "{outputatoms[@]}=${outputatoms[@]}"
	#echo "{#outputatoms[@]}=${#outputatoms[@]}"
	#echo "{outputatoms[*]}=${outputatoms[*]}"
	#echo "{#outputatoms[*]}=${#outputatoms[*]}"
	for i in ${outputatoms[@]} ; do if [[ $i == '' ]] ; then unset ${outputatoms[$i]} ; fi ; done
	#tempoutputatoms=$( echo ${outputatoms[@]//pref*/} )
	#unset outputatoms
	#outputatoms=${tempoutputatoms[@]}
	#echo "{outputatoms[@]}=${outputatoms[@]}"
	#echo "{#outputatoms[@]}=${#outputatoms[@]}"
	#	IN PROGRESS: loop over elements of the one-dimensional array $outputatoms
	#for i in "${outputatoms[@]}"
	#do
	#	echo -n "\"output.txt\" every $natoms::($i+FIRSTLINES) u 2:7:(0) t \"$i\"" >> 21-4-11.gp
	#	if [ $i -le $[$natoms-2] ]
	#		then echo -e " ,\\" >> 21-4-11.gp
	#	fi
	#done
else
	echo "outputatoms.txt does not exist. Preparing initial output atom list of all atoms."
	i=0
	while [ "$natoms" -gt "$i" ] ; do
	        outputatoms=( "${outputatoms[@]}" "$i" )
        	i=$[ $i + 1 ]
	done
fi

echo "output atoms selected: ${outputatoms[@]}"
echo ${outputatoms[@]} >> outputatoms.txt

echo "Finished creating list of atom indexes for output."


if [ 1 -eq $bProduction ] ; then bDO_EM=0 ; bDO_NM=0 ; bDO_MD=1 ; fi

if [ 1 -eq $bDO_NDX ] ; then echo -e "\nGenerating index file...\n" ; mkndx ; if [ "$?" != "0" ] ; then echo "Error in GROMACS generation of index file. exiting."; exit 1; fi; fi;
if [ 1 -eq $TRAJ_COUNT ] ; then scp index.ndx $PBS_O_INITDIR/$INSTANCE/index-$INSTANCE-$TRAJ_COUNT.ndx ; fi

if [ 1 -eq $bDO_EM ] ; then echo -e "\nRunning GROMACS Energy Minimization...\n" ; em ; if [ "$?" != "0" ] ; then echo "Error in GROMACS energy minimization. exiting."; exit 1; fi; fi;

if [ 1 -eq $bDO_NM ] ; then echo -e "\nRunning GROMACS Normal Mode analysis...\n" ; nm ; if [ "$?" != "0" ] ; then echo "Error in GROMACS Normal Mode analysis. exiting."; exit 1; fi; fi;
if [ 1 -eq $bDO_MD ] ; then echo -e "\nRunning GROMACS Molecular Dynamics...\n" ; md ; if [ "$?" != "0" ] ; then echo "Error in GROMACS molecular dynamics. exiting.";	exit 1 ; fi ; fi
if [ 1 -eq $bDO_G_TRAJ ] ; then echo -e "\nRunning GROMACS trajectory analysis utility...\n" ; echo -e "0\n" | $GTRAJ -n -ox -ov -of -fp -quiet ; if [ "$?" != "0" ] ; then echo "Error in GROMACS trajectory analysis utility. exiting.\n" ; exit 1 ; fi ; rm -f *.xvg.*# ; fi
if [ 1 -eq $bDO_G_ENERGY ] ; then echo -e "\nRunning GROMACS energy analysis utility...\n" ; g_nrg ; if [ "$?" != "0" ] ; then echo -e "\nError in GROMACS energy analysis utility. exiting.\n" ; exit 1 ; fi ; fi
#echo -e "\nClearing unnecessary GROMACS output files...\n" ; rm ener.edr topol.tpr traj.trr *# ; if [ "$?" != "0" ] ; then echo "Error in deleting unnecessary GROMACS output files. exiting.";   exit 1; fi;

date
free -m
if [ 1 -eq $bDO_MD ] ; then
echo -e "\nRunning parser & heat current calculation...\n"
/usr/bin/time -v ./$parsefilename trajectory.txt forces.txt output$SegLength.txt $SegLength 
ExitCode=$?
if [ $ExitCode != "0" ]
then echo -e "Error in parsing GROMACS output and/or heat current calculation. Exit code $ExitCode not equal to zero. exiting.\n" 
scp trajectory.txt	$PBS_O_INITDIR/$INSTANCE/trajectory-$TRAJ_COUNT.txt
scp forces.txt 		$PBS_O_INITDIR/$INSTANCE/forces-$TRAJ_COUNT.txt
#scp traj.trr            $PBS_O_INITDIR/$INSTANCE/traj-$TRAJ_COUNT.trr
scp topol.tpr           $PBS_O_INITDIR/$INSTANCE/topol-$TRAJ_COUNT.tpr
scp output$SegLength.txt $PBS_O_INITDIR/$INSTANCE/output-$SegLength-$TRAJ_COUNT.txt
#scp RanFor.txt          $PBS_O_INITDIR/$INSTANCE/RanFor-$TRAJ_COUNT.txt
scp $parsefilename	$PBS_O_INITDIR/$INSTANCE/$parsefilename
exit 1
fi #end if exitcode!=0

#if [ "$TRAJ_COUNT" == "1" ] ; then scp trajectory.txt $PBS_O_INITDIR/$INSTANCE/trajectory-$TRAJ_COUNT.txt ; scp forces.txt $PBS_O_INITDIR/$INSTANCE/forces-$TRAJ_COUNT.txt ; fi #end if traj_count=1


if [ ! -f output$SegLength.txt ] ; then echo "output$SegLength.txt not found! Can't copy it to output.txt. Exiting." ; exit 98 ; fi
cp output$SegLength.txt output.txt
#	echo -e "\nRunning parser & heat current calculation...\n" ; ./$parsefilename trajectory.txt forces.txt output$SegLength.csv $SegLength ; if [ "$?" != "0" ] ; then echo "Error in parsing GROMACS output and/or heat current calculation. exiting."; exit 1; fi;
#	echo "Cutting out all the commas in Comma Seperated Value output file..."
#	tr ',' ' ' <output$SegLength.csv >output.txt

if [ -f outputatoms.txt ] ; then
echo "outputatoms.txt exists. Preparing revised output atom list of selected atoms."
OLDIFS=$IFS
IN="$( cat outputatoms.txt)"
set -- "$IN"
IFS=" "
declare -a outputatoms=($*)
IFS=$OLDIFS
        for i in ${outputatoms[@]} ; do if [[ $i == '' ]] ; then unset ${outputatoms[$i]} ; fi ; done
	echo "#{ " ${outputatoms[@]} " } = " ${#outputatoms[@]}
else
        echo "outputatoms.txt does not exist. Preparing revised output atom list of all atoms."
        i=0
        while [ "$natoms" -gt "$i" ] ; do
                outputatoms=( "${outputatoms[@]}" "$i" )
                i=$[ $i + 1 ]
        done
fi #end if DO_MD

if [ 1 -eq $bDO_EOG ] ; then 
	xmgraceformat ; 
	#else cut -d " "  -f2- hroutput$SegLength.txt >> hroutput$SegLength.xvg ; 
fi #Replace commas in CSV format file by spaces and run XMGrace if requested

mv hroutput$SegLength.txt hroutput$SegLength.xvg

#	rm *.gp
#	numgpscripts=0
#	for gpscriptname in *.gp ; do numgpscripts=$[ 1 + $numgpscripts ] ; done
	#if [ ! -f *.gp ] ; then echo -e "\nNo GNUPlot script files. Generating...\n"; mkgp ; if [ "$?" != "0" ] ; then echo "Error in generation of GNUPlot script file. exiting."; exit 1 ; fi ; gpscriptname="7-8-11.gp" ; fi
#	gpscriptname="7-8-11.gp" ; rm $gpscriptname ;  echo -e "\nNo GNUPlot script files. Generating...\n"; mkgp ; if [ "$?" != "0" ] ; then echo "Error in generation of GNUPlot script file. exiting."; exit 1 ; fi
#	if [ 1 -lt $numgpscripts ] ; then echo -e "\nMultiple GNUPlot script files -- keep only the one you want to use. exiting...\n"; exit 1 ; fi
#	echo -e "\nRunning GNUPlot...\n" ; gnuplot $gpscriptname ; if [ "$?" != "0" ] ; then echo "Error in GNUPlotting. exiting."; exit 1; fi;
	#cat hroutput$SegLength.dat
	#rm *.dat *.txt
#	if [ 100000 -le $SimDuration ] ; then rm trajectory.txt forces.txt ; fi
#	if [ 1 -eq $bDO_EOG ] ; then echo ; echo "Running EyeOfGNOME... (close EOG to continue)" ; echo ; (eog mixed-7-8-11.svg &) ; fi

#	echo -e "\nRunning Inkscape conversion from Scalable Vector Graphics format to raster format...\n"
#	if [ 1 -eq $bINKSCAPE ] ; then inkscape -z -f mixed-7-8-11.svg -e mixed-7-8-11.png -d 180 # lossy
#	if [ "$?" != "0" ] ; then echo "Error in Inkscape conversion from SVG to raster. exiting."; exit 1; fi;
#	else convert mixed-7-8-11.svg mixed-7-8-11.png
	#else convert mixed-7-8-11.svg mixed-7-8-11.jpeg
#	fi
	#inkscape -z -f mixed-21-4-11.svg -E mixed-21-4-11.eps #less lossy (note caps in option -E does matter -- not the same as -e)
	
#	echo -e "\nPlotting steady-state values from fit...\n" ; gp_steady_state ; if [ "$?" != 0 ] ; then echo -e "Error plotting steady-state fit. exiting."; exit 1 ; fi
	#inkscape -z -f $STEADY_STATEname.svg -e $STEADY_STATEname.png -d 180 #lossy
	
#	if [ 1 -eq $bDO_VID ] ; then echo -e "\nEncoding video...\n" ; vidscripts ; vid ; if [ "$?" != 0 ] ; then echo -e "Error encoding video. exiting."; exit 1 ; fi ; fi
	
fi ; echo
echo


echo =======================================================================================================================
echo ; echo printing trajectory counter and host name: ; echo

#part 3: interlude

#Print out some environment variables, most importantly the $HOSTNAME of the compute node on which this trajectory was run adn the value of the $TRAJ_COUNT (trajectory counter) which is unique for this trajectory, under this $INSTANCE of running the scripts
echo
echo TRAJ_COUNT=$TRAJ_COUNT, HOSTNAME=$HOSTNAME
echo                            \(compute-0-XX.local\)
echo


echo =======================================================================================================================
echo ; echo copying output to head node: ; echo

#part 4: copy output to sub-directory on $HOME

#Copy the meaningful output files from the current directory to the sub-directory of the $PBS_O_INITDIR which was created uniquely for this $INSTANCE of running the scripts
#scp trajectory.txt              $PBS_O_INITDIR/$INSTANCE/trajectory-$TRAJ_COUNT.txt
if [ 1 -eq $bDO_MD ] ; then
if [ 1 -eq $TRAJ_COUNT ] ; then

	if [ -f TermPairwiseCurrent.txt ] ; then 
		scp TermPairwiseCurrent.txt	$PBS_O_INITDIR/$INSTANCE/TermPairwiseCurrent-$TRAJ_COUNT.txt
	fi
	#if [ -f ener.edr ] ; then
	#scp ener.edr               $PBS_O_INITDIR/$INSTANCE/ener-$TRAJ_COUNT.edr
	#fi

	#scp trajectory.txt         $PBS_O_INITDIR/$INSTANCE/trajectory-$TRAJ_COUNT.txt
	#scp forces.txt             $PBS_O_INITDIR/$INSTANCE/forces-$TRAJ_COUNT.txt

	if [ 0 -eq $bProduction ] ; then
		scp traj.trr 							$PBS_O_INITDIR/$INSTANCE/traj-tss.trr
		scp topol.tpr 						$PBS_O_INITDIR/$INSTANCE/topol-tss.tpr
		#scp ener.edr              $PBS_O_INITDIR/$INSTANCE/ener-$TRAJ_COUNT.edr
	else
		scp traj.trr 							$PBS_O_INITDIR/$INSTANCE/traj-$TRAJ_COUNT.trr
		scp topol.tpr 						$PBS_O_INITDIR/$INSTANCE/topol-$TRAJ_COUNT.tpr
		scp traj-in.trr           $PBS_O_INITDIR/$INSTANCE/traj-in-$TRAJ_COUNT.trr
		#scp ener.edr							$PBS_O_INITDIR/$INSTANCE/ener-$TRAJ_COUNT.edr
	fi

else #14-5-13
	if [ 1 -eq $bProduction ] ; then
		if [ 3 -ge $TRAJ_COUNT ] ; then
	    scp traj.trr              $PBS_O_INITDIR/$INSTANCE/traj-$TRAJ_COUNT.trr
			scp traj-in.trr           $PBS_O_INITDIR/$INSTANCE/traj-in-$TRAJ_COUNT.trr
		fi # 3 -ge $TRAJ_COUNT
	fi # 1 -eq $bProduction
fi # 1 -eq $TRAJ_COUNT

#scp RanFor.txt 		    			  $PBS_O_INITDIR/$INSTANCE/RanFor-$TRAJ_COUNT.txt
scp BondPower.txt							$PBS_O_INITDIR/$INSTANCE/BondPower-$TRAJ_COUNT.txt
scp output.txt                $PBS_O_INITDIR/$INSTANCE/output-$TRAJ_COUNT.txt
scp current.txt               $PBS_O_INITDIR/$INSTANCE/current-$TRAJ_COUNT.txt
scp currenthr.txt							$PBS_O_INITDIR/$INSTANCE/currenthr-$TRAJ_COUNT.txt
scp hroutput$SegLength.xvg		$PBS_O_INITDIR/$INSTANCE/hroutput-$SegLength-$TRAJ_COUNT.xvg
#scp hroutput$SegLength.xvg		$PBS_O_INITDIR/$INSTANCE/
#scp mixed-7-8-11.svg					$PBS_O_INITDIR/$INSTANCE/$TRAJ_COUNT.svg
#scp mixed-7-8-11.png					$PBS_O_INITDIR/$INSTANCE/$TRAJ_COUNT.png
#scp STEADY_STATE.pdf					$PBS_O_INITDIR/$INSTANCE/$TRAJ_COUNT.pdf

scp *.xvg											$PBS_O_INITDIR/$INSTANCE/

#14-8-12
#if [ 1 -eq $TRAJ_COUNT ] ; then scp grompp.mdp $PBS_O_INITDIR/$INSTANCE/$INSTANCE-$TRAJ_COUNT.mdp ; fi

export dt ; if [ 0 != $? ] ; then echo -e "\ndt could not be exported back to head node... Exiting.\n" ; exit 11 ; fi
export N_EXPONENTIALS ; if [ 0 != $? ] ; then echo -e "\nN_EXPONENTIALS could not be exported back to head node... Exiting.\n" ; exit 11 ; fi

fi # 1 -eq $bDO_MD

if [ ! -f outputatoms.txt ] ; then echo "outputatoms.txt not found!" ; exit 119 ; fi
scp -v outputatoms.txt        $PBS_O_INITDIR/$INSTANCE/
if [ 0 -ne $? ] ; then echo "scp outputatoms.txt failed" ; exit 291 ; fi
#scp atomsmasses.txt          $PBS_O_INITDIR/$INSTANCE/

echo =======================================================================================================================
echo ; echo cleaning up compute node: ; echo

#part 5: cleanup compute /scratch

pwd
if [ 1 -eq $HAS_TREE ] ; then tree -L 1 ; fi
rm *

#Change directory to the sub-directory on the scratch directory named as the user's $LOGNAME and specifically for this $INSTANCE, remove the sub-directory with this $TRAJ_COUNT
cd /scratch/$LOGNAME/$INSTANCE
if [ -d $TRAJ_COUNT ] ; then rm -r $TRAJ_COUNT ; else echo "Couldn't find trajectory $TRAJ_COUNT directory to be removed in clean-up. Exiting." ; exit 75 ; fi

echo
date
echo
echo =======================================================================================================================
echo ; echo Normal termination of Single Trajectory Script. Exiting.

#Exit with succesful exit status ID = 0
exit 0
