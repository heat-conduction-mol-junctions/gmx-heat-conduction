#! /usr/local/bin/gnuplot

set datafile separator ','
set datafile missing 'nan'

#XTICS="`head -1 data.csv | cut -d, -f1- | sed 's/\, /\ /g'`"
COLUMNHEADERS="`head -1 data.srt | cut -d' ' -f1-`"
print "column headers = ", COLUMNHEADERS
#YTICS="`awk 'BEGIN{getline}{printf "%s ",$1}' data.srt`"
ROWHEADERS="`awk 'BEGIN{getline}{printf "%s ",$3}' instances.txt`"
print "row headers = ", ROWHEADERS
NATOMS=`head -n2 conf.gro | tail -n1 | awk '{print($1)}'`
print "natoms = ", NATOMS

set for [i=1:(words(COLUMNHEADERS) + 1)] xtics ( word(COLUMNHEADERS,i) i-2 )
set for [i=1:words(ROWHEADERS)] ytics ( word(ROWHEADERS,i) i )

set xtic rotate by -90 offset graph 0,graph -0.1

#set terminal jpeg transparent nocrop enhanced size 1600,2000 ; set output 'data.jpeg'
set terminal svg enhanced size 1600,2000 dynamic ; set output 'data.svg'
set multiplot layout 3,1 title "`echo $PWD | awk -F '/' '{print $NF}'`"


set title "Output Data Summary"
unset key
set view map
#set palette rgb 33,13,10
#set palette rgbformulae -3,11,3
set pal rgb 9,13,10 # each of the indeces leads the appropriate RGB component to follow a particular formula. See 'show pal rgbformulae' and then 'test pal'
#p 'data.csv' matrix every ::1:1 with image
#sp for[i=1:NATOMS] 'data.m3d' 3:4:(i) every ::1:1 w pm3d
set pm3d map corners2color c1
#set autoscale fix
`tail -n +2 data.csv | cut -d, -f2- > data.dat`
#`sed -i 's/nan/0/g' data.dat`
sp 'data.dat' matrix
unset pm3d
unset xtic
unset ytic


set title "All Simulated Temperature Profiles"
set xtic 1
set ytic
set datafile separator whitespace
filenames="`cat filenames.txt | while read filename ; do echo -e $filename' ' ; done`"
p [1:NATOMS] for[i in filenames] i w l lc 'black' noti, for[i in filenames] i w yerrorbars t i


set title "All Simulated Power And Heat Currents"
set for [i=1:words(ROWHEADERS)] xtics ( word(ROWHEADERS,i) i+1 )
set xtic rotate by -90 offset 0,graph -0.075

set ylabel "Power / [KJ/mole per ps]"
set y2tic
set y2label "Temperature / [10K]"
set datafile separator ','
set key above

p for [i=1:3] 'data.csv' every ::1 u 0:(column(3+2*i)):(column(4+2*i)):xticlabel(1) w yerrorbars pt 7 t word(COLUMNHEADERS, 3+2*i), 'data.csv' every ::1 u 0:($3 / 10):($3 / 10):(($3 + $4)/10):xticlabel(1) w yerrorbars pt 5 ps 1 t word(COLUMNHEADERS, 3+2*0)
