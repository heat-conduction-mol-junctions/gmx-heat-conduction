reset

#set term jpeg enhanced size 1000,707 # A4 on Sodium
#set term jpeg enhanced size 1200,850 # A4 at home
#set term jpeg enhanced size 1300,920 # Full screen at home
set term jpeg enhanced size 1260,891  # Full window at home
set output "EnergyConservation-23-10-12.jpeg"

outputFILE="output.txt"
SegLength=`cut -d "=" -f2 options.dat | head -n3 | tail -n1` # `head -n3 N.dat | tail -n1`
nsteps=`cut -d "=" -f2- options.dat | head -n1` - SegLength
dt=`cut -d "=" -f2- options.dat | head -n2 | tail -n1`
SimDuration=nsteps*dt
#pr SimDuration
SegSkip=SimDuration/10
SegSkip=1
#pr SegSkip

set xlabel "t / [ps]"
set ylabel "Energy (per time-step) / [KJ/mole]"

set multiplot layout 2,2

p outputFILE u 2:6 every 3:SegSkip:0:1 t "DE_1", '' u 2:6 every 3:SegSkip:1:(SegSkip/2) t "DE_2", '' u 2:7 every 3:SegSkip:0:1 lc 1 ps 0.5 t "P_1", '' u 2:7 every 3:SegSkip:1:(SegSkip/2) lc 2 ps 0.5 t "P_2"

p [(0.9*SimDuration):*] outputFILE u 2:6 every 3::0:1 t "DE_1", '' u 2:6 every 3::1:1 t "DE_2", '' u 2:7 every 3::0:1 lc 1 ps 0.5 t "P_1", '' u 2:7 every 3::1:1 lc 2 ps 0.5 t "P_2"

p [dt*(nsteps-100*SegLength):*] outputFILE u 2:6 every 3::0:1 t "DE_1", '' u 2:7 every 3::0:1 ps 0.2 t "P_1", '' u 2:($6-$7) every 3::0:1 t "DE_1-P_1"

p [dt*(nsteps-100*SegLength):*] outputFILE u 2:6 every 3::1:1 t "DE_2", '' u 2:7 every 3::1:1 ps 0.2 t "P_2", '' u 2:($6-$7) every 3::1:1 t "DE_2-P_2"

unset multiplot
