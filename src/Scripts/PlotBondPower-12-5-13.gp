reset

INSTANCE="`echo $(head -n 11 N.dat | tail -n 1)`"
NATOMS=`head -n2 conf-*.gro|tail -n1` #; pr NATOMS
#BONDPOWERLINES=(NATOMS+1)*NSTEPS
#pr BONDPOWERLINES
TAVG=`head options.dat -n12 | tail -n1 | cut -d "=" -f2-`

CONFFILE="`ls conf-*.gro`"
#pr "Plotting ",CONFFILE
#set term dumb
set term unknown
p CONFFILE u 4:5:3 w labels noti
CONF_X_MIN=GPVAL_X_MIN-(GPVAL_X_MAX-GPVAL_X_MIN)/(NATOMS*5)
CONF_X_MAX=GPVAL_X_MAX+(GPVAL_X_MAX-GPVAL_X_MIN)/(NATOMS*5)
CONF_Y_MIN=GPVAL_Y_MIN-(GPVAL_Y_MAX-GPVAL_Y_MIN)/(NATOMS*5)
CONF_Y_MAX=GPVAL_Y_MAX+(GPVAL_Y_MAX-GPVAL_Y_MIN)/(NATOMS*5)


#set term x11 enhanced
#DATE="`date +%d-%m-%y`"
#set term svg enhanced size 1200,900 ; OUTFILENAME=sprintf("BondPower-%s.svg",INSTANCE) ; set output OUTFILENAME
set term png enhanced size 1200,900 ; OUTFILENAME=sprintf("BondPower-%s.png",INSTANCE) ; set output OUTFILENAME



set title INSTANCE
set multiplot layout 2,2
set xtics 1
set key below


set title "Local Temperature"
p 'output.txt' u (1+$3):9 pt 7 ps 2 t "<T_{i}>", TAVG lt 0 lc rgb "black" t sprintf("%d K",TAVG)


set title "Local Current in the direction of the x axis"
p 'current.txt' u (1+$3):4 pt 7 ps 2 t "<I_{i}^{x}>", 0 lc rgb "black" noti

set title "x-y plot of initial Molecular Configuration, with labels"
unset xtics
unset ytics
unset grid
#p [-0.65:0][0:.5] CONFFILE u 4:5:3 w labels noti, '' u ($4+.01):5:2 w labels noti
#p CONFFILE u 4:5:3 w labels noti, '' u ($4+(CONF_X_MAX-CONF_X_MIN)/(NATOMS*5)):5:2 w labels noti
p [CONF_X_MIN:CONF_X_MAX][CONF_Y_MIN:CONF_Y_MAX] CONFFILE u 4:5:3 w labels noti, '' u ($4+(CONF_X_MAX-CONF_X_MIN)/(NATOMS*10)):5:2 w labels noti
set xtics
set ytics
set grid

set title "Bond Power matrix heat map"
set size square
set ytics 1
set grid
#set pal grey
set pal defined (-1 "blue", 0 "white", 1 "red")
#plot [0:NATOMS-1][0:NATOMS-1] 'BondPower.txt' matrix w image noti
#set pm3d interpolate 2,2
set pm3d interpolate 0,0
set view map
#set arrow from graph 0,0,0 to graph (NATOMS-1),(NATOMS-1),0 nohead lt -1 lw 1.5
#set style line 77 lt 2 lc rgb "black" lw 2
#splot u,v,u with pm3d, u,NATOMS-1-u,0 ls 77
#sp [0:NATOMS-1][0:NATOMS-1] 'BondPower.txt' matrix w pm3d noti
set parametric
sp [1:NATOMS][1:NATOMS] 'BondPower.txt' matrix u (1+$1):(1+$2):3 w pm3d noti, u,u,0 lt 0 lc rgb "black" noti
unset arrow
unset size
unset parametric


unset multiplot

#for future use:
#2-D vectors: p '' u X0:Y0:DX:DY w vectors head filled
