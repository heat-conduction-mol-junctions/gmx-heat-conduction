#! /bin/bash
# This file is meant to simulate the master script that calls N instances of GROMACS (for N trajectories) and finally averages their results

#TEMPDIR=$PWD
#setenv TEMPDIR /home/inonshar/PBS_templates/
TEMPDIR=/home/inonshar/PBS_templates/
export TEMPDIR
echo
echo TEMPDIR	-1=		$TEMPDIR
echo
./bar.sh
echo
echo TEMPDIR	0=		$TEMPDIR
echo
qsub -v TEMPDIR foo.sh
echo
echo
echo TEMPDIR	2=		$TEMPDIR
echo
echo
#unset TEMPDIR
echo foo done
echo
echo printing STDERR file:
echo
cat foo_std.err
echo
echo printing STD-I\/O file:
echo
cat foo_std.io
echo
echo TEMPDIR	4=		$TEMPDIR
