#PBS -N MkScratchDir
#PBS -l nodes=1:ppn=1,walltime=72:00:00
#PBS -o MkScratchDir.io
#PBS -e MkScratchDir.err
echo
echo this script will create the directory /scratch/$LOGNAME on the local host, $HOSTNAME
cd /scratch
mkdir -p $LOGNAME
cd /scratch/$LOGNAME
echo
pwd
echo
