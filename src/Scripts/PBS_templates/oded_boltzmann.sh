#PBS -N BoltzT10V0.01
#PBS -l nodes=1:ppn=1,walltime=4800:00:00
#PBS -l mem=1500mb
#--------------------------------
#print to screen the host node name
echo "Node Name: $HOSTNAME"
#set enviornmental variable SCRDIR (path to a temporary directory on the local scratch drive) under a dirctory named as the user's logname (so that it won't be run over by other users) and a sub-directory named as the job ID
setenv SCRDIR /scratch/$LOGNAME/$PBS_JOBID/
#make the directory discribed in the previous line
/bin/mkdir -p $SCRDIR
#switch to the working directory
cd $PBS_O_WORKDIR
#run the code -- note that the code uses the system(SCRDIR) command to get the environmental variable SCRDIR, in order to know where to send output to
./Boltzmann.x
#when the code ends, send all data from the temporary directory on the scratch drive to the working directory (on the head node?)
cp -f $SCRDIR/* $PBS_O_WORKDIR
#and remove the temprorary directory on the scratch drive
rm -rf $SCRDIR


