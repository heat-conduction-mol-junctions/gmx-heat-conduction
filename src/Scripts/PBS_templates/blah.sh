#! /bin/bash
#######	This script shows that an environment variable can be set within a script to contain a directory path or the path to the present working directory
TEMPDIR1=$PWD
echo $TEMPDIR1
TEMPDIR2=/home/inonshar/PBS_templates/
echo $TEMPDIR2

export TEMPDIR1
#######	The environment variables are wiped upon exiting from the script
