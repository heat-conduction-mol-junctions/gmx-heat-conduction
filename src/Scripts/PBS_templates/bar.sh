#######	This file is meant to simulate any of the GROMACS programs (grompp,mdrun,etc.) which will be called during a single instance (trajectory) calculation.
#######	bar.sh is a script that contains some BASH commands in order to demonstrate work under PBS
#######	bar.sh is called by foo.sh which itself is the script submitted using qsub
echo
date
echo
#echo for a complete listing of environment variables type in the command env
#echo
#echo to add a value \(ADDEDVALUE\) to a new environment variable \(NEWVARIABLE\) write
#echo NEWVARIABLE\=ADDEDVALUE
#echo
#echo to add two values \(ADDEDVALUE1,ADDEDVALUE2\) to a new environment variable \(NEWVARIABLE\) write
#echo NEWVARIABLE\=\ADDEDVALUE1:ADDEDVALUE2
#echo
#echo to append a value \(ADDEDVALUE\) to an existing environment variable \(EXISTINGVARIABLE\) write 
#echo EXISTINGVARIABLE\=\$EXISTINGVARAIBLE:ADDEDVALUE
#echo
#echo to make a variable globaly available use the command export
#echo
#echo to remove a varaible use the command unset
echo
#Present Working Directory (PWD) given as home directory, regardless of anything
pwd
echo PWD			=$PWD
echo HOSTNAME			=$HOSTNAME
echo LOGNAME			=$LOGNAME
echo HOME			=$HOME
echo PBS_JOBID			=$PBD_JOBID
echo PBS_O_JOBID		=$PBS_O_JOBID
echo PBS_JOBNAME		=$PBS_JOBNAME
echo PBS_NODEFILE		=$PBS_NODEFILE
echo PBS_QUEUE			=$PBS_QUEUE
echo PBS_O_HOME			=$PBS_O_HOME
echo PBS_O_LANG			=$PBS_O_LANG
echo PBS_O_LOGNAME		=$PBS_O_LOGNAME
echo PBS_O_WORKDIR		=$PBS_O_WORKDIR
echo PBS_O_MAIL			=$PBS_O_MAIL
echo PBS_O_SHELL		=$PBS_O_SHELL
echo PBS_O_TZ			=$PBS_O_TZ 
echo PBS_O_HOST			=$PBS_O_HOST
echo PBS_O_QUEUE		=$PBS_O_QUEUE
echo PBS_O_PATH			=$PBS_O_PATH
echo PBS_O_ENVIRONMENT		=$PBS_O_ENVIRONMENT 
echo PBS_BATCH / PBS_INTERACTIVE
echo
pwd
echo
#cd $PBS_O_WORKDIR
ls -BCX
#cd /scratch
#mkdir -p $LOGNAME
#mkdir -p /scratch/$LOGNAME/	#	-p option to refrain from writing an error if the directory already exists, and make parent directories as needed
echo
#cd /scratch/$LOGNAME/
echo
pwd
echo
#touch test.txt
#sed '1i\testing' test.txt
echo
echo TEMPDIR	1=		$TEMPDIR
echo
#/opt/maui/bin/checkjob $PBS_JOBID

