#! /bin/bash
######	This file is meant to simulate the single instance of GROMACS run for each trajectory on a single processor
######	foo.sh is a script file to be submitted to TORQUE via the qsubs command
######	foo.sh calls bar.sh to execute some bash commands in order to demonstrate work under PBS
#PBS -N test
#PBS -l nodes=1:ppn=1,walltime=99:99:99
#PBS -o foo_std.io
#PBS -e foo_std.err
#PBS -d /home/inonshar/GROMACS/
#######	All PBS files are run (using qsub) starting from the HOME directory.
#######	Therefore, all commands should be entered as if you are in the HOME directory,
#######	conversely, you could Change Directory (using cd) to the working directory of your choice
####### in our case, an environmental variable called TEMPDIR was created prior to running qsub (in test.sh) which contains this working directory
#######	The STD-I/O and STDERR files are created in the directory from which you issued qsub, and not necessarily in HOME



#Final Note for May 11th: for some reason, the cd command doesn't work (ls before and after are the same: they both give the listing for the home directory and not the TEMPDIR)
#The TEMPDIR env varialbe has been exported and is checked out to be global outside the PBS. echoing the variable from inside (foo.sh) gives no response, as if the variable is empty!

echo
cd $PBS_O_WORKDIR
echo contents of PBS_O_WORKDIR	=	GROMACS directory
ls -BCX
echo
echo TEMPDIR	3=		$TEMPDIR
echo
cd $TEMPDIR
echo contents of TEMPDIR	=	$TEMPDIR
ls -BCX
cd /
echo
ls -BCX
echo
cd $TEMPDIR

echo ****************************
mkdir -p /scratch/$LOGNAME
cd /scratch/$LOGNAME
pwd
echo ****************************


$TEMPDIR/bar.sh









echo
echo bar done
echo
echo PBS_JOBID	=		$PBS_JOBID
echo
#Present Working Directory (PWD) given as home directory, regardless of anything
pwd
echo PWD	=		$PWD

#/opt/maui/bin/checkjob $PBS_JOBID
