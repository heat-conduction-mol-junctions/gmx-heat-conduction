#! /bin/bash
# Get printf'd lines from *-std.io
#set -e

function ReadOutputAtoms(){
#Reading output atoms into array (only the array size is important here)
OLDIFS=$IFS
IN="$( cat outputatoms.txt)"
set -- "$IN"
IFS=" "
outputatoms=($*)
IFS=$OLDIFS
for i in ${outputatoms[@]} ; do if [[ $i == '' ]] ; then unset ${outputatoms[$i]} ; fi ; done
#echo -e "\n#{ "${outputatoms[@]} "} = " ${#outputatoms[@]}
}

function GetEnsAvgPrgrm(){
cp ../EnsembleAverage-*.out .
        NumEnsAvgProgs=0
        for EnsAvgProgName in EnsembleAverage-*.out ; do NumEnsAvgProgs=$[ 1 + $NumEnsAvgProgs ] ; done
        if [ 0 -eq $NumEnsAvgProgs ] ; then echo "No Ensemble Averaging Programs! Exiting..." ; exit 5 ; fi
  # ./$EnsAvgProgName
        if [ 1 -lt $NumEnsAvgProgs ] ; then echo "Multiple Ensemble Averaging Programs! Choose the one you want. Exiting..." ; exit 5 ; fi
}

#main
ReadOutputAtoms
MAX_TRAJ=$(head -n1 N.dat)
GetEnsAvgPrgrm

GoodTraj=0
for i in $( seq 1 $MAX_TRAJ) ; do
	if [ "Normal termination of Single Trajectory Script. Exiting." == "$(tail -n1 $i-std.io)" ] ; then
		echo -n "$i "
		if [ 0 -eq $GoodTraj ] ; then
			LastLine=$[  $(grep "\#Normal termination of parse-\*.c"            $i-std.io -n | cut -d ":" -f1) - 2 ]
			FirstLine=$[ $(grep "Running parser \& heat current calculation..." $i-std.io -n | cut -d ":" -f1) + 2 ]
		fi
      head -n $LastLine $i-std.io | tail -n $[ $LastLine - $FirstLine + 1 ] > STDIO-$i.txt
    if [ 0 -eq $GoodTraj ] ; then
			cp STDIO-$i.txt STDIO-avg-1.txt
		else
			./$EnsAvgProgName STDIO-$i.txt STDIO-avg-$GoodTraj.txt $GoodTraj STDIO-avg-$[ 1 + $GoodTraj].txt $[ ${#outputatoms[@]} + 1 ] 1 ${#outputatoms[@]}
			if [ 0 -ne $? ] ; then 
				echo "Error in 
      ./$EnsAvgProgName STDIO-$i.txt STDIO-avg-$GoodTraj.txt $[ 1 + $GoodTraj ] STDIO-avg-$[ 1 + $GoodTraj].txt $[ ${#outputatoms[@]} + 1 ] 1 ${#outputatoms[@]}
				. Exiting."
			fi
		fi
	GoodTraj=$[ 1 + $GoodTraj ]
	else
		echo -n "($i) "
	fi
done
echo

echo "$GoodTraj good trajectories"
cp STDIO-avg-$GoodTraj.txt STDIO-avg.txt

echo
