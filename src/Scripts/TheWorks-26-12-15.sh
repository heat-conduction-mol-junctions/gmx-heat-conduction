#! /bin/bash -e

##### This script automates the calculation of various equilibrium and non-equilibrium properties to do with heat conduction for a given molecular model.
#####
##### The inputs are:
#####		1. Molecular topology file in GROMACS formats (topol.top).
#####		2. Initial molecular configuration file in GROMACS format conf.gro).
#####		3. Set of temperatures to serve as average temperature for constant temperature/s ensembles (e.g. 78K, 300K, 1000K).
#####		4. Thermal reservoir coupling strenth constants, or system-bath relaxation life-times (e.g. 1 ps).
#####
#####	Extra, optional inputs:
#####		1.	Set of temperture differences to serve as temperature gradients for two-terminal non-equilibrium ensembles (e.g. 1K, 10K, 30K).
#####					(validity of linear response: Fourier's law, Green-Kubo treatment)
#####		2.	Non-Markovian bath memory life-time.
#####		3.	Flag for activation of periodic boundary conditions in the plane of the bulks' surfaces.
#####		4.	Properties for external time-dependent force.
#####
##### The outputs are:
#####		1. NVE:   	For a constant energy ensemble, (single realization):
#####   	a.	Normal Mode analysis of the molecule at its energetically minimized configuration.
#####			b.	Heat conductance from Peierls normal mode expression:
#####				i.	RESULT: Boltzmann population distribution.
#####				ii. RESULT: Bose-Einstein populations distribution.
#####		2. NVT:    	For a constant temperature ensemble:
#####			a.	Local average kinetic energy ("temperature") of each atom.
#####			b.	Heat conductance through the junction from linear response:
#####				i.		Instantaneous net heat current through the junction.
#####       ii.		Extension: Histogram (full counting statistics).
#####				iii.	Heat Current Autocorrelation Function (HCACF).
#####				iv.		RESULT: Heat conductance through the junction from Green-Kubo relations.
#####		3. NVT1T2:	For a two-terminal non-equilibrium ensemble centered about a given average:
#####     a.	RESULT: Local average kinetic energy ("temperature") of each atom.
#####			b.	Extension: Instantaneous local power into and instantaneous local heat current through each atom.
#####			c.	Net heat current through the junction:
#####				i.	Time-dependence
#####       ii. Extension: Histogram (full counting statistics).
#####				iii.	Ensemble & time average
#####			d.	RESULT: Heat conductance through the junction from Fourier's law.


# job control:
# ============
# [CTRL+Z] ; bg																											# To send current job to background
# jobs ; fg %1																											# To display jobs and then send the first one on the list to the foreground
# ./TheWorks-*.sh &>log.txt &																	# Redirects both STDOUT and STDERR streams to log file, and forks.
# nohup ./TheWorks-*.sh > log.io 2> log.err < /dev/null &			# Also redirects STDIN to NULL device, and does not get the HUP (hangup) signal if SSH is closed.
# ./TheWorks-*.sh > log.io 2> log.err < /dev/null & disown	-h		# Remove job from active jobs table, and ignore SIGHUP. 'disown' is not shell-portable...

echo "Starting $0"
date

declare SCRIPTS="$HOME/gmx-heat-conduction/src/Scripts"
if [ "hydrogen.tau.ac.il" == $HOSTNAME ] ; then SCRIPTS="$HOME/Documents/GROMACS/Scripts" ; fi
#PARSE="$HOME/gmx-heat-conduction/bin/Parse"
#if [ "hydrogen.tau.ac.il" == $HOSTNAME ] ; then PARSE="$HOME/Documents/GROMACS/Parse" ; fi

#if [ -f SingleTrajectory-*.sh ] ; then for s in SingleTrajectory-*.sh ; do if [ diff <(ls $(basename $s)) <(ls $(basename $(ls -tr1 $SCRIPTS/SingleTrajectory-*.sh | tail -n1))) ] ; then echo "deprecated SingleTrajectory script file present! aborting" ; exit 259 ; fi ; done ; fi
#if [ -f parse-*.out ] ; then for p in parse-*.out ; do if [ diff <(ls $(basename $p)) <(ls $(basename $(ls -tr1 $PARSE/$p | tail -n1)) ] ; then echo "deprecated parse program file present! aborting" ; exit 866 ; fi ; done ; fi
#if [ -f SingleTrajectory-*.sh ] || [ -f parse-*.out ] ; then echo "SingleTrajectory script or parse program file present in local directory, and might be deprecated! aborting" ; exit 776 ; fi
for i in $(find . -name "parse-*.out") ; do rm $i ; echo $i ; done ; for i in $(find . -name "SingleTrajectory-*.sh") ; do rm $i ; echo $i ; done

declare -r MasterControlScriptFile=$(ls -tr1 $SCRIPTS/MasterControl-*.sh | tail -n1)
if [ ! -f $MasterControlScriptFile ] ; then echo "$MasterControlScriptFile missing. Aborting." ; exit 2 ; fi
declare -r HowLongLeftScriptFile=$(ls -tr1 $SCRIPTS/HowLongLeft-*.sh | tail -n1)
if [ ! -f $HowLongLeftScriptFile ] ; then echo "$HowLongLeftScriptFile missing. Aborting." ; exit 2 ; fi
declare -r EnsemblePairwisePowersScriptFile=$(ls -tr1 $SCRIPTS/EnsemblePairwisePowers-*.sh | tail -n1)
if [ ! -f $EnsemblePairwisePowersScriptFile ] ; then echo "$EnsemblePairwisePowersScriptFile missing. Aborting." ; exit 2 ; fi

# nAtoms=$(head -n2 ../'$INSTANCE'/conf.gro | tail -n1)
declare -ir nAtoms=$(head -n2 conf.gro | tail -n1)
if [ -z $nAtoms ] ; then echo "Got no value for nAtoms!" ; exit 24 ; fi
echo "nAtoms=$nAtoms"


declare -ir isProduction=1
echo "isProduction=$isProduction"
if [ 1 -eq $isProduction ]
	then
	# MAXSTEPS=745000 # 2000000
	# MAXSTEPS=$(echo $nAtoms | awk '{print 10**(6 - int(log($1)/log(10)))}')
	declare -ir MAXSTEPS=100000
	declare -i N_TRAJ=700
else
	declare -ir MAXSTEPS=10000
	declare -i N_TRAJ=200
fi
declare -ir DEFAULT_SEG_LENGTH=$(echo $MAXSTEPS | awk '{print 10**(int(log(sqrt($1)/log(10))-2))}') # 10000 # 5000
echo "MAXSTEPS=$MAXSTEPS\nDEFAULT_SEG_LENGTH=$DEFAULT_SEG_LENGTH"
if [ 1 != $(echo $MAXSTEPS $DEFAULT_SEG_LENGTH | awk '{if (0 == $1 % $2) print(1) ; else print(2)}') ] ; then echo "MAXSTEPS / DEFAULT_SEG_LENGTH is not an integer. Aborting." ; exit 34 ; fi

case $HOSTNAME in
        "hydrogen.tau.ac.il") QUEUE="default";;
        "power.tau.ac.il") QUEUE="nano1";; # default is "short"
esac
echo "QUEUE=$QUEUE"
for i in $(qmgr -c "list queue $QUEUE" | grep -e 'max_.*queuable' | cut -d'=' -f2)
        do
	echo "$N_TRAJ \?\> $i"
        if [ $N_TRAJ -gt $i ]
                then N_TRAJ=$i
        fi
done
echo "N_TRAJ=$N_TRAJ"


declare -r DATAFILE="$PWD/data.txt"
if [ ! -f $DATAFILE ]
	then
	echo -n "INSTANCE N_{traj} T_{cold} dT P sd[P] I_{interfacial} sd[I_{interfacial}] I_{spacial} sd[I_{spacial}]" >> $DATAFILE
	for j in $(seq 1 $nAtoms) ; do jMin1=$[ $j - 1 ] ; echo -n " T[$jMin1] sd[T[$jMin1]]" >> $DATAFILE ; done
	echo >> $DATAFILE
	#then echo $INSTANCE $(tail -n 1 LowerTriangleRowsFirst.txt | cut -d " " -f1) $Tcold $dT $(tail -n 1 LowerTriangleRowsFirst.txt | awk -F " " '{ for (i=(NF-5); i<=NF; i++) print $i }') $(cat EnsembleAveragedTemperatures.txt) >> $DATAFILE
fi
#date >> $DATAFILE

echo
echo "$0: Potential Energy Minimization and subsequent Normal Mode Analysis:"
echo "======================================================================"
echo
echo "Active Jobs: $(showq -u inonshar | grep "Active Jobs:" | cut -d':' -f3 | awk '{print $1}')"
$MasterControlScriptFile -m -d -r 1
if [ 0 -ne $? ] ; then echo "$0: Error in $MasterControlScriptFile -m -d -r 1 while EM & NA analysis!" ; exit 443 ; fi
declare INSTANCE=$(cut -d "=" -f2 INSTANCE.txt) # INSTANCE=$(tail -n 1 options.dat | cut -d "=" -f2)
cd $INSTANCE
  echo "$0: Running Potential Energy Minimization and Normal Mode Analysis Molecular Dynamics ensemble simulation:"
	$HowLongLeftScriptFile $INSTANCE NO_DO_MD # 9-8-14 HowLong handle MasterControl*.sh with -d (do not DO_MD)
	cp conf_em-*.gro ../conf.gro
	#cp eigen* ..

	echo
	echo "$0: Number of modes and maximal eigenfrequency (in wavenumbers):" 
	echo "----------------------------------------------------------------"
	echo
	tail -n 1 eigenfreq-*.xvg # | cut -d " " -f 14
	declare -i nmodes=$(tail -n1 eigenfreq-*.xvg | awk '{print($1)}')
cd ..

echo
echo "$0: Exporting normal mode analysis products for calculation of Peierls-Landauer heat current:"
echo "---------------------------------------------------------------------------------------------"
echo
if [ -d PeierlsLandauer ] ; then if [ -d PeierlsLandauerOld ] ; then rm -r PeierlsLandauerOld ; fi ;  mv PeierlsLandauer PeierlsLandauerOld ; fi
#if [ -d PeierlsLandauer ] ; then echo "Root PeierlsLandauer directory exists! Backup and re-run. Aborting." ; exit 998 ; fi
if [ ! -d PeierlsLandauer ] ; then mkdir PeierlsLandauer ; fi
cd PeierlsLandauer
	#cp $(ll -tr $SCRIPTS/PeierlsLandauer-*.sh | tail -n1 | cut -d ':' -f2 | cut -d ' ' -f2) . ; ./PeierlsLandauer*.sh
	$(ls -tr1 $SCRIPTS/PeierlsLandauer-*.sh | tail -n1)
	if [ 0 -ne $? ] ; then echo "$0: Error in PeierlsLandauer-*.sh" ; exit 665 ; fi
	declare -ir tau_m=0.0833333
	declare gamma_L=$(echo $tau_m 20 | awk '{print($1 * $2)}')
	declare gamma_R=$(echo $tau_m 20 | awk '{print($1 * $2)}')
	if [ -z $nAtoms ] ; then echo "Got no value for nAtoms!" ; exit 24 ; fi
	echo "$0 DBG: Entering nAtoms, tau_m, gamma_L & gamma_R and zero temperatures"
	sed -i "1s/^/$nAtoms\n$tau_m\n$gamma_L\ $gamma_R\n0\.0\ 0\.0\n/" input.txt
cd ..


echo
echo "$0: Set of values for temperature of cold bath:"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
if [ 1 -eq $isProduction ]
	then declare -ar Tcolds=( 77 300 1000 )
else
	declare -ar Tcolds=( 77 )
fi
echo ${Tcolds[@]}

echo
echo "$0: Set of values for temperature bias:"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
if [ 1 -eq $isProduction ]
        then 
	#dTs=( 1 2 3 4 5 7 10 )
	#dTs=( 0 1 2 5 10 )
	declare -ar dTs=( 0 1 2 )
else
        declare -ar dTs=( 0 1 )
fi

echo ${dTs[@]}
echo

for j in $(seq 0 $[ ${#dTs[@]} - 1 ] )
	do
	if [ 1 -eq $(echo ${dTs[$j]} | awk '{if(int($1) != $1) print 1 ; else print 0 }') ]
		then 
		echo "$0: Non-integer dT (${dTs[$j]}) not yet supported"
		exit 99
	fi
done

for i in $(seq 0 $[ ${#Tcolds[@]} - 1 ] )
	do
	quota ; if [ 0 -ne $? ] ; then echo "disk usage over quota. Aborting." ; exit 989 ; fi
	declare -i Tcold=${Tcolds[$i]}
	echo
  echo "$i: Tcold=$Tcold"
	echo "----------------"
	echo
	if [ ! -d Tcold_$Tcold ] ; then mkdir Tcold_$Tcold ; fi
	cd Tcold_$Tcold
	set +e # DBG 20-9-13
	cp ../* .
	set -e # DBG 20-9-13

	echo
	echo "$0: Towards equilibrium at $Tcold K:"
	echo "===================================="
	echo
	set +e
  echo "Active Jobs: $(showq -u inonshar | grep "Active Jobs:" | cut -d':' -f3 | awk '{print $1}')"
  #$MasterControlScriptFile -b 1 -c $Tcold -x 0.001 -l 2000000 -s 10000 -r 1 # ToEq
  $MasterControlScriptFile -b 1 -c $Tcold -x 0.001 -l $MAXSTEPS -s $DEFAULT_SEG_LENGTH -k -r 1 # nsteps<745400
  #$MasterControlScriptFile -b 1 -c $Tcold -x 0.001 -l 502000 -s 1000 -r 1 # DBG
	if [ 0 -ne $? ] ; then cd .. ; echo "ERROR 186 Towards equilibrium at $Tcold K failed!" ; continue ; fi
	set -e
  INSTANCE=$(cut -d "=" -f2 INSTANCE.txt) # INSTANCE=$(tail -n 1 options.dat | cut -d "=" -f2)
	cd $INSTANCE
    echo "$0: Running ToEq Molecular Dynamics ensemble simulation for Tcold=$Tcold :"
		$HowLongLeftScriptFile $INSTANCE

		if [ "Normal termination of Single Trajectory Script. Exiting." == "$(tail -n 1 1-std.io)" ]
			then 
			echo "$0: Normal termination of towards equilibrium trajectory."
		else 
			echo "ERROR 201 $0: Error in towards equilibrium trajectory (Tcold=$Tcold , INSTANCE=$INSTANCE)! Aborting..." 
			tail -n 1 1-std.io
			#exit 1 
			cd ../..
			continue
		fi
		
		mv traj-tss.trr ..
		mv topol-tss.tpr ..
	cd ..


	for j in $(seq 0 $[ ${#dTs[@]} - 1 ] )
		do
		dT=${dTs[$j]}
		echo
    echo "$j: dT=$dT"
		echo "----------"
		echo
		if [ ! -d dT_$dT ] ; then mkdir dT_$dT ; fi
		cd dT_$dT
	  set +e # DBG 20-9-13
		cp ../* .
	  set -e # DBG 20-9-13

		echo
		echo "$0: Towards non-equilibrium steady-state at $Tcold K and $[ $Tcold + $dT ] K:"
		echo "============================================================================="
		echo

#####			TO-DO:
#####     Heat Current Autocorrelation Function (HCACF)
#####     Heat conductance from Green-Kubo relations

		set +e
		echo "Active Jobs: $(showq -u inonshar | grep "Active Jobs:" | cut -d':' -f3 | awk '{print $1}')"

		#$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l 2000000 -s 10000 -p -r 1 # ToNESS
		#$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l $MAXSTEPS -s $DEFAULT_SEG_LENGTH -p -r 1 # nsteps<745400
		$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l $MAXSTEPS -s $DEFAULT_SEG_LENGTH -r 1
		#$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l 502000 -s 1000 -p -r 1 # DBG
		if [ 0 -ne $? ] ; then cd .. ; echo "ERROR 236 Towards non-equilibrium steady-state at $Tcold K and $[ $Tcold + $dT ] K" ; continue ; fi
		set -e
    INSTANCE=$(cut -d "=" -f2 INSTANCE.txt) # INSTANCE=$(tail -n 1 options.dat | cut -d "=" -f2)
		cd $INSTANCE
			echo "$0: Running ToNESS Molecular Dynamics ensemble simulation for Tcold=$Tcold , dT=$dT :"
		  $HowLongLeftScriptFile $INSTANCE
    if [ "Normal termination of Single Trajectory Script. Exiting." == "$(tail -n 1 1-std.io)" ] ; then echo "$0: Normal termination of towards non-equilibrium steady-state trajectory." ; else echo "ERROR 242 $0: Error in towards non-equilibrium steady-state trajectory (Tcold=$Tcold , dT=$dT , INSTANCE=$INSTANCE)! Aborting..." ; echo $(tail -n 1 1-std.io) ; cd ../.. ;  continue ; fi
		  #eog LocalStuff-*
		  #mv traj-1.trr ../traj-tss.trr
      mv traj-tss.trr ..
      #mv topol-1.tpr ../topol-tss.tpr
		  mv topol-tss.tpr ..

			# Use results of current temperature bias to improve initial conditions for subsequent biases (as long as they're sorted from small to large).
			cp ../traj-tss.trr ../..
			cp ../topol-tss.tpr ../..
		cd ..

		echo
		echo "$0: At non-equilibrium steady-state at $Tcold K and $[ $Tcold + $dT ] K:"
		echo "========================================================================"
		echo
		set +e
		echo "Active Jobs: $(showq -u inonshar | grep "Active Jobs:" | cut -d':' -f3 | awk '{print $1}')"
		#$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l 900000 -s 450000 -p -r 200 #AtNESS
		$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l $[ 2 * $MAXSTEPS ] -s $MAXSTEPS -p -r $N_TRAJ #AtNESS
		#$MasterControlScriptFile -b 2 -a $dT -c $Tcold -x 0.001 -l 2000 -s 1000 -p -r 5 #DBG
		if [ 0 -ne $? ] ; then cd .. ; echo "ERROR 260  At non-equilibrium steady-state at $Tcold K and $[ $Tcold + $dT ] K failed!" ; continue ; fi
		set -e
		INSTANCE=$(cut -d "=" -f2 INSTANCE.txt) # INSTANCE=$(tail -n 1 options.dat | cut -d "=" -f2)
		cd $INSTANCE
      echo "$0: Running NESS Molecular Dynamics ensemble simulation for Tcold=$Tcold , dT=$dT :"
			$HowLongLeftScriptFile $INSTANCE
			#cat BondPower.txt
			if [ ! -f BondPower.txt ] ; then echo "ERROR 886 Missing prerequisite file: BondPower.txt. Aborting!" ; cd .. ; continue ; fi
			$EnsemblePairwisePowersScriptFile
			if [ 0 -ne $? ] ; then echo "$0: Error in $EnsemblePairwisePowersScriptFile!" ; exit 887 ; fi

			#DBG 16-8-14
			#echo "$0 DBG 16-8-14: LowerTriangleRowsFirst.txt"
			#cat LowerTriangleRowsFirst.txt
			#echo "$0 END DBG 16-8-14"

			#echo $Tcold $dT $(tail -n 1 LowerTriangleRowsFirst.txt)
			echo $INSTANCE $(tail -n 1 LowerTriangleRowsFirst.txt | cut -d " " -f1) $Tcold $dT $(tail -n 1 LowerTriangleRowsFirst.txt | awk -F " " '{ for (i=(NF-5); i<=NF; i++) print $i }') $(cat EnsembleAveragedTemperatures.txt) >> $DATAFILE
			if [ 0 -ne $(ls *.trr | wc -l) ] ; then rm *.trr ; fi # conserve HD space...
		cd ..
		if [ 0 -ne $(ls *.trr | wc -l) ] ; then rm *.trr ; fi # conserve HD space...

	  echo
	  echo "$0: Calculating Peierls-Landauer heat current:"
	  echo "----------------------------------------------"
	  echo
	  if [ ! -d PeierlsLandauer ] ; then mkdir PeierlsLandauer ; fi
		cd PeierlsLandauer
		cp ../../../PeierlsLandauer/input.txt .
		echo "$0 DBG: Entering new temperatures"
		sed -i "4s/.*/$T_R $T_L/" input.txt
		set +e
		qsub $(ls -tr1 $HOME/LandauerHeatCurrent/src/LndrHtCnd-*.sh | tail -n1)
                EXIT_CODE=$?
		while [ 0 -ne $(qstat -u $USER | grep LndrHtCnd | wc -l) ] ; do sleep 0.5 ; qstat -u $USER | grep LndrHtCnd ; done
		echo "DBG 2015-12-25 EXIT_CODE=$EXIT_CODE tail LndrHtCnd.io = $(tail -n1 LndrHtCnd.io)"
		if [ 0 -ne $EXIT_CODE ] || [ $(tail -n1 LndrHtCnd.io | grep "\#Normal termination of Scan Over Temperature and/or Oscillator Frequency script." | wc -l) -eq 0 ] ; then echo "ERROR 776 Error in LndrHtCnd-*.sh!" ; fi
		set -e
		cd ..

		cd ..
		echo "$0 Finished $i: Tcold=$Tcold , $j: dT=$dT"

#####			TO-DO:
#####     Histogram
#####     Average

	done #for j (over dT)

	# insert newline into DATAFILE for gnuplot isoline parsing:
	echo $DATAFILE

	if [ 0 -ne $( ls *.trr | wc -l ) ] ; then rm *.trr ; fi  # conserve HD space...
	cd ..
  echo "$0 Finished $i: Tcold=$Tcold"

done #for i (over Tcold)

echo "Finished temperature scan. Now summarizing results."
rm INSTANCE.txt

### ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## @fn PLOTTING OUTPUT DATA SUMMARY
summarizeData() {
	# First attemp: Plot only cold reservoir temepratures to data-$INSTANCE.svg
	declare -ir nHeaders=4
	declare -ir nPairsOffDiagonalElems=$[ $[$nAtoms * $[ $nAtoms - 1 ] ] / 2 ]
	declare -ir nPairsDiagonalElems=$nAtoms
	declare -ir nAtomTemperatures=$nAtoms
	declare -ir nValueAndDeviation=2
	#nColumns=$[ $nHeaders + $[ $[ $nPairsOffDiagonalElems + $nPairsDiagonalElems + $nAtomTemperatures ] * $nValueAndDeviation ] ] 
	declare -ir nColumns=$( tail -n1 $DATAFILE | wc -w )
	declare -ir nColumnsMin1=$[ $nColumns - 1 ]
	set +e
	gnuplot -e "set term svg enhanced ; \
	set output \"data-$INSTANCE.svg\" ; 
	set title 'T_{hot}' ; \
	set view map ; \
	set xtics 1 ; \
	set palette defined ( \
	$(echo $Tcold | awk '{print(0 * $1)}') \"black\", \
	$(echo $Tcold | awk '{print(0.5 * $1)}') \"blue\", \
	$(echo $Tcold | awk '{print(1 * $1)}') \"white\", \
	$(echo $Tcold | awk '{print(1.5 * $1)}') \"red\", \
	$(echo $Tcold | awk '{print(2 * $1)}') \"yellow\") ; \
	set xlabel 'dt/[K]' ; \
	set ylabel 'T_{cold}/[K]' ; \
	sp '$DATAFILE' u 4:3:$nColumnsMin1 w image t ''"
	EXIT_CODE=$?
	set -e
	if [ 0 -ne $EXIT_CODE ] ; then echo "GnuPlotting instance data failed!" ; fi

	#Invert sign of power (fifth column)
	declare -r INV_POWER_SGN="data.ipw"
	touch $INV_POWER_SGN
	tail -n +2 $DATAFILE | awk '{for (i=1;i<=NF;i++) { if (5==i) printf("%f ", -1 * $i); else printf("%s ", $i)} printf("\n")}' > $INV_POWER_SGN

	# Sort the output of the various simulations by cold reservoir temeprature first, and then by temperature bias
	declare -r SORTED_DATAFILE="data.srt"
	declare -r TMP_DATAFILE="data.tmp"
	tail -n +2 $INV_POWER_SGN | sort -k 4 | sort -rk 3 > $TMP_DATAFILE
	head -n1 $DATAFILE > $SORTED_DATAFILE ; cat $TMP_DATAFILE >> $SORTED_DATAFILE
	rm $INV_POWER_SGN $TMP_DATAFILE

	# convert sorted output to Comma-Separated Values format
	declare -r SORTED_CSV_FILE="data.csv"
	sed 's/\ /,/g' $SORTED_DATAFILE > $SORTED_CSV_FILE
	#sed "s/$/$\n/" $SORTED_CSV_FILE > data.m3d

	# split local temperatues data by simulation instance
	if [ -f instances.txt ] ; then rm instances.txt ; fi
	declare -ir lastcolumn=$[ 10 + 2 * $nAtoms]
	tail -n +2 $SORTED_DATAFILE | while read -a SIM_RESULT
		do
		declare INSTANCE=${SIM_RESULT[0]}
		declare -i T=${SIM_RESULT[2]}
		declare -i dT=${SIM_RESULT[3]}
	  echo $T $dT $INSTANCE >> instances.txt
		declare INST_ATM_TEMP=AtomicTemperatures-$T-$dT-$INSTANCE.txt
		if [ -f $INST_ATM_TEMP ] ; then rm $INST_ATM_TEMP ; fi
		for i in $(seq 8 2 $lastcolumn)
			do echo $[ $i / 2 - 4 ] ${SIM_RESULT[$i]} ${SIM_RESULT[ $[ $i + 1 ] ]} >> $INST_ATM_TEMP
		done
	done

	# list all AtomicTemperatures files' names in filenames.txt
	#n=0
	#declare -a filenames ; for i in AtomicTemperatures-*.txt ; do filenames[$n]=$i ; n=$[ $n + 1 ] ; done
	#for i in ${filenames[@]} ; do echo $i ; done > filenames.txt
	if [ 0 -lt $(ls AtomicTemperatures-*.txt | wc -l) ]
		then
		ls -1 AtomicTemperatures-*.txt > filenames.txt
	else
		if [ -f filenames.txt ] ; then rm filenames.txt ; fi
		echo "Found no AtomicTemperatures files!"
		touch filenames.txt
	fi

	echo "Running gnuplot script to plot summary, temperature profiles, and power & heat conduction"
	gnuplot $(ls -tr1 $SCRIPTS/data.gp | tail -n1)
	if [ 0 -eq $? ]
		then
		rm instances.txt filenames.txt data.dat
		mv $SORTED_DATAFILE $DATAFILE
	fi
}
summarizeData


date
rm SingleTrajectory-*.sh parse-*.out
echo "#Normal termination of The Works script ($0)."
exit 0
