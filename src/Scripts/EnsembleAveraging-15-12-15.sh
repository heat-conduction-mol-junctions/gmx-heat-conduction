#! /bin/bash -e
# the -e flag at the top (also "set -e") makes the script exit if any command not inside an "if" statement exits with an unsuccessfull exit code. This can be reveresd using "set +e"
# This script goes over all trajectories in a GROMACS run instance, and averages over the HeatCurrent files
# then it formats the average heat current file as an XMGrace document, and fits and plots it using GNOplot
# finally, the graph (as a JPEG file) is shown using the Eye Of GNOME image viewer

BASE_PATH="$HOME/gmx-heat-conduction"
SCRIPTS=$BASE_PATH/src/Scripts
PARSE=$BASE_PATH/bin/Parse
if [ $HOSTNAME == "hydrogen.tau.ac.il" ]
	then 
	BASE_PATH="$HOME/Documents/GROMACS" 
	SCRIPTS=$BASE_PATH/Scripts
	PARSE=$BASE_PATH/Parse
fi

function Setup(){
echo "Setting up ensemble averaging..."

n=1
	while read GMXSimParam
	do
		#echo $GMXSimParam
		if [ $n -eq 1 ] ; then MAX_TRAJ=$GMXSimParam ; fi
                if [ $n -eq 2 ] ; then dt=$GMXSimParam ; fi
                if [ $n -eq 3 ] ; then SegLength=$GMXSimParam ; fi
                if [ $n -eq 4 ] ; then N_EXPONENTIALS=$GMXSimParam ; fi
                if [ $n -eq 5 ] ; then bDO_EOG=$GMXSimParam ; fi
                if [ $n -eq 6 ] ; then bINKSCAPE=$GMXSimParam ; fi
                if [ $n -eq 7 ] ; then bDO_VID=$GMXSimParam ; fi
                if [ $n -eq 8 ] ; then STEADY_STATEname=$GMXSimParam ; fi
		if [ $n -eq 9 ] ; then DeltaT=$GMXSimParam ; fi
		if [ $n -eq 10 ] ; then bProduction=$GMXSimParam ; fi
                if [ $n -eq 11 ] ; then INSTANCE=$GMXSimParam ; fi
		n=$[ 1 + $n ]
	done < N.dat
	echo -e "Parameters read in:\nMAX_TRAJ=$MAX_TRAJ\ndt=$dt\nSegLength=$SegLength\nN_EXPONENTIALS=$N_EXPONENTIALS\nbDO_EOG=$bDO_EOG\nbINKSCAPE=$bINKSCAPE\nbDO_VID=$bDO_VID\nSTEADY_STATEname=$STEADY_STATEname\nDeltaT=$DeltaT\nbProduction=$bProduction\nINSTANCE=$INSTANCE"
	if ( [ -z $MAX_TRAJ ] || [ -z $dt ] || [ -z $SegLength ] || [ -z $N_EXPONENTIALS ] || [ -z $bDO_EOG ] || [ -z $bINKSCAPE ] || [ -z $bDO_VID ] || [ -z $DeltaT ] || [ -z $bProduction ] || [ -z $INSTANCE ] ) ; then echo -e "\nRequired environment variable not defined (see above). Exiting.\n" ; exit 4 ; fi



#	COUNTER=1                                       ###initialize counter
#	while read line                                 ###outer loop necessary for eof test
#	do
#	#echo -e "\n$COUNTER	$line"                  ###print counter and line to stdout (-e enables \n)
#	ARRAY[$COUNTER]=$line                           ###define array, one element at a time
#	#echo "$COUNTER'	${ARRAY[$COUNTER]}"     ###print array to stdout, one element at a time
#	COUNTER=$[$COUNTER+1]                           ###advance counter
#	done < N.dat					###get input lines to be read from N.dat file

#echo;echo;echo                                  ###spacera

#N=${ARRAY[1]}
#echo "N	=	$N"
#INSTANCE=${ARRAY[2]}
#echo "INSTANCE	=	$INSTANCE"
}

function ReadOutputAtoms(){
#Reading output atoms into array (only the array size is important here)

if [ ! -f "outputatoms.txt" ] ; then echo "outputatoms.txt missing! Aborting EnsembleAveraging-*.sh ..." ; exit 879 ; fi

OLDIFS=$IFS
IN="$( cat outputatoms.txt)"
set -- "$IN"
IFS=" "
outputatoms=($*)
IFS=$OLDIFS
for i in ${outputatoms[@]} ; do if [[ $i == '' ]] ; then unset ${outputatoms[$i]} ; fi ; done
echo -e "\n#{ "${outputatoms[@]} "} = " ${#outputatoms[@]}
}

function GetEnsAvgPrgrm(){
#cp ../EnsembleAverage-*.out .
cp $(ls -tr1 $PARSE/EnsembleAverage-*.out | tail -n1) . 
        NumEnsAvgProgs=0
        for EnsAvgProgName in EnsembleAverage-*.out ; do NumEnsAvgProgs=$[ 1 + $NumEnsAvgProgs ] ; done
        if [ 0 -eq $NumEnsAvgProgs ] ; then echo "No Ensemble Averaging Programs! Exiting..." ; exit 5 ; fi
  # ./$EnsAvgProgName
        if [ 1 -lt $NumEnsAvgProgs ] ; then echo "Multiple Ensemble Averaging Programs! Choose the one you want. Exiting..." ; exit 5 ; fi
}

function AverageOutput(){
echo -e "\nAveraging over ensemble:"

echo "DEBUG (23-7-12): outputatoms = ${outputatoms[@]}"

LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do

if [ -f output-$i.txt ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then 
		cp output-$i.txt EnsembleAverageHeatCurrent-$i.dat
	else 
		./$EnsAvgProgName output-$i.txt EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageHeatCurrent-$i.dat $NumOutputFields $NumCompFields ${#outputatoms[@]}
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over output of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
        	        echo -e "./$EnsAvgProgName output-$i.txt EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageHeatCurrent-$i.dat $NumOutputFields $NumCompFields ${#outputatoms[@]}\n"
			echo -e "Exiting...\n"
			exit 1
		fi
		rm EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat
	fi

  LastGoodTrajectory=$i
	#rm output-$i.txt
	#rm -f $i-std.io
	#rm -f $i-std.err

		#print to screen the number of the current trajectory which was processed (but do not print new-line character)
		echo -n "$i "
else
		echo -n " ($i NOT FOUND)  "
		jobId=$(head $i-std.io -n29 | tail -n1 | cut -d " " -f3-)
		echo ; set +e ; tracejob -v $jobId ; set -e
		NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]
		if [ 1 -eq $MAX_TRAJ ] ; then
			echo "Single trajectory attmpted and failed. Cannot continue -- crashing..."
			exit 921
		fi
fi

	#advance trajectory counter
	i=$[$i+1]
done

#       #mv EnsembleAverageHeatCurrent-$MAX_TRAJ.dat EnsembleAveragedHeatCurrent.dat
#rm EnsembleAverageHeatCurrent-$MAX_TRAJ.dat
mv EnsembleAverageHeatCurrent-$LastGoodTrajectory.dat output.txt

#####rm EnsembleAveraging-24-2-10.out
}

function AverageHROutput(){
echo -e "\n\nEnsemble averaging over human readable files..."

#	#add XMGrace formatting information to final EnsembleAverageHeatCurrent-N.dat file
#	sed '1i\# this line is a comment line\n@    title "Ensemble Average Heat Current and Energy"\n@    xaxis  label "Time [picoseconds]"\n@    yaxis  label "Energy [kJ/mole] or Energy Current [kJ/mole ps\\S-1\\N]"\n\@TYPE xy\n@ view 0.2, 0.1, 1.25, 0.875\n@ legend on\n@ legend loctype view\n@ legend 0.85, 0.85\n@ s0 legend "Atom Index"\n@ s1 legend "Atom Energy"\n@ s2 legend "X Energy Current"\n@ s3 legend "Y Energy Current"\n@ s4 legend "Z Energy Current"' EnsembleAverageHeatCurrent-$N.dat >EnsembleAveragedHeatCurrent.xvg

#rename file for first trajectory as average over the first trajectory, and remove the trajectory standard output
#mv output-1.txt EnsembleAverageHeatCurrent-1.dat
#rm -f 1-std.io
#rm -f 1-std.err
LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f hroutput-$SegLength-$i.xvg ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then
               cp hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$i.dat
        else
		./$EnsAvgProgName hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedPower-$i.dat $NumHROutputFields $NumHRCompFields ${#outputatoms[@]}
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over human-readable heat current output of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
													echo -e "./$EnsAvgProgName hroutput-$SegLength-$i.xvg EnsembleAveragedPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedPower-$i.dat $NumHROutputFields $NumHRCompFields ${#outputatoms[@]}\n"
							echo -e "Exiting...\n"
							exit 1
						fi
					rm EnsembleAveragedPower-$LastGoodTrajectory.dat
					fi
								LastGoodTrajectory=$i
								#rm hroutput-$SegLength-$i.xvg

					#print to screen the number of the current trajectory which was processed (but do not print new-line character)
					echo -n "$i "

				else
						echo -n " ($i NOT FOUND)  "
						NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]

				fi	

				#advance trajectory counter
				i=$[$i+1]
				done

				mv EnsembleAveragedPower-$LastGoodTrajectory.dat hroutput$SegLength.xvg
echo -e "\nFinished Averaging over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories.\n"
}

function AverageCurrent(){
echo -e "\n\nEnsemble averaging over energy current files..."
echo

#####cp ../I-24-2-10.gp .
#####/usr/local/bin/gnuplot I-24-2-10.gp &>$INSTANCE.gpl			#fit and plot using GNUplot, output to JPEG file
#####mv EnsembleAveragedHeatCurrent.svg $INSTANCE.svg

#read ISSM <conductance.dat
#echo $INSTANCE $ISSM >> ../conductances.dat

LastGoodTrajectory=0
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f current-$i.txt ] ; then
        if [ 0 -eq $LastGoodTrajectory ] ; then
                cp current-$i.txt EnsembleAverageCurrent-$i.dat
        else
                ./$EnsAvgProgName current-$i.txt EnsembleAverageCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageCurrent-$i.dat $NumCurrentOutputFields $NumCompFields ${#outputatoms[@]}
                if [ "$?" != "0" ] ; then
                        #if the last command returned an unsuccesful exit code, exit this script with code 1
                        echo -e "\nError in Ensemble Averaging over current file of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
                        echo -e "./$EnsAvgProgName current-$i.txt EnsembleAverageCurrent-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAverageCurrent-$i.dat $NumCurrentOutputFields $NumCompFields ${#outputatoms[@]}\n"
                        echo -e "Exiting...\n"
                        exit 1
                fi
                rm EnsembleAverageCurrent-$LastGoodTrajectory.dat
        fi
        LastGoodTrajectory=$i
        #rm current-$i.txt

                #print to screen the number of the current trajectory which was processed (but do not print new-line character)
                echo -n "$i "
else
                echo -n " ($i NOT FOUND)  "
                NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]
fi
        #advance trajectory counter
        i=$[$i+1]
done
echo
mv EnsembleAverageCurrent-$LastGoodTrajectory.dat current.txt
sed '/^$/d' current.txt > current-no-spaces.txt
}


function AverageHRCurrent(){
echo -e "\n\nEnsemble averaging over human readable current files..."

LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f currenthr-$i.txt ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then
               cp currenthr-$i.txt EnsembleAveragedCurrentHR-$i.dat
        else
		./$EnsAvgProgName currenthr-$i.txt EnsembleAveragedCurrentHR-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedCurrentHR-$i.dat $NumHRCurrentFields $NumHRCompFields ${#outputatoms[@]}
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over human-readable heat current of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
        	        echo -e "./$EnsAvgProgName currenthr-$i.txt EnsembleAveragedCurrentHR-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedCurrentHR-$i.dat $NumHRCurrentFields $NumHRCompFields ${#outputatoms[@]}\n"
			echo -e "Exiting...\n"
			exit 1
		fi
	rm EnsembleAveragedCurrentHR-$LastGoodTrajectory.dat
	fi
        LastGoodTrajectory=$i

	#print to screen the number of the current trajectory which was processed (but do not print new-line character)
	echo -n "$i "

else
		echo -n " ($i NOT FOUND)  "
		NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]

fi	

#advance trajectory counter
i=$[$i+1]
done

mv EnsembleAveragedCurrentHR-$LastGoodTrajectory.dat currenthr$SegLength.xvg
echo -e "\nFinished Averaging over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories.\n"
}


function AverageBondPower(){
echo -e "\n\nEnsemble averaging over Bond Power files..."

LastGoodTrajectory=0

#for each next trajectory, average it with previous weighted average then remove the current trajectory output
NumSkippedTrajectories=0
i="1"
while [ "$i" -le "$MAX_TRAJ" ]
do
if [ -f BondPower-$i.txt ] ; then
	if [ 0 -eq $LastGoodTrajectory ] ; then
               cp BondPower-$i.txt EnsembleAveragedBondPower-$i.dat
        else
		./$EnsAvgProgName BondPower-$i.txt EnsembleAveragedBondPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedBondPower-$i.dat $NumBondPowerFields $NumBondPowerCompFields ${#outputatoms[@]}
		if [ "$?" != "0" ] ; then
			#if the last command returned an unsuccesful exit code, exit this script with code 1
			echo -e "\nError in Ensemble Averaging over Bond Power of trajectory $i ($[ $i-1-$NumSkippedTrajectories ]th overall):\n"
        	        echo -e "./$EnsAvgProgName BondPower-$i.txt EnsembleAveragedBondPower-$LastGoodTrajectory.dat $[ $i - $NumSkippedTrajectories ] EnsembleAveragedBondPower-$i.dat $NumBondPowerFields $NumBondPowerCompFields ${#outputatoms[@]}\n"
			echo -e "Exiting...\n"
			exit 1
		fi
	rm EnsembleAveragedBondPower-$LastGoodTrajectory.dat
	fi
        LastGoodTrajectory=$i

	#print to screen the number of the current trajectory which was processed (but do not print new-line character)
	echo -n "$i "

else
		echo -n " ($i NOT FOUND)  "
		NumSkippedTrajectories=$[ 1 + $NumSkippedTrajectories ]

fi	

#advance trajectory counter
i=$[$i+1]
done

mv EnsembleAveragedBondPower-$LastGoodTrajectory.dat BondPower.txt
echo -e "\nFinished Averaging over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories.\n"
}


function PlotCurrent(){

echo "outputatoms = ${outputatoms[@]}"
echo ${#outputatoms[@]} >> N.dat

cp $(ls -tr1 $SCRIPTS/current-*.gp | tail -n1) .
        NumPlotScripts=0
        for PlotScriptName in current-*.gp ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
        if [ 0 -eq $NumPlotScripts ] ; then echo "No current plot scripts! Exiting..." ; exit 5 ; fi
        if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting current from script" ; gnuplot ./$PlotScriptName ; if [ 0 -ne $? ] ; then echo "Error GNUPlotting current file. Exiting." ; exit 10 ; fi ; fi
        if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple current plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi
        rm $PlotScriptName


#echo -n "presenting plot using Eye Of GNOME"
#echo
#	#eog EnsembleAveragedHeatCurrent.jpg &	#show JPG file using Eye Of GNOME
#eog $INSTANCE.svg &
}

function PlotOutput(){
if [ 0 -eq $bProduction ] ; then
	cp $(ls -tr1 $SCRIPTS/Plot-*.sh | tail -n1) .
	NumPlotScripts=0
	for PlotScriptName in Plot-*.sh ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
	if [ 0 -eq $NumPlotScripts ] ; then echo "No plot scripts! Exiting..." ; exit 5 ; fi
	if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting from script" ; ./$PlotScriptName ; fi
	if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi

else

	if [ 1 -eq $bProduction ] ; then

	cp $(ls -tr1 $SCRIPTS/PlotStSt-*.sh | tail -n1) .
        NumPlotScripts=0
        for PlotScriptName in PlotStSt-*.sh ; do NumPlotScripts=$[ 1 + $NumPlotScripts ] ; done
        if [ 0 -eq $NumPlotScripts ] ; then echo "No steady-state plot scripts! Exiting..." ; exit 5 ; fi
        if [ 1 -eq $NumPlotScripts ] ; then echo "Plotting steady-state from script" ; ./$PlotScriptName ; fi
        if [ 1 -lt $NumPlotScripts ] ; then echo "Multiple steady-state plot scripts! Choose the one you want. Exiting..." ; exit 5 ; fi

	fi

fi

rm $PlotScriptName  $EnsAvgProgName
}


function SetupBathPowerPlot(){

echo "Setting up data files for bath power plots:"

DATAFILE=1-std.io # loop over trajectories and find first one which terminated normally
SimDuration=$(head -n1 options.dat | cut -d "=" -f2)
seg_length=$(head -n3 N.dat | tail -n1)
nsegs=$[ ( $SimDuration - $seg_length ) / $seg_length ]

cat $DATAFILE | head -n $[ $nsegs + 136 ] | tail -n $nsegs > test.txt

cut -d "=" -f11 test.txt | sed -n '1~2p' > LeftSulfurBondPower.txt
cut -d "=" -f11 test.txt | sed -n '0~2p' > RightSulfurBondPower.txt

echo "Presenting the top and bottom of LeftSulfurBondPower.txt for your inspection:"
head LeftSulfurBondPower.txt
echo "..."
tail LeftSulfurBondPower.txt

cut test.txt -d "=" -f 7 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurDissipationPower.txt
cut test.txt -d "=" -f 7 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurDissipationPower.txt

cut test.txt -d "=" -f 7 | cut -d " " -f1 | sed -n '0~2p' > RightSulfurDissipationPower.txt
cut test.txt -d "=" -f 8 | cut -d " " -f1 | sed -n '1~2p' > LeftSulfurRanForPower.txt
cut test.txt -d "=" -f 8 | cut -d " " -f1 | sed -n '0~2p' > RightSulfurRanForPower.txt
}


function PlotAllScripts(){
echo "Plotting all GNUPlot scripts in the parent directory:"
for i in ../*.gp
	do gnuplot $i
done
}


#main
#set -e
Setup
declare -a outputatoms ; ReadOutputAtoms # can't "declare" global variables from within a function
GetEnsAvgPrgrm

#case "${#outputatoms[@]}" in
#       2) NumOutputFields=15;;#diatomic
#       [?]) NumOutputFields=18;;
#esac

#if [ 2 -eq ${#outputatoms[@]} ] ; then NumOutputFields=15 ; NumHROutputFields=2 ; NumCurrentOutputFields=14
#else \
NumOutputFields=19 ; NumHROutputFields=$[ 1 + ${#outputatoms[@]} ] ; NumCurrentOutputFields=12 ; NumHRCurrentFields=$[ 1 + 3 * ${#outputatoms[@]} ] ; NumBondPowerFields=${#outputatoms[@]} ; NumCompFields=3 ; NumHRCompFields=1 ; NumBondPowerCompFields=0
#fi

#Send $INSTNACE as command-line arguemtn to plotting scripts, to be used as file name
AverageOutput
AverageHROutput
AverageCurrent # Check advantages of changing GP script to BASH script
AverageHRCurrent
AverageBondPower
#PlotCurrent
echo "5-9-14: skipped plotting current. Consider restoring this for non-steady-state trajectories."
if [ 1 -eq $(echo $DeltaT | awk '{if (0 == $1) print(0) ; else print(1)}') ]
	then
	PlotOutput
fi

#Added 29-5-13
NBATHS=$(head -n11 options.dat | tail -n1 | cut -d "=" -f2)
if [ 2 -eq $NBATHS ] ; then
	#Added 19-5-13
	SetupBathPowerPlot
fi

echo "5-9-14: skipping plotting of bath powers. To restore these, the gnuplot script should be called here."
#set +e
#PlotAllScripts
#set -e


#Script termination
date +%H:%M:%S.%N
echo -e "\n(Averaged over Ensemble of $[ $MAX_TRAJ - $NumSkippedTrajectories ] good trajectories out of $MAX_TRAJ submitted)\n"

echo

#if all went correctly, exit with code 0
echo "#Normal termination of Ensemble Averaging script."
exit 0
