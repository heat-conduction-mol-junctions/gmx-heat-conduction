#! /bin/bash

PARSE=$HOME/gmx-heat-conduction/bin/Parse
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then PARSE=$HOME/Documents/GROMACS/Parse ; fi

nsteps=$(head -n3 grompp-*-1.mdp | tail -n1 | cut -d "=" -f2)
if [ 30000 -gt $nsteps ] ; then ConvolutionWindow=$nsteps ; else ConvolutionWindow=30000 ; fi
#ConvolutionWindow=5000

MAX_TRAJ=1
MAX_TRAJ=$(head -n1 N.dat)
for i in $(seq 1 $MAX_TRAJ) ; do
	echo -e "\n\n$i\n"
	head -n $[ 121 + $nsteps ] $i-std.err | tail -n $[ $nsteps - 122 ] | sed -n '3~3p' | cut -d " " -f4 > RanForX-$i.txt # from line 122 to line 10120, inclusive
	$PARSE/Convolute-23-12-12.out RanForX-$i.txt $ConvolutionWindow Convolution-19-12-12-$i.txt 
	if [ 0 -ne $? ] ; then exit 10 ; fi
	rm RanForX-$i.txt
	if [ 1 -eq $i ] ; then 
		mv Convolution-19-12-12-1.txt EnsAvg_CRR-1.txt 
	else  
		$PARSE/EnsembleAverage-23-11-12.out Convolution-19-12-12-$i.txt EnsAvg_CRR-$[ $i - 1].txt $i EnsAvg_CRR-$i.txt 1 1 
		if [ 0 -ne $? ] ; then exit 9 ; fi
		rm Convolution-19-12-12-$i.txt EnsAvg_CRR-$[ $i - 1].txt
	fi
done

#CRR0=(2*m*kB*T/(tau_t*dt))
k_B=8.31446
ref_T=$(grep ref_t grompp-*-1.mdp | cut -f4)
m=1.
tau_T=$(grep tau_t grompp-*-1.mdp | cut -f4)
dt=$(grep dt grompp-*-1.mdp | cut -d "=" -f2)

echo -e "\n\nk_B=$k_B\nref_T/K=$ref_T\nm/a.m.u.=$m\ntau_T/ps=$tau_T\ndt/ps=$dt\nConvolutionWindow=$ConvolutionWindow\nCRR(0) = $(head -n1 EnsAvg_CRR-$MAX_TRAJ.txt)\nCRR(dt)/CRR(0) = $(echo $(head -n2 EnsAvg_CRR-$MAX_TRAJ.txt | tail -n1) $(head -n1 EnsAvg_CRR-$MAX_TRAJ.txt) | awk '{print($1/$2)}')\n2*m*kB*T/tau_t*dt=$(echo $m $k_B $ref_T $tau_T $dt | awk '{print(2*$1*$2*$3/($4*$5))}')\nCRR(0)/(2*m*kB*T/tau_t*dt)=$(echo $(head -n1 EnsAvg_CRR-$MAX_TRAJ.txt) $m $k_B $ref_T $tau_T $dt | awk '{print($1/(2*$2*$3*$4/($5*$6)))}')\n"
