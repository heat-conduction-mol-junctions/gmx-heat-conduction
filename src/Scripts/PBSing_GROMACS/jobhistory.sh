#! /bin/bash

##### Find submitted PBS job history by searching /opt/torque

##### change USER for which to look
USER=$LOGNAME

##### Find all dates on which USER submitted a job
cd /opt/torque/server_logs
grep $USER * -l

##### Output is list of text file names with the format YYYYMMDD

echo "Enter day to look in (format YYYYMMDD):"
read JobDate
grep $USER@$HOSTNAME $JobDate 

##### Output is a Semicolon Seperated Values table with the following field: 
##### For successful jobs:
##### "DD/MM/YYYY HH:MM:SS" ; "0008" ; Submitted to ("PBS_Server") ; Submition type ("Job") ; "Job ID".$HOSTNAME ; Other
##### Other is a Comma Seperated Values line with the fields:
##### Job Queued at request of "$USER@$HOSTNAME", owner = "$USER@$HOSTNAME", job name = "", queue = "default"
##### For unsuccessful jobs:
##### "DD/MM/YYYY HH:MM:SS" ; "0080" ; Submitted to ("PBS_Server") ; Submition type ("Req") ; "req_reject" ; Other
##### Other is a Comma Seperated Values line with the fields:
##### "Reject reply code=15001(Unknown Job Id)" or "Reject reply code=15001(Unknown Job Id MSG=cannot locate job)", "aux=0", type="SignalJob" or "LocateJob" ; from $USER@$HOSTNAME
#####
##### This can be parsed


##### Choose Job ID
JobId=


##### Number of microseconds since 1-1-1970
MicroSecondsSince1970=$[ `date +%s%N` / 1000 ]

##### Full days since 2010
FullDaysSince2010=$[ `date +%s` / ( 24 * 60 * 60 ) - 365 * ( 2010 - 1970 ) ]

##### Number of days in the past to look through
DaysToLook=$FullDaysSince2010 # without the argument the default value is 1 day

tracejob -v -n $DaysToLook $JobId

exit 0
