#PBS -v INSTANCE,TRAJ_COUNT
#PBS -N GROMACS-$LOGNAME-$INSTANCE-$TRAJ_COUNT
#PBS -l nodes=1:ppn=1,walltime=72:00:00
#PBS -o $INSTANCE/File_Locations-std-$TRAJ_COUNT.io
#PBS -e $INSTANCE/File_Locations-std-$TRAJ_COUNT.err

#Explanation of the PBS header:
#use 1 processor on 1 node and a maximal wall-time of three days
#forward the -v environment variables along with the default ones (PBS_O_*) from the head node to the compute node
#put the standard I/O and error files in the sub-directory called $INSTANCE of the $PBS_O_INITDIR where this script was called. use the $TRAJ_COUNT to identify this trajectory's files.
#Make sure there are no space characters in the paths!


echo =======================================================================================================================


#part 1: make a sub-directory in the local scratch directory, change to that dirctory, and copy the contents of the origin dirctory there

echo
echo ==============================
echo PBS_O_INITDIR = $PBS_O_INITDIR
echo ==============================
echo
#Make a sub-directory under the scratch directory on the compute (remote) node, where GROMACS will get its input files and send its output files, and change to that directory
mkdir -p /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
cd /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
echo
#Copy to the current directory the entire contents of the original directory (under the $HOME directory) where the original script calling this one was run
scp $PBS_O_INITDIR/* .
echo

echo =======================================================================================================================

#part 2: GROMACS

maxwarn="0"

ndxgrp1="2"	# the index group from which the heat flows
ndxgrp2="1"	# the index group into which the heat flows

# generate random velocities for atoms from a 300K Maxwell-Boltzmann distribution
grompp_mpi -f eq -c b4eq -n -t b4eq -maxwarn "$maxwarn" # preprocess using b4eq.gro as input configuration, in double precision (from b4eq.trj file)
mdrun_mpi -c b4md -v                                    # run and output configuration to b4md.gro

# couple leads to thermal reservoirs
grompp_mpi -f md -c b4md -n -t -maxwarn "$maxwarn"	# preprocess using b4md.gro as input configuration, in double precision (from trj file)
mdrun_mpi -c md -v					# run and output configuration to md.gro


echo | echo 7 8 | g_energy				# extract Kinetic and Total Energies from ener.edr and output energy.xvg


# extract phase data (velocity) for the index groups named "ndxgrp2" and "ndxgrp1"
# note that the *.trr and not the *.xtc file needs to be read in order to get velocities
echo $ndxgrp2 | g_traj -f traj.trr -n -ov v_ndxgrp2	# select index group 2 in interactive menu of g_traj
echo $ndxgrp1 | g_traj -f traj.trr -n -ov v_ndxgrp1	# select index group 1 in interactive menu of g_traj

# rerun last trajectory, and throw out all energetic information of interactions other than relevant pair (Au-S)
grompp -f md_rerun -c b4md -n -p topol_rerun.top -t -maxwarn "$maxwarn"	# preprocess using existing traj.trr file from previous md run, and "rerun" options in md_rerun.mdp & topol_rerun.top
mdrun -o rerun -rerun traj.trr -v						# rerun using traj.trr and output force data into rerun.trr

# extract force data for the index group named "ndxgrp2" 
echo $ndxgrp2 | g_traj -f rerun.trr -n -of f_ndxgrp2	# select index group 2 in interactive menu of g_traj


echo "cleaning phase data files and preparing for computation of heat current"
#deleting first 21 lines in each phase data file -- which contain formatting information for xmgrace -- and outputing to *.dat files
echo "-----------------------------------------------------------------------"
echo

sed '1,21d' f_ndxgrp2.xvg >fs2.dat
sed '1,21d' v_ndxgrp1.xvg >vs1.dat
sed '1,21d' v_ndxgrp2.xvg >vs2.dat
sed '1,20d' energy.xvg >energy.dat

echo
echo "computing heat-current for trajectory number" $TRAJ_COUNT
echo "--------------------------------------------------------"
echo
# takes f on the ndxgrp2 atom from first argument & v on the ndxgrp1 and ndxgrp2 atoms from second and third arguments, outputs into fourth argument.
./parse-19-5-09.out fs2.dat vs1.dat vs2.dat HeatCurrent.dat


echo =======================================================================================================================


#part 3: interlude

#Print out some environment variables, most importantly the $HOSTNAME of the compute node on which this trajectory was run adn the value of the $TRAJ_COUNT (trajectory counter) which is unique for this trajectory, under this $INSTANCE of running the scripts
echo
echo TRAJ_COUNT=$TRAJ_COUNT, HOSTNAME=$HOSTNAME
echo 				\(compute-0-XX.local\)
echo


echo =======================================================================================================================


#part 4: copy output to sub-directory on $HOME

#Copy the meaningful output files from the current directory to the sub-directory of the $PBS_O_INITDIR which was created uniquely for this $INSTANCE of running the scripts
scp ener.edr $PBS_O_INITDIR/$INSTANCE/ener-$TRAJ_COUNT.edr
scp traj.trr $PBS_O_INITDIR/$INSTANCE/traj-$TRAJ_COUNT.trr
scp md.log   $PBS_O_INITDIR/$INSTANCE/md-$TRAJ_COUNT.log


echo =======================================================================================================================


#part 5: cleanup compute /scratch

#Change directory to the sub-directory on the scratch directory named as the user's $LOGNAME and specifically for this $INSTANCE, remove the sub-directory with this $TRAJ_COUNT
cd /scratch/$LOGNAME/$INSTANCE
rm -r $TRAJ_COUNT


echo =======================================================================================================================

#Exit with succesful exit status ID = 0
exit 0
