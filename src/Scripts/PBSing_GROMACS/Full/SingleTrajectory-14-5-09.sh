#! /bin/bash
#PBS -N $LOGNAME@$HOSTNAME-GROMACS-$$
#PBS -l nodes=1:ppn=1,walltime=96:00:00

SCR_DIR="/scratch/$LOGNAME/$PBS_JOBID/"
mkdir -p $SCR_DIR

cp $PBS_O_WORKDIR/b4eq.gro	$SCR_DIR	# copy minimized energy configuration (*.gro) file to scratch dir
cp $PBS_O_WORKDIR/*.itp		$SCR_DIR	# copy included topology (*.itp) files to scratch dir
cp $PBS_O_WORKDIR/*.mdp		$SCR_DIR	# copy MD preprocess (*.mdp) files to scratch dir
cp $PBS_O_WORKDIR/index.ndx	$SCR_DIR	# copy group index (*.ndx) file to scratch dir
cp $PBS_O_WORKDIR/*.out		$SCR_DIR	# copy compiled C programs (*.out) to scratch dir
cp $PBS_O_WORKDIR/b4eq.trr	$SCR_DIR	# copy minimized energy trajectory (*.trr) file to scratch dir


maxwarn="0"

ndxgrp1="2"	# in the thiol case, group 3 is the Sulfur atom, and the pairwise force is with respect to group 4 which is the Gold atom
ndxgrp2="1"	# in the thiol case, group 3 is the Sulfur atom, and the pairwise force is with respect to group 4 which is the Gold atom

HeatCurrentFileName=EnsembleAveragedHeatCurrent$N.dat


sed >$HeatCurrentFileName

TEMPDIR="/home/inonshar/GROMACS/"

# generate random velocities for atoms from a 300K Maxwell-Boltzmann distribution
grompp_mpi -f eq -c b4eq -n -t b4eq -maxwarn "$maxwarn"	# preprocess using b4eq.gro as input configuration, in double precision (from b4eq.trj file)
mdrun_mpi -c b4md -v					# run and output configuration to b4md.gro

# couple leads to thermal reservoirs
grompp_mpi -f md -c b4md -n -t -maxwarn "$maxwarn"	# preprocess using b4md.gro as input configuration, in double precision (from traj.trr file)
mdrun_mpi -c md -v					# run and output configuration to md.gro


echo | echo 7 8 | g_energy_d				# extract Kinetic and Total Energies from ener.edr and output energy.xvg


# extract phase data (velocity) for the index groups named "ndxgrp2" and "ndxgrp1"
# note that the *.trr and not the *.xtc file needs to be read in order to get velocities
echo $ndxgrp2 | g_traj_mpi -f traj.trr -n -ov v_ndxgrp2	# select index group 2 in interactive menu of g_traj
echo $ndxgrp1 | g_traj_mpi -f traj.trr -n -ov v_ndxgrp1	# select index group 1 in interactive menu of g_traj

# rerun last trajectory, and throw out all energetic information of interactions other than relevant pair (Au-S)
grompp_mpi -f md_rerun -c b4md -n -p topol_rerun.top -t -maxwarn "$maxwarn"	# preprocess using existing traj.trr file from previous md run, and "rerun" options in md_rerun.mdp & topol_rerun.top
mdrun_mpi -o rerun -rerun traj.trr -v						# rerun using traj.trr and output force data into rerun.trr

# extract force data for the index group named "ndxgrp2" 
echo $ndxgrp2 | g_traj_mpi -f rerun.trr -n -of f_ndxgrp2	# select index group 2 in interactive menu of g_traj


echo
echo "cleaning phase data files and preparing for computation of heat current"
#deleting first 21 lines in each phase data file -- which contain formatting information for xmgrace -- and outputing to *.dat files
echo "-----------------------------------------------------------------------"
echo

sed '1,21d' f_ndxgrp2.xvg >fs2.dat
sed '1,21d' v_ndxgrp1.xvg >vs1.dat
sed '1,21d' v_ndxgrp2.xvg >vs2.dat
sed '1,20d' energy.xvg >energy.dat

echo
echo "computing heat-current for trajectory number $i"
echo "-----------------------------------------------"
echo
# takes f on the ndxgrp2 atom from first argument & v on the ndxgrp1 and ndxgrp2 atoms from second and third arguments, outputs into fourth argument, where the existing data in the fourth argument file is averaged with the new data according to the weight of the number of past trajectories (the fifth argument) already averaged and output into the sixth argument.
./parse-12-4-09.out fs2.dat vs1.dat vs2.dat $HeatCurrentFileName TempHeatCurrent.txt $i energy.dat
mv TempHeatCurrent.txt $HeatCurrentFileName

echo "done."
echo

# remove all left over files
#rm fs2.dat
#rm vs1.dat
#rm vs2.dat
#rm *.dat
rm f_ndxgrp2.xvg
rm v_ndxgrp1.xvg
rm v_ndxgrp2.xvg
rm energy.dat
rm energy.xvg
rm ener.edr
rm mdout.mdp
rm *.cpt
rm *#

echo
echo "===================================="
echo
echo "Finished. Presenting through xmgrace"
sed '1i\# this line is a comment line\n@    title "Heat Current and Energy"\n@    xaxis  label "Time [femtoseconds]"\n@    yaxis  label "Energy / Energy Current [kJ/mole]/[kJ/mole per ps]"\n\@TYPE xy\n@ view 0.1, 0.1, 1.25, 0.875\n@ legend on\n@ legend loctype view\n@ legend 1, 0.85\n@ s0 legend "Heat Current"\n@ s1 legend "Kinetic Energy"\n@ s2 legend "Total Energy"' $HeatCurrentFileName >EnsembleAveragedHeatCurrent$N.xvg 
echo
xmgrace -nxy EnsembleAveragedHeatCurrent$N.xvg  &


