#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Static dimensions:
#define DIM 3
#define N_SCHEMES 3
// Other definitions:
#define Rgas 8.3144 		//Gas constant in J per mole Kelvin
#define FORCE_NAME_LENGTH 100
#define MAX_PARTICIPANTS 4	//ignore the case of 5-member interactions (or more)

//DEV NOTE: Check whether any of the A[B].C[D][E] should be A[B]->C[D][E] (so far, the compiler did not object to X.Y but did to X->Y, for all existing instances)
//DEV NOTE: Try to make some variables static or dynamic


//==========================================================================================================

typedef double dvec[DIM];//define new variable type, "dvec": DIM-dimensional vector of double-percission numbers
typedef double schemecoef[N_SCHEMES];//define new variable type, "schemecoef": N_SCHEMES-dimensional vector of double-percission numbers

typedef struct {//define structure template "iterm_t" for holding a single interaction term and all its relevant information
	int nr;//number of participating atoms in this term
	int *iatoms;//atom indexes for participants in this term (pointer to array/vector of length "nr")
	dvec *f;//force vectors for each participant in this term (array of length "nr" of vectors of length "DIM")
	double vterm;//potential energy due to this term
	schemecoef *c;//potential localization coefficients for this term (array of length "nr" of vectors of length "N_SCHEMES")
	//"iatoms", "f" and "c" could be merged into one (composite) data structure of dimensionality "nr"
	double *termPower0;
	double termPower0tot;
	schemecoef *termLocalPower;
} iterm_t;//suffix _t stands for template (due to typedef)

typedef struct {//define structure template "state_t" for holding phase space coordinates for all atoms, in a given time-step
	//int natoms;//originally in state.h
	double t;//originally local in md.c
	dvec *x;//positions  (array of length "natoms" of vectors of length "DIM")
	dvec *v;//velocities (array of length "natoms" of vectors of length "DIM")
	dvec *vfrog;//anti- leap-frogged velocities (array of length "natoms" of vectors of length "DIM")
} state_t;

//-----------------------------------------------------------------------------------------------------------


//DEV NOTE: When pointers to scalars (int, double...) are passed as arguments to functions, only their value is copied and passed, not the original's address. Therefore, any changes made to these variables is local to the function, rather than global. To change the global value of a scalar pointed to by a pointer, pass the pointer adress to the function, and then change what appears at that adress from within the function.

//function prototypes:
int ColumnHeadings(int natoms, FILE *outfile, FILE *hrout, int a_hr);
//int ReadTop(FILE *trajectory, int natoms, double m[]);
int ReadNAtoms(FILE *trajectory, int natoms);
int ReadAtomMasses(FILE *trajectory, int natoms, double m[]);
int MaxTerms(int natoms, int maxterms);
int ReadPhase(FILE *trajectory, int natoms, double **TempPhaseTime, state_t state);
int ChkAtmSuprpos(int natoms, state_t state, double TempTime);
int CalcKin(int natoms, double m[], double k[], state_t state);
int AntiLeapFrog(int natoms, state_t state, state_t stateprev, double k[],
		double kprev[], double kfrog[]);
int ReadForce(FILE *forces, int tau, iterm_t *itermlist);
int PotLoc(int tau, iterm_t *itermlist, schemecoef *u);
int CalcTermPow(int tau, state_t state, iterm_t *itermlist, schemecoef *power);
int CoarseGrainTimeSeg(state_t state, state_t stateprev, int step, FILE *hrout,
		FILE *outfile, schemecoef *poweracc, schemecoef *DeltaE,
		int seg_length, int natoms, double kfrog[], schemecoef *u, int a_hr);
int SetupNextTimeStep(int natoms, state_t state, state_t stateprev, double k[],
		double kprev[], double kfrog[], double kfrogprev[], schemecoef *u,
		schemecoef *uprev, schemecoef *power);
int FinishUp(int step, int seg_length, FILE *outfile, FILE *hrout);

//==========================================================================================================

//MAIN:
int main(int argc, char *argv[], char *envp[]) {
	//this C program takes two input files (one of velocities and another of forces) and calculates the energy current through the atoms
	/* program arguments:
	 argv[1]	trajectory file name
	 argv[2]	forces file name
	 argv[3]	output file name
	 argv[4]	output time-averaging segment length (in number of steps)
	 argv[5]	number of atoms (NATOMS) : read -r NATOMS < trajectory.txt ; echo $NATOMS
	 in all: 5 argument variables (argc=6)
	 */

//-----------------------------------------------------------------------------------------------------------

	//	if (argc!=6) {fprintf(stderr,"\n\nWrong number of arguments: %d instead of 5\n\n",argc-1);exit (1);}
	if (argc != 5) {
		fprintf(stderr, "\n\nWrong number of arguments: %d instead of 4\n\n",
				argc - 1);
		exit(1);
	}

	int bForcesStepOver = 0, a_hr = 0, maxterms = 0, LastTermIndex = 0, tau;
	int i, j, a, q, step = 0, scanfnum;
	const int seg_length = atoi(argv[4]);

	int natoms = 0;
	/*
	 int	natoms=atoi(argv[5]);//this should also be const but then we wouldn't be able to define variable length arrays (with length natoms).
	 */

	FILE *trajectory = fopen(argv[1], "rt");
	if (trajectory == NULL) {
		fputs("\n\nError opening trajectory file\n", stderr);
		exit(1);
	}
	FILE *forces = fopen(argv[2], "rt");
	if (forces == NULL) {
		fputs("\n\nError opening forces file\n", stderr);
		exit(1);
	}
	FILE *outfile = fopen(argv[3], "wt");
	if (outfile == NULL) {
		fputs("\n\nError opening out file\n", stderr);
		exit(1);
	}
	char hroutfilename[FILENAME_MAX];
	sprintf(hroutfilename, "hr%s", argv[3]);
	FILE *hrout = fopen(hroutfilename, "wt");
	if (hrout == NULL) {
		fputs("\n\nError opening human-readable out file\n", stderr);
		exit(1);
	}
	char tempterm[FORCE_NAME_LENGTH];
	double tforces, *TempPhaseTime = NULL, *m = NULL, *k = NULL, *kprev = NULL,
			*kfrog = NULL, *kfrogprev = NULL;
	dvec *b = NULL;

	iterm_t *itermlist = NULL, *templist = NULL;//interaction term list (array of length "maxterms" of structures of type interactionTerm)

	state_t state, stateprev;//structure "state" (unnecessary, but in keeping with the GROMACS md.c form: state->x[j][q] , etc.)

	schemecoef *power = NULL, *poweracc = NULL, *DeltaE = NULL, *u = NULL,
			*uprev = NULL;

//==========================================================================================================

	//read natoms from top of trajectory file
	natoms = ReadNAtoms(trajectory, natoms);
	if (0 == natoms) {
		fputs("\nFailed to read natoms from top of trajectory file\n", stderr);
		exit(1);
	}
	/*	if (0!=ReadNAtoms(trajectory,natoms)){
	 fputs("\nFailed to read natoms from top of trajectory file\n",stderr);exit(1);}
	 */
	/*	if (0!=ReadTop(trajectory,natoms,m)){//read molecular topology from top of trajectory file
	 fputs("\nFailed to read molecular topology from top of trajectory file\n",stderr);exit(1);}
	 */
	if (natoms == 0) {
		fputs("\nnatoms=0, exiting...\n\n", stderr);
		exit(32);
	}//DEBUG
	//	else printf("\n\nDEBUG: natoms=%d\n\n",natoms);//DEBUG

//-----------------------------------------------------------------------------------------------------------

	//DEV NOTE: put this into a function:
	//dynamically allocate data structures with dimensionality "natoms" here
	m = (double *) malloc(natoms * sizeof(double));
	if (m == NULL) {
		fputs("\nCould not allocate memory for atom masses\n", stderr);
		exit(1);
	}

	state.x = (dvec *) malloc(natoms * sizeof(dvec));
	if (state.x == NULL) {
		fputs("\nCould not allocate memory for positions\n", stderr);
		exit(1);
	}
	state.v = (dvec *) malloc(natoms * sizeof(dvec));
	if (state.v == NULL) {
		fputs("\nCould not allocate memory for velocities\n", stderr);
		exit(1);
	}
	state.vfrog = (dvec *) malloc(natoms * sizeof(dvec));
	if (state.vfrog == NULL) {
		fputs("\nCould not allocate memory for leap-frog velocities\n", stderr);
		exit(1);
	}
	stateprev.x = (dvec *) malloc(natoms * sizeof(dvec));
	if (stateprev.x == NULL) {
		fputs("\nCould not allocate memory for previous step positions\n",
				stderr);
		exit(1);
	}
	stateprev.v = (dvec *) malloc(natoms * sizeof(dvec));
	if (stateprev.v == NULL) {
		fputs("\nCould not allocate memory for previous step velocities\n",
				stderr);
		exit(1);
	}
	stateprev.vfrog = (dvec *) malloc(natoms * sizeof(dvec));
	if (stateprev.vfrog == NULL) {
		fputs(
				"\nCould not allocate memory for previous step leap-frog velocities\n",
				stderr);
		exit(1);
	}

	b = (dvec *) malloc(natoms * sizeof(dvec));
	if (b == NULL) {
		fputs("\nCould not allocate memory for bond lengths\n", stderr);
		exit(1);
	}
	k = (double *) malloc(natoms * sizeof(double));
	if (k == NULL) {
		fputs("\nCould not allocate memory for kinetic energies\n", stderr);
		exit(1);
	}
	kprev = (double *) malloc(natoms * sizeof(double));
	if (kprev == NULL) {
		fputs(
				"\nCould not allocate memory for previous half-step kinetic energies\n",
				stderr);
		exit(1);
	}
	kfrog = (double *) malloc(natoms * sizeof(double));
	if (kfrog == NULL) {
		fputs("\nCould not allocate memory for leap-frog kinetic energies\n",
				stderr);
		exit(1);
	}
	kfrogprev = (double *) malloc(natoms * sizeof(double));
	if (kfrogprev == NULL) {
		fputs(
				"\nCould not allocate memory for previous step leap-frog kinetic energies\n",
				stderr);
		exit(1);
	}

	power = (schemecoef *) malloc(natoms * sizeof(schemecoef));
	if (power == NULL) {
		fputs("\nCould not allocate memory for local power\n", stderr);
		exit(1);
	}
	poweracc = (schemecoef *) malloc(natoms * sizeof(schemecoef));//is this variable really necessary?
	if (poweracc == NULL) {
		fputs("\nCould not allocate memory for local power accumulator\n",
				stderr);
		exit(1);
	}//poweracc is an accumulator for power but shouldn't be absolutely necessary. However, using power as an accumulator in itself did not work for some reason (power just got the value of the power in the current time-step, as opposed to the sum of the powers over the multiple time-steps in the time-averaging segment (check for instance the case of seg_length=2).

	DeltaE = (schemecoef *) malloc(natoms * sizeof(schemecoef));
	if (DeltaE == NULL) {
		fputs("\nCould not allocate memory for local energy diference\n",
				stderr);
		exit(1);
	}
	u = (schemecoef *) malloc(natoms * sizeof(schemecoef));
	if (u == NULL) {
		fputs("\nCould not allocate memory for local potential energy\n",
				stderr);
		exit(1);
	}
	uprev = (schemecoef *) malloc(natoms * sizeof(schemecoef));
	if (uprev == NULL) {
		fputs(
				"\nCould not allocate memory for previous step local potential energy\n",
				stderr);
		exit(1);
	}

	maxterms = MaxTerms(natoms, maxterms);
	if (0 == maxterms) {
		fputs("\nError calculating maximum number of interaction terms\n",
				stderr);
		exit(1);
	}//maximum number of interaction terms (from combinatorical considerations)

//-----------------------------------------------------------------------------------------------------------

	//read atom masses from top of trajectory file
	if (0 != ReadAtomMasses(trajectory, natoms, m)) {
		fputs("\nFailed to read atom masses from top of trajectory file\n",
				stderr);
		exit(1);
	}
	//	printf("\nDEBUG: ");for (j=0;j<natoms;j++) printf("m[%d]=%7.14lf  ",j,m[j]);printf("\n\n");//DEBUG

//-----------------------------------------------------------------------------------------------------------

	//initialize variables
	//DEV NOTE: only accumulator variables need be initialized...
	state.t = stateprev.t = 0;
	for (j = 0; j < natoms; j++) {
		k[j] = 0;
		kprev[j] = 0;
		kfrog[j] = 0;
		for (i = 0; i < natoms; i++) {
			power[i][j] = 0;
		}
		for (a = 0; a < N_SCHEMES; a++) {
			power[j][a] = 0;
			poweracc[j][a] = 0;
			DeltaE[j][a] = 0;
		}
	}

//-----------------------------------------------------------------------------------------------------------

	if (0 != ReadPhase(trajectory, natoms, &TempPhaseTime, stateprev)) {//read zeroeth step phase space coordinates
		fputs("\nFailed to read first step phase data from trajectory file\n",
				stderr);
		exit(1);
	}
	stateprev.t = *TempPhaseTime;

//-----------------------------------------------------------------------------------------------------------

	if (0 != CalcKin(natoms, m, kprev, stateprev)) {
		;//calculate zeroeth step (minus half-step) kinetic energy
		fputs("\nFailed to calculate first step kinetic energy\n", stderr);
		exit(1);
	}

//==========================================================================================================

	if (0 != ColumnHeadings(natoms, outfile, hrout, a_hr)) {//print output column headings to file
		fputs("\nFailed to print column headings in output files\n", stderr);
		exit(1);
	}

//==========================================================================================================

	while (feof(trajectory) == 0) {//while the trajectory file hasn't returned end_of_file:

		//read entire trajectory file line for the next time-step: Since GROMACS uses a leap-frog algorithm, in this step we will only use the kinetic energy from the next (half) step, in order to get the velocities at this step)

		if (0 != ReadPhase(trajectory, natoms, &TempPhaseTime, state)) {//read current step phase space coordinates
			fputs("\nFailed to read phase data from trajectory file\n", stderr);
			exit(1);
		}
		state.t = *TempPhaseTime;

//-----------------------------------------------------------------------------------------------------------

		//read forces file lines for this step

		//dynamic allocation for unknown number of interaction terms
		//DEV NOTE: Put this in a function!
		if (step == 0) {//unknown # of terms ab-initio

			LastTermIndex = maxterms;
			itermlist = (iterm_t *) malloc(LastTermIndex * sizeof(iterm_t));
			if (itermlist == NULL) {
				fputs(
						"\nCould not allocate memory for interaction term list on first time step\n",
						stderr);
				exit(1);
			}

			for (tau = 0; tau < LastTermIndex; tau++) {
				itermlist[tau].nr = MAX_PARTICIPANTS;
				itermlist[tau].f = (dvec *) malloc(itermlist[tau].nr
						* sizeof(dvec));
				if (itermlist[tau].f == NULL) {
					fputs(
							"\nCould not allocate memory for force terms on first time step\n",
							stderr);
					exit(1);
				}
				itermlist[tau].iatoms = (int *) malloc(itermlist[tau].nr
						* sizeof(int));
				if (itermlist[tau].iatoms == NULL) {
					fputs(
							"\nCould not allocate memory for interacting atoms indexes on first time step\n",
							stderr);
					exit(1);
				}
				itermlist[tau].c = (schemecoef *) malloc(itermlist[tau].nr
						* sizeof(schemecoef));
				if (itermlist[tau].c == NULL) {
					fputs(
							"\nCould not allocate memory for localization coefficients on first time step\n",
							stderr);
					exit(1);
				}
				itermlist[tau].termPower0 = (double *) malloc(itermlist[tau].nr
						* sizeof(double));
				if (itermlist[tau].termPower0 == NULL) {
					fputs(
							"\nCould not allocate memory for local power base unit\n",
							stderr);
					exit(1);
				}
				itermlist[tau].termLocalPower = (schemecoef *) malloc(
						itermlist[tau].nr * sizeof(schemecoef));
				if (itermlist[tau].termLocalPower == NULL) {
					fputs("\nCould not allocate memory for local power\n",
							stderr);
					exit(1);
				}
			}

		}

//-----------------------------------------------------------------------------------------------------------

		//dynamic allocation for now known number of interaction terms
		//DEV NOTE: Put this in a function!
		if (step == 1) {//reallocate f[][][] according to # of terms known from last step. For step>1 no new reallocation is necessary (but remember to free up memory when closing!)

			templist = (iterm_t *) realloc(itermlist, LastTermIndex
					* sizeof(iterm_t));
			if (templist == NULL) {
				fputs(
						"\nCould not reallocate memory for interaction term list on second time step\n",
						stderr);
				exit(1);
			}
			itermlist = templist;

			/*
			 for(tau=0;tau<LastTermIndex;tau++){
			 templist[tau].nr=itermlist[tau].nr;
			 templist[tau].f=(dvec *)realloc(itermlist[tau].f,templist[tau].nr*sizeof(dvec));
			 if (tempforce==NULL){fputs("\nCould not reallocate memory for force terms on second time step\n",stderr);exit(1);}
			 itermlist[tau].f=tempforce;free(tempforce);
			 tempiatoms=(int *)realloc(itermlist[tau].iatoms,templist[tau].nr*sizeof(int));
			 if (tempiatoms==NULL){fputs("\nCould not reallocate memory for interacting atoms indexes on second time step\n",stderr);exit(1);}
			 itermlist[tau].iatoms=tempiatoms;free(tempiatoms);
			 tempcoef=(schemecoef *)realloc(itermlist[tau].c,templist[tau].nr*sizeof(schemecoef));
			 if (tempcoef==NULL){fputs("\nCould not reallocate memory for localization coefficients on second time step\n",stderr);exit(1);}
			 itermlist[tau].c=tempcoef;free(tempcoef);
			 }

			 */

		}

		//DEV NOTE: Put this in a function!
		tau = 0;
		bForcesStepOver = 0;

		while (1 != bForcesStepOver) {

			//read force type
			if (fgets(tempterm, FORCE_NAME_LENGTH, forces) == NULL) {
				fputs("\n\nError reading forces file (force headers)\n", stderr);
				exit(1);
			}

			//			printf("\nDEBUG: [%d][%lf] ----------------------------------------------------------------------- \n",tau,tforces);//DEBUG
			//			printf("\nDEBUG: tempterm[%d] string (including final \\n) is:	\"%s\"\n",tau,tempterm);//DEBUG
			//			printf("\nDEBUG: tempterm[%d] integer is:				\"%d\"\n\n",tau,(int)atoi(tempterm));//DEBUG


			if ((int) atoi(tempterm) == 0) {//if it's a a new time-step:
				scanfnum = fscanf(forces, "%lf\n", &tforces);
				if ((scanfnum != 1) || (fabs(tforces - stateprev.t) > (tforces
						* 1E-3))) {
					if (tforces != stateprev.t)
						fprintf(
								stderr,
								"\n\nDEBUG: tforces=%g - stateprev.t=%g = %g\n\n",
								tforces, stateprev.t, tforces - stateprev.t);
					fputs("\n\nError in forces file time-stamp\n", stderr);
					exit(1);
				}
				bForcesStepOver = 1;
				if (step == 0)
					LastTermIndex = tau;

				//printf("\n\n============================================================================================\n\n");//DEBUG
			}

			else {//it's a force term
				itermlist[tau].nr = (int) atoi(tempterm);
				ReadForce(forces, tau, itermlist);
				tau++;

				//DEV NOTE: Move PotLoc and TermPowCalc HERE, eliminating the need for *itermlist (instead, only one iterm_t is handled at any given time -- their contributions to the power are addative, and they are local in time).


			}
		}//end reading forces for this step


//==========================================================================================================

		//initialize energies (and related variables) for current step
		for (j = 0; j < natoms; j++)
			k[j] = 0;
		for (a = 0; a < N_SCHEMES; a++)
			for (j = 0; j < natoms; j++)
				u[j][a] = 0;

//-----------------------------------------------------------------------------------------------------------

		//vector pointing from j to j+1
		for (j = 0; j < natoms; j++) {
			for (q = 0; q < DIM; q++) {
				b[j][q] = (j != natoms - 1) ? stateprev.x[j + 1][q]
						- stateprev.x[j][q] : -1.0 * b[j - 1][q];
			}
		}

//-----------------------------------------------------------------------------------------------------------

		//calculate leap-frog kinetic energy and velocities based on the previous and next half-steps
		if (0 != CalcKin(natoms, m, k, state)) {
			fputs("\nError in recalculating kinetic energies\n", stderr);
			exit(1);
		}//next half-step kinetic energy
		if (0 != AntiLeapFrog(natoms, state, stateprev, k, kprev, kfrog)) {
			fputs("\nError in reversing leap-frog algorithm\n", stderr);
			exit(1);
		};

//==========================================================================================================

		for (tau = 0; tau < LastTermIndex; tau++)
			PotLoc(tau, itermlist, u);//localize potential energy

//==========================================================================================================

		//(accumulator of the) local energy change during the current time-step
		for (j = 0; j < natoms; j++)
			for (a = 0; a < N_SCHEMES; a++)


				{//DEBUG


//				DeltaE[j][a] += kfrog[j] + u[j][a] - kfrogprev[j] - uprev[j][a];
				DeltaE[j][a] += u[j][a] - uprev[j][a];
/*
				//DEBUG
				if ((step == 0) && (a == 0) && (j == 0)) printf("step	a	j	kfrog		u		kfrogprev	uprev		DeltaE\n");
				if (step != 0) printf("%d	%d	%d	%e	%e	%e	%e	%+e\n",step,a,j,kfrog[j],u[j][a],kfrogprev[j],uprev[j][a],DeltaE[j][a]);
*/
				}//DEBUG

//==========================================================================================================

		//calculate power:

		for (tau = 0; tau < LastTermIndex; tau++) {//loop over interaction terms (the pot. energy is addative in these terms, therefore so is the power)
			CalcTermPow(tau, state, itermlist, power);
		}//end loop over interaction terms "tau"

//==========================================================================================================

		step++;

		for (a = 0; a < N_SCHEMES; a++)
			for (j = 0; j < natoms; j++)
				poweracc[j][a] += power[j][a];

//-----------------------------------------------------------------------------------------------------------

		//print output to file
		if (step % seg_length == 0)
			if (0 != CoarseGrainTimeSeg(state, stateprev, step, hrout, outfile,
					poweracc, DeltaE, seg_length, natoms, kfrog, u, a_hr)) {
				fputs("\nError in Coarse Graining over Time Segment\n", stderr);
				exit(2);
			}//coarse-grain over time segments


//-----------------------------------------------------------------------------------------------------------

		//set up next time-step: rename the current time-step as the previous one
		if (0 != SetupNextTimeStep(natoms, state, stateprev, k, kprev, kfrog,
				kfrogprev, u, uprev, power)) {
			fprintf(stderr, "\nError setting up next time step on step %d\n",
					step);
			exit(3);
		}
		stateprev.t = state.t;//these are pointers to scalars, and therefore any changes made to them within the function SetupNextTimeStep will be actually only made to the copies of them which were sent to the function. Here, the change is done outside the function, with global scope.


/*
		double utot[N_SCHEMES],vtermtot;//DEBUG -- compare total potential energy sus (sum over terms with sum over atoms)

		//DEBUG
		vtermtot=0; for (tau=0;tau<=LastTermIndex;tau++) vtermtot+=itermlist[tau].vterm;
		for (a=0;a<N_SCHEMES;a++){utot[a]=0; for (j=0;j<natoms;j++) utot[a]+=u[j][a];}
		if (step==1) printf("step	vtot		utot[0]		utot[1]		utot[2]\n");
		printf("\n%d	%e	%e	%e	%e\n			%e	%e	%e\n",step,vtermtot,utot[0],utot[1],utot[2],vtermtot-utot[0],vtermtot-utot[1],vtermtot-utot[2]);
*/


//==========================================================================================================

	}//trajectory file eof

//==========================================================================================================

	//finish up
	if (0 != FinishUp(step, seg_length, outfile, hrout)) {
		fputs("\nError finishing up\n", stderr);
		exit(8);
	}

	return 0;
}//end main

//==========================================================================================================

//functions:

int ColumnHeadings(int natoms, FILE *outfile, FILE *hrout, int a_hr) {

	fprintf(outfile,"  #				a=0				a=1				a=2\n  #\n  #   t   j kfrog       u[0]   DE[0]  P[0] DE-P[0]	u[1]   DE[1]  P[1] DE-P[1]	u[2]   DE[2]  P[2] DE-P[2]");

	if (natoms == 2)
		fprintf(hrout,"    a=%d   |         j=0             |         j=1\n          |                         |\n   #   t  |   DE[0]     P[0] DE-P[0]|   DE[1]     P[1] DE-P[1]\n__________|_________________________|_________________________",a_hr);

	if (natoms == 3) {
		//	fprintf(outfile,"\n#  tprev  j kfrog[j]	u[0][j]	DE^{0}_{j}(t) P^{0}_{j}*dt (DE-P)/min(DE,P)	u[1][j]	DE^{1}_{j}(t) P^{1}_{j}*dt (DE-P)/min(DE,P)	u[2][j]	DE^{2}_{j}(t) P^{2}_{j}*dt (DE-P)/min(DE,P)\n");
		fprintf(hrout, "#	tprev	DE0		P0	DE0-P0	DE1		P1	DE1-P1	DE2		P2	DE2-P2");
	}

	if (natoms == 4) {
		//columns:	 1     2   3         4          5         6        7          8         9        10       11
//		fprintf(outfile,"#			a=0				a=1				a=2\n#\n#   dt  j kfrog   u[0]  DE[0]     P[0]   DE-P[0]  u[1]  DE[1]     P[1]   DE-P[1] u[2]  DE[2]     P[2]   DE-P[2]");
		//#				a=0				a=1				a=2\n#\n#   t   j kfrog		u[0]   DE[0]   P[0]   DE-P	u[1]   DE[1]   P[1]   DE-P	u[2]   DE[2]   P[2]   DE-P");
		/*"#     t   j  kfrog[j]    u[0][j]  DE[0][j]  P[0][j] DE-P[0][j]   u[1][j]  DE[1][j]  P[1][j] DE-P[1][j]   u[2][j]  DE[2][j]  P[2][j] DE-P[2][j]"*/
		fprintf(hrout,"#   t     |   DE0       P0   DE0-P0 |   DE1       P1   DE1-P1 |   DE2       P2   DE2-P2 |   DE3       P3   DE3-P3\n__________|_________________________|_________________________|_________________________|_________________________");
	}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

/*
 int ReadTop(FILE *trajectory, int natoms, double m[]){
 int scanfnum,j;
 scanfnum=fscanf(trajectory,"%d\n",&natoms);

 printf("\n\nDEBUG: natoms=%d\n\n",natoms);//DEBUG

 if (scanfnum!=1){fputs ("\n\nError reading trajectory file (natoms)\n",stderr); exit (3);}
 for(j=0;j<natoms;j++){
 scanfnum=fscanf(trajectory,"%lf ",&m[j]);
 if (scanfnum!=1){fprintf (stderr,"\n\nError reading trajectory file (m[%d])\n",j); exit (4);}
 }
 scanfnum=fscanf(trajectory,"\n");
 return 0;
 }
 */

//-----------------------------------------------------------------------------------------------------------


int ReadNAtoms(FILE *trajectory, int natoms) {
	int scanfnum;
	scanfnum = fscanf(trajectory, "%d\n", &natoms);

	//	printf("\n\nDEBUG: natoms=%d\n\n",natoms);//DEBUG

	if (scanfnum != 1) {
		fputs("\n\nError reading trajectory file (natoms)\n", stderr);
		exit(3);
	} else
		return natoms;
	//DEV NOTE: why does "natoms" not change on the global scale when changed in this function? To solve this problem temporarily, "natoms" is used as the return value of the function.
}

//-----------------------------------------------------------------------------------------------------------

int ReadAtomMasses(FILE *trajectory, int natoms, double m[]) {
	int scanfnum, j;
	for (j = 0; j < natoms; j++) {
		scanfnum = fscanf(trajectory, "%lf ", &m[j]);
		if (scanfnum != 1) {
			fprintf(stderr, "\n\nError reading trajectory file (m[%d])\n", j);
			exit(4);
		}
	}
	scanfnum = fscanf(trajectory, "\n");
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int MaxTerms(int natoms, int maxterms) {
	int h, i, j, ppp, qqq, maxtypes[6] = { 0, 0, 4, 6, 2, 1 };
	maxterms = 0;
	if (natoms==2) return 4;
	else for (i = 2; i < ((natoms < MAX_PARTICIPANTS) ? natoms : MAX_PARTICIPANTS); i++) {//interaction orders
		for (h = 0; h < maxtypes[i]; h++) {//number of distinct interaction types for a given order
			ppp = 1;
			qqq = 1;
			for (j = 0; j < i; j++) {
				ppp = ppp * (natoms - j);
				qqq = qqq * (j + 1);
			}
			maxterms += ppp / qqq;//add number of permutations to counter
		}
	}
	return maxterms;
}

//-----------------------------------------------------------------------------------------------------------

int ReadPhase(FILE *trajectory, int natoms, double **TempPhaseTime,
		state_t state) {
	int scanfnum, j, q;
	double TempTime;

	scanfnum = fscanf(trajectory, "%lf\n", &TempTime);
	*TempPhaseTime = &TempTime;

	/*
	 double Temp,*TempTime=NULL;

	 scanfnum=fscanf(trajectory,"%lf\n",&Temp);
	 *TempTime=Temp;
	 **TempPhaseTime=*TempTime;
	 */

	if (scanfnum != 1) {
		fputs("\n\nError reading trajectory file (first line time)\n", stderr);
		exit(1);
	}
	for (j = 0; j < natoms; j++) {
		for (q = 0; q < DIM; q++) {
			scanfnum = fscanf(trajectory, "%lf ", &state.x[j][q]);
			if (scanfnum != 1) {
				fputs(
						"\n\nError reading trajectory file (first line position)\n",
						stderr);
				exit(1);
			}
		}
		for (q = 0; q < DIM; q++) {
			scanfnum = fscanf(trajectory, "%lf ", &state.v[j][q]);
			if (scanfnum != 1) {
				fputs(
						"\n\nError reading trajectory file (first line velocity)\n",
						stderr);
				exit(1);
			}
		}
	}

	//Check to make sure no two atoms are superpositioned -- possibly due to energy minimization glitch
	if (ChkAtmSuprpos(natoms,state,TempTime) != 0) {fprintf(stderr,"\n\nError in reading position coordinates: At time %g ns, atom superposition test failed. Exiting...\n\n",TempTime);exit(3);}

	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int ChkAtmSuprpos(int natoms, state_t state, double TempTime){
	//Check to make sure no two atoms are superpositioned -- possibly due to energy minimization glitch
	int i,j,q,bSuperposition;
	for (j = 0; j< natoms; j++) for (i = 0; i < j; i++){
		bSuperposition=0;
		for (q = 0; q < DIM; q++) bSuperposition += (state.x[i][q] == state.x[j][q]) ? 1 : 0;//check for superposition on each axis
		if (bSuperposition == 3) {fprintf(stderr,"\n\nError in reading position coordinates: At time %g ns, atoms %d and %d (in [0 , natoms-1]) are superpositioned! Exiting...\nDumping problematic position coordinates:\n%d %+lf %+lf %+lf\n%d %+lf %+lf %+lf\n\n",TempTime,i,j,i,state.x[i][0],state.x[i][1],state.x[i][2],j,state.x[j][0],state.x[j][1],state.x[j][2]);exit(3);}
		}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int CalcKin(int natoms, double m[], double k[], state_t state) {
	int j, q;
	for (j = 0; j < natoms; j++) {
		for (q = 0; q < DIM; q++) {
			k[j] += state.v[j][q] * state.v[j][q] * m[j] / 2.0;
		}
	}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int AntiLeapFrog(int natoms, state_t state, state_t stateprev, double k[],
		double kprev[], double kfrog[]) {//doesn't need the masses
	int j, q;
	for (j = 0; j < natoms; j++) {
		for (q = 0; q < DIM; q++)
			state.vfrog[j][q] = ((stateprev.v[j][q] + state.v[j][q]) == 0) ? 0
					: ((stateprev.v[j][q] + state.v[j][q]) / fabs(
							stateprev.v[j][q] + state.v[j][q])) * sqrt(
							(stateprev.v[j][q] * stateprev.v[j][q]
									+ state.v[j][q] * state.v[j][q]) / 2.0);//ternary operator to avoid division by zero for atoms at rest
		kfrog[j] = (kprev[j] + k[j]) / 2.0;//the leap-frog algorithm pertains to the kinetic energy, not the velocities, but since the mass remain unchanged, vfrog \propto \sqrt{vprev^2+v^2} (the proportionality factor is a unit vector in the direction of vfrog, which is taken to be halfway between the direction of vprev and the direction of v
	}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int ReadForce(FILE *forces, int tau, iterm_t *itermlist) {
	//reads forces from forces.txt and sets the values of the  static localization scheme coefficients according to the number of participating atoms (does nothing for the dynamic values).
	int scanfnum = 0, i, a, q;

	//	printf("\n\nDEBUG: tau=%d itermlist[tau].nr=%d\n\n",tau,itermlist[tau].nr);//DEBUG
	for (i = 0; i < itermlist[tau].nr; i++)
		scanfnum += fscanf(forces, "%d", &itermlist[tau].iatoms[i]);
	if (scanfnum != itermlist[tau].nr) {
		fputs("\n\nError reading forces file (wrong number of indexes)\n",
				stderr);
		exit(1);
	}
	scanfnum = 0;

	switch (itermlist[tau].nr) {//use seperate force completion depending on number of participants (save read-write time to hard-disk)
	case 2:
		for (q = 0; q < DIM; q++)
			scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[0][q]);
		if (scanfnum != (itermlist[tau].nr - 1) * DIM) {
			fputs(
					"\n\nError reading forces file (wrong number of force components)\n",
					stderr);
			exit(1);
		}
		if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm)) {
			fputs("\n\nError reading forces file (energy)\n", stderr);
			exit(1);
		}
		for (q = 0; q < DIM; q++)
			itermlist[tau].f[1][q] = -itermlist[tau].f[0][q];//force completion
		for (a = 1; a < N_SCHEMES; a++)
			for (i = 0; i < itermlist[tau].nr; i++)
				itermlist[tau].c[i][a] = 0.5;
		break;
	case 3:
		for (q = 0; q < DIM; q++) {
			scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[0][q]);
			scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[2][q]);
		}
		if (scanfnum != (itermlist[tau].nr - 1) * DIM) {
			fputs(
					"\n\nError reading forces file (wrong number of force components)\n",
					stderr);
			exit(1);
		}
		if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm)) {
			fputs("\n\nError reading forces file (energy)\n", stderr);
			exit(1);
		}
		for (q = 0; q < DIM; q++)
			itermlist[tau].f[1][q] = -itermlist[tau].f[0][q]
					- itermlist[tau].f[2][q];//force completion
		for (i = 0; i < itermlist[tau].nr; i++) {
			itermlist[tau].c[i][1] = (1.0 / itermlist[tau].nr);//equipartition
			itermlist[tau].c[i][2] = ((i == 0) || (i == (itermlist[tau].nr - 1))) ? 0 : (1.0 / (itermlist[tau].nr - 2));//apex/vertex
		}
		break;
	case 4:
		for (q = 0; q < DIM; q++)
			for (i = 0; i < itermlist[tau].nr; i++)
				scanfnum += fscanf(forces, "%lf ", &itermlist[tau].f[i][q]);//no force completion
		if (scanfnum != itermlist[tau].nr * DIM) {
			fputs(
					"\n\nError reading forces file (wrong number of force components)\n",
					stderr);
			exit(1);
		}
		if (1 != fscanf(forces, "%lf\n", &itermlist[tau].vterm)) {
			fputs("\n\nError reading forces file (energy)\n", stderr);
			exit(1);
		}
		for (i = 0; i < itermlist[tau].nr; i++) {
			itermlist[tau].c[i][1] = (1.0 / itermlist[tau].nr);//equipartition
			itermlist[tau].c[i][2] = ((i == 0) || (i == (itermlist[tau].nr - 1))) ? 0 : (1.0 / (itermlist[tau].nr - 2));//apex/vertex
		}
		break;
	default:
		fputs(
				"\nTerm has either less than 2 or more than 4 participating atoms\n",
				stderr);
		exit(1);
	}

/*
	 //DEBUG: Do the variables changed in this function not change on the global scope?
	 printf("%d %d %g\n",tau,itermlist[tau].nr,itermlist[tau].vterm);
	 for(i=0;i<itermlist[tau].nr;i++) printf(" %d %+g %+g %+g\n",itermlist[tau].iatoms[i],itermlist[tau].f[i][0],itermlist[tau].f[i][1],itermlist[tau].f[i][2]);
*/

	return 0;
}

//------------------------------------------------------------------------------------------------------------

int PotLoc(int tau, iterm_t *itermlist, double u[][N_SCHEMES]) {//localize potential energy for given term
	int i, a, ai, q, nr = itermlist[tau].nr;
	double f2tot = 0, f2[nr], fiq;

	//............................................................................................................

	//total squared angle bending forces for given term
	for (i = 0; i < nr; i++) {//loop over participating atoms. This loop is necessary only for the dynamic localization scheme (0) since u[0][j] depends on f2tot. When using simpler (static) localization schemes, the distribution of the term's potential energy among the participating atoms is given in advance, and f2tot needs not be calculated at all.
		f2[i] = 0.0;
		for (q = 0; q < DIM; q++) {
			fiq = itermlist[tau].f[i][q];
			f2[i] += fiq * fiq;
		}
		f2tot += f2[i];
/*
//DEBUG
if ((tau==0)&&(i==0)) printf("\ntau nr i ai	f2[i]	f2tot\n");
printf("%d   %d  %d %d	%+e	%e\n",tau,itermlist[tau].nr,i,itermlist[tau].iatoms[i],f2[i],f2tot);
*/

	}

//............................................................................................................

	//total dynamic localization scheme
	for (i = 0; i < nr; i++)
		itermlist[tau].c[i][0] = (f2tot == 0.0) ? 0.0 : (f2[i] / f2tot);

//............................................................................................................

	//localize potential energy
	for (a = 0; a < N_SCHEMES; a++)
		for (i = 0; i < nr; i++) {
			ai = itermlist[tau].iatoms[i];
			u[ai][a] += itermlist[tau].c[i][a] * itermlist[tau].vterm;

/*
			//DEBUG
			if ((tau==0)&&(a==0)&&(i==0)) printf("\ntau nr a i ai	c[i][a] *	 vterm	 =	u\n");
			printf("%d   %d  %d %d %d	%e	%e	%e\n",tau,itermlist[tau].nr,a,i,ai,itermlist[tau].c[i][a],itermlist[tau].vterm,u[ai][a]);
*/
		}

	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int CalcTermPow(int tau, state_t state, iterm_t *itermlist, schemecoef *power) {
	int i, j, aj, a, q;

	//calculate itermlist[tau].locpower0[i][a] & itermlist[tau].termPower0tot[i]
	itermlist[tau].termPower0tot = 0;
	for (i = 0; i < itermlist[tau].nr; i++) {//loop over atoms "i" participating in interaction term "tau"
		itermlist[tau].termPower0[i] = 0;
		for (q = 0; q < DIM; q++)
			itermlist[tau].termPower0[i] += itermlist[tau].f[i][q]
					* state.vfrog[i][q];//power going into atom "i" due to its interaction in the "tau" term
		itermlist[tau].termPower0tot += itermlist[tau].termPower0[i];//total power going into the potential energy of the interaction term "tau"
	}//end loop over participating atoms "i" in term "tau"

//............................................................................................................


	//calc itermlist[tau].termLocalPower[i] and add to power[j][a]
	for (a = 0; a < N_SCHEMES; a++) {//loop over potential localization schemes
		//compute termLocalPower[][] using c[][a]
		for (j = 0; j < itermlist[tau].nr; j++) {//loop over atoms "j" participating in interaction term "tau"
			itermlist[tau].termLocalPower[j][a] = itermlist[tau].termPower0[j]
					- itermlist[tau].c[j][a] * itermlist[tau].termPower0tot;//local power into atom "j" due to interaction term "tau"
			aj=itermlist[tau].iatoms[j];
			power[aj][a] += itermlist[tau].termLocalPower[j][a];//aggregate this term's contribution to net local power into atom j


/*
			if ((tau == 0) && (j == 0) && (a == 0)) printf("\ntime		tau a j aj	P0	-	c	*	P0tot	=	P\n");//DEBUG
			printf("%e	%d   %d %d %d	%+e	%lf	%+e	%+e\n",state.t,tau,a,j,aj,itermlist[tau].termPower0[j],itermlist[tau].c[j][a],itermlist[tau].termPower0tot,itermlist[tau].termLocalPower[j][a]);//DEBUG
*/


//		printf("%d	%d	%d	%e\n",tau,a,aj,power[aj][a]);//DEBUG


		}
	}//end "a" loop (potential energy localization scheme)

	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int CoarseGrainTimeSeg(state_t state, state_t stateprev, int step, FILE *hrout,
		FILE *outfile, schemecoef *poweracc, schemecoef *DeltaE,
		int seg_length, int natoms, double *kfrog, schemecoef *u, int a_hr) {
	int j, a;
	double Deltat = state.t - stateprev.t,smallerof[natoms][N_SCHEMES];

//	schemecoef powertot;
	fprintf(hrout, "\n%4d %.0e", step, state.t);

	for (j = 0; j < natoms; j++) {

		//normalize accumulators
		for (a = 0; a < N_SCHEMES; a++) {
			poweracc[j][a] /= seg_length;
			DeltaE[j][a] /= seg_length;
			smallerof[j][a] = (fabs(poweracc[j][a]* Deltat) < fabs(DeltaE[j][a])) ? fabs(poweracc[j][a]* Deltat) : fabs(DeltaE[j][a]);
		}

		//print output to file (block format)
		if (natoms == 2) {
			if (j == 0) {//since DE[1] \approx -DE[0] and P[1]=-P[0] , only j=0 is necessary
				fprintf(outfile, "\n%3d %5.0e %d %e ", step, state.t, j, kfrog[j]);
				for (a = 0; a < N_SCHEMES; a++)
					fprintf(outfile, "    %e %e %e %e", u[j][a], DeltaE[j][a],	poweracc[j][a] * Deltat, (poweracc[j][a] * Deltat-DeltaE[j][a]) / smallerof[j][a]);
				
			}
		} else {
			fprintf(outfile, "\n%d %e %d %e ", step, state.t, j,kfrog[j]);
			for (a = 0; a < N_SCHEMES; a++)	fprintf(outfile, "    %e %+e %+e %e %e", u[j][a],DeltaE[j][a], poweracc[j][a] * Deltat, (poweracc[j][a] * Deltat - DeltaE[j][a]) / smallerof[j][a],kfrog[j]/(0.5*Rgas*1000));
		}

		//print output to human readable output file (one line per time-step format)
		a = a_hr;
		if (natoms==2) fprintf(hrout, "|%+e %+e %+g%%", DeltaE[j][a], poweracc[j][a]
				* Deltat, (smallerof == 0) ? 0 : 100.0 * (poweracc[j][a] * Deltat
				- DeltaE[j][a]) / smallerof[j][a]);
		else fprintf(hrout, "|%+.1e %+.1e %+6.1g%%", DeltaE[j][a], poweracc[j][a]
				* Deltat, (smallerof == 0) ? 0 : 100.0 * (poweracc[j][a] * Deltat
				- DeltaE[j][a]) / smallerof[j][a]);

	}//end j loop


	fprintf(outfile,"\n");//iso-scan line for GNUPlot pm3d algorithm
	//fprintf(hroutfile,"\n");


/*
	//DEBUG -- compare total power (different localization schemes)
	printf("\n"); if (step==1) printf("step	a	power[0][a](t)	power[1][a](t)	power[2][a](t)	power[3][a](t)		powertot[a](t)\n");
	for (a=0;a<N_SCHEMES;a++){printf("%d	%d	",step,a);powertot[a]=0; for (j=0;j<natoms;j++) {powertot[a]+=poweracc[j][a];printf("%+e	",poweracc[j][a]*Deltat);}printf("	%+e\n",powertot[a]*Deltat);}
*/


	//nullify segment accumulators
	for (j = 0; j < natoms; j++) {
		for (a = 0; a < N_SCHEMES; a++) {
			poweracc[j][a] = 0;
			DeltaE[j][a] = 0;
		}
	}
	return 0;
}//end CoarseGrainTimeSeg (*if step % seg_length*)

//-----------------------------------------------------------------------------------------------------------

int SetupNextTimeStep(int natoms, state_t state, state_t stateprev, double k[],
		double kprev[], double kfrog[], double kfrogprev[], schemecoef *u,
		schemecoef *uprev, schemecoef *power) {
	int j, a, q;

	for (j = 0; j < natoms; j++) {
		kprev[j] = k[j];//kinetic energy of the next half-step becomes that of the previous half-step
		kfrogprev[j] = kfrog[j];//(leap-frog) kinetic energy of this step becomes that of the previous step
		for (a = 0; a < N_SCHEMES; a++) {
			uprev[j][a] = u[j][a];
			power[j][a] = 0;
		}
		for (q = 0; q < DIM; q++) {
			stateprev.x[j][q] = state.x[j][q];
			stateprev.v[j][q] = state.v[j][q];
		}
	}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------

int FinishUp(int step, int seg_length, FILE *outfile, FILE *hrout) {

	if ((step % seg_length) != 0) {
		fprintf(
				stderr,
				"\n\nIrregular segment length in last segment: final step=%d	seg_length=%d	steps left over=%d\n\n",
				step, seg_length, step % seg_length);
		exit(8);
	}
	fprintf(outfile, "\n");
	fclose(outfile);
	fprintf(hrout, "\n");
	fclose(hrout);
	//DEV NOTE: (+ leave this comment even after implementing it)
	//remember to free up all nested pointers (e.g. *f in *termlist) explicitly before freeing up the nesting data structure (*termlist)
	return 0;
}
