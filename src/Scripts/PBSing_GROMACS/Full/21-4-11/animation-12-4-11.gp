#set term gif size 200,200 dynamic enhanced
#set term gif animate transparent opt delay 10 size 200,200 x000000
set term gif enhanced animate delay 10 # delay is given in centiseconds
set output "image-12-4-11.gif"

#set initial values, increment size, formatting, etc.
reset
#set title "Temperature Profile (from Kinetic Energy)\n as a function of Atomic Site and Time = %g", counter
set title "Temperature Profile (from Kinetic Energy)\n as a function of Atomic Site and Time"
set view map
set data style image
set xlabel "n"
set xtics 1
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic
set pal defined (0  "blue", 1 "red")
set cblabel "Temperature/[K]"
set autoscale
counter=1
limit_loop_calls=100 # set this to an integer product of simulated time-segments

#consider putting multiplot here somehow...
splot "output.txt" every :counter u ($3+0.5):2:9 noti
load "animation-loop-12-4-11.gp"

