#! /bin/bash

#Default values for command-line options
bDO_EM=0  # 1 if you want energy minimization to be preformed (changed using command-line option -e)
bDO_EOG=0 # 1 if you want the resulting graphic to be put up using Eye of GNOME (changed using command-line option -g)
SegLength=1 # no coarse-graining in time (changed using command-line option -l, followed by segment length in time-steps)
bDO_NM=0  # 1 if you want normal mode analysis to be preformed (changed using command-line option -m)
bDO_NDX=0 # 1 if you want the atomic index file index.ndx to be automatically generated
if [ ! -f index.ndx ] ; then bDO_NDX=1 ; fi # if index.ndx does not exist, it will be automatically generated
bDO_G_TRAJ=0 # 1 if you want g_traj to be executed (changed using command-line option -t)
bDO_VID=0 # 1 if you want video output (changed using command-line option -v)

while getopts "eghl:mntv" optionName; do
case "$optionName" in
	e) bDO_EM=1;;
	g) bDO_EOG=1;;
	h) echo -e "
Usage: ./SingleTrajectory-DD-MM-YY.sh [OPTIONS]
Set-up and execute a single instance of GROMACS.
\nCommand-line options:\n
	-e	Perform Energy Minimization before Molecular Dynamics run
	-g	Visualize local heat current and energy continuity  for the trajectory, using Eye Of GNOME
	-h	Display this Help message
        -l X    Set Time coarse-graining to X time-steps
	-m	Perform Normal Mode analysis (if em.trr does not exist, energy minimization will be performed immediately beforehand)
	-n	Automatically generate an index file (will be performed anyway if one doesn't exist)
	-t	Performs trajectory decoding using g_traj
\nMandatory input files:\n
	topol.itp		Included Topology file for GROMACS (forcefield parameters and other options)
	conf.gro                GROMACS initial molecular configuration (single-precission)
	grompp.mdp		GROMACS pre-processor instructions (for MD run)
	parse-DD-MM-YY.out	Parser program (parses GROMACS output and calculates expression for heat current)
	DD-MM-YY.gp             GNUPlot script (for visualization)
	and this file.
\nOptional input files:\n
	index.ndx               GROMACS atom group index (for analysis and for coupling only parts of the system)
\nOptional output files:\n
	em.trr                  GROMACS final Energy Minimized configuration in double-precission
\nInon Sharony, Abraham Nitzan group, Department of Chemical Physics, Tel Aviv University, Israel (2011).
\n"; exit 0;;
        l) SegLength="$OPTARG";;
	m) bDO_NM=1; if [ ! -f em.trr ] ; then bDO_EM=1; fi;;
	n) bDO_NDX=1;;
	t) bDO_G_TRAJ=1;;
	v) bDO_VID=1;;
	[?]) echo -e "\nBad command line option (not e, g, h, l, m, n, t, or v). Exiting.\n"; exit 1;;
esac # end BASH "case"
done # end "while" loop


#make generic topol.top file
function mktop(){

echo -e "; Include forcefield parameters
#include \"ffG43a1.itp\"
; Included topology
#include \"topol.itp\"


[system]
Generic System Name

[ molecules ]
MOL 1

" > topol.top

}


#make index file
function mkndx(){

#get number of atoms by reading second line of conf.gro
natoms=`sed -n '2p' conf.gro`

#create make_ndx script for molecule of size "natoms"
if [ -f make_ndx.txt ] ; then rm make_ndx.txt ; fi # remove any previous make_ndx script, as the following function only appends lines to it
i=1
while [ $i -le $natoms ]; do
echo -e "a $i\nname $[2 + $i] $[$i - 1]" >>make_ndx.txt # append to end of make_ndx.txt. Default groups already created are: 0 system, 1 , 2 ResidueName. therefore we start with atom 0 as group 3
i=$[1 + $i] # increment atom index counter
done
echo -e "q\n" >>make_ndx.txt #input q(uit) signal for interactive make_ndx program

#run make_ndx using the script as input
cat make_ndx.txt | make_ndx_d -f -quiet

#clean up after yourself
rm make_ndx.txt


#DEV NOTE:
#Add the appropriate index groups as energy_grps to grompp.mdp!
}

#Energy Minimization Subroutine
function em(){
echo ; echo ; date +20%y-%m-%d\ %H:%M:%S%N ; echo ; echo

#create Energy Minimization pre-processor options file
echo -e "nsteps              =  5000
constraints	    =  none
integrator	    =  cg
emtol		    =  1e-12
coulombtype         =  Cut-off
optimize_fft  	    =  yes\n" > em.mdp
if [ "$?" != "0" ] ; then echo "Error creating em.mdp. Exiting."; exit 1; fi;

if [ -f conf.gro ]; then cp conf.gro conf_b4em.gro ; fi
#sometimes we need energy minimization to ignore one gromacs pre-processor warning (the one about using cut-offs) then we add the flag -maxwarn 1
if [ -f em.trr ]; then #if em.trr exists then use it, otherwise don't
	grompp_d -f em -n -quiet -t em
else
	grompp_d -f em -n -quiet
fi
#if grompp did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "grompp error. exiting.";
	exit 1;
	fi;

#remove forces and trajectory output from previous runs
rm forces.txt trajectory.txt

#perform molecular dynamics run
mdrun_d -quiet -v -nt 1
#if mdrun did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
	exit 1;
	fi;

#center entire molecule in box, and rename new configuration file
#DEV NOTE: This supposedly aligns the molecule with the box's principle axes.
#DEV NOTE: Alignment along a given vector, e.g. by issuing the flag -align 1 0 0,
#DEV NOTE: does not seem to align the molecule with any box vector either
echo 0 0 0 | editconf_d -f confout -n -c -princ -quiet
#if editconf did not return valid exit code
if [ "$?" != "0" ] ; then
        echo "editconf error. exiting.";
        exit 1;
        fi;
mv out.gro conf.gro

#rename new energy minimized full-percission trajectory
mv traj.trr em.trr

#remove unnecessary files
rm confout.gro em.mdp ener.edr md.log mdout.mdp *#
}


#normal mode analysis -- perform high percission energy minimization first! Fmax<<1e-10
function nm(){

#create Normal Mode pre-processing options file
echo -e "integrator = nm\n" > nm.mdp

#gromacs pre processing
grompp_d -f nm -n -quiet -maxwarn 0 -t em
#if grompp did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "grompp error. exiting.";
	exit 1;
	fi;

#remove forces and trajectory output from previous runs
rm forces.txt trajectory.txt

#perform molecular dynamics run
mdrun_d -mtx hessian.mtx -quiet -v -nt 1
#if mdrun did not return valid exit code
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
	exit 1;
	fi;

#perform normal mode analysis to find eigenfrequencies and eigenvectors
g_nmeig_d -quiet
#if nmeig did not return valid exit code
if [ "$?" != "0" ] ; then
        echo "g_nmeig error. exiting.";
        exit 1;
        fi;

#remove unnecessary files
rm md.log mdout.mdp nm.mdp *#

#print eigenfrequencies (in wavenumbers) to screen (XMGrace format)
cat eigenfreq.xvg
}


#Molecular Dynamics Subroutine
function md(){
echo ; echo ; date +20%y-%m-%d\ %H:%M:%S%N ; echo ; echo
if [ -f conf_md.gro ] ; then
	mv conf.gro conf_b4md.gro;
	cp conf_md.gro conf.gro;
	fi;
#if [-f traj.trr] ; then	cp traj.trr traj_b4md.trr ; fi ; #for cases where a specific phase space coordinate is given for the starting positions & velocities
grompp_d -n -quiet -maxwarn 0
if [ "$?" != "0" ] ; then 
	echo "grompp error. exiting.";
	exit 1;
	fi;
if [ -f forces.txt ] ; then rm forces.txt ; fi
if [ -f trajectory.txt ] ; then rm trajectory.txt ; fi
mdrun_d -quiet -v -nt 1
if [ "$?" != "0" ] ; then 
	echo "mdrun error. exiting.";
	exit 1;
	fi;
rm state* confout.gro md.log mdout.mdp *#
}

#Video encoding subroutine
function vid(){
if [ ! -f animation-12-4-11.gp ] || [ ! -f animation-loop-12-4-11.gp ] ; then echo -e "\nMissing video encoding gnuplot scripts. exiting.\n" ; exit 1 ; fi
gnuplot animation-12-4-11.gp
FileName="image-12-4-11"
if [ -f $FileName.avi ] ; then echo -e "\nOutput file "$FileName.avi" already exists! ffmpeg will overwrite it... (-y flag). Exiting.\n" ; exit 1 ; fi
mkdir Frames # create temporary directory in which to store the frames
convert $FileName.gif Frames\frame%07d.jpg # to prevent problems in encoding, first convert each frame in the *.gif file to a *.jpg image
if [ $? -ne 0 ] ; then echo -e "\nError in conversion of *.gif frames to *.jpg files. Exiting\n" ; exit 2 ; fi
ffmpeg -r 24 -i Frames\frame%07d.jpg -y -an $FileName.avi # framerate = 24 fps (-r 24), automatic overwrite of previous output authorized (-y), audio not recorded (-an)
if [ $? -ne 0 ] ; then echo -e "\nError in encoding of *.jpg frames to *.avi file. Exiting\n" ; exit 3 ; fi
rm Frames\frame*.jpg # remove *.jpg files (frames)
rmdir Frames
rm $FileName.gif
}

#main
if ( [ ! -f topol.itp ] || [ ! -f conf.gro ] || [ ! -f grompp.mdp ] || [ ! -f parse-*.out ] || [ ! -f *.gp ] ) ; then echo -e "\nMissing necessary file\s. exiting...\n"; exit 1 ; fi
if [ "$?" != "0" ] ; then echo "Error in checking existence of necessary files. exiting."; exit 1; fi;
if [ ! -f topol.top ] ; then echo -e "\nGenerating generic topol.top...\n" ; mktop ; if [ "$?" != "0" ] ; then echo "Error in generation of generic topol.top file. exiting."; exit 1; fi; fi;
if [ -z $SegLength ] ; then echo "Please enter length of coarse-graining time-segment in time-steps as command line argument." ; exit 1 ; fi ;
touch empty#
if [ 1 -eq $bDO_NDX ] ; then echo -e "\nGenerating index file...\n" ; mkndx ; if [ "$?" != "0" ] ; then echo "Error in GROMACS generation of index file. exiting."; exit 1; fi; fi;
if [ 1 -eq $bDO_EM ] ; then echo -e "\nRunning GROMACS Energy Minimization...\n" ; em ; if [ "$?" != "0" ] ; then echo "Error in GROMACS energy minimization. exiting."; exit 1; fi; fi;
if [ 1 -eq $bDO_NM ] ; then echo -e "\nRunning GROMACS Normal Mode analysis...\n" ; nm ; if [ "$?" != "0" ] ; then echo "Error in GROMACS Normal Mode analysis. exiting."; exit 1; fi; fi;
echo -e "\nRunning GROMACS Molecular Dynamics...\n" ; md ; if [ "$?" != "0" ] ; then echo "Error in GROMACS molecular dynamics. exiting.";	exit 1;	fi;
if [ 1 -eq $bDO_G_TRAJ ] ; then echo -e "\nRunning GROMACS trajectory utility...\n" ; echo -e "0\n" | g_traj_d -n -ox -ov -of -fp -quiet ; if [ "$?" != "0" ] ; then echo "Error in GROMACS trajectory uitlity. exiting.\n" ; exit 1 ; fi ; rm *.xvg.*# ; fi
#echo -e "\nClearing unnecessary GROMACS output files...\n" ; rm ener.edr topol.tpr traj.trr *# ; if [ "$?" != "0" ] ; then echo "Error in deleting unnecessary GROMACS output files. exiting.";   exit 1; fi;
echo -e "\nRunning parser & heat current calculation...\n" ; ./parse-21-4-11.out trajectory.txt forces.txt output$SegLength.dat $SegLength ; if [ "$?" != "0" ] ; then echo "Error in parsing GROMACS output and/or heat current calculation. exiting."; exit 1; fi;
cp output$SegLength.dat output.txt
echo -e "\nRunning GNUPlot...\n" ; gnuplot 21-4-11.gp ; if [ "$?" != "0" ] ; then echo "Error in GNUPlotting. exiting."; exit 1; fi;
#cat hroutput$SegLength.dat
#rm *.dat *.txt
if [ 1 -eq $bDO_EOG ] ; then echo ; echo "Running EyeOfGNOME..." ; echo ; eog mixed-21-4-11.svg ; fi ;
if [ 1 -eq $bDO_VID ] ; then echo -e "\nEncoding video...\n" ; vid ; if [ "$?" != 0 ] ; then echo -e "Error encoding video. exiting."; exit 1 ; fi ; fi
