#This file is the loopand.
#
#First advance the counter
#Repeat the loop if there isn't an exit condition or if there is one, but it hasn't yet been reached
#       Set up this step's plot
#       Replot
#       Increment whatever needs to be (for the next plot)
#       Re-read this file

counter=counter+1
if ((!limit_loop_calls) || (counter<=limit_loop_calls)) \
  unset label 1; \
  set label 1 "Time = %.0f",counter at graph 0.7,0.9 left tc rgbcolor "white" front; \
  replot; \
  reread
