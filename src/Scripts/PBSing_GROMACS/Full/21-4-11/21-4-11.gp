set term svg size 1200,900 dynamic enhanced
set output "mixed-21-4-11.svg"

#set term jpeg size 1200,900 enhanced
#set output "mixed-21-4-11.jpeg"

set view map
set pal defined (-1 "blue", 0 "black", 1 "red")
set style data image

FIRSTLINES=0

set multiplot title "Energy Continuity\n(dynamic localization of potential energies)"
set multiplot layout 2,3

set xlabel "n"
set xtics 1 		# x axis tics every 1 (integers only)
set ylabel "t/[ps]"
#set ytics 0,0.02	# y axis begins at zero with major tics every 2 femtoseconds
set mytics 2 		# y axis minor tic every 1/2 of a major tic

set cblabel "P/[W]"
#set cbrange [-1e-17:1e-17]
set zlabel rotate
#set zlabel "P/[W]"
set autoscale
set zrange [] writeback
set cbrange [] writeback
splot "output.txt" every ::FIRSTLINES u ($3+0.5):2:7 t "Power as a function of Atomic Site and Time"

#set cblabel "DeltaE/[W]"
#set zlabel "DeltaE/[W]"
set zrange restore
set cbrange restore
splot "output.txt" every ::FIRSTLINES u ($3+0.5):2:6 t "Energy Difference (per time-step) \n as a function of Atomic Site and Time"

set pal defined (-2.01 "white", -2 "magenta", -1 "purple", 0 "black", 1 "green", 2 "yellow", 2.01 "white")
set cblabel "[W]"
set autoscale
set cbrange restore
set zrange restore
splot "output.txt" every ::FIRSTLINES u ($3+0.5):2:($7-$6) t "Error in Energy Continuity\n as a function of Atomic Site and Time (discontinuities in white)"


set title "Power as a function of Atomic Site and Time"
set style data points
set pointsize 0.1
set ylabel "P/[W]"
set xlabel "Time/[ps]"
set xtics auto
#set xtics 0.02
set autoscale
#loop "i" over natoms: "natoms::(i+FIRSTLINES)"
#set key samplen 100
plot \
"output.txt" every 4::(0+FIRSTLINES) u 2:7:(0) t "0", \
"output.txt" every 4::(1+FIRSTLINES) u 2:7:(0) t "1", \
"output.txt" every 4::(2+FIRSTLINES) u 2:7:(0) t "2", \
"output.txt" every 4::(3+FIRSTLINES) u 2:7:(0) t "3"
unset title

set view map
set style data image
set xlabel "n"
set xtics 1             # x axis tics every 1 (integers only)
set ylabel "t/[ps]"
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic

set pal defined (0  "blue", 1 "red")
set cblabel "Temperature/[K]"
set autoscale
splot "output.txt" every ::FIRSTLINES u ($3+0.5):2:9 t "Temperature Profile (from Kinetic Energy)\n as a function of Atomic Site and Time"

set pal defined (-2.01 "white", -2 "magenta", -1 "purple", 0 "black", 1 "green", 2 "yellow", 2.01 "white")
set cblabel "%"
set autoscale
set cbrange [-100:100]
set zlabel "%" norotate
set zrange [-10:10]
splot "output.txt" every ::FIRSTLINES u ($3+0.5):2:8 t "Relative Error in Energy Continuity \n as a function of Atomic Site and Time (discontinuities in white)"


unset multiplot
