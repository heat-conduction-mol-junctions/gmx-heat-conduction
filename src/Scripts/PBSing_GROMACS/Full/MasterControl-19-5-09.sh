#! /bin/bash

#part 1: define some variables

#Define a unique name for this instace of running the script
#This name should be discriptive, for future refference
#Define an environment variable $INSTANCE which holds this name
#Export the variable $INSTANCE so that it may be forwarded to the PBS script running on the compute (remote) node
#Create a directory with the name stored in $INSTANCE under the directory from where this script was run
#(this directory will hold the output from all the nodes)
DATEANDTIME=$(date +%y%m%d%H%M%S%N)
INSTANCE=$(echo ""$DATEANDTIME" 1000 / 16 o p" | dc)	#	DATEANDTIME (with microsecond accuracy) in hexadecimal base (instead of decimal base) - dc is a command line calculator...!
export INSTANCE
mkdir $INSTANCE

#If the previous command failed, return 
if [ "$?" -ne "0" ]
then
echo failed exporting INSTANCE
exit 1
fi

#initialize trajectory counter
TRAJ_COUNT="1";


#part 2: loop over N trajectories, submitting a single-trajectory script for each trajectory

#get number of trajectories to average over from the user
echo
echo Please enter number of trajectories to run:
read N
#N="2"

#loop over each trajectory from 1 to N
while [ "$TRAJ_COUNT" -le "$N" ]
do

#Export the variable $TRAJ_COUNT so that it may be forwarded to the PBS script running on the compute (remote) node
echo
export TRAJ_COUNT
#Submit the single-trajectory script to the queue
#qsub SingleTrajectory-19-5-09.sh 
qsub SingleTrajectory-19-5-09-Full.sh -N GMX-$INSTANCE-$TRAJ_COUNT -d $PWD

#advance the trajectory counter and close the loop
TRAJ_COUNT=$[$TRAJ_COUNT+1]
done

echo =============================
echo DONE
echo
#Once all the trajectories are running, print to screen a snapshot of the cluster status
qstat

#part 3: finish

#Print out the discriptive name of this instance of running the script
#This is also the name of the sub-directory under the current directory where the output files will be located
echo
echo INSTANCE=$INSTANCE
echo
#Exit the script with a succesful status ID = 0
exit 0
