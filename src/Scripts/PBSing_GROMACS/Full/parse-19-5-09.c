#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[], char *envp[]){

//=============================================================
//arguments passed to the program from the command line:
//------------------------------------------------------
//
//argc is the number of command line arguments
//argv is an array of the command line arguments as strings
//
//	argv[1]	=	f_ndxgrp2
//	argv[2]	=	v_ndxgrp1
//	argv[3]	=	v_ndxgrp2
//	argv[4]	=	HeatCurrent.dat
//	argv[5]	=	energy.dat
//	.	.
//	.	.
//	.	.
//
//envp is a pointer to the array of environment variables
//=============================================================

	if (argc!=6){ printf("\n\nwrong number of command line arguments\n\n"); return 100; }
	
	FILE 	*f_2	= fopen(argv[1],"rt"); 		// [t] = ps , [x] = nm , [m] = amu , [f] = amu nm ps^-2
		if (f_2==NULL) {fputs ("\n\nError opening f_ndxgrp2 file\n",stderr); exit (1);}
	FILE 	*v_1 	= fopen(argv[2],"rt"); 		// [t] = ps , [v] = nm ps^-1
		if (v_1==NULL) {fputs ("\n\nError opening v_ndxgrp1 file\n",stderr); exit (1);}
	FILE 	*v_2 	= fopen(argv[3],"rt"); 		// [t] = ps , [v] = nm ps^-1
		if (v_2==NULL) {fputs ("\n\nError opening v_ndxgrp2 file\n",stderr); exit (1);}

	FILE	*I_file	= fopen(argv[4],"wt"); 	// [t] = ps , [I] = amu nm^2 ps^-3
		if (I_file==NULL) {fputs ("\n\nError creating I file\n",stderr); exit (1);}
	FILE 	*ener 	= fopen(argv[5],"rt"); 		// [t] = ps , [E] = KJ mole^-1
		if (ener==NULL) {fputs ("\n\nError opening energy file\n",stderr); exit (1);}

	double	f[4],v1[4],v2[4],v[4],I_f[4],e[3],temp;
	int	q,mismatch=1;				// line number of mismatching time indeces

	while ( feof(f_2) == 0 ){	//while the f_2 file hasn't returned end_of_file:

		fscanf(f_2,"%lf %lf %lf %lf\n",&f[0] ,&f[1] ,&f[2] ,&f[3] );
		fscanf(v_1,"%lf %lf %lf %lf\n",&v1[0],&v1[1],&v1[2],&v1[3]);
		fscanf(v_2,"%lf %lf %lf %lf\n",&v2[0],&v2[1],&v2[2],&v2[3]);
		fscanf(ener,"%lf %lf %lf %lf\n",&e[0],&e[1],&e[2],&e[3]);


		for (q=0;q<=4;q++){ v[q]=(v1[q]+v2[q])/2.0;}

		// if the time indeces agree, set I_f[0], otherwise notify and exit
		if (f[0]==v[0]){
			I_f[0] = f[0];
		}
		else{
			printf("\nin line %d time indeces to dot match\n",mismatch);
			return 200;
		}

		// scalar multiply f_2*([v_1+v_2]/2)
		temp = f[1]*v[1]+f[2]*v[2]+f[3]*v[3];
		I_f[1] = temp;

		I_f[2] = e[1];//Potential Energy
		I_f[3] = e[2];//Total Energy
		
		
		fprintf(I_file,"%lf %lf %lf %lf\n",I_f[0],I_f[1],I_f[2],I_f[3]);

		mismatch++;

		}

	fclose(f_2);
	fclose(v_1);
	fclose(v_2);
	fclose(ener);
	fclose(I_file);

	return 0;
}

