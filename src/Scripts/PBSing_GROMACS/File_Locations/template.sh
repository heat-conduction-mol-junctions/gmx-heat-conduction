#! /bin/bash
# The default working directory is hydrogen:/home/inonshar/
# The STDIO and STDERR files are created in the directory from which this script was added to the queue (e.g. hydrogen:/home/inonshar/GROMACS/)
### name this job as "test"
#PBS -N test
### run this job on 1 group of processors, each group having 1 processors,and maximum wall time for this job to run before being aborted in WW:MM:SS format
#PBS -l nodes=1:ppn=1,walltime=99:99:99
### set the initial working directory environment variable PBS_O_INITDIR (default value is the home directory)
#PBS -o std.io
#PBS -e std.err
#PBS -d /home/inonshar/PBS_templates
### environment variable list to be transfered to the script (make sure you export the variable first)
### e.g.: #PBS -v $AAA

###Environment Variable	Description
###====================	===========
###PBS_ENVIRONMENT 	set to PBS_BATCH to indicate that the job is a batch job; 
###			otherwise, set to PBS_INTERACTIVE to indicate that the job is a PBS interactive job
###PBS_JOBID 		the job identifier assigned to the job by the batch system
###PBS_JOBNAME 		the job name supplied by the user
###PBS_NODEFILE 	the name of the file that contains the list of the nodes assigned to the job
###PBS_QUEUE 		the name of the queue from which the job is executed
###PBS_O_HOME 		value of the HOME variable in the environment in which qsub was executed
###PBS_O_LANG 		value of the LANG variable in the environment in which qsub was executed
###PBS_O_LOGNAME 	value of the LOGNAME variable in the environment in which qsub was executed
###PBS_O_PATH 		value of the PATH variable in the environment in which qsub was executed
###PBS_O_MAIL 		value of the MAIL variable in the environment in which qsub was executed
###PBS_O_SHELL	 	value of the SHELL variable in the environment in which qsub was executed
###PBS_O_TZ 		value of the TZ variable in the environment in which qsub was executed
###PBS_O_HOST 		the name of the host upon which the qsub command is running
###PBS_O_QUEUE	 	the name of the original queue to which the job was submitted
###PBS_O_WORKDIR 	the absolute path of the current working directory of the qsub command 

###PWD
###HOSTNAME
###LOGNAME
###HOME
###PBS_O_JOBID



#echo for a complete listing of environment variables type in the command env
#echo
#echo to add a value \(ADDEDVALUE\) to a new environment variable \(NEWVARIABLE\) write
#echo NEWVARIABLE\=ADDEDVALUE
#echo
#echo to add two values \(ADDEDVALUE1,ADDEDVALUE2\) to a new environment variable \(NEWVARIABLE\) write
#echo NEWVARIABLE\=\ADDEDVALUE1:ADDEDVALUE2
#echo
#echo to append a value \(ADDEDVALUE\) to an existing environment variable \(EXISTINGVARIABLE\) write 
#echo EXISTINGVARIABLE\=\$EXISTINGVARAIBLE:ADDEDVALUE
#echo
#echo to make a variable globaly available use the command export
#echo
#echo to remove a varaible use the command unset

echo $$ fine
date
touch GROMACS/testfile$$
