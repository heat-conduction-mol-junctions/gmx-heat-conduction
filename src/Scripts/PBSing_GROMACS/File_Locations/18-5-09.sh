#PBS -N File_Locations
#PBS -l nodes=1:ppn=1,walltime=72:00:00
#PBS -v ORIGIN_DIR,INSTANCE,TRAJ_COUNT
#PBS -o $INSTANCE/File_Locations-std-$TRAJ_COUNT.io
#PBS -e $INSTANCE/File_Locations-std-$TRAJ_COUNT.err
#PBS -d /home/inonshar/GROMACS/PBSing_GROMACS/File_Locations/

#Explanation of the PBS header:
#use 1 processor on 1 node and a maximal wall-time of three days
#forward the -v environment variables along with the default ones (PBS_O_*) from the head node to the compute node
#put the standard I/O and error files in the sub-directory called $INSTANCE of the $ORIGIN_DIR where this script was called. use the $TRAJ_COUNT to identify this trajectory's files.
#use the -d directory as $INIT_DIR


#part 1: make a sub-directory in the local scratch directory, change to that dirctory, and copy the contents of the origin dirctory there

echo
#Make a sub-directory under the scratch directory on the compute (remote) node, where GROMACS will get its input files and send its output files, and change to that directory
mkdir -p /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
cd /scratch/$LOGNAME/$INSTANCE/$TRAJ_COUNT/
echo
#Copy to the current directory the entire contents of the original directory (under the $HOME directory) where the original script calling this one was run
scp $ORIGIN_DIR/* .
echo

#part 2: GROMACS

maxwarn="1"
# generate random velocities for atoms from a 300K Maxwell-Boltzmann distribution
grompp -f eq -c b4eq -n -t b4eq -maxwarn "$maxwarn" # preprocess using b4eq.gro as input configuration, in double precision (from b4eq.trj file)
mdrun -c b4md -v                                    # run and output configuration to b4md.gro

#part 3: interlude

#Print out some environment variables, most importantly the $HOSTNAME of the compute node on which this trajectory was run adn the value of the $TRAJ_COUNT (trajectory counter) which is unique for this trajectory, under this $INSTANCE of running the scripts
echo
echo TRAJ_COUNT=$TRAJ_COUNT, NNODES=$NNODES, MYRANK=$MYRANK, HOSTNAME=$HOSTNAME
echo 					\(1\)		\(0\)	compute-0-XX.local
echo

#part 4: copy output to sub-directory on $HOME

#Copy the meaningful output files from the current directory to the sub-directory of the $ORIGIN_DIR which was created uniquely for this $INSTANCE of running the scripts
scp ener.edr $ORIGIN_DIR/$INSTANCE/ener-$TRAJ_COUNT.edr
scp traj.trr $ORIGIN_DIR/$INSTANCE/traj-$TRAJ_COUNT.trr
scp md.log $ORIGIN_DIR/$INSTANCE/md-$TRAJ_COUNT.log

#part 5: cleanup compute /scratch

#Change directory to the sub-directory on the scratch directory named as the user's $LOGNAME and specifically for this $INSTANCE, remove the sub-directory with this $TRAJ_COUNT
cd /scratch/$LOGNAME/$INSTANCE
rm -r $TRAJ_COUNT

#Exit with succesful exit status ID = 0
exit 0
