#! /bin/bash
echo
echo
echo
echo
echo "/////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
echo "This script calls GROMACS in order to calculate the ensemble-averaged heat-current, as explained in 'procedure.pdf'."
# Files needed in the current directory: topol.top (and included *.itp files therein), minimized energy b4eq.gro file along with a (double precision) traj.trr file of the minimized configuration, index.ndx, eq.mdp & md.mdp

# For Novermber 4th procedure (regarding calculation of pairwise forces using mdrun_d -rerun) you will also need to create and edit topol_rerun.top and *.itp along with md_rerun.mdp.
echo "/////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
echo
echo
echo "press CTRL+C at any time to abort..."
echo
#echo "Please enter maximum number of warnings to ignore (due to small index groups, for instance)"
#echo "--------------------------------------------------------------------------------------------"
echo
#read maxwarn
maxwarn="0"



# According to the November 4th procedure, the use of index group 1 is deprecated, and only index group 2 will be used.

echo
#echo "Please enter name for the index group from which the heat flows (index group 1)"
#echo "--------------------------------------------------------------------------------"
echo
#read ndxgrp1
ndxgrp1="2"	# in the Ethanethiol case, group 3 is the Sulfur atom, and the pairwise force is with respect to group 4 which is the Gold atom

echo
#echo "Please enter name for the index group into which the heat flows (index group 2)"
#echo "--------------------------------------------------------------------------------"
echo
#read ndxgrp2
ndxgrp2="1"	# in the Ethanethiol case, group 3 is the Sulfur atom, and the pairwise force is with respect to group 4 which is the Gold atom



echo
echo "Please enter ensemble size (number of trajectories to average over)"
echo "-------------------------------------------------------------------"
echo
# N="20"
read N
HeatCurrentFileName=EnsembleAveragedHeatCurrent$N.dat

if [ "$N" -eq "1" ]; then
echo
echo "Note: the ensemble averaging program 'parse5.c' will give bogous results for N=1"
echo
# read -p "press any key to continues regardless..."
fi

i="1"	# initialize counting index

sed >$HeatCurrentFileName

while [ "$i" -le "$N" ]
do

clear

#If this is the last trajectory, backup the average so far
if [ "$i" -eq "$N" ]; then
echo
echo "Last trajectory - saving average so far..."
echo
cp $HeatCurrentFileName EnsembleAveragedHeatCurrentBackup.dat
fi

echo
echo "================================================================================"
echo
echo "performing run for trajectory number $i"
echo "---------------------------------------"
echo

# erase irrelevant past files (take up too much hard disk space)
rm *#
rm *.xtc						# compressed trajectory format (no velocities) don't need it

# generate random velocities for atoms from a 300K Maxwell-Boltzmann distribution
grompp_d -f eq -c b4eq -n -t -maxwarn "$maxwarn"	# preprocess using b4eq.gro as input configuration, in double precision (from trj file)
mdrun_d -c b4md -v					# run and output configuration to b4md.gro

# couple leads to thermal reservoirs
grompp_d -f md -c b4md -n -t -maxwarn "$maxwarn"	# preprocess using b4md.gro as input configuration, in double precision (from trj file)
mdrun_d -c md -v					# run and output configuration to md.gro


echo | echo 7 8 | g_energy_d				# extract Kinetic and Total Energies from ener.edr and output energy.xvg


# extract phase data (velocity) for the index groups named "ndxgrp2" and "ndxgrp1"
# note that the *.trr and not the *.xtc file needs to be read in order to get velocities
echo $ndxgrp2 | g_traj_d -f traj.trr -n -ov v_ndxgrp2	# select index group 2 in interactive menu of g_traj
echo $ndxgrp1 | g_traj_d -f traj.trr -n -ov v_ndxgrp1	# select index group 1 in interactive menu of g_traj

# rerun last trajectory, and throw out all energetic information of interactions other than relevant pair (Au-S)
grompp_d -f md_rerun -c b4md -n -p topol_rerun.top -t -maxwarn "$maxwarn"	# preprocess using existing traj.trr file from previous md run, and "rerun" options in md_rerun.mdp & topol_rerun.top
mdrun_d -o rerun -rerun traj.trr -v						# rerun using traj.trr and output force data into rerun.trr

# extract force data for the index group named "ndxgrp2" 
echo $ndxgrp2 | g_traj_d -f rerun.trr -n -of f_ndxgrp2	# select index group 2 in interactive menu of g_traj


echo
echo "cleaning phase data files and preparing for computation of heat current"
#deleting first 21 lines in each phase data file -- which contain formatting information for xmgrace -- and outputing to *.dat files
echo "-----------------------------------------------------------------------"
echo

sed '1,21d' f_ndxgrp2.xvg >fs2.dat
sed '1,21d' v_ndxgrp1.xvg >vs1.dat
sed '1,21d' v_ndxgrp2.xvg >vs2.dat
sed '1,20d' energy.xvg >energy.dat

echo
echo "computing heat-current for trajectory number $i"
echo "-----------------------------------------------"
echo
# takes f on the ndxgrp2 atom from first argument & v on the ndxgrp1 and ndxgrp2 atoms from second and third arguments, outputs into fourth argument, where the existing data in the fourth argument file is averaged with the new data according to the weight of the number of past trajectories (the fifth argument) already averaged and output into the sixth argument.
./parse-12-4-09.out fs2.dat vs1.dat vs2.dat $HeatCurrentFileName TempHeatCurrent.txt $i energy.dat
mv TempHeatCurrent.txt $HeatCurrentFileName

echo "done."
echo

# advance the trajectory counter
i=$[$i+1]
done

# remove all left over files
#rm fs2.dat
#rm vs1.dat
#rm vs2.dat
#rm *.dat
rm f_ndxgrp2.xvg
rm v_ndxgrp1.xvg
rm v_ndxgrp2.xvg
rm energy.dat
rm energy.xvg
rm ener.edr
rm mdout.mdp
rm *.cpt
rm *#

echo
echo "===================================="
echo
echo "Finished. Presenting through xmgrace"
sed '1i\# this line is a comment line\n@    title "Heat Current and Energy"\n@    xaxis  label "Time [femtoseconds]"\n@    yaxis  label "Energy / Energy Current [kJ/mole]/[kJ/mole per ps]"\n\@TYPE xy\n@ view 0.1, 0.1, 1.25, 0.875\n@ legend on\n@ legend loctype view\n@ legend 1, 0.85\n@ s0 legend "Heat Current"\n@ s1 legend "Kinetic Energy"\n@ s2 legend "Total Energy"' $HeatCurrentFileName >EnsembleAveragedHeatCurrent$N.xvg 
echo
xmgrace -nxy EnsembleAveragedHeatCurrent$N.xvg  &


