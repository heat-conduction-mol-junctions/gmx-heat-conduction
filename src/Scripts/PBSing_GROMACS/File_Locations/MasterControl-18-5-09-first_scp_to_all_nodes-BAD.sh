#! /bin/bash

ORIGIN_DIR=$PWD
PROCESS_ID=$$

i="1";

#THE FOLLOWING PART IS OPTIONAL:
#before everything else, copy the input files from the directory of origin (the one in which the Master Control Program was initiated) to a sub-directory of each of the scratch directories of all the nodes (the sub-directory is named as the process ID of the Master Control Program process ID)
#while [ "$i" -le "11" ]
#do

#ssh compute-0-$i.local
#cd /scratch/
#mkdir -p $LOGNAME
#cd $LOGNAME
#mkdir -p $PROCESS_ID
#cd $PROCESS_ID
#this command copies the entire contents of the origin directory to the process sub-directory of the scratch directories.
#this could be more efficient if only some of the files are copied (only those which are actually needed)
#scp hydrogen:$ORIGIN_DIR/* .

#done
#SO MUCH FOR THE OPTIONAL PART...


echo
echo Please enter number of trajectories to run:
read N
#N="2"

while [ "$i" -le "$N" ]
do

#FIRST OPTION:
#mkdir -p $i
#cp * $i\
#cd $i

#OTHER OPTION:
#make the sub-directory in the local scratch directory, change to that dirctory, and copy the contents of the origin dirctory there
echo
hostname
echo
cd /scratch
echo
pwd
echo
mkdir -p $LOGNAME
cd $LOGNAME
mkdir -p $i
cd $i
cp $ORIGIN_DIR/* .
qsub 18-5-09.sh; qstat

i=$[$i+1]
done

echo DONE
qstat

