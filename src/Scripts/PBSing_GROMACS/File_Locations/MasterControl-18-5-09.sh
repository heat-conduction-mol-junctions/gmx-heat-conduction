#! /bin/bash

#part 1: define some variables

#Define a unique name for this instace of running the script
#This name should be discriptive, for future refference
#Define an environment variable $INSTANCE which holds this name
#Export the variable $INSTANCE so that it may be forwarded to the PBS script running on the compute (remote) node
#Create a directory with the name stored in $INSTANCE under the directory from where this script was run
#(this directory will hold the output from all the nodes)
INSTANCE=$$
export INSTANCE
mkdir $INSTANCE

#Define an environment variable $ORIGIN_DIR which holds the name of the directory where this script was run
#Export the variable $ORIGIN_DIR so that it may be forwarded to the PBS script running on the compute (remote) node
ORIGIN_DIR=$PWD
export ORIGIN_DIR

#If the previous command failed, return 
if [ "$?" -ne "0" ]
then
echo failed exporting ORIGIN_DIR
exit 1
fi

#initialize trajectory counter
TRAJ_COUNT="1";


#part 2: loop over N trajectories, submitting a single-trajectory script for each trajectory

#get number of trajectories to average over from the user
echo
echo Please enter number of trajectories to run:
read N
#N="2"

#loop over each trajectory from 1 to N
while [ "$TRAJ_COUNT" -le "$N" ]
do

#Export the variable $TRAJ_COUNT so that it may be forwarded to the PBS script running on the compute (remote) node
echo
export TRAJ_COUNT
#Submit the single-trajectory script to the queue and print out the current status of the cluster
qsub 18-5-09.sh; qstat

#advance the trajectory counter and close the loop
TRAJ_COUNT=$[$TRAJ_COUNT+1]
done

echo =============================
echo DONE
echo
#Once all the trajectories are running, print out one final snapshot of the cluster status
qstat

#part 3: finish

#Print out the discriptive name of this instance of running the script
#This is also the name of the sub-directory under the current directory where the output files will be located
echo
echo INSTANCE=$INSTANCE
echo
#Exit the script with a succesful status ID = 0
exit 0
