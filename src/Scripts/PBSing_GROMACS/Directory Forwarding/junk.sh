#! /bin/bash
#PBS -N GROMACS_test
#PBS -l nodes=1:ppn=1,walltime=99:99:99
#PBS -o junk.io
#PBS -e junk.err
#######	All PBS files are run (using qsub) starting from the HOME directory.
#######	Therefore, all commands should be entered as if you are in the HOME directory,
#######	conversely, you could Change Directory (using cd) to the working directory of your choice
####### in our case, an environmental variable called TEMPDIR was created prior to running qsub (in test.sh) which contains this working directory
#######	The STD-I/O and STDERR files are created in the directory from which you issued qsub, and not necessarily in HOME



#Final Note for May 11th: for some reason, the cd command doesn't work (ls before and after are the same: they both give the listing for the home directory and not the TEMPDIR)
#The TEMPDIR env varialbe has been exported and is checked out to be global outside the PBS. echoing the variable from inside (foo.sh) gives no response, as if the variable is empty!
echo
pwd
echo
ls -BC
echo
grompp
echo
echo done
echo
echo PBS_JOBID	=		$PBS_JOBID
echo
#Present Working Directory (PWD) given as home directory, regardless of anything
pwd
echo PWD	=		$PWD
#/opt/maui/bin/checkjob $PBS_JOBID
