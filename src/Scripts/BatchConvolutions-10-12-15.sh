#! /bin/bash

# Loops over realizations of a given instance
# and calculate random force time-convolution for each instance

PARSE=$HOME/gmx-heat-conduction/bin/Parse
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then PARSE=$HOME/Documents/GROMACS/Parse ; fi

NumConv=0
for ConvProg in $PARSE/Convolute-*.out ; do NumConv=$[ 1 + $NumConv ] ; done
case "$NumConv" in 
	0) echo "No Convolution program! Exiting..."; exit 5 ;;
	1) echo "Found one Convolution program";;
	[?]) echo "Too many Convolution programs! Exiting..." ; exit 6 ;;
esac

MAX_TRAJ=$(head -n1 N.dat)
i=1
j=1
nsteps=$(head grompp-*-1.mdp -n3 | tail -n1 | cut -d "=" -f2-)
if [ 30000 -gt $nsteps ] ; then ConvolutionWindow=$nsteps ; else ConvolutionWindow=30000 ; fi
#ConvolutionWindow=30000

while [ $MAX_TRAJ -ge $i ] ; do
	#echo -e "\n\n\n\t\t\t$i\n\n\n" ; sleep 0.1
	if [ "Normal termination of Single Trajectory Script. Exiting." = "$(tail $i-std.io -n2)" ] ; then
		head $i-std.io -n $[ $nsteps + 151 ] | tail -n $nsteps | cut -d " " -f1 > RanForMemX-$i.txt
		./$ConvProg RanForMemX-$i.txt $ConvolutionWindow Convolution-23-11-12-$i.txt
		if [ $? -ne 0 ] ; then echo "Error convoluting\n" ; exit 2 ; fi
		if [ 1 -lt $j ] ; then
			$PARSE/EnsembleAverage-23-11-12.out Convolution-23-11-12-$i.txt EnsAvgConv-$[ $j - 1].txt $j EnsAvgConv-$j.txt 1 1
	                if [ $? -ne 0 ] ; then echo "Error ensemble averaging\n" ; exit 2 ; fi
			rm Convolution-23-11-12-$i.txt EnsAvgConv-$[ $j - 1].txt
		else
			mv Convolution-23-11-12-$i.txt EnsAvgConv-1.txt
		fi
		rm RanForMemX-$i.txt
		j=$[ 1 + $j]
	else
		tail $i-std.io -n2
		echo -e "\ntrajectory $i did not terminate normally.\n\n\n"
	fi
	i=$[ 1 + $i ]
	echo "Finished trajectory $[$i-1]/$MAX_TRAJ=$(echo $[$i-1] $MAX_TRAJ | awk '{print 100*$1/$2}')%, $[$j-1] good trajectories."
done


echo "\
reset
#set term x11 enhanced
set term jpeg enhanced
set output \"Convolution-25-12-12.jpeg\"

#FILENAME=\`find . -name \"EnsAvgConv_*.txt\" | sed 's/^..//'\`
FILENAME=\"EnsAvgConv-$[$j-1].txt\"

kB=8.31446                                   # kB = 1 [J/K] = R [J/mole per K] = 0.00831446 KJ/mole per K

m=1.                                         # get from topol*.top
T=\`grep ref_t grompp-*-1.mdp | cut -f4\`
tau_t=\`grep tau_t grompp-*-1.mdp | cut -f4\`
dt=\`grep dt grompp-*-1.mdp | cut -d \"=\" -f2\`
tau_t_co=0.6                                    # grep tau_t_co ~/GROMACS_Hydrogen_Sources/gromacs-4.5.5/src/mdlib/update.c

CRR0=(2*m*kB*T/(tau_t*dt))
#Cee0=CRR0/(m*m*tau_t_co/dt)
Cee0=2*kB*T/(m*tau_t*tau_t_co)


#fit A0*Cee0*exp(-x/tau_t_co) FILENAME u (dt*\$0):1 via A0 ; fit A0*Cee0*exp(-x/(A1*tau_t_co)) FILENAME u (dt*\$0):1 via A1
FIT_LIMIT=1e-7
fit A0*Cee0*exp(-x/(A1*tau_t_co)) FILENAME u (dt*\$0):1 via A0,A1

set xlabel \"time/ps\"
set ylabel \"nm^2 ps^{-2}\"

p FILENAME u (dt*\$0):1 w l lw 8 noti,\
A0*Cee0*exp(-x/(A1*tau_t_co)) t sprintf(\"%g*exp(-t/%g)\",A0*Cee0,A1*tau_t_co) lw 5,\
Cee0*exp(-x/tau_t_co) lw 2 t sprintf(\"%g*exp(-t/%g)\",Cee0,tau_t_co)

pr \"\"
pr \"m = \",m
pr \"T = \",T
pr \"tau_t = \",tau_t
pr \"dt = \",dt
pr \"nsteps = $nsteps\"
pr \"ConvolutionWindow = $ConvolutionWindow\"
pr \"tau_t_co = \",tau_t_co
pr \"Cee^{\\circ} = CRR0/(m*m*tau_t_co/dt) = \",Cee0
pr \"Cee(0) = $(head -n1 EnsAvgConv-*.txt)\"
pr \"A0 = \",A0,\" <?> 1\"
pr \"\"
" > Convolution-25-12-12.gp

gnuplot Convolution-25-12-12.gp
if [ 0 -eq $? ]
then echo "Normal termination of Batch GNUPlot Script"
else echo "Abnormal termination of Batch GNUPlot Script"
echo -e "dt=$(grep dt grompp-*-1.mdp | cut -d "=" -f2)\nCee(0) = $(head -n1 EnsAvgConv-*.txt)"
fi
exit 0
