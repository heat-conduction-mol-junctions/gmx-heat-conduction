#! /bin/bash -e
SCRIPTS="$HOME/gmx-heat-conduction/src/Scripts"
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then SCRIPTS="$HOME/Documents/GROMACS/Scripts" ; fi
EnsembleAveragingScript=$(ls -tr1 $SCRIPTS/EnsembleAveraging-*.sh | tail -n1)
NodeJanitorScript=$(ls -tr1 $SCRIPTS/NodeJanitor-*.sh | tail -n1)

#clear
echo
n=1
if [ ! -z $1 ]
	then
	jStart=$(qstat -u inonshar | grep $1 | wc -l)
else
	jStart=$(showq -u inonshar | grep Total | cut -d ":" -f2 | cut -d " " -f2)
fi
jLast=$jStart

jLeft=$jStart
while [ 0 -lt $jLeft ]
do 

	if [ ! -z $1 ]
  	then
		jActive=$(qstat -u inonshar | grep $1 | grep " R " | wc -l)
		jLeft=$(qstat -u inonshar | grep $1 | wc -l)
	else
	  jActive=$(showq -u inonshar | grep Running | wc -l)
	  jLeft=$(showq -u inonshar | grep Total | cut -d ":" -f2 | cut -d " " -f2)
	fi

	if [ $jLeft -ne $jStart ]
	then

		if [ $jActive -ne $jLast ]
			then tCalcd=$(echo $jStart $jLeft $n | awk '{OFMT = "%.0f" ; print (1 + ((3.0 * $3)/($1 - $2)) * $2)}') # ((3.0 * n)/(jStart - jLeft))*jLeft
			jLast=$jActive
		else
	    tLeft=$(echo $tCalcd | awk '{print ($1 - 3.0)}')
		fi

		if [ 1 -eq $(echo $tLeft | awk '{if (100.0 > $1) print 1 ; else print 0}') ]
			then tLeft=":($tLeft s)"
		else
			if [ 1 -eq $(echo $tLeft | awk '{if (60.0*100.0 > $1) print 1 ; else print 0}') ]
				then 
				tLeft=$(echo $tLeft | awk '{OFMT = "%.0f" ; print ($1/60.0)}')
				tLeft=":($tLeft m)"
			else
				tLeft=$(echo $tLeft | awk '{OFMT = "%.0f" ; print ($1/(60.0*60.0))}')
				tLeft=":($tLeft h)"
			fi
		fi

	else
		#tLeft="N/A"
		tLeft=""
	fi

	#echo -n "$jActive/$jLeft$tLeft  "
	echo -en "\e[1A" ; echo -e "\e[0K\r$jActive/$jLeft$tLeft  "
	n=$[1+$n]
	sleep 1

done

echo "#Normal termination of jobs."

if [ 0 -eq $(qstat -u inonshar | wc -l) ]
	then 
	echo "#No jobs running. Executing Node Janitor script."
	$NodeJanitorScript
else
	echo "#Other Instances running:"
	qstat -u inonshar | tail -n $[ $(qstat -u inonshar| wc -l) - 5 ] | cut -d " " -f9 | cut -d "-" -f2 | sort | uniq -c | sort -nr
fi


#if [ -f output-*.txt ]
set +e
ls output-*.txt > /dev/null 2>&1 # send STDIO to NULL and send STDERR to STDIO, i.e. ignore both
bOutputFiles=$?
set -e
if [ 0 -eq $bOutputFiles ]
	then 
	echo "Output run:"
	set +e # DBG 20-9-13
	$EnsembleAveragingScript
  set -e # DBG 20-9-13

#	if [ 0 -eq $? ]
#  	then 
#		if [ -f LocalStuff-*.svg ] 
#			then 
#			eog LocalStuff-*.svg
#		else 
#			eog LocalStuff-*.jpeg
#		fi # -f LocalStuff-*.svg
#	fi # 0 -eq $?

fi # -f output-*.txt (alternatively, ls output-*.txt )

echo
date
echo "#Normal termination of How Long Left script."
exit 0
