#! /bin/bash

if [ "hydrogen.tau.ac.il" == $HOSTNAME ]
	then
  PREFIX="/usr/local/bin/"
else
	PREFIX="$HOME/gmx-heat-conduction/bin/gromacs/bin/"
fi

if [ ! -f eigenvec-*-1.trr ]
	then
	echo "No input eigenvector file specified. Getting last GMX INSTANCE from ../INSTANCE.TXT and retrieving files..."
	INSTANCE=`cut -d "=" -f2 ../INSTANCE.txt`
	vecFileName="eigenvec-$INSTANCE-1.trr"
	cp ../$INSTANCE/$vecFileName .
	if [ 0 -ne $? ] ; then echo "Failed to retrieve eigenvector file from INSTANCE = $INSTANCE! Aborting." ; exit 2 ; fi
else
	vecFileName=$1
	if [ -z $vecFileName ] ; then echo "Missing input eigenvector file name! Aborting." ; exit 1 ; fi
	if [ ! -f $vecFileName ] ; then echo "Missing input eigenvector file $vecFileName! Aborting." ; exit 1 ; fi
fi

function PreviewEigenV(){

echo "DBG: Previewing parsed eigenvectors:"
for modeFile in splitModes/mode*.txt
#for mode in $(seq 1 $nModes)
  do
  mode=$(echo $modeFile | sed 's/^.\{15\}//' | cut -d "." -f1 | awk '{printf("%0'$digitsModes'd", $1)}')
  if [ "00" == $mode ] ; then continue ; fi # skip zeroeth "mode" -- the configuration at which the normal mode analysis was done
  echo mode=$mode
  tail -n $nAtoms splitModes/mode$mode
  cat splitModes/mode$mode.txt
  echo
done

}

function verifyEigenvectorComponentNormalization()
{
echo -e "\nVerification: Sum of the products of the atom masses by the square of the eigenvector components should be 1 for each mode:"
for modeFile in splitModes/mode*.txt
  do
  modeSum=0
  for atom in $(seq 1 $nAtoms)
    do
    for dim in $(seq 1 3)
    do
      c=$(head -n $atom $modeFile | tail -n1 | cut -d ' ' -f $[ 2 + $dim ] )
      #echo "DBG: $modeFile $atom $c"
      modeSum=$(echo "$modeSum ${atomMasses[ $atom - 1 ]} $c" | awk '{print($1 + $2 * $3 ** 2)}')
    done
  done
  modeSumRelError=$(echo $modeSum | awk '{print(100 * ($1 - 1))}')
  echo $modeFile $modeSum $modeSumRelError%
done
}



#MAIN:
#=====

if [ -d splitModes ] ; then echo "The directory 'splitModes' already exists! Make sure you're not overwriting stuff. Aborting." ; exit 12 ; fi

echo "Getting number of modes and atoms."
freqFileName=$(echo $vecFileName | sed -e s/vec/freq/ | sed -e s/trr/xvg/)
cp ../$INSTANCE/$freqFileName .
if [ ! -f $freqFileName ] ; then echo "Missing input eigenfrequency file $freqFileName! Aborting." ; exit 1 ; fi
nModes=$(echo $(wc -l $freqFileName | cut -d " " -f1) | awk '{print($1-13)}')
echo "DBG: nModes=$nModes"
digitsModes=$(echo $nModes | awk '{printf("%d",1+log($1)/log(10))}') # number of digits needed to number modes (in decimal base)
nAtoms=$(echo $nModes | awk '{print($1/3)}') # $(head -n 2 ../conf.gro | tail -n 1) 
echo "DBG: nAtoms=$nAtoms"

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

echo "Getting atom types, masses, and local coupling strengths."
if [ ! -f ../conf.gro ] ; then echo "Missing configuration file in parent directory -- can get atom types. Aborting." ; exit 23 ; fi
#types=()
shopt -s extglob 
for j in $(seq 1 $nAtoms)
	do
	#echo "DBG: $j"
	newType=$(head -n $[ 2 + $j ] ../conf.gro | tail -n 1 | awk '{print($2)}')
	echo "DBG: $j: $newType"
	#types=( $types $newType )
	jMin1=$[ $j - 1 ]
	#echo "DBG: ${types[$jMin1]}"
	#case "${types[$jMin1]}" in
	tau_t=1 # in [ps] 
	g_isotropic=$(echo $tau_t | awk '{print(sqrt(((1 / $1) ** 2) / 3))}') # g=1/tau_t , 3 * g_isotropic^2 = g^2 ==> g_isotropic = sqrt(g^2 / 3) = sqrt((1/tau_t)^2 / 3)
	case "$newType" in 
 		S) atomMasses=( ${atomMasses[@]} "32.06" ); gammaA=( ${gammaA[@]} $g_isotropic );;
		O) atomMasses=( ${atomMasses[@]} "15.999" ); gammaA=( ${gammaA[@]} "0" );;
		#CH2) atomMasses=( $atomMasses "14.027" ); gammaA=( $gammaA "0" );;
		C+([[:digit:]])) echo "Carbon found!" ; atomMasses=( ${atomMasses[@]} "14.027" ); gammaA=( ${gammaA[@]} "0" );;
		[?]) echo "Unrecognized atom type. Aborting!" ; exit 117 ;;
  esac
	#echo "DBG: atomMasses=${atomMasses[@]}"
done

#atomMasses=()
#for j in $(seq 1 $nAtoms)
#	do
#	jMin1=$[ $j - 1 ]
#	case "${types[$jMin1]}" in 
#		S) atomMasses=( $atomMasses "32.06" );; 
#		CH2) atomMasses=( $atomMasses "14.027" );; 
#	esac
#	echo "DBG: ${atomMasses[@]}"
#done  
echo "DBG: Atom masses array: ${atomMasses[*]}"

#gammaA=("1" "1")
echo "DBG: Atom bath-coupling array: ${gammaA[@]}"

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

echo "Computing projection of each mode on each atomic degree of freedom."
compProjFile=$(echo $vecFileName | sed -e s/rr/xt/ | sed -e s/eigenvec/comp/)
$PREFIX/g_anaeig_d -v $vecFileName -last -1 -comp $compProjFile -quiet

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

echo "Dumping eigenvectors to text file, and splitting it according to modes."
#
#	For the generalized coordinates of a given mode, the magnitude of the displacement of each of the atoms is WEIGHTED BY THE MASS OF THE ATOM,
#	times the the sum of the squares of the Cartesian components given by the eigenvector matrix.
# Therefore, for normalization we need to multiply the matrix elements by the square root of the mass of each atom.
#	The sum of the MASS WEIGHTED displacements of each of the atoms yields zero for any given mode (no Center Of Mass Motion).
#
#	For example, for mode i:
#
# mode$i.txt:
# ________________________________________
#	|	c[i][0][x] | c[i][0][y] | c[i][0][z] |
# |	c[i][1][x] | c[i][1][y] | c[i][1][z] |
#	|	     .     |      .     |      .     |
# |	     .     |      .     |      .     |
# |	     .     |      .     |      .     |
# |	c[i][j][x] | c[i][j][y] | c[i][j][z] |
# |	     .     |      .     |      .     |
# |	     .     |      .     |      .     |
# |	     .     |      .     |      .     |
# |	c[i][n][x] | c[i][n][y] | c[i][n][z] |
# |______________________________________|
#
#
#	For example, eigenvector i for a diatomic in 3-D:
#
# c[i]=
# c[i][0][x]
# c[i][0][y]
# c[i][0][z]
# c[i][1][x]
# c[i][1][y]
# c[i][1][z]
#
#	Normalization of the magnitude of eigenvector i:
#
# m[0] * c[i][0][x]^2 + m[0] * c[i][0][y]^2 + m[0] * c[i][0][z]^2 + m[1] * c[i][1][x]^2 + m[1] * c[i][1][y]^2 + m[1] * c[i][1][z]^2 = 1
#
#
#	|r[i][j]| = |c[i][j]|^2 = c[i][j][x]^2 + c[i][j][y]^2 + c[i][j][z]^2
#
#	1 = Sum_j (m[j] * |r[i][j]|) = Sum_j (m[j] * |c[i][j]|^2) , for all i.
#
#
# Therefore, a normal mode where only one atom moves (call this atom 'j'), would be: 1 = m[j] * r[i][j] = m[j] * sqrt( c[i][j][x]^2 + c[i][j][y]^2 + c[i][j][z]^2 ), 
#	so, c[i][j][x]^2 + c[i][j][y]^2 + c[i][j][z]^2 = (1 / m[j])^2
#	For example, c[i][j][x] = c[i][j][y] = c[i][j][z] = sqrt(1 / 3) / m[j], and zero for all other atoms.
#
#
# After multiplying each matrix element by the mass of the atom, we have a new MASS WEIGHTED matrix, such that 
#
# C[i][j][x] = sqrt(m[j]) * c[i][j][x]
# |C[i][j]|^2 = C[i][j][x]^2 + C[i][j][y]^2 + C[i][j][z]^2 = m[j] * c[i][j][x]^2 + m[j] * c[i][j][y]^2 + m[j] * c[i][j][z]^2 
# Sum_j |C[i][j]|^2 = Sum_j (m[j] * |c[i][j]|^2) = 1
#
#	============================================================================================================
# |Which makes |C[i][j]|^2 the weight (unrelated to 'mass') of the motion of atom 'j' in the normal mode 'i'.|
# ============================================================================================================
#
# C[][] * r[] = q[]
#	q[i] = Sum_j (C[i][j] * r[j])
#
#	Local coupling strength:				gamma[j] = (delta[j][0] + delta[j][n])/tau_t
# (for the case of two baths coupled locally to atoms '0' and 'n' with coupling constant 1/tau_t)
#
# Normal mode coupling strength:	gamma[i] = Sum_j (|C[i][j]|^2 * gamma[j])
#
# If |C[i][j]|^2 = delta[j][0],										then gamma[i] = gamma[j]
# If |C[i][0]|^2 = |C[i][n]|^2 = 0,								then gamma[i] = 0
# If |C[i][j]|^2 = (delta[j][0] + delta[j][n])/2,	then gamma[i] = (gamma[0] + gamma[n])/2
#

gammaModes=()

txtFileName=$(echo $vecFileName | sed -e s/rr/xt/)
$PREFIX/gmxdump_d -f $vecFileName -quiet > $txtFileName
if [ -d splitModes ] ; then echo "The directory 'splitModes' already exists! Make sure you're not overwriting stuff. Aborting." ; exit 12 ; fi
mkdir splitModes
cd splitModes

	echo 
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo

	split -a $digitsModes -dl $[ 7 + $nAtoms ] ../$txtFileName mode

	for modeFile in mode*
	#for mode in $(seq 1 $nModes)
		do
		echo
		mode=$(echo $modeFile | sed 's/^.\{4\}//' | awk '{printf("%0'$digitsModes'd", $1)}')
    if [ 0 -eq $mode ] ; then continue ; fi # skip zeroeth "mode" -- the configuration at which the normal mode analysis was done
		echo "mode=$mode"
		touch mode$mode.txt
		gammaM=0
		for atom in $(seq 1 $nAtoms)
			do
			#echo "  atom=$atom atomMass=${atomMasses[ $atom - 1 ]}"
			echo -n "$mode $[ $atom - 1 ] " >> mode$mode.txt
			for dim in {1..3}
				do

				#echo "    dim=$dim"

				x=$(tail -n $[ $nAtoms - $atom + 1 ] mode$mode | head -n 1 | cut -d "{" -f2 | awk '{print $'$dim'}')
				#echo "     DBG: dim=$dim x=$x"

				y="${x%?}" # remove last character
				#y=$(echo $x | cut -c 1-1)
				#echo "     DBG: dim=$dim y=$y"
        #echo -n "$y " >> mode$mode.txt

				# The normal modes, being normal, are massless. Therefore all mass information should be absent before getting the normal mode coordinates. 
				# This implies that for a heteronuclear diatomic, we don't expect to find a mode as symmetric as $ q_{S}=\frac{\sqrt{2}}{2}x_{1}-\frac{\sqrt{2}}{2}x_{2} $ , 
				# because that would mean the internal stretching gets mixed with center of mass motion. Rather, we expect the two to be orthogonal: 
				# One mode for COMM, and one mode for bond stretching (the latter while keeping the COM stationary). 
				# In conclusion, it should not be expected that the eigenvector coefficients should be normalized so that the sum of their squares equals 1, 
				# rather the sum of the products of their squares by their masses should equal 1.
				# i.e.  $ \sum_{i}C_{ij}^{2}\not=\sum_{i}m_{i}C_{ij}^{2}=1\quad\forall j $.
				#z=$(echo $y ${atomMasses[ $atom - 1 ]} | awk '{printf($1 * sqrt($2))}') # reduce mass weighting
        if [ 1 -eq $(echo $y | awk '{if (1e-4 < $1 || -1e-4 > $1) print 1 ; else print 0}') ] ; then z=$y ; else z=0 ; fi
				#echo "     DBG:          z=$z"
				echo -n "$z " >> mode$mode.txt

				atomMin1=$[ $atom - 1 ]
				gammaM=$(echo $gammaM ${gammaA[$atomMin1]} $z | awk '{print($1 + $2 * $3 * $3)}')
				#echo "DBG: mode=$mode atom=$atom dim=$dim atomMin1=$atomMin1 z=$z gammaM=${gammaM[@]}"

			done #dim
    echo >> mode$mode.txt
		done #atom
    gammaModes=(${gammaModes[@]} $gammaM)
	done #mode
	echo "DBG: gammaModes=${gammaModes[@]}"

cd ..

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

echo
echo "DBG: sum of gammaModes:"
#sumgammaModes=0 ; for mode in $(seq 1 $nModes) ; do m=$[ $mode - 1 ] ; echo "$mode: $sumgammaModes ${gammaModes[$m]}" ; sumgammaModes=$(echo $sumgammaModes ${gammaModes[$m]} | awk '{print($1 + $2)}') ; done ; echo "DBG: sum=$sumgammaModes"
sumgammaModes=0 ; for mode in $(seq 0 $[ $nModes - 1 ] ) ; do echo "$mode: $sumgammaModes ${gammaModes[$mode]}" ; sumgammaModes=$(echo $sumgammaModes ${gammaModes[$mode]} | awk '{print($1 + $2)}') ; done ; echo "DBG: sum=$sumgammaModes"
echo
echo "DBG: ROOT of the SUM of gammaModes SQUARED:"
SSmodes=0 ; for mode in $(seq 0 $[ $nModes - 1 ] ) ; do SSmodes=$(echo $SSmodes ${gammaModes[$mode]} | awk '{print($1 + $2 * $2)}') ; done ; echo "DBG: SSmodes=$SSmodes , RSSmodes=$(echo $SSmodes | awk '{print(sqrt($1))}')"


echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo "Outputting eigenfrequencies and eigenvector coefficients for Landauer Heat Conduction claculation:"

#echo -n $(tail -n $nModes eigenfreq-*-1.xvg | sed 's/.* //g') ; echo
WVNMBR_2_NVRS_PS="0.0299792458" # conversion factor between wavenumbers and inverse picoseconds. Exact by definition of the speed of light and the pico- prefix.
WVNMBR_2_RADperPS=$(echo $WVNMBR_2_NVRS_PS | awk '{print($1 * 2 * atan2(0, -1))}')
##echo -n $(tail -n $nModes eigenfreq-*-1.xvg | awk '{print($2 * '$WVNMBR_2_NVRS_PS')}') ; echo

#echo -n $(tail -n $nModes eigenfreq-*-1.xvg | awk '{print($2 * '$WVNMBR_2_RADperPS')}') ; echo
echo -n $(tail -n $nModes eigenfreq-*-1.xvg | awk '{print($2 * '$WVNMBR_2_RADperPS')}') >> input.txt ; echo >> input.txt

for modeFile in splitModes/mode*.txt
	do
	#echo -n $(cut -d " " -f3- $modeFile) ; echo
	echo -n $(cut -d " " -f3- $modeFile) >> input.txt ; echo >> input.txt
done

verifyEigenvectorComponentNormalization

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo


#PreviewEigenV

echo 
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo


echo "#Normal termination of Peierls-Landauer script."
exit 0
