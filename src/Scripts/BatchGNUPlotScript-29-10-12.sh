#! /bin/bash

# Loops over instance sub-directories under this directory,
# whose file-system status was changed less than MINUTES mintues ago (e.g. created),
# and presents parameters and energy conservation graph.

function mkgpscrpt(){

#1000,707 # A4 on Sodium
#1200,850 # A4 at home
#1260,890 # Full window at home
OutputSizeWidth=1260
OutputSizeHeight=890

bProduction=`head options.dat -n9 | tail -n1 | cut -d "=" -f2-`
if [ 1 -eq $bProduction ] ; then outputFILE="output.txt" ; else outputFILE="output-1.txt" ; fi

SimDuration=`cut -d "=" -f2- options.dat | head -n1`

dt=`cut -d "=" -f2- options.dat | head -n2 | tail -n1`
# dc does not accept scientific notation!
if [ "2e-4" = $dt ] ; then dt=0.0002 ; fi
#if [ 0 -le $dt ] ; then echo "Invalid value for dt = $dt. Exiting" ; exit 3 ; fi

t=$(echo "$SimDuration $dt * p" | dc)
echo "DEBUG: t = $t"
SegSkip=$(echo " $SimDuration 10 / p" | dc)
echo "DEBUG: SegSkip = $SegSkip"

echo "
reset

set fit errorvariables
fit DE1 \"$outputFILE\" u 2:6 every 3:$SegSkip:0:1 via DE1
fit DE2 \"$outputFILE\" u 2:6 every 3:$SegSkip:1:($SegSkip/2) via DE2
fit P1 \"$outputFILE\" u 2:7 every 3:$SegSkip:0:1 via P1
fit P2 \"$outputFILE\" u 2:7 every 3:$SegSkip:1:($SegSkip/2) via P2

set term jpeg enhanced size $OutputSizeWidth,$OutputSizeHeight
set output \"$NRGconsName.jpeg\"

set xlabel \"t / [ps]\"
set ylabel \"Energy (per time-step) / [KJ/mole]\"

set multiplot layout 2,2

pr \"Plotting full trajectory every $SegSkip points\"
p \"$outputFILE\" u 2:6 every 3:$SegSkip:0:1 t \"DE_1\",\
 '' u 2:6 every 3:$SegSkip:1:($SegSkip/2) t \"DE_2\",\
 '' u 2:7 every 3:$SegSkip:0:1 lc 1 ps 0.5 t \"P_1\",\
 '' u 2:7 every 3:$SegSkip:1:($SegSkip/2) lc 2 ps 0.5 t \"P_2\"

p [(0.9*$t):*] \"$outputFILE\" u 2:6 every 3::0:1 t \"DE_1\",\
 '' u 2:6 every 3::1:1 t \"DE_2\",\
 '' u 2:7 every 3::0:1 lc 1 ps 0.5 t \"P_1\",\
 '' u 2:7 every 3::1:1 lc 2 ps 0.5 t \"P_2\"

p [($t-0.05):*] \"$outputFILE\" u 2:6 every 3::0:1 t \"DE_1\",\
 '' u 2:7 every 3::0:1 ps 0.2 t \"P_1\",\
 '' u 2:(\$6-\$7) every 3::0:1 t \"DE_1-P_1\",\
 DE1 lc 1 t \"<DE_1>\",\
 P1 lc 2 t \"<P_1>\"

p [($t-0.05):*] \"$outputFILE\" u 2:6 every 3::1:1 t \"DE_2\",\
 '' u 2:7 every 3::1:1 ps 0.2 t \"P_2\",\
 '' u 2:(\$6-\$7) every 3::1:1 t \"DE_2-P_2\",\
 DE2 lc 1 t \"<DE_2>\",\
 P2 lc 2 t \"<P_2>\"

unset multiplot
" > $NRGconsName.gp

}
#==============================================================================
#
#main

NRGconsName="SingleEnergyCons-24-10-12"

echo "Would you like previous Energy Conservation scripts with the name $NRGconsName to be deleted from all subdirectories? (press y to acknowledge)"
read KeyPressed
if [ "y" = $KeyPressed ] ; then
	for i in $(find . -type d) ; do 
		if [ "." != $i ] ; then rm $i/$NRGconsName.gp ; fi
	done
fi

MINUTES=1600
for i in $(find . -type d -cmin -$MINUTES) ; do 
if [ "." != $i ] ; then 
	cd $i
	if [ "Normal termination of Single Trajectory Script. Exiting." = "$(tail 1-std.io -n2)" ] ; then
		if [ -f "$NRGconsName.gp" ] ; then 
			echo "Energy Conservation scripts exists in \"$i/\"! Exiting."
			exit 1
		fi
		mkgpscrpt
		if [ 0 -ne $? ] ; then echo "Error making GNUPlot script. Exiting." ; exit 2 ; fi
		gnuplot $NRGconsName.gp
		cat grompp-*.mdp
		echo $i
		eog $NRGconsName.jpeg
	else
		tail 1-std.io -n2
		echo -e "\n$i did not terminate normally.\n\n\n"
	fi
	cd ..
fi ;
done

echo "Normal termination of Batch GNUPlot Script"
exit 0
