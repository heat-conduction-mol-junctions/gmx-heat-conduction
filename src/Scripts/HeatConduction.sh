#! /bin/bash

SCRIPTS="$HOME/gmx-heat-condution/src/Scripts"
if [ "hydrogen.tau.ac.il" == $HOSTNAME ] ; then SCRIPTS="$HOME/Documents/GROMACS/Scripts" ; fi

if [ ! -f  HeatConduction*.txt ]
	then head -n1 data.txt | cut -d' ' -f8-17 > HeatConduction.txt
fi

tail -n +2 data.txt |  cut -d' ' -f1-10 >> HeatConduction.txt

$SCRIPTS/heat_conduction.gp

