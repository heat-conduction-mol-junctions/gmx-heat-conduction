#! /bin/bash -e

THE_WORKS_SCRIPT=$(ls -tr1 $HOME/gmx-heat-conduction/src/Scripts/TheWorks-*.sh | tail -n1)
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then THE_WORKS_SCRIPT=$(ls -tr1 $HOME/Documents/GROMACS/Scripts/TheWorks-*.sh | tail -n1) ; fi
CONFGRO="conf.gro"
LENGTH=${#CONFGRO}
if [ ! -z $1 ] ; then MAXDEPTH=$1 ; else MAXDEPTH=3 ; fi
SLEEP=30
#SLEEP=$[ 60 * 60 * 12 ] # 12 hours

pwd=$PWD
tree -L 1
for d in $(find . -maxdepth $MAXDEPTH -name "$CONFGRO") 
	do
	sd=$(echo $d | sed 's/.\{8\}$//')
        echo $(date) $sd
	cd $sd
	pwd
	rm -f SingleTrajectory-*.sh parse-*.out
	sleep $SLEEP
	set +e
	#(nohup $THE_WORKS_SCRIPT > log.io 2> log.err < /dev/null &)
	nohup $THE_WORKS_SCRIPT > log.io 2> log.err < /dev/null
	if [ 1 -ne $(grep "Normal termination of The Works script" log.io | wc -l) ]
		then
		echo ; 	tail log.err ; echo
		echo "ERROR Abnormal termination of $PWD"
	fi
	set -e
	cd $pwd
done

