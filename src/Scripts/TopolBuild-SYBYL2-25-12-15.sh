#! /bin/bash

i=0
#for InputMOL2 in *.MOL2 ; do i=$[1+$i] ; done
for InputMOL2 in *.mol2 ; do i=$[1+$i] ; done
if [ 1 -eq $i ] ; then echo -e "\nUsing MOL2 file $InputMOL2...\n" ; fi
if [ 0 -eq $i ] ; then echo -e "\nMOL2 file missing. Exiting.\n" ; exit 1 ; fi
if [ 1 -lt $i ] ; then echo -e "\nMultiple ($i) MOL2 files. Exiting.\n" ; exit 1 ; fi
#if [ ! -f DRGFIN.MOL2 ] ; then echo -e "\nMOL2 file missing. Exiting.\n" ; exit 1 ; fi

TopolBuildDir="$HOME/gmx-heat-conduction/bin/Topology_Generation/topolbuild1_3"
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then TopolBuildDir="$HOME/Documents/GROMACS/Topology_Generation/topolbuild1_3" ; fi

if [ -z $TopolBuildDir ] ; then echo -e "\nHost is neither Sodium nor Hydrogen. Location of TopolBuild directory is unknown. Exiting.\n" ; fi


echo -e "\nChoose output molecule files name:\n"
read MOL2NAME
while [ $InputMOL2 == $MOL2NAME.mol2 ]
	do
	echo -e "This name will cause the input molecule file to be overwritten!\nChoose a different output molecule files name, or break and change input molecule file name.\n"
	read MOL2NAME
done

#Change molecule name from ***** to $MOL2NAME (2nd line of MOL2 file).
sed -e 's/\*\*\*\*\*/'$MOL2NAME'/g' $InputMOL2 > $MOL2NAME.mol2

Avogadro="1"
echo -e "\nDoes the file originate from Avogadro? (0=No/1=Yes)\n"
read Avogadro
#if second line in MOL2 file (molecule name) is missing (Avogadro), or *****, change it to MOL2NAME
#if [ 1 -eq $Avogadro ] ; then sed 2s/\Q*****\E/$MOL2NAME/" < $MOL2NAME.mol2 ; fi
#still needs to be decided whether to use USER_CHARGES (ProDrg) or GASTEIGER charges (Avogadro)...

#if MOL2 file is missing substructure name in the last 2 lines (Avogadro), add a generic entry
if [ 1 -eq $Avogadro ] ; then echo -e "@<TRIPOS>SUBSTRUCTURE\n  1  DRG    1" >> $MOL2NAME.mol2 ; fi

# tripos, gaff, glycam## or amber## (where ## designates the year of the glycam or amber file (the number between parm or the glycam and the .dat in /dat/leap/parm)), gmx#### (where #### designates the particular gromacs force field, or oplsaa.
echo -e "\nEnter Force-Field: (e.g. tripos,gaff,oplsaa,glycam##,amber## [amber99] ,gmx#### [gmx53a6] )\n"
read TOPOLBUILDFF

echo "DBG: FF is $TOPOLBUILDFF"

case "$TOPOLBUILDFF" in
	oplsaa) FFdir=$TopolBuildDir"/dat/gromacs";;
	gmx*) FFdir=$TopolBuildDir"/dat/gromacs";;
	amber*) FFdir=$TopolBuildDir;;
	gaff) FFdir=$TopolBuildDir"/dat/leap/parm";;
	taff) FFdir=$TopolBuildDir"/dat/TAFF";;
	[?]) echo "unrecognized force field. aborting." ; exit 3 ;;
esac

echo "DBG: FF dir is $FFdir"

echo -e "\nBuilding topology...\n"
#topolbuild -n [common name for .mol2 input file and all output files: e.g. $MOL2NAME] -dir [directory for force-field topology data] -ff [chosen force-field]
$TopolBuildDir/src/topolbuild -n $MOL2NAME -dir $FFdir -ff $TOPOLBUILDFF
if [ 0 -ne $? ] ; then echo -e "\nError building topology. Exiting...\nMake sure all line ends are UNIX compatible, and that the file ends with a terminal '@<TRIPOS>' directive." ; rm $MOL2NAME.mol2 ; exit 1 ; fi

echo -e "\nTopology built!\nRenaming files to GROMACS standard..."
mv $MOL2NAME.top topol.top #copy .top file to standard name
sed -i 's/ff'$MOL2NAME'.itp/topol.itp/g' topol.top #search and replace in topol.top to match new .itp file name
mv ff$MOL2NAME.itp topol.itp #copy .itp file to standard name
mv $MOL2NAME.gro conf.gro #copy .gro file to standard name

#other, unnecessary, output files: Clean MOL2 file ($MOL2NAMEMOL.mol2), log file ($MOL2NAME.log), position restraint file (posre$MOL2NAME.log)

#write a sub-routine to make sure the box dimensions are larger than the electrostatic cut-off
#wrtie a sub-routine to make each atom in a separate charge group (since they are going to be in separate energy groups)

echo -e "\nTopology build complete!\n"

if TOPOLBUILDFF="oplsaa" #creating a link from oplsaa.ff/ffnonbonded.itp to ffoplsaanb.itp
	#then ln /home/inonshar/gromacs/share/gromacs/top/oplsaa.ff/ffnonbonded.itp ffoplsaanb.itp ; if [ 0 -ne $? ] ; then echo -e "\nFailed to create a link for force-field non-bonded included topology. Exiting.\n" ; exit 1 ; fi
	then touch ffoplsaanb.itp # using user (MOL2) charges
fi

echo -e "\nNote: Still need to automate the following
 1. Box size is sometimes too small for default Coulomb cut-off (conf.gro).

			sed -i '\$s/.*/   6.00000    6.00000    6.00000/' conf.gro

 2. Replace include directive for included topology (topol.itp) by coping the entire *.itp file into topol.top instead of the include line.
 3. Replace include directive for non-bonded topology (near tenth line of topology file topol.top) with absolute path. For example,
		a. OPLS-AA: 

			#include \"~/gmx-heat-conduction/bin/gromacs/share/gromacs/top/oplsaa.ff/ffnonbonded.itp\"

		b. GROMOS53a6:

			#include \"~/gmx-heat-conduction/bin/gromacs/share/gromacs/top/gromos53a6.ff/ffnonbonded.itp\"
		where '~' is replaced by the explicit \$HOME directory

 4. Change atom numbering according to left-to-right current direction (from hot bath to cold bath).
 5. If an all-atom force-field was chosen, seperate each atom to an individual charge group (topol.top).
 6. Go over atomic charges and symmetrize, if necessary.
 7. Restraint the position of the molecule inside the junction in one of three ways:
		a. Anchor atoms (infinitely heavy \"wall\" atoms)
			1) Change thiol Hydrogens to anchor atoms: H --> XXX , m = 1.008 a.m.u. --> m = 99999.9 a.m.u. (only in topol.top -- i.e. not in conf.gro).
			2) Add anchor atom type manually at top of topology file (topol.top):

					[ atomtypes ]
					; atomtype   m(u)       q(e)      part.type   V(cr)   W(cr)
				  XXX        999999.9   0.00000   A           1E-05   1E-05

					; (Make sure the atom mass and charge correspond with those appearing under the \" [ atoms ] \" directive.

		b. Position restraints on the left and right Sulfur atoms (external force).

				[ position_restraints ]
				;  i funct       fcx        fcy        fcz
				   2    1      50000        200      200
				   N    1      50000        200      200

				right after the [ dihedrals ] directive

		c. Artificial removal of the center-of-mass motion (COMM) at each time-step. This procedure is not energy conserving.

 8. If there are any residue names which are RNA residues, change these to LIG (for ligand) to avoid these being mathced unnecessarily to residues in the database.
 9. Energy minimization.\n"

echo "#Normal termination of $0"
exit 0
