#! /bin/bash
# This BASH script produces a GNUPlot script named "current-*.gp" and executes it using GNUPlot to plot the energy current

DIM=3

function mkgpscript(){
echo " pr \"\" # pr = print
pr \"Preliminaries:\"
reset

bin(X,WIDTH)=WIDTH*floor(X/WIDTH) + BINWIDTH/2.0 # for histogram binning
max(x,y) = (x > y) ? x : y # for multiple histograms using same bins
min(x,y) = (x < y) ? x : y # for histogram bins
PI = 3.14159265

#Current file columns:
# 1   2 3       4              5              6                7                 8                9              10              11              12             13
#step t j I_{eq}^{acc,x} I_{eq}^{acc,y} I_{eq}^{acc,z} I_{vrtx}^{acc,x}  I_{vrtx}^{acc,y} I_{vrtx}^{acc,z} I_{dyn}^{acc,x} I_{dyn}^{acc,y} I_{dyn}^{acc,z} I_{eq}^{j,x} I_{eq}^{j,y} I_{eq}^{j,z} I_{vrtx}^{j,x}  I_{vrtx}^{j,y} I_{vrtx}^{j,z} I_{dyn}^{j,x} I_{dyn}^{j,y} I_{dyn}^{j,z}
#     14           15             16              17             18            19            20            21

\`echo \${outputatoms[@]}\`
#OUTPUTATOMS=\`\${#outputatoms[@]}\`  # every OUTPUTATOMS::FIRSTLINES
OUTPUTATOMS = \`echo \$(head -n 12 N.dat | tail -n 1)\`
pr \"OUTPUTATOMS = \",OUTPUTATOMS
MAX_TRAJ=\`echo \$(head -n 1 N.dat)\`
dt=\`echo \$(head -n 2 N.dat | tail -n 1)\`
SEGLENGTH=\`echo \$(head -n 3 N.dat | tail -n 1)\`
SIMLENGTH=\`echo \$(grep nsteps *-1.mdp | cut -d \" \"  -f3-)\`
NSTEPS=SIMLENGTH/SEGLENGTH
if ( 9000 < NSTEPS) FIRSTLINES=1000 ; else FIRSTLINES=0
pr \"NSTEPS=\",NSTEPS
NBINS=min(NSTEPS,100.)" > $GNUPlotCurrentFileName

INSTANCE=`echo $(head -n 11 N.dat | tail -n 1)`

echo "INSTANCE=`echo $INSTANCE`
pr \"INSTANCE = \",INSTANCE
CURRENTFILENAME=\"current.txt\"
NOSPACESFILENAME=\"current-no-spaces.txt\"


pr \"Finding extremal values for histogram binning:\"

set term unknown # no output

FIT_LIMIT = 1e-4
# keep fitting residuals for error estimation
set fit errorvariables" >> $GNUPlotCurrentFileName

#do for [i=4:10:3]{
#  pr " Column $i"
#  p NOSPACESFILENAME u 2:i noti
#  Y_MIN_$i=GPVAL_Y_MIN
#  Y_MAX_$i=GPVAL_Y_MAX
#}

i0=4 # 4=x, 5=y, 6=z
imax=$i0+2*$DIM
for i in {$i0..$imax..$DIM} ; do

case "$i" in 
	4) echo "pr \"Column $i : Equipartition\"" >> $GNUPlotCurrentFileName;;
	7) echo "pr \"Column $i : Vertex\"" >> $GNUPlotCurrentFileName;;
	10) echo "pr \"Column $i : Dynamic\"" >> $GNUPlotCurrentFileName;;
	[?]) echo "pr \"Column $i : Unknown partition scheme\"" >> $GNUPlotCurrentFileName;;
esac

echo "
p NOSPACESFILENAME u 2:$i noti
Y_MIN_$i=GPVAL_Y_MIN
Y_MAX_$i=GPVAL_Y_MAX
Y_AVG_$i=1e-10
fit Y_AVG_$i  NOSPACESFILENAME every ::FIRSTLINES u 2:$i via Y_AVG_$i
fit_title_$i=sprintf(\"N(%.3g,%.3g)\",Y_AVG_$i,Y_AVG_$i"_err")
#Pareto fitting:
AL=0
if (Y_AVG_$i"_err" != 0) ; AL=(1-(Y_AVG_$i**2)/(Y_AVG_$i"_err"**2))**(0.5)
XMP=AL*Y_AVG_$i/(1+AL)
XMN=-1.0*(AL+AL*AL)*Y_AVG_$i
paretoP(x)=abs((1+AL)*(XMP**(1+AL))/(x**(2+AL)))
paretoN(x)=abs((1-AL)*(XMN**(1-AL))/(x**(2-AL)))
" >> $GNUPlotCurrentFileName

done


echo "if (OUTPUTATOMS!=2) pr \"Finding extremal values for color pallete:\" ; pr \" Column "$[$i0+3*$DIM]" -- Local heat current, equipartitioned scheme\" ; splot NOSPACESFILENAME u 2:3:"$[$i0+3*$DIM]" noti ; pr \" (       empty   range for diatomic molecule    )\" ; Z_MIN_"$[$i0+3*$DIM]"=GPVAL_Z_MIN ; Z_MAX_"$[$i0+3*$DIM]"=GPVAL_Z_MAX #if statement only executes commands on the same line...
Z_"$[$i0+3*$DIM]"_SGN=0 ; if  (Z_MIN_"$[$i0+3*$DIM]" > 0) Z_"$[$i0+3*$DIM]"_SGN=1 ; else if (Z_MIN_"$[$i0+3*$DIM]" < 0) Z_"$[$i0+3*$DIM]"_SGN=-1 ; else Z_"$[$i0+3*$DIM]"_SGN=0


pr \"Starting output to file:\"

" >> $GNUPlotCurrentFileName
OUTPUTFILENAME="current-$INSTANCE.sh"
echo "
if (1000 < NSTEPS) set term png size 1000,1400 enhanced ; set output OUTPUTFILENAME.".png" ; else set term svg dynamic enhanced size 1000,1400 ; set output OUTPUTFILENAME.".svg" # uses the GNUPlot concatenation operator "."

set datafile commentschars "#%"

set title "Heat Current"

#step = $1 , t = $2 , j = $3

#set multip layout 2,2 rowsfirst
if (OUTPUTATOMS!=2) set multip layout 3,2 rowsfirst ; else set multip layout 2,2 rowsfirst


pr " Plotting Net Heat Current..."
set title "Net Heat Current"
set style data dots
set pointsize 0.1
set ylabel "Heat Current/[KJ/mole per ps per nm]"
set xlabel "Time/[ps]"
set xtics auto
set autoscale
#set key outside
set key below 

p\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:4 t "I_{eq}^{x}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:5 t "I_{eq}^{y}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:6 t "I_{eq}^{z}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:7 t "I_{vrtx}^{x}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:8 t "I_{vrtx}^{y}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:9 t "I_{vrtx}^{z}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:10 t "I_{dyn}^{x}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:11 t "I_{dyn}^{y}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:12 t "I_{dyn}^{z}(t)"


pr " Plotting Heat Current Histogram..."
set title "Time-averaged Heat Current Histogram"
#set style data boxes
set style data filledcurves
set style fill transparent solid 0.5 noborder
set ylabel "Relative Frequency"
set xlabel "Heat Current/[KJ/mole per ps per nm]"
set xtics rotate by 90 offset 0,-1 out
#set x2tics auto
set grid lw 0.1

BINWIDTH4=(Y_MAX_4-Y_MIN_4)/NBINS
BINWIDTH7=(Y_MAX_7-Y_MIN_7)/NBINS
BINWIDTH10=(Y_MAX_10-Y_MIN_10)/NBINS
BINWIDTH=max(BINWIDTH4,max(BINWIDTH7,BINWIDTH10))
set boxwidth BINWIDTH

yavg4labeltext = sprintf("Y_{AVG}^4 = %.3g\n{/Symbol D}Y_{AVG}^4 = %.3g",Y_AVG_4,Y_AVG_4_err)
yavg7labeltext = sprintf("Y_{AVG}^7 = %.3g\n{/Symbol D}Y_{AVG}^7 = %.3g",Y_AVG_7,Y_AVG_7_err)
set label 1 yavg4labeltext at graph 0.55, graph 0.85
set label 2 yavg7labeltext at graph 0.05, graph 0.85

p NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u (bin($4,BINWIDTH)):(1.0/NSTEPS) smooth freq t "I_{eq}^{x}(t)",\
	NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u (bin($7,BINWIDTH)):(1.0/NSTEPS) smooth freq t "I_{vrtx}^{x}(t)",\
	NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u (bin($10,BINWIDTH)):(1.0/NSTEPS) smooth freq t "I_{dyn}^{x}(t)"

f(x,A,B,C) = C/((abs(x-B))**A)
f2(x,A,B,C) = C*exp(-abs(x-B)/A)
L(x,x0,g,C) = (1/PI)*C*g/((x-x0)**2+g**2)


#set parametric ; p NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u (1.0/NSTEPS),(bin($4,BINWIDTH4)) smooth freq


set xtics norotate nooffset in
#unset x2tics
unset grid
set xrange [*:*]
unset label 1 ; unset label 2


pr "Plotting Equipartitioned Net Heat Current..."
set title "Net Heat Current\nEquipartitioned localization scheme"
set style data dots
set xlabel "Time/[ps]"
set ylabel "Heat Current / [ (KJ/mole) nm per ps ]"
p\
	NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:4 t "I_{eq}^{x}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:5 t "I_{eq}^{y}(t)",\
        NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:6 t "I_{eq}^{z}(t)"


pr " Plotting Squared Net Heat Current..."
set title "Net Heat Current Squared\nComparison of Localization Schemes"
set style data dots
set xlabel "Time/[ps]"
set ylabel "Heat Current Squared / [(KJ/mole)^{2} s^{-12} nm^{-1}"
p\
       NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:($4*$4+$5*$5+$6*$6) t "I_{eq}^2(t)",\
       NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:($7*$7+$8*$8+$9*$9) t "I_{vrtx}^2(t)",\
       NOSPACESFILENAME every OUTPUTATOMS::FIRSTLINES u 2:($10*$10+$11*$11+$12*$12) t "I_{dyn}^2(t)"


pr " Plotting parameters list panel..."
unset xlabel ; unset ylabel
unset xtics ; unset ytics ; unset border
unset key
#set title 'This panel intetionally left empty'
set title 'Parameters:'

PARAMLIST = sprintf("\
CURRENTFILENAME=%s\n\
NOSPACESFILENAME=%s\n\
FIRSTLINES=%d\n\
OUTPUTATOMS=%d # every OUTPUTATOMS::FIRSTLINES\n\
NSTEPS=%d # for normalization of histogram relative frequencies,\n\
	    this should actually be NSTEPS/SEG-LENGTH,\n\
	    but current-$TRAJ.txt is printed out EACH STEP\n\
NBINS=min(NSTEPS,1000.)=%d # for histogram binning\
",\
CURRENTFILENAME,\
NOSPACESFILENAME,\
FIRSTLINES,\
OUTPUTATOMS,\
NSTEPS,\
NBINS)

set label 4 PARAMLIST at graph 0.0, graph 1.0
p [][0:1] 2
unset label 4
set xtics ; set ytics ; set border


pr " Plotting Heat Current as a function of Atomic Site and Time..."
set view map
#set style data image
set style data pm3d
set ylabel "Atom"

#DEV 24-4-12: set ytics according to outputatoms.txt

set xlabel "Time/[ps]"
set cblabel "Heat Current/[KJ/mole per ps per nm]"
set title "Heat Current as a function of Atomic Site and Time: I_{eq}^{z}(n,t)"
if (OUTPUTATOMS!=2) if (Z_13_SGN==0) set pal defined (Z_MIN_13 "blue" , 0 "white" , Z_MAX_13 "red") ; sp CURRENTFILENAME u 2:3:13 noti ; else set pal defined (Z_MIN_13 "blue" , Z_MAX_13 "red") ; sp CURRENTFILENAME u 2:3:13 noti



#set title "Heat Current as a function of Atomic Site and Time"
#sp\
        #NOSPACESFILENAME u 2:3:13 t "I(j=$3,equi,x)",\
        #NOSPACESFILENAME u 2:3:14 t "I(j=$3,equi,y)",\
        #NOSPACESFILENAME u 2:3:15 t "I(j=$3,equi,z)",\
        #NOSPACESFILENAME u 2:3:16 t "I(j=$3,vertex,x)",\
        #NOSPACESFILENAME u 2:3:17 t "I(j=$3,vertex,y)",\
        #NOSPACESFILENAME u 2:3:18 t "I(j=$3,vertex,z)",\
        #NOSPACESFILENAME u 2:3:19 t "I(j=$3,dyn,x)",\
        #NOSPACESFILENAME u 2:3:20 t "I(j=$3,dyn,y)",\
        #NOSPACESFILENAME u 2:3:21 t "I(j=$3,dyn,z)"

unset multip

#Current file columns:
#p NOSPACESFILENAME u 2:3:4,5,6 # j= acc , a=0
#p NOSPACESFILENAME u 2:3:7,8,9 # j= acc , a=1
#p NOSPACESFILENAME u 2:3:10,11,12 # j= acc , a=2
#p NOSPACESFILENAME u 2:3:13,14,15 # j= $3 , a=0
#p NOSPACESFILENAME u 2:3:16,17,18 # j= $3 , a=1
#p NOSPACESFILENAME u 2:3:19,20,21 # j= $3 , a=2

pr " End of Heat Current GNUPlot script."
pr ""

}

#begin main

echo
# if no current-*.gp file exists, generate one

GNUPlotCurrentFileName="GNUPlotCurrent-`date +%d-%m-%y`.gp"

# run the current-*.gp script

#exit
echo
echo "GNUPlotCurrent-*.sh exit successfully"
exit 0
