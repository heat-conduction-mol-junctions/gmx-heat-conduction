#! /bin/bash -e

echo "Setting up input arguments:"
nSplit=$1
if [ -z $nSplit ] 
	then 
		echo "No arguments entered: Enter nSplit. Aborting."
		exit 12
	else
		echo "nSplit=$nSplit"
fi

PARSE=$HOME/gmx-heat-conduction/bin/Parse
if [ $HOSTNAME == "hydrogen.tau.ac.il" ] ; then PARSE=$HOME/Documents/GROMACS/Parse ; fi
parser="$(ls -tr1 $PARSE/parse-*.out | tail -n1)"

natoms=$(head -n2 ../conf*.gro | tail -n1)
echo "natoms=$natoms"
lSulfur=$(echo $(grep S ../conf*.gro -n | head -n1 | cut -d ":" -f1) | awk '{print ($1 - 2)}')
echo "lSulfur=$lSulfur"
rSulfur=$(echo $(grep S ../conf*.gro -n | tail -n1 | cut -d ":" -f1) | awk '{print ($1 - 2)}')
echo "rSulfur=$rSulfur"

nsteps=$(gmxdump_d -s ../topol*.tpr -quiet | grep nsteps  | cut -d "=" -f2)
echo "nsteps=$nsteps"
dt=$(    gmxdump_d -s ../topol*.tpr -quiet | grep delta_t | cut -d "=" -f2) 
echo "dt=$dt ps"


echo "Determining times to split at:"
if [ -f SplitUp-$nSplit.txt ] ; then echo "$nSplit already exists! aborting..." ; exit 12 ; fi
tSplit=$(echo $nsteps $dt $nSplit | awk '{print ($1 * $2 / $3)}') 
if [ 1 -eq $(echo $tSplit | awk '{if (0 == $1 % 1) print 1 ; else print 0}') ] # check whether tSplit is an integer multiple of picoseconds
	then 
		echo "tSplit=$tSplit ps"
	else
		tSplit=$(echo $tSplit | awk '{print ($1 - $1 % 1)}') # effectively takes only integer part (rounds down)
		if [ 0 -eq $tSplit ] ; then echo "tSplit=$tSplit" ; echo "Split time must be a positive definite integer multiple of picoseconds. Aborting..." ; exit 38 ; fi
		nSplit=$(echo $nsteps $dt $tSplit | awk '{print (($1 * $2 / $3) - ($1 * $2 / $3) % 1)}') # redefines $nSplit to agree with the new, rounded value of tSplit
    echo "tSplit=$tSplit ps"
fi
touch SplitUp-$nSplit.txt
if [ -z $GMX_MAXBACKUP ]
	then
		#export GMX_MAXBACKUP=$[1 + $nSplit]
		export GMX_MAXBACKUP=-1
	else
		if [ $GMX_MAXBACKUP -le $nSplit ]
			then
				echo "GROMACS wont make more than GMX_MAXBACKUP=$GMX_MAXBACKUP backups, -1 disables them altogether."
				#export GMX_MAXBACKUP=$[1 + $nSplit]
		    export GMX_MAXBACKUP=-1
		fi
fi
echo "GMX_MAXBACKUP=$GMX_MAXBACKUP"
#echo "Please press any key to continue..." ; read



echo "Splitting traj.trr into traj-\$i.trr:"
trjconv_d -split $tSplit -f ../traj*.trr -o traj-.trr -quiet # the argument of the -split flag (i.e. $tSplit) must be an integer multiple of picoseconds
rm traj-$nSplit.trr # last file contains only one frame... discard it
#for i in $(seq 1 $nSplit) ; do mv traj-$[ $i - 1 ].trr traj-$i.trr ; done # the split trajectories start numbering at zero, not one -- rectified by this line

for i in $(seq 0 $[ $nSplit - 1 ] )
	do
	echo "Rerunning traj-$i.trr to create trajectory-$i.txt, forces-$i.txt, etc.:"
	echo "DBG 1-8-13: GMX_MAXBACKUP=$GMX_MAXBACKUP"
	mdrun_d -s ../topol*.tpr -rerun traj-$i.trr -quiet -nt 1
	wc -l forces.txt     		#DBG 31-7-13
  wc -l trajectory.txt 		#DBG 31-7-13
	mv trajectory.txt trajectory-$i.txt
	mv forces.txt forces-$i.txt
	rm ener.edr md.log DBG_Non_Markov_Mem.txt RanForMem.txt

	echo "Calculating power for trajectory-$i.txt, forces-$i.txt, etc.:"
	set +e # DBG 17:16
	wc -l forces-$i.txt			#DBG 31-7-13
  wc -l trajectory-$i.txt #DBG 31-7-13
	$parser trajectory-$i.txt forces-$i.txt output-$i.txt $(echo $tSplit $dt | awk '{print ($1 / $2 - 1)}') # DEV: could be generalized for all versions of parse-*.out
	if [ 0 -ne $? ] ; then echo "Error in parsing split trajectory (../parse-*.out trajectory-$i.txt forces-$i.txt output-$i.txt $(echo $tSplit $dt | awk '{print ($1 / $2 - 1)}')). Aborting." ; exit 15 ; fi
	set -e # DBG 17:16
  rm TermPairwiseCurrent.txt RanFor.txt outputatoms.txt hroutput-$i.txt gmon.out current.txt currenthr.txt atomsmasses.txt

	echo "Parsing BondPower.txt:"
	#mv BondPower.txt BondPower-$i.txt
	P_l=$(head -n $lSulfur BondPower.txt | tail -n1 | cut -d " " -f $[ 2 * $lSulfur ] )
  P_r=$(head -n $rSulfur BondPower.txt | tail -n1 | cut -d " " -f $[ 2 * $rSulfur ] )
	P_avg=$(echo $P_l $P_r | awk '{print (($1 - $2) / 2)}')
	echo "$i $P_l $P_r $P_avg" >> SplitUp-$nSplit.txt

	echo "Results after $[ 1 + $i ] averages:"
	cat SplitUp-$nSplit.txt

done

#cp BondPower-0.txt EnsembleAveragedBondPower-0.dat
#for i in $(seq 1 $[ $nSplit - 1 ] )
#	do
#	../EnsembleAveraging-15-7-13.sh BondPower-$i.txt EnsembleAveragedBondPower-$i.dat $i EnsembleAveragedBondPower-$[ 1 + $i ].dat $natoms 0 $natoms
#done

#gnuplot -e "fit A$nSplit 'SplitUp-$nSplit.txt' u 1:4 via A$nSplit ; pr A$nSplit,FIT_STDFIT"
i=0
gnuplot -p -e "fit A$nSplit 'SplitUp-$nSplit.txt' u 1:4 via A$nSplit ; pr A$nSplit,FIT_STDFIT ; p 'SplitUp-$nSplit.txt' u 1:4 pt 7 ps 4/1.1**$i lc $i noti, A$nSplit lt 0 lc $i noti"

exit 0
