#! /bin/bash

#GNUPlot script generation subroutine
function mkgp(){

#if [ 2 -ge $natoms ] ; then echo -e "\n\nWarning! natoms = $natoms . Graphical output is only meaningful for natoms>2.\n\n" ; fi

echo "print \"	Top of GNUPlot script.\"
set term svg size 1000,1400 dynamic enhanced
set output \"mixed-7-8-11.svg\"

#set term jpeg size 1200,900 enhanced
#set output \"mixed-7-8-11.jpeg\"

set view map
set pal defined (-1 \"blue\", 0 \"black\", 1 \"red\")
set style data image

FIRSTLINES=0

print \"\" ; print \"	GNUPlot setting MultiPlot...\"
#set multiplot title \"Energy Continuity\\n(dynamic localization of potential energies)\"
#set multiplot layout 2,3
set multiplot layout 3,2 rowsfirst

set xlabel \"n\"
print \"	GNUPlot setting x tics...\" " > $gpscriptname

j=1
echo -n "set xtics (" >> $gpscriptname
for i in ${outputatoms[@]}
do
	echo -n "$i" >> $gpscriptname
	if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> $gpscriptname ; fi
	j=$[ $j + 1 ]
done
echo  ")" >> $gpscriptname # add x-axis ticks only for atoms in outputatoms

#echo "set xtics 1 		# x axis tics every 1 (integers only)

echo "set ylabel \"t/[ps]\"
#set ytics 0,0.02	# y axis begins at zero with major tics every 2 femtoseconds
set mytics 2 		# y axis minor tic every 1/2 of a major tic

print \"	GNUPlot plotting P(n,t)...\"
set title \"Power as a function of Atomic Site and Time\"
set cblabel \"P/[W]\"
#set cbrange [-1e-17:1e-17]
set zlabel rotate
#set zlabel \"P/[W]\"
set autoscale
set zrange [] writeback
set cbrange [] writeback
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:7 noti # t \"Power as a function of Atomic Site and Time\"

print \"	GNUPlot plotting DeltaE(n,t)...\"
set title \"Local energy change during a time-step\nas a function of Atomic Site and Time\"
#set cblabel \"DeltaE/[W]\"
#set zlabel \"DeltaE/[W]\"
set zrange restore
set cbrange restore
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:6 noti # t \"Energy Difference (per time-step) \\n as a function of Atomic Site and Time\"

print \"\" ; print \"	GNUPlot fitting and plotting P_n(t)...\"
set title \"Power as a function of Atomic Site and Time\"
set style data points
set pointsize 0.1
set ylabel \"P/[KJ/mole per ps]\"
set xlabel \"Time/[ps]\"
set xtics auto
#set xtics 0.02
set autoscale
#set key below
set key outside
#set key box
#set key samplen 100

print \"	GNUPlot looping over output atoms...\"

FIT_LIMIT = 1e-5
# keep fitting residuals for error estimation
set fit errorvariables
DT=$dt
DELAY=25
ONSET=DELAY*DT" >> $gpscriptname

if [ ! -f $STEADY_STATEname.txt ] ; then touch "$STEADY_STATEname.txt" ; fi

echo "#loop \"i\" over natoms: \"natoms::(i+FIRSTLINES)\""

#loop "i" over natoms: "natoms::(i+FIRSTLINES)"
#i=0
j=1 # the index "j" is used to identify the last output atom, which is important for the formatting of the plot command
#while [ $i -le $[$natoms-1] ]
for i in "${outputatoms[@]}"
do
	echo "entry # " $j " atom # " $i # to stdout
	echo "print \"\";print \"\";print \"              GNUPlot fitting atom #$i...\"" >> $gpscriptname
	echo "# Restrict a to the range of [LOW:HIGH] using arcus tangent to span the entire dummy-variable \"x\" axis
	LOW1=0.001
	HIGH1=10
	RATE_1_$i(x) = (HIGH1-LOW1)/pi*(atan(x)+pi/2)+LOW1
	LOW2=1
	HIGH2=50
	RATE_2_$i(x) = (HIGH2-LOW2)/pi*(atan(x)+pi/2)+LOW2
	#STEADY_STATE_1_$i=10
	#STEADY_STATE_2_$i=-20
	LOW1S=-500
        HIGH1S=100
        STEADY_STATE_1_$i(x)= (HIGH1S-LOW1S)/pi*(atan(x)+pi/2)+LOW1S
	LOW2S=-500
        HIGH2S=100
        STEADY_STATE_2_$i(x)= (HIGH2S-LOW2S)/pi*(atan(x)+pi/2)+LOW2S

	#13-11-12
	LOW0=0.001
	HIGH0=10
	RATE_0_$i(x)= (HIGH0-LOW0)/pi*(atan(x)+pi/2)+LOW0
	LOW0S=0.001
	HIGH0S=10
	STEADY_STATE_0_$i(x)= (HIGH0S-LOW0S)/pi*(atan(x)+pi/2)+LOW0S

	#14-11-12
	#DUMB_2_$i=1e4
        set zrange [-1e5:1e5] # [-500:50]

	#logistics + 2 exp
	#f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)*(1-exp(-x/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_2_$i(DUMB_2_$i)))+STEADY_STATE_0_$i(DUMMY_0_$i)/(1+exp(-x/RATE_0_$i(DUMB_0_$i)))
        #fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] via DUMMY_0_$i,DUMB_0_$i,DUMMY_1_$i,DUMB_1_$i,DUMMY_2_$i,DUMB_2_$i

	#logistics
	#f(x) = STEADY_STATE_0_$i(DUMMY_0_$i)/(1+exp(-x/RATE_0_$i(DUMB_0_$i)))
        #fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ]:(1) via DUMMY_0_$i,DUMB_0_$i

	#2 exp*logistics
	#f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)*exp(-x/RATE_0_$i(DUMB_0_$i))/(1+exp(-x/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_0_$i(DUMB_0_$i)))/(1+exp(-x/RATE_2_$i(DUMB_2_$i)))
        #fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ]:(1) via DUMB_0_$i,DUMMY_1_$i,DUMB_1_$i,DUMMY_2_$i,DUMB_2_$i

	
	#logistics+exp
	#f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)/(1+exp((DUMMY_0_$i-x)/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp((DUMB_0_$i-x)/RATE_2_$i(DUMB_2_$i)))
        #fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ]:(1) via DUMMY_0_$i,DUMB_0_$i,DUMMY_1_$i,DUMB_1_$i,DUMMY_2_$i,DUMB_2_$i


	#2exp
	f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)*exp(x/RATE_1_$i(DUMB_1_$i))*exp(-x/RATE_0_$i(DUMB_0_$i))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_2_$i(DUMB_2_$i)))*(1-exp(-x/RATE_0_$i(DUMB_0_$i)))
        fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] via DUMB_0_$i,DUMMY_1_$i,DUMB_1_$i,DUMMY_2_$i,DUMB_2_$i

	#exp+exp*gauss
	#f(x) = STEADY_STATE_1_$i(DUMMY_1_$i)*(2-exp((-x-x*x)/RATE_1_$i(DUMB_1_$i))-exp(-x/RATE_2_$i(DUMB_2_$i)))
        #fit f(x) \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] via DUMMY_1_$i,DUMB_1_$i,DUMB_2_$i

	print \"\" ; print \"debug:\",f(1) ; print \"\"

	#print \"\"
	#print\"		0.001-10		1-50		10-1000\"
	#print DUMB_1_$i,RATE_1_$i(DUMB_1_$i),DUMB_2_$i,RATE_2_$i(DUMB_2_$i)
	#print \"\"" >> $gpscriptname

	#RATE_1_$i\_err=(HIGH1-LOW1)/pi* d atan(x) / dx * DUMB_1_$i\_err
	#RATE_2_$i\_err=((HIGH2-LOW2)/pi)*DUMB_2_$i\_err/(1+DUMB_2_$i*DUMB_2_$i)
	echo >> $gpscriptname
	echo -n RATE_1_$i\_err >> $gpscriptname
	echo -n "=((HIGH1-LOW1)/pi)*" >> $gpscriptname
	echo -n DUMB_1_$i\_err >> $gpscriptname
	echo -n "/(1+DUMB_1_$i*DUMB_1_$i)" >> $gpscriptname
	echo >> $gpscriptname
	echo -n RATE_2_$i\_err >> $gpscriptname
	echo -n "=((HIGH2-LOW2)/pi)*" >> $gpscriptname
	echo -n DUMB_2_$i\_err >> $gpscriptname
	echo -n "/(1+DUMB_2_$i*DUMB_2_$i)" >> $gpscriptname	
	echo >> $gpscriptname

	echo -n STEADY_STATE_1_$i\_err >> $gpscriptname
        echo -n "=((HIGH1S-LOW1S)/pi)*" >> $gpscriptname
        echo -n DUMMY_1_$i\_err >> $gpscriptname
        echo -n "/(1+DUMMY_1_$i*DUMMY_1_$i)" >> $gpscriptname
        echo >> $gpscriptname
####        echo -n STEADY_STATE_2_$i\_err >> $gpscriptname
####        echo -n "=((HIGH2S-LOW2S)/pi)*" >> $gpscriptname
####        echo -n DUMMY_2_$i\_err >> $gpscriptname
####        echo -n "/(1+DUMMY_2_$i*DUMMY_2_$i)" >> $gpscriptname
        echo >> $gpscriptname

echo "set print \"$STEADY_STATEname.txt\"" >> $gpscriptname
echo -n "print \"$i \"" >> $gpscriptname
	n=1
####	while [ $N_EXPONENTIALS -ge $n ] ; do
####        	echo -n "," >> $gpscriptname
####	        echo -n STEADY_STATE_$n\_$i >> $gpscriptname
####		echo -n "(" >> $gpscriptname
####                echo -n DUMMY_$n\_$i >> $gpscriptname
####                echo -n ")" >> $gpscriptname
####	        echo -n ",\" \"," >> $gpscriptname
####        	echo -n STEADY_STATE_$n\_$i\_err >> $gpscriptname
####	        echo -n ",\" \"," >> $gpscriptname
####	        echo -n RATE_$n\_$i >> $gpscriptname
####	        echo -n "(" >> $gpscriptname
####	        echo -n DUMB_$n\_$i >> $gpscriptname
####	        echo -n ")" >> $gpscriptname
####	        echo -n ",\" \"," >> $gpscriptname
####	        echo -n RATE_$n\_$i\_err >> $gpscriptname
####	        echo -n ",\" \"" >> $gpscriptname
####	        n=$[ $n + 1 ]
####	done
echo
echo -e "\nunset print">> $gpscriptname
	
#	echo -n "\"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] t \"$i\" , STEADY_STATE_$i w lines lw 0.1 t \"fit $i\"" >> $gpscriptname

#print using loop over n in N_EXPONENTIALS

#echo -n "\"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] t \"$i\" , STEADY_STATE_1_$i(DUMMY_1_$i)*(1-exp(-x/RATE_1_$i(DUMB_1_$i)))+STEADY_STATE_2_$i(DUMMY_2_$i)*(1-exp(-x/RATE_2_$i(DUMB_2_$i)))  w lines lw 0.1 t \"fit $i\"" >> $gpscriptname
echo "print \"\" ; print \"debug:\",f(1) ; print \"\"" >> $gpscriptname
echo "plot \"hroutput$SegLength.xvg\" u 1:$[ $j + 1 ] t \"$i\" , f(x)  w lines lw 0.1 t \"fit $i\"" >> $gpscriptname
#        echo -n "\"output.txt\" every ${#outputatoms[@]}::($j-1+FIRSTLINES) u 2:7:(0) t \"$i\", STEADY_STATE_$i w l t \"fit $i\"" >> $gpscriptname
	
#For all but the last atom, add a comma and an escaped (\) escape character (\) after each output atom for GNUPlot to understand that the newlines should be treated as one long line (one long plot command)
#        if [ $i -le $[$natoms-2] ]
#        if [ ${#outputatoms[@]} -gt $j ]
#                then echo " ,\\" >> $gpscriptname # add \\ and newline character
#	fi
#        i=$[ $i + 1 ]
        j=$[ $j + 1 ]

done
echo >> $gpscriptname


j=1
echo -n "set xtics (" >> $gpscriptname
for i in ${outputatoms[@]}
do
        echo -n "$i" >> $gpscriptname
        if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> $gpscriptname ; fi
        j=$[ $j + 1 ]
done
echo  ")" >> $gpscriptname # add x-axis ticks only for atoms in outputatoms


#set xtics 1             # x axis tics every 1 (integers only)


echo "unset title
unset key

set style data image

print \"	GNUPlot plotting [P-DeltaE](n,t)...\"
set title \"Energy Discontinuity\\n as a function of Atomic Site and Time (discontinuities in white)\"
set pal defined (-2.01 \"white\", -2 \"magenta\", -1 \"purple\", 0 \"black\", 1 \"green\", 2 \"yellow\", 2.01 \"white\")
set cblabel \"[W]\"
set autoscale
#set cbrange restore
#set zrange restore
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:(\$7-\$6) noti # t \"Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"


print \"	GNUPlot plotting T(n,t)...\"
set title \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
set view map
set style data image
set xlabel \"n\" " >> $gpscriptname

echo "set ylabel \"t/[ps]\"
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic
set pal defined (0  \"blue\", 1 \"red\")
set cblabel \"Temperature/[K]\"
set autoscale
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:9 noti # t \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"

print \"	GNUPlot plotting [[P-DeltaE]/DeltaE](n,t)...\"
set title \"Relative Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"
set pal defined (-2.01 \"white\", -2 \"magenta\", -1 \"purple\", 0 \"black\", 1 \"green\", 2 \"yellow\", 2.01 \"white\")
set cblabel \"%\"
set autoscale
#set cbrange [-100:100]
set zlabel \"%\" norotate
set zrange [-10:10]
plot \"output.txt\" every ::FIRSTLINES u (\$3+0.5):2:8 noti # t \"Relative Error in Energy Continuity\\n as a function of Atomic Site and Time (discontinuities in white)\"


unset multiplot
print \"	GNUPlot finished.\"" >> $gpscriptname

}

#GNUPlotting steady-state fit from previous GP run
function mk_gp_steady_state_script(){
echo "#set term svg dynamic enhanced size 1000,1400
#set output \"$STEADY_STATEname.svg\"

set term epslatex size 8,11 standalone color
set output \"$STEADY_STATEname.tex\"

#set term postscript eps enhanced size 1000,1400
#set output \"$STEADY_STATEname.eps\"

#set term jpeg enhanced size 1200,850
#set output \"$STEADY_STATEname.jpeg\"

#KJ/mole per ps to Watt=J per second:
NA=6.0221416e+23
GMX2SI=1000*1e+12/NA

#Temperature difference:
DT=300

#set multiplot title \"Steady State Local Power, Conductance and Local Transient Rates with errorbars\"
#set multiplot layout 2,3 columnsfirst
set multiplot layout 3,2 rowsfirst

set key below" >> $STEADY_STATEgpscriptname

j=1
echo -n "set xtics (" >> $STEADY_STATEgpscriptname
for i in ${outputatoms[@]}
do
        echo -n "$i" >> $STEADY_STATEgpscriptname
        if [ ${#outputatoms[@]} -gt $j ] ; then echo -n "," >> $STEADY_STATEgpscriptname ; fi
        j=$[ $j + 1 ]
done
echo  ")" >> $STEADY_STATEgpscriptname # add x-axis ticks only for atoms in outputatoms

echo "
set title \"Steady State Local Power\"
set xlabel \"Atomic Site\"
set ylabel \"Power [W]\"
set yrange [] writeback
set style data points" >> $STEADY_STATEgpscriptname

echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI):($[ $i + 1 ]*GMX2SI) t \"\$P_$n(t=\\\infty)\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Steady State Local Power Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI):($[ $i + 1 ]*GMX2SI) t \"\$P_$n(t=\\\infty)\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Conductance\"
set xlabel \"Atomic Site\"
set ylabel \"Conductance [W per K]\"
set autoscale
set style data points
set yrange [] writeback" >> $STEADY_STATEgpscriptname

#plot \"$STEADY_STATEname.txt\" u 1:($2*GMX2SI/DT):($3*GMX2SI/DT) t "k1", "$STEADY_STATEname.txt" u 1:($6*GMX2SI/DT):($7*GMX2SI/DT) t "k2", "$STEADY_STATEname.txt" u 1:($10*GMX2SI/DT):($11*GMX2SI/DT) t "k3", "$STEADY_STATEname.txt" u 1:(($2+$3+$4)*GMX2SI/DT):($3*GMX2SI/DT) t "ktot"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI/DT):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\kappa_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Conductance Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

#plot \"$STEADY_STATEname.txt\" u 1:($2*GMX2SI/DT):($3*GMX2SI/DT) t \"k1\", "$STEADY_STATEname.txt" u 1:($6*GMX2SI/DT):($7*GMX2SI/DT) t "k2", "$STEADY_STATEname.txt" u 1:($10*GMX2SI/DT):($11*GMX2SI/DT) t "k3", "$STEADY_STATEname.txt" u 1:(($2+$3+$4)*GMX2SI/DT):($3*GMX2SI/DT) t "ktot"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=2+(n-1)*4
i=$[ 4 * $n - 2 ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i*GMX2SI/DT):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\kappa_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Transient Rates\"
set logscale y
set xlabel \"Atomic Site\"
set ylabel \"Relaxation Lifetime [ps]\"
set autoscale
set yrange [] writeback
set style data points" >> $STEADY_STATEgpscriptname

#plot "$STEADY_STATEname.txt" u 1:4:5 t "tau1", "$STEADY_STATEname.txt" u 1:8:9 t "tau2", "$STEADY_STATEname.txt" u 1:12:13 t "tau3"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=4+(n-1)*4
i=$[4 * $n ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\tau_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
set title \"Local Transient Rates Variance\"
set style data errorbars
set yrange restore" >> $STEADY_STATEgpscriptname

#plot "$STEADY_STATEname.txt" u 1:4:5 t "tau1", "$STEADY_STATEname.txt" u 1:8:9 t "tau2", "$STEADY_STATEname.txt" u 1:12:13 t "tau3"
echo -n "plot " >> $STEADY_STATEgpscriptname
n=1 ; while [ $N_EXPONENTIALS -ge $n ] ; do
#i=4+(n-1)*4
i=$[4 * $n ]
echo -n "\"$STEADY_STATEname.txt\" u 1:($i):($[ $i + 1 ]*GMX2SI/DT) t \"\$\\\tau_$n\$\"" >> $STEADY_STATEgpscriptname
if [ $N_EXPONENTIALS -gt $n ] ; then echo -n ", " >> $STEADY_STATEgpscriptname ; fi
n=$[ $n + 1]
done

echo "
unset multiplot" >> $STEADY_STATEgpscriptname
}

#GNUPlotting steady-state fit from previous GP run
function gp_steady_state(){

#A4 = 148 mm × 210 mm
#A4 after margins = 145 mm × 200 mm 
#DPI = 300 dots per inch = 118.11023622 dots per cm
#--------------------------
#A4 = 17480.31496063 dots × 24803.1496062 dots = 17480 dots × 24803 dots
#A4 after margins = 17125.984251969 dots × 23622.047244094 dots = 17126 dots × 23622 dots = 404.55 Mdots = 5709 px  × 7874 px = 
# prop. to = 1000 px × 1379 px


STEADY_STATEgpscriptname=$STEADY_STATEname.gp
if [ ! -f $STEADY_STATEgpscriptname ] ; then mk_gp_steady_state_script ; if [ $? -ne 0 ] ; then echo -e "\nError creating steady-state fit GNUPlot script. exiting...\n" ; exit 1 ; fi ; fi
touch STEADY_STATE.aux
gnuplot $STEADY_STATEgpscriptname ; if [ 0 -ne $? ] ; then echo -e "\nError running Steady State GNUPlot script. Exiting.\n" ; exit 3 ; fi

latex -halt-on-error STEADY_STATE.tex ; if [ 0 -ne $? ] ; then echo -e "\nError compiling PDF from TeX (LaTeX). Exiting.\n" ; exit 3 ; fi
#pdflatex -halt-on-error STEADY_STATE.tex ; if [ 0 -ne $? ] ; then echo -e "\nError compiling PDF from TeX (LaTeX). Exiting.\n" ; exit 3 ; fi

dvipdf STEADY_STATE.dvi ; if [ 0 -ne $? ] ; then echo -e "\nError converting DVI to PDF. Exiting.\n" ; exit 3 ; fi
#; acroread STEADY_STATE.pdf &
if [ $? -ne 0 ] ; then echo -e "\nError running steady-state fit GNUPlot script. exiting...\n" ; exit 1 ; fi
#rm $STEADY_STATEgpscriptname
}

#Video encoding GNUPlot scripts
function vidscripts(){
echo -n "#set term gif size 200,200 dynamic enhanced
#set term gif animate transparent opt delay 10 size 200,200 x000000
set term gif enhanced animate delay 10 # delay is given in centiseconds
set output \"image-12-4-11.gif\"

#set initial values, increment size, formatting, etc.
reset
#set title \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
set view map
set style data image
set xlabel \"n\"
set xtics 1
#set ytics 0,0.02       # y axis begins at zero with major tics every 2 femtoseconds
set mytics 2            # y axis minor tic every 1/2 of a major tic
set pal defined (0  \"blue\", 1 \"red\")
set cblabel \"Temperature/[K]\"
set autoscale
counter=1
limit_loop_calls= " > animation-$gpscriptname
echo -n "$SimDuration" >>animation-$gpscriptname
echo -n " # set this to an integer product of simulated time-segments

#consider putting multiplot here somehow...
#plot \"output.txt\" every :counter u (\$3+0.5):2:7 t \"Heat Current\\nas a function of Atomic Site and Time\"
plot \"output.txt\" every :counter u (\$3+0.5):2:9 t \"Temperature Profile (from Kinetic Energy)\\n as a function of Atomic Site and Time\"
load \"animation-loop-12-4-11.gp\"

" >> animation-$gpscriptname

echo -n "#This file is the loopand.
#
#First advance the counter
#Repeat the loop if there isn't an exit condition or if there is one, but it hasn't yet been reached
#       Set up this step's plot
#       Replot
#       Increment whatever needs to be (for the next plot)
#       Re-read this file

counter=counter+1
if ((!limit_loop_calls) || (counter<=limit_loop_calls)) \\
  unset label 1; \\
  print \"		Animating frame\" counter \" out of\" limit_loop_calls \" ...\"; \\
  set label 1 \"Time = %.0f\",counter at graph 0.7,0.9 left tc rgbcolor \"white\" front; \\
  replot; \\
  reread
" > animation-loop-12-4-11.gp
}


#Video encoding subroutine
function vid(){
if [ ! -f animation-$gpscriptname ] || [ ! -f animation-loop-12-4-11.gp ] ; then echo -e "\nMissing video encoding gnuplot scripts. exiting.\n" ; exit 1 ; fi
gnuplot animation-$gpscriptname
FileName="image-12-4-11"
if [ -f $FileName.avi ] ; then echo -e "\nOutput file "$FileName.avi" already exists! ffmpeg will overwrite it... (-y flag). Exiting.\n" ; exit 1 ; fi
if [ -d Frames ] ; then rm -r Frames ; fi # if Frames from a previous encoding exist, remove them
mkdir Frames # create temporary directory in which to store the frames
convert $FileName.gif Frames\frame%07d.jpg # to prevent problems in encoding, first convert each frame in the *.gif file to a *.jpg image
if [ $? -ne 0 ] ; then echo -e "\nError in conversion of *.gif frames to *.jpg files. Exiting\n" ; exit 2 ; fi
ffmpeg -r 24 -i Frames\frame%07d.jpg -y -an $FileName.avi # framerate = 24 fps (-r 24), automatic overwrite of previous output authorized (-y), audio not recorded (-an)
if [ $? -ne 0 ] ; then echo -e "\nError in encoding of *.jpg frames to *.avi file. Exiting\n" ; exit 3 ; fi
rm Frames\frame*.jpg # remove *.jpg files (frames)
rmdir Frames
rm $FileName.gif
rm animation-$gpscriptname animation-loop-12-4-11.gp
}

#main

	n=1
	while read GMXSimParam
	do
		echo $GMXSimParam
		if [ $n -eq 1 ] ; then MAX_TRAJ=$GMXSimParam ; fi
                if [ $n -eq 2 ] ; then dt=$GMXSimParam ; fi
                if [ $n -eq 3 ] ; then SegLength=$GMXSimParam ; fi
                if [ $n -eq 4 ] ; then N_EXPONENTIALS=$GMXSimParam ; fi
                if [ $n -eq 5 ] ; then bDO_EOG=$GMXSimParam ; fi
                if [ $n -eq 6 ] ; then bINKSCAPE=$GMXSimParam ; fi
                if [ $n -eq 7 ] ; then bDO_VID=$GMXSimParam ; fi
                if [ $n -eq 8 ] ; then STEADY_STATEname=$GMXSimParam ; fi
                if [ $n -eq 9 ] ; then INSTANCE=$GMXSimParam ; fi
		n=$[ 1 + $n ]
	done < N.dat

#	read -a GMXSimParams -d \EOF < N.dat
#	#echo "${GMXSimParams[@]}"
#	n=1
#	for i in ${GMXSimParams[@]}
#	do
#		echo $i
#		if [ $n -eq 1 ] ; then MAX_TRAJ=$i ; fi
#		if [ $n -eq 2 ] ; then dt=$i ; fi
#		if [ $n -eq 3 ]	; then SegLength=$i ; fi
#		if [ $n -eq 4 ] ; then N_EXPONENTIALS=$i ; fi
#		if [ $n -eq 5 ] ; then bDO_EOG=$i ; fi
#		if [ $n -eq 6 ] ; then bINKSCAPE=$i ; fi
#		if [ $n -eq 7 ] ; then bDO_VID=$i ; fi
#		if [ $n -eq 8 ] ; then STEADY_STATEname=$i ; fi
#		if [ $n -eq 9 ] ; then INSTANCE=$i ; fi
#		n=$[ 1 + $n ]
#	done

        #echo "MAX_TRAJ - dt - SegLength - N_EXPONENTIALS - bDO_EOG  - bINKSCAPE - bDO_VID - INSTANCE"
        #echo "$MAX_TRAJ - $dt - $SegLength - $N_EXPONENTIALS - $bDO_EOG  - $bINKSCAPE - $bDO_VID - $INSTANCE"
	echo -e "MAX_TRAJ=$MAX_TRAJ\ndt=$dt\nSegLength=$SegLength\nN_EXPONENTIALS=$N_EXPONENTIALS\nbDO_EOG=$bDO_EOG\nbINKSCAPE=$bINKSCAPE\nbDO_VID=$bDO_VID\nSTEADY_STATEname=$STEADY_STATEname\nINSTANCE=$INSTANCE"
	if ( [ -z $MAX_TRAJ ] || [ -z $dt ] || [ -z $SegLength ] || [ -z $N_EXPONENTIALS ] || [ -z $bDO_EOG ] || [ -z $bINKSCAPE ] || [ -z $bDO_VID ] || [ -z $INSTANCE ] ) ; then echo -e "\nRequired environment variable not defined (see above). Exiting.\n" ; exit 4 ; fi
	# bDO_EOG bINKSCAPE
	

	if [ -z $dt ] ; then echo -e "\nUndefined Molecular Dynamics time-step duration. Exiting.\n" ; exit 2 ; fi
        if [ -z $N_EXPONENTIALS ] ; then echo -e "\nUndefined number of exponential decay terms in fitting function. Exiting.\n" ; exit 2 ; fi

	OLDIFS=$IFS
	IFS=" "
        IN="$( cat outputatoms.txt)"
        set -- "$IN"
        declare -a outputatoms=($*)
        IFS=$OLDIFS
	echo "#{ " ${outputatoms[@]} " } = " ${#outputatoms[@]}


	rm *.gp
	#rm $STEADY_STATEgpscriptname
	numgpscripts=0
	if [ -f *.gp ] ; then for gpscriptname in *.gp ; do numgpscripts=$[ 1 + $numgpscripts ] ; done ; fi
	#gpscriptname="$gpscriptname" ; rm $gpscriptname ;  echo -e "\nNo GNUPlot script files. Generating...\n"; mkgp ; if [ "$?" != "0" ] ; then echo "Error in generation of GNUPlot script file. exiting."; exit 1 ; fi
	if [ 0 -eq $numgpscripts ] ; then echo -e "\nNo GNUPlot script files. Generating...\n"; gpscriptname="15-12-11.gp" ; mkgp ; if [ "$?" != "0" ] ; then echo "Error in generation of GNUPlot script file. exiting."; exit 1 ; fi ; fi
	if [ 1 -lt $numgpscripts ] ; then echo -e "\nMultiple GNUPlot script files -- keep only the one you want to use. exiting...\n"; exit 1 ; fi


	#echo -e "\nRunning GNUPlot...\n" ; gnuplot $gpscriptname &>$INSTANCE.gpl ; if [ "$?" != "0" ] ; then echo "Error in GNUPlotting. exiting."; exit 1; fi;
        echo -e "\nRunning GNUPlot...\n" ; gnuplot $gpscriptname ; if [ "$?" != "0" ] ; then echo "Error in GNUPlotting. exiting."; exit 1; fi;


	#cat hroutput$SegLength.dat
	#rm *.dat *.txt
#	if [ 100000 -le $SimDuration ] ; then rm trajectory.txt forces.txt ; fi

	if [ 1 -eq $bDO_EOG ] ; then echo ; echo "Running EyeOfGNOME... (close EOG to continue)" ; echo ; (eog mixed-7-8-11.svg &) ; fi ;

	if [ 1 -eq $bINKSCAPE ] ; then echo -e "\nRunning Inkscape conversion from Scalable Vector Graphics format to raster format...\n" ; inkscape -z -f mixed-7-8-11.svg -e mixed-7-8-11.png -d 180 # lossy
	else echo -e "\nNo InkScape, running conversion from Scalable Vector Graphics format to raster format using ImageMagick...\n" ; convert mixed-7-8-11.svg mixed-7-8-11.png
	#else convert mixed-7-8-11.svg mixed-7-8-11.jpeg
	if [ "$?" != "0" ] ; then echo "Error in conversion from SVG to raster. exiting."; exit 1; fi;
	fi
	#inkscape -z -f mixed-21-4-11.svg -E mixed-21-4-11.eps #less lossy (note caps in option -E does matter -- not the same as -e)

	echo -e "\nPlotting steady-state values from fit...\n" ; gp_steady_state ; if [ "$?" != 0 ] ; then echo -e "Error plotting steady-state fit. exiting."; exit 1 ; fi
	#inkscape -z -f $STEADY_STATEname.svg -e $STEADY_STATEname.png -d 180 #lossy
	
	if [ 1 -eq $bDO_VID ] ; then echo -e "\nEncoding video...\n" ; vidscripts ; vid ; if [ "$?" != 0 ] ; then echo -e "Error encoding video. exiting."; exit 1 ; fi ; fi
