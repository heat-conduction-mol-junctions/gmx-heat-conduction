reset

#set term jpeg enhanced size 1000,707 # A4 on Sodium
#set term jpeg enhanced size 1200,850 # A4 at home
#set term jpeg enhanced size 1300,920 # Full screen at home
set term jpeg enhanced size 1260,891 # Full window at home
set output "EnergyConservation-23-10-12.jpeg"

outputFILE="output-1.txt"
SimDuration=`cut -d "=" -f2- options.dat | head -n1`
dt=`cut -d "=" -f2- options.dat | head -n2 | tail -n1`
nsteps=SimDuration*dt
SegSkip=nsteps*10


set fit errorvariables
fit DE1 outputFILE u 2:6 every 3:SegSkip:0:1 via DE1
fit DE2 outputFILE u 2:6 every 3:SegSkip:1:(SegSkip/2) via DE2
fit P1 outputFILE u 2:7 every 3:SegSkip:0:1 via P1
fit P2 outputFILE u 2:7 every 3:SegSkip:1:(SegSkip/2) via P2


set multiplot layout 2,2

p outputFILE u 2:6 every 3:SegSkip:0:1 t "DE_1", '' u 2:6 every 3:SegSkip:1:(SegSkip/2) t "DE_2", '' u 2:7 every 3:SegSkip:0:1 lc 1 ps 0.5 t "P_1", '' u 2:7 every 3:SegSkip:1:(SegSkip/2) lc 2 ps 0.5 t "P_2"

p [(0.9*nsteps):*] outputFILE u 2:6 every 3::0:1 t "DE_1", '' u 2:6 every 3::1:1 t "DE_2", '' u 2:7 every 3::0:1 lc 1 ps 0.5 t "P_1", '' u 2:7 every 3::1:1 lc 2 ps 0.5 t "P_2"

p [(nsteps-0.05):*] outputFILE u 2:6 every 3::0:1 t "DE_1", '' u 2:7 every 3::0:1 ps 0.2 t "P_1", '' u 2:($6-$7) every 3::0:1 t "DE_1-P_1", DE1 lc 1 t "<DE_1>", P1 lc 2 t "<P_1>"

p [(nsteps-0.05):*] outputFILE u 2:6 every 3::1:1 t "DE_2", '' u 2:7 every 3::1:1 ps 0.2 t "P_2", '' u 2:($6-$7) every 3::1:1 t "DE_2-P_2", DE2 lc 1 t "<DE_2>", P2 lc 2 t "<P_2>"

unset multiplot
