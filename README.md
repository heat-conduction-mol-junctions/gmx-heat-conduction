# gmx-heat-conduction
Calculation of heat conduction from [GROMACS](http://www.gromacs.org/)-simulated trajectories
# Synopsis
## Installation
### Alter GROMACS source
See below section on custom changes to GMX code.
### Make sure FFTW is installed (prerequisite)
If you don't have the required source files (including `fftw3.h`), download them from the FFTW website and build (`configure` -> `make` -> `make install`). Make sure to pass `CFLAGS="-fPCI"` to `configure`.
### Configure and install GROMACS
run `./src/GROMACS/gromacs-*/configure --help` to find out more about GROMACS installation configuration options.
change directory to the GROMACS source version of your choice, for example `cd src/GROMACS/gromacs-4.5.5`
run `../GROMACS_install_script.sh`

### Compile C programs
```bash
mkdir -p bin/Parse
cd bin/Parse
for i in $(ls ../../src/Parse/) ; do echo -e "\n$i" ; gcc -g3 -O3 ../../src/Parse/$i -o $(echo $i | cut -d'.' -f1).out -lm -Wall -Wextra ; done
```

## Usage
1. Obtain the required input files for the chosen molecular model (see Input section). You can either download a molecular model from a website (this can be either in native GROMACS format, or in another format which is convertible to the GROMACS format), or you can draw an arbitrary molecule (using such visual molecular editors as Avogadro).
    1. Download the appropriate files from a website, if available (make sure connectivity information is included)
        1. GROMACS native input from [HIC-Up](http://xray.bmc.uu.se/hicup/)
        2. `*.pdb` (Protein DataBase) from [NCBI PubChem](http://pubchem.ncbi.nlm.nih.gov/search/search.cgi) or [RCSB](http://www.rcsb.org/pdb/ligand/chemAdvSearch.do). Convert to GROMACS format using the `pdb2gmx` utility.
        3. OBGMX ​is a website which uses ​Open Babble to convert PDB files to GMX topologies, using the Universal Force-Field (UFF), an all-atom force field.
        4. SwissParam is a website which converts MOL2 or PDB files to GMX topologies, using the CHARMM Force-Field, an all-atom force field.
        5. [ParmEd (parameter editor) can convert](https://parmed.github.io/ParmEd/html/readwrite.html#writing-files-with-structure-save) between MOL2, PDB and many other formats, among which is GROMACS (topology and configuration), see second [example](https://github.com/ParmEd/ParmEd#examples).
    2. Draw the molecule using such software as [Avogadro](avogadro.cc), and export in `*.MOL2` format. Use the `TopolBuilder-*.sh` script to convert this file to GROMACS readable format.
2. Make sure all of the prerequisite conditions detailed at the end of the `TopolBuilder-*.sh` script are met. For example, ordering the atomic indexes is easy to do using an atomic coordinate editor, such as the one in Avogadro.
3. Create a directory per molecular model, and copy the required input files into it.
4. Execute `TheWorks-*.sh` in the directory to produce full output (simulate all of the `NVE`, `NVT`, and `NVT_1T_2` ensembles).
5. Alternatively, execute `MasterControl-*.sh` in the directory to produce a specific simulation (one of the above ensembles). Each invocation of `MasterControl-*.sh` is timestamped, and the timestamp converted to a base 36 (case insensitive alphanumeric) label, which we name the invocation **instance**.
## Input
The only required input are two GROMACS format files:

1. Initial molecular configuration: `conf.gro`
2. Molecular topology (force-field): `topol.top`

There are other parameters, which are set automatically, but can be overridden. Some can be overridden by passing a command-line argument, others by changing the hard-coded value. See the Advanced Usage Input section for details. Some examples are

1. Choice of integration algorithm
2. Simulated time and averaging
    1. Time-step duration (in picoseconds)
    2. Total simulated time (in time-steps)
    3. Time-domain coarse-graining segment length (in time-steps).
    4. Number of simulated trajectories (realizations).
3. MD parameters
    1. Restraining the molecule to the junction gap, center-of-mass motion (COMM) removal and periodic boundary conditions (PBC).
    2. Thermostat and bath properties
    3. The transient time after which sampling may begin (assumes all transients have died off by then).

* In the most basic case, we will use implicit baths and implicit bulk surfaces. Two atoms in the molecule will be coupled to each of the two baths, and the atom position will be restrained to some point in space by an external force term (position restraint). In this depiction, the bulk/bath is not represented by any (dummy) atom.
* Non-bonded electrostatic interactions (part of the molecular topology) will be neglected in the calculation of the heat current, as the specifics of their calculation in GROMACS is hardly accessible (assembly code).

1. [gromacs can be forced to use non assembly routines to do force calculations (easier to debug) by defining "NOASSEMBLYLOOPS=1" as environment variable (e.g. put "export NOASSEMBLYLOOPS=1" without the quotes in your /etc/profile).](http://www.gromacs.org/Developer_Zone/Programming_Guide/Programmer's_Guide#Debugging)
2. ["modifying nonbonded interactions" (2008)](https://mailman-1.sys.kth.se/pipermail/gromacs.org_gmx-developers/2008-October/002803.html)
3. [In Gromacs 4.6 the assembly and Fortran non-bonded kernels have been removed.](http://www.gromacs.org/Documentation/Acceleration_and_parallelization#CPU_acceleration.3a_SSE.2c_AVX.2c_etc)


## Job control:
In the following examples, `TheWorks-*.sh` is a placeholder for the executed script name (could also be `MasterControl-*.sh` or anything else).

* `[CTRL+Z] ; bg -` # To send current job to background
* `jobs ; fg %1` # To display jobs and then send the first one on the list to the foreground
* `./TheWorks-*.sh &>log.txt &`									# Redirects both STDOUT and STDERR streams to log file, and forks.
* `nohup ./TheWorks-*.sh > log.io 2> log.err < /dev/null &` # Also redirects STDIN to NULL device, and does not get the HUP (hangup
) signal if SSH is closed.
* `./TheWorks-*.sh > log.io 2> log.err < /dev/null & disown -h` # Remove job from active jobs table, and ignore SIGHUP. 'disown' is not
 shell-portable...

## Output
1. `data.svg`: Summary plot of all heat current and atomic temperatures for all instances, plot of all temperature profiles, and plot of all power and heat currents results.
2. `data-*.svg`: Summary of all simulated cold bath temperatures and bias temperatures simulated for a given instance of `MasterControl-*.sh`.

# Advanced usage manual
## Sub-components:

### GROMACS
#### grompp
GROMACS pre-processor, called by SingleTrajectory (see below). See gromacs documentation for details.
#### mdrun
MD runner, called by SingleTrajecotry (see below).
#### Custom changes to GMX code
All changes are to GROMACS source files in `src/GROMACS/gromacs-4.5.5/src`.
For the latest version of GROMACS, see the [GROMACS master branch Documentation Nightly build](http://jenkins.gromacs.org/job/Documentation_Nightly_master/javadoc/#documentation-for-developers).
##### MD output
1. **Atomic masses** written to ﬁle at ﬁrst time-step (in lieu of parsing the input topology ﬁles): `kernel/md.c +1010` (in GROMACS 5, this file is found at `programs/mdrun/md.cpp`, and looping over time-steps begins at line 680)
2. Full- (e.g. double-) precision **phase state** (positions and velocities) written to ﬁle every time-step. Due to this need, the feature has since been added to the ofﬁcial GROMACS release: `kernel/md.c +1209`
3. **Bonded forces** written to ﬁle in full precision, term-by-term and atom-by-atom, every time-step. Only the net force per atom is currently available via the normal GROMACS interface: `gmxlib/bondfree.c` (in GROMACS 5, this file is found at `gromacs/listed-forces/bonded.h`)
4. Bath-system **random forces** written to ﬁle in full precision, atom-by-atom, every time-step. This is currently unavailable via the normal GROMACS interface. `mdlib/update.c` (in GROMACS 5, this file is found at `gromacs/mdlib/update.h`)
##### MD algorithm
1.  Added option for **non-Markovian bath** random force, according to Ornstein-Uhlnbeck process with user-deﬁned time-constant (setting this constant to anything less than the time-step length will reduce back to the case of a Markovian bath): `mdlib/update.c`

### Computationally intensive programs (C)
#### Parsing GROMACS output ﬁles and calculation of heat current (`parse-*.c`)
1. Reads input from GROMACS custom output ﬁles after the MD run has ended.
2. Deﬁnes system constitution (atom masses, interactions, etc.) on ﬁrst time-step.
3. Calculates energetics and heat current (via pairwise power terms).
4. Prints output to ﬁle (energetics, temperature, and heat current, per atom, per time-step; pairwise power terms per time-step).
### Ensemble averaging of the various realizations of a given system (`EnsembleAveraging-*.c`)
### Cluster job automation scripts (BASH)
Non-interactive calculations were ﬁrst tested locally, on a desktop PC (`sodium.tau.ac.il`), then scaled up to run in **batches** on the `hydrogen.tau.ac.il` and `power.tau.ac.il` clusters. In all cases, the operating system used was Linux. The clusters are managed using the Adaptive Computing [TORQUE](http://docs.adaptivecomputing.com/torque/5-1-0/help.htm)/MAUI alternative to OpenPBS.

1. `TheWorks-*.sh`: Given a molecule (`conf.gro` and `topol.top`) and range of temperatures and temperature biases, breaks the various combinations of bath temperatures into calculation “instances” – a given molecule, a given (cold) bath temperature, and a given inter-bath temperature bias – and executes the following for each instance:
2. `LndrHeatCurrent-*.sh`: Given the molecular normal mode analysis, system-bath coupling strength, and Debye frequency, calculates the Landauer heat current, heat conduction, and differential heat conduction for the harmonic approximation of such a system. An internal option allows setting the functional form of bath relaxation time (e.g. instantaneous, exponential decay, etc.).
3. `MasterControl-*.sh`: Given time-step length, time-domain coarse-graining segment length, total simulated time, skipped time towards steady-state, etc., calculates various energetic and heat current properties of the MD ensemble describing such a system.
4. `SingleTrajectory-*.sh`: Calculates various energetic and heat current properties of a single realization of such an (NVT) ensemble. Uses high precision trajectory output and input (`*.trr`) to start production runs from previously reached steady-state phase-space coordinates.
5. `HowLongLeft-*.sh`: Given a calculation instance (optional), prints to screen the number of pending jobs and estimates time needed for the instance’s completion. Executes `EnsembleAveraging-*.sh` once all the instance jobs 
have terminated.
6. `EnsembleAveraging-*.sh`: Runs the aforementioned `EnsembleAveraging-*.c` and `Plot*.sh` (equilibrium or NESS variant, accordingly)
7. `Plot*.sh`: Calls gnuplot to visualize the ensemble averaged output.
8. `TopolBuilder-*.sh`: Automates conversion of common chemical ﬁle types (e.g. `*.MOL2` or `*.pdb`) to GROMACS native format: `*.gro` and `*.top`.

## Input
### Command-line arguments
The following parameters can optionally be passed to `MasterControl-*.sh`. To show the accepted syntax, execute 

```bash
MasterControl-*.sh -h
```

#### Integration algorithm
* Leap-frog
* Velocity-Verlet
* Average kinetic energy velocity-Verlet
* Stochastic dynamics (solves Langevin equation)
* Resource-efficient stochastic dynamics
#### Simulated time
* Time-step duration (in picoseconds)
* Total simulated time (in time-steps)
#### Time averaging
* Time-domain coarse-graining segment length (in time-steps).
#### Ensemble averaging
* Number of simulated trajectories (realizations).
### Hard-coded parameters
Used to generate MD parameters (`*.mdp`) input file.
#### Simulation step
Values are dependent on the simulation step:

1. Potential **Energy Minimization** (EM): To serve as a common starting configuration and for *Normal Mode Analysis*.
2. **Towards Steady-State** (TSS): Including the specific case of equilibrium steady-state (obeying detailed balance).
3. **At Steady-State** (AtStSt): For *production* runs (i.e. *time-averaging*) at equilibrium or non-equilibrium steady-state.

Each simulation step takes as input the output of the preceding simulation step. For instance, simulation towards steady-state at some temperatue `T` and temperature bias `dT + 1` will take as the starting configuration that of the preceding step, at temperature `T` and bias `dT`.

#### MD parameters
1. **Center-of-mass** motion (COMM) removal and periodic boundary conditions (PBC)
    1. Neither COMM nor PBC: Explicit constraints in the molecular topology (`topol.top`)
        * **Explicit bath**
        * **Anchor atoms**: Both introduce a high frequency artifact. Since we are dealing with a two-terminal junction, the axis which runs through the two anchoring points(the major system axis)serves as an axis of rotation of the entire molecular bridge as a rigid body (very low frequency).
            * **Position restraints** (harmonic spring with high force constant)
            * **Dummy atoms** with high mass
    2. **Removal of COMM** of rotational degrees of freedom (DOF) or of both rotational and translational DOF will cause lack of conservation in energy. This happens regardless of any periodicity (or lack of) in the system.
    3. Using **PBC** we will be able to replicate the junction unit cell in order to study inter-molecular interactions, provided correlations between molecules can be neglected.
2. **Thermostat**
    1. Number of baths: Currently supports zero (`NVE`), one (`NVT`), or two (`NVT_1T_2`)
    2. Thermostat type: Velocity rescaling, Berendsen, Nose-Hoover.
    3. **System-bath coupling** strength: Dissipation life-time (in picoseconds)
    4. **Temperature** of the cold bath, and **temperature bias**, in Kelvins.
#### Meta-simulation parameters
* **Transient time** skip: The time after which sampling may begin (assumes all transients have died off by then).

### Some internal variables

1. `QUEUE` [string]: Name of cluster queue to submit to.

2. Ensemble averaging:
    1. `isProduction` [boolean]: If non-zero, perform more extensive computation to improve Signal to Noise Ratio.
    2. `N_TRAJ` (TheWorks)  / `N` (MasterControl) [integer]: Number of trajectories/realizations in the simulated ensemble.

3. Time
    1. `MAXSTEPS` [integer]: Number of time-steps in each simulated trajectory.
    2. `DEFAULT_SEG_LENGTH` [integer]: Number of time-steps in temporal coarse-graining.
    3. `t0TSS` [float]: Minimal time required to reach steady-state, in picosecods. Should be significantly greater than any other time-scale.
    4. `DstepRealiz` [integer]: number of time-steps to skip between realization sampling.

4. Bath and system-bath modelling
    1. `tau_m` [float]: Bath memory lifetime in picoseconds. Zero for a Markovian bath.
    2. `gamma_L`, `gamma_R` [float]: System-bath coupling amplitude, in radians per picosecond.
    3. `Tcolds`, `dTs` [array of float]: Cold bath temperatures and temperature biases to calculate for.

### Environment parameters passed to `SingleTrajectory-*.sh`
1. Job control
    1. `INSTANCE` [string]: Unique identifier for this instance of calling `SingleTrajectory-*.sh`, generated in `MasterControl-*.sh` based on current datetime. All I/O will be saved to a sub-directory with this name.
    2. `TRAJ_COUNT` [integer]: Counter for this trajectory out of an ensemble of `N_TRAJ` such realizations.

2. `integrationAlgorithm` [char]: Chosed Integration Algorithm (see `MasterControl-*.sh` documentation for options).

3. Job type flags
    1. `bDO_NDX` [boolean]: If non-zero, generate GMX index.
    2. `bDO_EM` [boolean]: If non-zero, perform GMX Energy Minimization. 
    3. `bProduction` [boolean]: If non-zero, perform ensemble averaging.
    4. `bDO_NM` [boolean]: If non-zero, perform GMX Normal Mode analysis.
    5. `bDO_MD` [boolean]: If non-zero, perform GMX Molecular Dynamics simulation.

4. Time grid
    1. `dt` [float]: Time-step duration, in picoseconds.
    2. `SimDuration` [integer]: Number of time-steps in Simulation Duration. Generally, should be much greater than the longest period in the system (which is the system-bath relaxation life-time or the slowest molecular period). See also `MAXSTEPS` in `TheWorks-*.sh`.
    3. `SegLength` [integer]: Number of time-steps in temporal coarse-graining Segment Length. For a weakly-coupled system, should be greater than the slowest molecular period and smaller than the system-bath coupling.

5. Thermostatics
    1. `bVgen` [boolean]: If non-zero, perform GMX Velocity Generation.
    2. `Nbaths` [integer]: Number of baths (zero, one or two).
    3. `Tcold` [integer]: Temperature of the cold bath, in Kelvins.
    4. `DeltaT` [integer]: Temperature bias between baths, in Kelvins.

6. Analysis flags
    1. `bDO_G_TRAJ` [boolean]: If non-zero, perform GMX Trajectory analysis.
    2. `bDO_G_ENERGY` [boolean]: If non-zero, perform GMX Energetics analysis.
    3. `bDO_EOG` [boolean]: If non-zero, visulize interactively using Eye Of GNOME. Should be used when running interactively (c.f. batch job).

7. `N_EXPONENTIALS` [integer]: Number of exponentials to fit evanescent relaxation of current for a Towards-Steady-State calculation.

## Output
Output depends on simulation step, but all steps share some of the same outputs.
The following is a manifest of all output, from most summarizing to most specific.
### Potential Energy Minimization (EM) and Normal Mode Analysis (NMA)
1. Minimal energy configuration: `conf-em.gro`
2. Normal Modes: Eigenfrequencies (`eigenfrequencies-*.xvg`) and eigenvectors (`eigenvectors-*.xvg`).
3. Input for [the Landauer Heat Conduction program](https://bitbucket.org/heat-conduction-mol-junctions/landauer-heat-current).
### Equilibrium properties (including linear response)
1. Compiled topology file for Towards-Steady-State trajectory: `topol-tss.tpr`
2. Towards-steady-state trajectory, from which we sample initial configurations for the at-steady-state trajectories: `traj-tss.tpr`
3. Initial configurations for an at-steady-state trajectory: `traj-*.trr`
### Non-equilibrium steady-state (NESS) properties
1. `AtSteadyState-*.svg`: Plots atomic temperatures and local heat currents at steady state.
2. `STEADY_STATE.pdf`: Ensemble-averaged, time-averaged heat current and Fourier heat conduction.
### Transient / dynamic properties
See next section.
### Shared
The following are intermediate outputs, and have add little value by themselves.
#### MD
##### Reflected input
1. Molecule spatial configuration: `conf.gro`
2. Atom index file used: `index-*.ndx`
3. MD Parameters file used: `*.mdp`
4. Simulation options used: `N.dat` / `options.dat`
##### Altered GROMACS
6. Phase space trajectory: `trajectory-*.txt`
7. Forces, by interaction term (including external, stochastic forces): `forces-*.txt`
#### Parse MD, and calculate heat current
7. Thermodynamic output, per trajectory: `output-*.txt`, and a more human-readable version `hroutput-*.xvg`, where the name also contains the time-averaging coarse-graining segment length.
8. Heat current output, per trajectory: `current-*.txt`, and a more human-readable version `currenthr-*.txt`
#### Ensemble averaging
##### Power through system-bath interface
9. Power due to random force, dissipation power, and net bond power going through the right- and left-most atom (which are the only ones coupled to a bath): `RightSulfurRanForPower.txt`, `RightSulfurDissipationPower.txt`, and `RightSulfurBondPower.txt`, respectively; and in more human-readable form in `SulfurBathPowers.txt` and `BathPowers.svg`.
##### Local power throughout molecule
13. `TermPairwiseCurrent-*.txt`: Breaks down energetics of each potential energy term among the participating atoms, per the potential energy localization scheme.
12. `BondPower-*.txt`: Pairwise power (through bonds), as a summation over interaction terms.
18. `BondPower-*.png`: Visualization of `BondPower-*.txt`.
14. `LowerTriangleRowsFirst.txt`: Matrix representation of bond power.
16. `Energy_Current-*.svg`: Energy current, at each time-step, projected along the three Cartesian axes (the molecular principle axis is aligned with the x-axis), the vector magnitude, and histograms.
17. `current-*.svg`: Comparison of potential energy localization schemes -- time-dependent and time-averaged, local and averaged over atoms.
##### Thermodynamics
15. `EnsembleAveragedTemperatures.txt`: Ensemble-averaged atomic temperatures.
##### Verification
###### Random force generation
8. Random force time-summed histogram and fit for Gaussian random process: `RandomForce-*.svg`
8. Bath random force time-autocorrelation: `Convolution-*.jpeg`
###### Energy conservation and continuity
14. Energy conservation plots (relevant mostly to NVE): `EnergyConservation-*.jpeg`
15. Local energy continuity plots (relevant mostly to NVE and NVT): `LocalStuff-*.svg`
#### Common
11. Logs of STDIO and STDERR, per trajectory: `*-std.io` and `*-std.err`
### Output file content and column titles
		a. output.txt
			1) step
			2) t
			3) j
			4) kinetic energy of atom j
			5) for each partition scheme (0 <= a <= 2):
			    a) potential energy of atom j, under partition scheme a
			    b) change in energy (relative to pervious step)
			    c) power * dt (explained energy change)
			    d) rel. err. energy continuity (Pdt / DE - 1)
			    e) temperature of atom j (* should have preceded '5' ... no need to duplicate for each partition scheme*)
		b. hroutput*.txt
			for each partition scheme (0 <= a <= 2): power through atom j
		c. current.txt
			1) step
			2) t
			3) j
			4) for each partition scheme (0 <= a <= 2):
				for each spacial dimension (0 <= q <= 2):
					q component of the current into atom j, according to partion scheme a (I_{j}^{a,q})
		d. currenthr*.txt
			for each partition scheme (0 <= a <= 2):
				for each spacial dimension (0 <= q <= 2):
					q component of the current into atom j, according to partion scheme a (I_{j}^{a,q})
		e. BondPower*.txt
			for each atom j:
				for each partition scheme (0 <= a <= 2):
					for each atom i:
						power into atom i from atom j, according to partion scheme a (P_{tau,i<-j}^a)
		f. TermPairWisePower (for equilibirum partion scheme only)
			1) time (t)
			2) interaction term index (tau)
			3) atom index (ai)
			4) partition coefficient (C_{tau,i}^a)
			5) local power due to this interaction term (P_{tau,i}^a)
		g. TermPairWiseCurrent (see explicit column headers in file)
			1) step 
			2) t 
			3) tau
			4) number of participant atoms in this interaction term (nr_tau)
			5) ai
			6) aj			
			7) for each spacial dimension (0 <= q <= 2): q components of position of atom i
			10) for each spacial dimension (0 <= q <= 2): q components of position of atom j
			13) for each spacial dimension (0 <= q <= 2): q components of position difference between atom i and j
			16) distance between atom i and j
			17) same as 7-16 for velocities
			27) this interaction term's partition coefficeint for atom i
			28) this interaction term's partition coefficeint for atom j
			29) for each spacial dimension (0 <= q <= 2): q components of force on atom i
			32) for each spacial dimension (0 <= q <= 2): q components of force on atom j
			33) power due to this term going from i to j
			34) power due to this term going from j to i
			35) power due to this term going into i
			36) for each spacial dimension (0 <= q <= 2): q components of current due to this term going through i (I_{tau,i}^{a,q})
			39) power due to this term going into j
			40) for each spacial dimension (0 <= q <= 2): q components of current due to this term going through j
			43) total power going from atom i to j
			44) total power going from atom j to i
			45) net power going into atom i
			46) net power going into atom i, summed over current time-domain coarse-graining segment
			47) for each spacial dimension (0 <= q <= 2): q components of current going into i from the left
		h. `EnsemblePairwisePowers-*.sh` 
			1) `LowerTriangleRowsFirst.txt`
				a) ensemble averaged  from `BondPower-*.txt` files:
					i. uniformly increasing trajectory counter (n)
					ii. trajectory counter from job (n_i)
					iii. for each unique pair of atoms, j & k: P_{j<-k}
					iv. P
					v. sd[P]
					vi. I_interfacial
					vii. sd[I_interfacial]
					viii. I_spacial
					ix. sd[I_spacial]
				b) same data from pre-averaged `BondPower.txt`
			2) `EnsembleAveragedTemperatures.txt`:
				a) average temperature of each atom
				b) standard deviation of temperature of each atom
		i. Uncomment calls to plot functions in `EnsembleAveraging-*.sh` line 450.
			1) `gnuplot EnergyCons-*.gp`: energy conservation
			2) `gnuplot LocalStuff-*.gp`: local energy continuity plots
			3) `gnuplot CurrentHistogram-*.gp`
		j. `GNUPlotCurrent-*.sh`: "This BASH script produces a GNUPlot script named `current-*.gp` and executes it using GNUPlot to plot the energy current"		
		k. `EnsembleSize-*.sh`: Plot convergence of output as function of ensemble size (given existing results).
		l. `BatchConvolutions-*.sh`: "Loops over realizations of a given instance and calculate random force time-convolution for each instance" (see also `BatchCRR-*.sh`)

# Citing and Contributing
Contact [Inon Sharony](mailto:InonShar@TAU.ac.IL)